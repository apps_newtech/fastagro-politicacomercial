import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemDetalheComponent } from './item-detalhe/item-detalhe.component';
import { PeditensComponent } from './peditens.component';

const routes: Routes = [
  {  path:'',    component: PeditensComponent},
  {  path:'itens',    component: ItemDetalheComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeditensRoutingModule { }
