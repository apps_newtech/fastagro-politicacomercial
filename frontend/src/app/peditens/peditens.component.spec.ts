import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PeditensComponent } from './peditens.component';

describe('PeditensComponent', () => {
  let component: PeditensComponent;
  let fixture: ComponentFixture<PeditensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PeditensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PeditensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
