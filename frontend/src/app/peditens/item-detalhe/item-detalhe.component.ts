import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PoBreadcrumb, PoNotificationService, PoPageEditLiterals } from '@po-ui/ng-components';

@Component({
  selector: 'app-item-detalhe',
  templateUrl: './item-detalhe.component.html',
  styleUrls: ['./item-detalhe.component.scss']
})
export class ItemDetalheComponent implements OnInit {
  title = 'Novo Item do Peiddo';    

  public breadcrumb: PoBreadcrumb = {
    items: [{ label: 'Pedido de Venda', link: '/home/peditens' }, { label: 'Novo Item' }]
  }; 
  

  constructor(
    private poNotification: PoNotificationService,
    private route: Router,
  ) {}

  ngOnInit() {    
  }   

  salvar() {
    this.poNotification.success(`Save successfully`);
  }
  
  cancelar() {
    window.history.back();
  }
}
