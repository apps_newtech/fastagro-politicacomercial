import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PoBreadcrumb } from '@po-ui/ng-components';
import { PoTableColumn } from '@po-ui/ng-components';

@Component({
  selector: 'app-peditens',
  templateUrl: './peditens.component.html',
  styleUrls: ['./peditens.component.scss']
})
export class PeditensComponent implements OnInit {
  
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public onClick() {
    alert('Clicked in menu item2')
  } 

  public item() {    
      this.router.navigate(['home/pedidoitens/itens'])          
  }

  breadcrumb: PoBreadcrumb = {
    items: [{ label: 'Pedido de Venda', link: '/filtroexportacao' }],    
  };  

  /***************Item********************/ 
  
  columnsItem: Array<PoTableColumn> = this.getColumnsItem();
  itemsItem: Array<any> = [];  

  incluirItem() {
    this.router.navigate(['/home/peditens/itens'])
  }

  getColumnsItem(): Array<PoTableColumn> {
    return [
      { property: 'Numero Item', type: 'number', width: '8%' },
      { property: 'Cor da Faixa', width: '10%'  },
      { property: 'Qtde. Embalagem' },
      { property: 'Quantidade' },
      { property: 'Un' },
      { property: 'Descricao Item', width: '300px'  },
      { property: 'Origem' },
      { property: 'Frete R$/kg' },
      { property: 'Preço NET/FOB target', visible: false },
      { property: 'Preço NET/FOB (R$Unid.)', visible: false },
      { property: 'Preço EF', visible: false },
      { property: 'Preço NF', visible: false },
      { property: 'Valor Total', visible: false }
    ]
  }  

  /*****************Comissao***********************/
  columnsComissao: Array<PoTableColumn> = this.getColumnsComissao();
  itemsComissao: Array<any> = [];

  getColumnsComissao(): Array<PoTableColumn> {
    return [
      { property: 'Sequencial', type: 'number', width: '8%' },
      { property: 'Descrição Item', width: '300px'},
      { property: 'Comissao Total(R$)' },
      { property: 'Comissão %' },
      { property: 'Preço NET/FOB com EF' },
      { property: 'Comissão MAX Faixa'},
      { property: '% Agente' },
      { property: 'Comissão Agente(R$/kg)',  visible: false },       
    ]
  }

  /*********************Gerencial***********************/

  columnsGerencial: Array<PoTableColumn> = this.getColumnsGerencial();
  itemsGerencial: Array<any> = [];

  getColumnsGerencial(): Array<PoTableColumn> {
    return [
      { property: 'Preço Net/FOB + EF', type: 'number', width: '8%' },
      { property: 'Comissao Ajustada'},
      { property: 'Premio Distribuição' },
      { property: 'Comissão (R$/Unid.)' },
      { property: 'Custo' },
      { property: 'Comissão Total'},
      { property: 'Comissão%' },
      { property: 'MB à Prazo',  visible: false },       
      { property: 'MB à Vista(R$/kg)',  visible: false },       
      { property: 'MB à Prazo(R$/kg)',  visible: false },       
      { property: 'Margem Comissão % à Vista',  visible: false },       
      { property: 'Margem Comissão R$ à Vista',  visible: false },       
      { property: 'Financeiro por Produto',  visible: false },       
      { property: 'ICMS',  visible: false },       
      { property: 'PIS/Cofins',  visible: false },       
      { property: 'Qtde. MIDAS',  visible: false },       
      { property: 'Foliar',  visible: false },       
      { property: 'Qtde. Foliares',  visible: false },       
      { property: 'Check Foliares',  visible: false },       
      { property: 'Qtde. ULEXITA',  visible: false },       
      { property: 'Check ULEXITA',  visible: false },       
      { property: 'F.Preto',  visible: false },       
      { property: 'F.Vermelho',  visible: false },       
      { property: 'F.Amarelo',  visible: false },       
      { property: 'F.Verde',  visible: false },       
      { property: 'Mínimo',  visible: false },       
      { property: 'Faixa',  visible: false },       
      { property: 'Faixa Acima %',  visible: false },       
      { property: 'Comissão Base Faixa %',  visible: false },       
      { property: 'Delta Preço Faixa (R$)',  visible: false },       
      { property: 'Var % Preço Faixa',  visible: false },       
      { property: 'Delta Comis. Faixa',  visible: false },       
      { property: 'Var Comis. Faixa ',  visible: false },       
      { property: 'Formula Base p/Frete',  visible: false }      

    ]
  }


}
