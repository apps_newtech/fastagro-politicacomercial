import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PeditensRoutingModule } from './peditens-routing.module';
import { PeditensComponent } from './peditens.component';
import { PoTabsModule, PoFieldModule, PoBreadcrumbModule, PoContainerModule, PoButtonModule, PoPageModule  }  from '@po-ui/ng-components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemDetalheComponent } from './item-detalhe/item-detalhe.component';
import { PoTableModule, PoWidgetModule, PoInfoModule  } from '@po-ui/ng-components';
import { PoPageDynamicTableModule, PoPageDynamicEditModule } from '@po-ui/ng-templates';
@NgModule({
  declarations: [PeditensComponent, ItemDetalheComponent],
  imports: [
    CommonModule,
    PeditensRoutingModule,
    PoTabsModule,
    PoFieldModule,
    PoBreadcrumbModule,
    FormsModule, 
    ReactiveFormsModule,
    PoContainerModule ,
    PoButtonModule,
    PoPageModule,
    PoTableModule,
    PoWidgetModule,
    PoInfoModule,
    PoPageDynamicTableModule,
    PoPageDynamicEditModule
    
  ]
})
export class PeditensModule { }
