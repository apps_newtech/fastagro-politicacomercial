import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  PoBreadcrumb,
  PoPageAction,
  PoTableColumn,
  PoTableAction,
  PoPageFilter,
  PoModalComponent,
  PoDialogService,
  PoNotificationService,
  PoSelectOption,
  PoModalAction,
} from '@po-ui/ng-components';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { Filtros, PedidoWeb } from './pedido.model';
import { PedidoService } from './pedido.service';
import jsPDF from 'jspdf';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ExcelService } from '../shared/services/excel.service';

export interface LinhaPedido {
  nr_pedido_web: number;
  nr_pedcli: string;
  nome: string;
  dat_criacao: Date;
  situacao: number;
  situacaoPedido: number;
  statusAprovacao: number;
  statusAprovacaoMLA: number;
  aprovadorMLA: string;
  action: any[];
  observacao: string;
  pedidosDatasul: string;
}

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.scss'],
})
export class PedidoComponent implements OnInit {
  breadcrumb: PoBreadcrumb = {
    items: [{ label: 'Pedido', link: '/pedido' }],
  };

  pageActions: PoPageAction[] = [
    { label: 'Incluir', action: this.onNew.bind(this) },
    { label: 'Filtrar Pedidos', action: this.filtrarPedidos.bind(this) },
    { label: 'Exportar Excel', action: this.exportaExcel.bind(this) },
  ];

  page: number = 1;
  items$!: Observable<PedidoWeb[]>;
  hasMore$!: Observable<boolean>;
  data: LinhaPedido[] = [];
  p_carrega!: boolean;
  filtroForm!: FormGroup;
  showFiltro!: boolean;
  showImpressao!: boolean;
  showCotacao!: boolean;
  poModalTitulo!: string;
  perfil_valido!: boolean;
  logo!: string;
  filtros!: Filtros;
  mensagem_representante!: string;

  tableActions: PoTableAction[] = [
    //{ action: this.onPdf.bind(this), label: 'Imprimir' },
    /*{
      action: this.visualizar.bind(this),
      label: 'Visualizar',

    },*/
    {
      action: this.editar.bind(this),
      label: 'Editar',
      disabled: this.validaSituacao.bind(this),
    }/*,
    {
      action: this.cotacao.bind(this),
      label: 'Cotação',
      type: 'warning',
      disabled: this.validaSituacao.bind(this),
    }*/,
    {
      action: this.excluir.bind(this),
      label: 'Apagar',
      type: 'danger',
      disabled: this.validaSituacao.bind(this),
    },
    {
      action: this.integrar.bind(this),
      separator: true,
      label: 'Enviar',
      disabled: this.validaSituacao.bind(this),
    },
  ];

  tableColumns: PoTableColumn[] = [
    { label: 'Nr Pedido Web', property: 'nr_pedido_web' },
    {
      label: 'Situação WEB',
      property: 'situacao',
      type: 'label',
      labels: [
        {
          value: 0,
          color: 'color-01',
          label: 'Criado',
          tooltip: 'Situação do Pedido WEB',
        },
        {
          value: 1,
          color: 'color-09',
          label: 'Integrado',
          tooltip: 'Situação do Pedido WEB',
        },
        {
          value: 3,
          color: 'color-07',
          label: 'Com erro',
          tooltip: 'Situação do Pedido WEB',
        },
        {
          value: 4,
          color: 'color-09',
          label: 'Em Aprovação',
          tooltip: 'Situação do Pedido WEB',
        },
        {
          value: 5,
          color: 'color-01',
          label: 'Cotação',
          tooltip: 'Situação do Pedido WEB',
        },
      ],
    },
   // { label: 'Nr Pedido Cliente', property: 'nr_pedcli' },
   { label: 'Nr Pedido Cliente', property: 'pedidosDatasul' },
    { label: 'Cliente', property: 'nome' },
    {
      label: 'Data Emissão',
      property: 'dat_criacao',
      type: 'dateTime',
      format: 'dd/MM/yyyy HH:mm:ss',
    },
    {
      label: 'Situação PD4000',
      property: 'situacaoPedido',
      type: 'label',
      labels: [
        {
          value: 0,
          color: 'color-08',
          label: 'Não Integrado',
          tooltip: 'Ainda não ocorreu Integração com Datasul',
        },
        {
          value: 1,
          color: 'color-01',
          label: 'Aberto',
          tooltip: 'Situação do Pedido no PD4000(Datasul)',
        },
        {
          value: 2,
          color: 'color-09',
          label: 'Atendido Parcial',
          tooltip: 'Situação do Pedido no PD4000(Datasul)',
        },
        {
          value: 3,
          color: 'color-10',
          label: 'Atendido Total',
          tooltip: 'Situação do Pedido no PD4000(Datasul)',
        },
        {
          value: 4,
          color: 'color-08',
          label: 'Pendente',
          tooltip: 'Situação do Pedido no PD4000(Datasul)',
        },
        {
          value: 5,
          color: 'color-07',
          label: 'Suspenso',
          tooltip: 'Situação do Pedido no PD4000(Datasul)',
        },
        {
          value: 6,
          color: 'color-07',
          label: 'Cancelado',
          tooltip: 'Situação do Pedido no PD4000(Datasul)',
        },
        {
          value: 7,
          color: 'color-12',
          label: 'Fatur Balcão',
          tooltip: 'Situação do Pedido no PD4000(Datasul)',
        },
      ],
    },
    {
      label: 'Situação Crédito',
      property: 'statusAprovacao',
      type: 'label',
      labels: [
        {
          value: 0,
          color: 'color-08',
          label: 'Não Integrado',
          tooltip: 'Ainda não ocorreu Integração com Datasul',
        },
        {
          value: 1,
          color: 'color-02',
          label: 'Não Avaliado',
          tooltip: 'Situação da Aprovação Comercial do Pedido',
        },
        {
          value: 2,
          color: 'color-03',
          label: 'Avaliado',
          tooltip: 'Situação da Aprovação Comercial do Pedido',
        },
        {
          value: 3,
          color: 'color-10',
          label: 'Aprovado',
          tooltip: 'Situação da Aprovação Comercial do Pedido',
        },
        {
          value: 4,
          color: 'color-07',
          label: 'Não Aprovado',
          tooltip: 'Situação da Aprovação Comercial do Pedido',
        },
        {
          value: 5,
          color: 'color-08',
          label: 'Pendente Informação',
          tooltip: 'Situação da Aprovação Comercial do Pedido',
        },
      ],
    },
    {
      label: 'Aprovação Preço',
      property: 'statusAprovacaoMLA',
      type: 'label',
      labels: [
        {
          value: 0,
          color: 'color-08',
          label: 'Não Integrado',
          tooltip: 'Ainda não ocorreu Integração com Datasul',
        },
        {
          value: 1,
          color: 'color-09',
          label: 'Sem Aprovação',
          tooltip: 'Situação da Aprovação MLA do Pedido',
        },
        {
          value: 2,
          color: 'color-03',
          label: 'Não Enviado',
          tooltip: 'Situação da Aprovação MLA do Pedido',
        },
        {
          value: 3,
          color: 'color-01',
          label: 'Pendente',
          tooltip: 'Situação da Aprovação MLA do Pedido',
        },
        {
          value: 4,
          color: 'color-10',
          label: 'Aprovado',
          tooltip: 'Situação da Aprovação MLA do Pedido',
        },
        {
          value: 5,
          color: 'color-07',
          label: 'Rejeitado',
          tooltip: 'Situação da Aprovação MLA do Pedido',
        },
        {
          value: 6,
          color: 'color-10',
          label: 'ReAprovado',
          tooltip: 'Situação da Aprovação MLA do Pedido',
        },
      ],
    },
    { label: 'Aprovador', property: 'aprovadorMLA' },
    {
      property: 'action',
      label: 'Ações',
      type: 'icon',
      icons: [
        {
          action: this.copiar.bind(this),
          icon: 'po-icon-copy',
          tooltip: 'Copiar Pedido',
          value: 'copiar',
        },
        {
          action: this.onPdf.bind(this),
          icon: 'po-icon-print',
          tooltip: 'Imprimir Pedido',
          value: 'imprimir',
        },
        {
          action: this.visualizar.bind(this),
          icon: 'po-icon-document',
          tooltip: 'Visualizar Pedido',
          value: 'visualizar',
        },
      ],
    },
  ];
  public readonly filterSettings: PoPageFilter = {
    //action: this.filterAction.bind(this),
    //advancedAction: this.advancedFilterActionModal.bind(this),
    placeholder: 'Pesquisar',
  };

  @ViewChild(PoModalComponent, { static: true }) poModal!: PoModalComponent;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private service: PedidoService,
    private poAlert: PoDialogService,
    private poNotification: PoNotificationService,
    private datePipe: DatePipe,
    private excelService: ExcelService
  ) {
    poModal: PoModalComponent;
  }

  ngOnInit(): void {

    this.logo = ''
    this.configuraFiltro();
    this.filtros = { ...this.filtroForm.value };
    this.p_carrega = false;
    this.showFiltro = true;
    let cPerfil: any;
    let PerfilAcesso: string;
    this.service.getPerfil().subscribe((perfil) => {
      cPerfil = perfil;
      console.dir(perfil);
      environment.perfil = cPerfil['pcPerfilAcesso'];
      this.perfil_valido = true;
      this.updateData();
    },(error) => {
      this.perfil_valido = false;
      this.p_carrega = true;

      });

  }

  public onClick() {
    alert('Clicked in menu item2');
  }

  public pagBrowser() {
    this.router.navigate(['browser']);
  }
  public pagfiltro() {
    this.router.navigate(['itens']);
  }

  public exportaExcel() {
    this.service.getExcel(this.filtros).subscribe((result) => {
      console.dir(result);
      this.downloadExcel(result.data, 'sample');
    });
  }

  public filtrarPedidos() {
    this.poModalTitulo = 'Filtrar Pedidos';
    this.showFiltro = true;
    this.showImpressao = false;
    this.showCotacao = false;
    this.poModal.open();
  }

  private updateData() {
    this.data = [];
    console.debug(this.filtros);
    const obs$ = this.service.putAll(this.filtros);
    this.items$ = obs$.pipe(pluck('items'));
    this.items$.subscribe(
      (result: PedidoWeb[]) => {
        result.forEach((el: PedidoWeb) => {
          const lp: LinhaPedido = {
            nr_pedido_web: el.nr_pedido_web,
            nr_pedcli: el.pedidosDatasul,
            pedidosDatasul: el.pedidosDatasul,
            nome: el.cliente.nome,
            dat_criacao: el.dat_criacao,
            situacao: el.situacao,
            situacaoPedido: el.situacaoPedido,
            statusAprovacao: el.statusAprovacao,
            statusAprovacaoMLA: el.statusAprovacaoMLA,
            aprovadorMLA: el.aprovadorMLA,
            action: ['copiar', 'imprimir', 'visualizar'],
            observacao: ''
          };
          this.data.push(lp);
        });
        this.p_carrega = true;
      },
      (error) => {
        console.log(error);
        this.p_carrega = true;
      }
    );

    this.hasMore$ = obs$.pipe(pluck('hasNext'));
  }
  public readonly StatusPedidoWebOptions: Array<PoSelectOption> = [
    { label: 'Criado', value: '0' },
    { label: 'Integrado', value: '1' },
    { label: 'Com Erro', value: '3' },
    { label: 'Em Aprovação', value: '4' }
  ];

  public readonly StatusPedidoOptions: Array<PoSelectOption> = [
    { label: 'Não integrado', value: '0' },
    { label: 'Aberto', value: '1' },
    { label: 'Atendido Parcial', value: '2' },
    { label: 'Atendido Total', value: '3' },
    { label: 'Pendente', value: '4' },
    { label: 'Cancelado', value: '5' },
    { label: 'Fatur Balcão', value: '6' },
  ];

  public readonly StatusAprovacaoOptions: Array<PoSelectOption> = [
    { label: 'Não integrado', value: '0' },
    { label: 'Não Avaliado', value: '1' },
    { label: 'Avaliado', value: '2' },
    { label: 'Aprovado', value: '3' },
    { label: 'Não Aprovado', value: '4' },
    { label: 'Pendente Informação', value: '5' },
  ];

  public readonly StatusAprovacaoMLAOptions: Array<PoSelectOption> = [
    { label: 'Não integrado', value: '0' },
    { label: 'Sem Aprovação', value: '1' },
    { label: 'Não enviado', value: '2' },
    { label: 'Pendente', value: '3' },
    { label: 'Aprovado', value: '4' },
    { label: 'Rejeitado', value: '5' },
    { label: 'ReAprovado', value: '6' },
  ];

  private validaSituacao(lp: LinhaPedido) {
    if (lp.situacao == 1 || lp.situacao == 2 || lp.situacao == 5) {
      return true;
    } else {
      return false;
    }
  }

  private configuraFiltro(): void {
    var dateini = new Date();
    dateini.setDate(dateini.getDate() - 7);
    var datefim = new Date();
    datefim.setDate(datefim.getDate() + 7);
    this.filtroForm = this.formBuilder.group({
      NrPedidoWebIni: '',
      NrPedidoWebFim: '999999999',
      NrPedidoTotvsIni: '',
      NrPedidoTotvsFim: '999999999',
      dataEmissaoIni: [this.datePipe.transform(dateini, 'yyyy-MM-dd')],
      dataEmissaoFim: [this.datePipe.transform(datefim, 'yyyy-MM-dd')],
      dataEntregaIni: ['2000-01-01'],
      dataEntregaFim: ['2999-12-31'],
      codClienteIni: [''],
      codClienteFim: ['999999999'],
      StatusPedidoWEBIni: ['0'],
      StatusPedidoWEBFim: ['5'],
      StatusPedidoIni: ['0'],
      StatusPedidoFim: ['6'],
      StatusAprovacaoIni: ['0'],
      StatusAprovacaoFim: ['5'],
      StatusAprovacaoMLAIni: ['0'],
      StatusAprovacaoMLAFim: ['6'],
    });
  }

  confirm: PoModalAction = {
    action: () => {
        this.aplicaFiltroPedidos();
    },
    label: 'Confirmar',
  };
  close: PoModalAction = {
    action: () => {
      this.closeModal();
    },
    label: 'Cancelar',
    danger: true,
  };

  closeModal() {
    this.poModal.close();
  }

  onShowMore() {
    this.page++;
    this.updateData();
  }

  onNew(args: any) {
    this.router.navigate(['pedido/new']);
  }

  editar(lp: LinhaPedido) {
    this.router.navigate(['pedido/edit', lp.nr_pedido_web]);
  }

  copiar(lp: LinhaPedido) {
    let iseq_item: any;
    console.debug(`Pedido copiado ${lp.nr_pedido_web}`)
    this.service.copyPedido(lp.nr_pedido_web).subscribe((sequencia) => {
      iseq_item = sequencia;
      console.debug(`Pedido gerado ${iseq_item['nr_pedido_web']}`)
      this.router.navigate(['pedido/edit', iseq_item['nr_pedido_web']]);
  });
  }

  visualizar(lp: LinhaPedido) {
    this.router.navigate(['pedido/view', lp.nr_pedido_web]);
  }

  excluir(lp: LinhaPedido) {
    this.poAlert.confirm({
      title: 'Confirmação de exclusão',
      message: ` Você tem certeza de que quer excluir o Pedido Nr. ${lp.nr_pedido_web} ?`,
      confirm: () => {
        this.service.excluiPedido(lp.nr_pedido_web).subscribe((data) => {
          const index = this.data.indexOf(lp);
          this.data.splice(index, 1);
          this.poNotification.success(
            `Pedido Nr. ${lp.nr_pedido_web} excluido com sucesso.`
          );
        });
      },
    });
  }

  cotacao(lp: LinhaPedido) {
    this.showFiltro = false;
    this.showImpressao = false;
    this.showCotacao = true;
    this.poAlert.confirm({
      title: 'Confirmação',
      message: ` Você tem certeza de que quer transformar o pedido ${lp.nr_pedido_web} numa cotação ? Não será possível voltar atras.`,
      confirm: () => {
        this.service.cotacaoPedido(lp).subscribe((data) => {
          const index = this.data.indexOf(lp);
          this.data.splice(index, 1);
          this.poNotification.success(
            `Pedido Nr. ${lp.nr_pedido_web} transformado em cotação.`
          );
        });
      },
    });
  }

  aplicaFiltroPedidos() {
    if (this.showFiltro == true) {
      this.filtros = { ...this.filtroForm.value };
      this.p_carrega = false;
      this.poModal.close();
      this.updateData();
    }
    this.poModal.close();
  }

  integrar(lp: LinhaPedido) {
    this.p_carrega = false;
    this.service.efetivaPedido(lp.nr_pedido_web).subscribe(
      (result: any) => {
        console.debug(result)
        lp.nr_pedcli = result.pedidosDatasul;
        lp.pedidosDatasul = result.pedidosDatasul;
        lp.situacao = result.situacao;
        if (result.situacao == "4"){
          lp.statusAprovacaoMLA = 3
        }
        this.poNotification.success(result.mensagem);
        this.p_carrega = true;
      },
      (error) => {
        lp.situacao = 3;
        this.p_carrega = true;
      }
    );
  }

  erro: any;
  public resultPedidoPdf: any;
  public resultItensPdf: any;

  onPdf(pedidoPdf: LinhaPedido, resultItensPdf: LinhaPedido) {
    this.poModalTitulo = 'Imprimir PDF';
    this.showFiltro = false;
    this.showImpressao = true;
    this.showCotacao = false;
    this.service.getPedidoPdf(pedidoPdf.nr_pedido_web).subscribe(
      (data) => {
        this.resultPedidoPdf = data;
      },
      (error: any) => {
        this.erro = error;
      }
    );

    this.service.getItensPedidoPdf(pedidoPdf.nr_pedido_web).subscribe(
      (data) => {
        this.resultItensPdf = data;
      },
      (error: any) => {
        this.erro = error;
      }
    );

    this.poModal.open();
  }

  gerarPDF(tipoImp: string) {
    let intFormat = Intl.NumberFormat('pt-BR', {
      minimumFractionDigits: 2
    })
    let impresaoDadosPdf = this.resultPedidoPdf;
    let impresaoitensPdf = this.resultItensPdf;
    let linhaItem: number;
    let pagina = 1;
    let totalpedido = 0;
    let itot: number = impresaoitensPdf.total - 1;
    let linhaloop = 0;
    console.debug(impresaoDadosPdf.cep)
    let documento = new jsPDF();
    var imgData =
      'data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAApCAYAAADESpC2AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAFlZJREFUeNrsm3mYXUWZ8H9v1Tnn3tv39pLudDoEQjYJMLJGENAPZJTF+QRB3BUYBdkHBkQFZUAWgyCLKAIO8AHOEJwZhWELMHwoyyAEiKKgJCwhG9nTne7bfZez1Tt/3NPJ7SSsLhN8+n2e5OnTXVWn6tbvvlu9JeOuHMewqEtwaYIgiDVgLGkUgRXUgMGSJo02RsTEzhULQWGyptFWYVj1cM6iTEJpi4WVxpo1xUJrLVGWxkm4OPBsPVVFsQTWBzHgW9RT0MYcJB8x8PMDSVZ3gk0ZIQI43fBspOlv2mig2c8qgIIacI71L2gWVYPhGKw5F5UWnLsa1R8C/SBgzPBb84gEoAouBMKR42w6NCKNnsMT16Y1bK6Py/rgGq+xkq1BQNyGjpr9pwrD01OBNG00ycbVuTexJYu8PfA80iQuuTR5jxHZLY6jGZ7YD6jqRJdGHqqigp99SqkgqfVyqYP5TtzjgR88nKp7RrHr/uLgCY3NGgnHVITrMfKRxubrYpx+AXgigw6gGzgKkd1BY3D3AvcAceNDk82TNwrenwy8NlHZy8XJoXFU/5CF7VySFIY5MNYgIk1KwKGJNjgQMNaosd6q1Mo9xgQ3+TaYg8hfBrzUgWc2t/7tQW9F2CObdBnlMOARNjSfDtwC7JM9Xw2cBdTQYZhHwfvzgGfk/U6TL6Vh/TBSN8Flm2+th9hgHbjFSrLCiB8aMbGi4jTNgWwNvMclUZtLXeNDsYIX+M9Zv+UiMf6d+Cb5i4BnaWjYkSYyj+jJCMej6oHchfI9YFUTeNOAm4D9sufLgH9CiTJzPQrenxY8i4gZE0fhF1wanpbE8XR1isFg/dxKfPmNNelv862dr6qN5taqvS+blMhYX1yKqkuNX+zcThLvA1GtspvT9AMuinZzSdLQgL6/1Mu3niFBcLt67o8FzyBaABGUKqJu8+BJ0843jSe6PUoLIr9DcdmIwzIF+DFwUPZ8Acr5TX7i2wEvh1DIAKpCBu+bg5cHyWX9QtD6Xyd4YsDIdBfHX4vD+hddkrSIMXh5O08sD/vt3mPi6eNprb6s2F1EczXW3P9eXLkN8cFriTFBStBTxx+/kGjA802udVeS8MhosPz5JIrGIeDn8s/aYtvxGsjcYf9L8hEDP9sEvLHAjgjtOF2J8BxKhJFtgZ0RZgA9GUprgRdRfQZ4eQR4G2vJxtZNByYBFmUZ8CJQb+JzCnA9cED2PBv4WebjFVDq4B5Fee0NwNsVZRdgJ4RuIEKZDfzXevg2BW8sKjsj7AxMBorZYBVgKc49h+F5kNV/JeClCDIjSaPL4lrtw6KK5IP+oBjcbm38L1IKf50OdFSiV2aQ1iMIi6iNqc6bBtUC+ClBd5VcZ4xKiGlfi530EkFPDan6xSR0n4vrg/8U1+uTjRFssXiLl8uf0YgkweQi1t15EMmarmHwBPgYMBNhPE4fB84EpmHkH4E9sgDAy5aRAmXQ51FuQs1tpC5eD95IpZcH+Q5wNCCoPpT5b0vY4K9uDF4I1LIt9lD6gFNAZ28INtwweB0gXwY+C+wAtK7XpcoVwLeBCpq5Bg3wPJQDsHIU6AdBxkGmJTdIDWU1uCcQ+TdUHiBNo3c3eC59v0vCq6Na+H6AIN8yVzrMNZ4xd9Z+tW+/kxrpUAlrxhBXDNHKAqjzyEWCMzGeI9dVIze2TrSuCGrwelZjCzG5KS9AUPMk8f++NjhwSRqGY03OWx60tB4v1s5GFZuL6L37YJK1I8D7PHAD0ILyMnAXwkeBnd5kfX2oXEzirsFSH6HnGlIAuRb4UmY2HwG+Aix4A/A29hPrWZ9ZI0yvSA8i52ZQt26m33eAmUC9ke4xINIB7jTQYzBm0ptvnwKyAtVbSNOrEFn9LgUv3jWJaj+OauHe6qDY0357kPcvCjV8fmj2gU7idtKIrVXTHYOe2n7xQDA56ssLiocTgyPGqgadtSToihamlWAOfvys8XSN8RXJDSGJT37X3xbSuHpxvdJ/uqiLvVzhYow3EzSWIGTggUNJ+8Y2g3cEcC0wLjM1ZKanDvoEKr9CGUDoRHgfygcRSlm7ARJ3AkZ+nmnDZvByID8ATsigmQ38A7DoDcArA+sy4+ijrAO+npnNYfBKGDkH5PSGVgVgEPQxnMwH1mDco8BcHEkDFjMWwzcQ/QeQwnoHUlkA8jzoMiBCZBvQHVHZEVmv5UNc+jOQs3G67N0G3oQ4rF4ZVYc+a32DbW/915bO0tlDT+6yvLq4HR0o7ue11w5Na97uSc2bZIxMcpH4WveQnEPyCRiFFNQZFSUGWWXb6s/5bdEjXnt4t0beS9QKtH7kIbDRTtU1tVujWm1XL+dfbrzgPNCaBCHlBw8j7e1uBu+TwI8yP244H7cIkStBH0RlIUqK4CGMJ+VwhK9j2Dpb2yOoHAMsbKzaNYN3FXBiE3inAItfFzzlZuAWBAWChrPPPKC3KU94IEZuhuz9wq+BH6H6CE5WAxHGOVCHE1AChFMxcj5IKXvPQsTdguovwS4BLQMpIu2o9uBkXywnZCa84SUKl5K476I6uMWD1/P9HlAXpElyWr1Snuk0DVqnFm6LX9rjzMpvJq00pXRG3Jf7rIbmY5rYHZxTK4qzhTTxx4TLbUf0aLQ2v1BjI7YllrTio07a/WK8ZxrbXeJBLyexFdMe/SHorM2yeLfEZbOm87CH8lG1/MPautpxfr4wy8/lv6qqq00upG/2x0h6u94IvFXAWYjcBhqjw2mU4dQ1eeBsLN/I/KMyKkcBd683UaLvDDz4NsrFCMmmaRElM6sXI3Ii4OF0ASJnYbkDp4rLxjVpw69TATgY4TpEpmTzeBbkfMTdj2rciIx0Q9CiCk48DB8GvQBk74YW15XE7iSMvVOfvmHLB09d+r56pXxrmkQ7BGO8x3TtTsdUHt5zkZr0ExTjM9Jybh+tWZFcGvld9cWFrvp9mshcOyZ8LZhYmV/+XdeapDcnfntMXA4wuaRQmjw4KRkKJod9wXRVOTzuz8/QWHK5sbXb08H8t/2ivtz2dz8/Z6hv8ELPlO71C4UTVHW5yUX0zT5oYx+vGTzF6S0IXwX6Gw71JuCBcVMw8hNU9gVSVIZ9qviPBO8y4LyGmd8IvMaY7wVuBNkbQUn1OtCv4ZkaTsHJxqcoHQjXIPKFbJBXUT0VuA/JItzNg9dwGdQdiMiPEJmenb7cAXKyPn3Dqi0dvJY4qn0rrJTPCdpYbCvbHz3wyAceUyfHqpd8J+3LjxdR8lOGFgbt9Vma2ruD1vj34ZpczeuqE2xTofxcF0lvjibwKE0eJC7ncJFgW6Ntk/7ch+KB/ClpzeyFcn9Q8o8r7jln37jl+X91YTDLz7ecquoGJRdSfuDQzWs8oQdliFRPQ7h5PWgjwNNhU2pQuR7MsZk2+g/gZKC3af3vBLxLgfM3C15jjA825ibbIfST6EmI/hvWNIDZpD37YeRWYCJQxel3ge80ouK3BB6IfBWRmZk/uRbHkfr0Df+1RYPX9f3u3ZLBgTuciyYVOjrPTZbscvHgc1M+ntb5f67ujTVGU39c/bHO/V+7OO3NP1xb0prmOiLCtTneEnihYEsxSdXHy6fTXcV+s7Ko/cigU2e17DHnhbT0h7O17v2H8fzTUeoS1Ck/9AnSvtfz8XQlKsei3LfpanSjHJ1cgfCPWarkl8CRwIqmE4Q/PXhwCMINwHhgMcpxwP9/nYoCA5yKyCUIeVR/R6rHI/L0+pwerwNeOrwGB45tsOY2DPsCMc7M1Kevv2CLBq/9is6j0/51Pwla7VxT2+1j9Xnv9aM+d3e0ojQDP0lapgzOsoX0ko4PLptfX9hOfVmJdwRexcf6jlxHvTPqK5xXWdZ+Um76syuDac+OsaZ0iZXCpYhLTS5i7T0HEb+eqVWWA8dnidw3EgtcloFnUB4Bvggsb4L0Tw+e8KksAu8GlgAnQfOXZEQCu9gYT0/M5nsbjpNBBxrjSqbBN0pMp67xr5llz1yHNSdmGeg7dM6Nn96iwev4bus9STz40Vyh4+u6fK+raku6vxsuLZztIkNhavmOoDU8Pa0GSzv2X0q4uO2PA88IQVuELSTjBl/pvjYM136y9H8eXloY5462aecjSorJRay6/SPEq8e8HngrsvTHPW8BvCsQTmtoPH4JHPUXAO8I4Los9bM0G3sDeDoCvE7g+4geldVVzEQ5b9NdatLmYkYeG25ocyHCtxrr1jk658Z9tmzwLsr3axAN+Trp4OiFvdLqkpZfJKvyE/I79i5s333tlzQxj2lsyW0zSLjobYLX30Ja8fHaIuJyjmCrVXilKmKUpNpy6OCL7bcVpr/0VHHaqqM1KiyHRgJ5zez9iHs73q3gfSI7230r4I0FfoDo5wGH40LgwtffrY2P4pqtt54LnJvlFufqUzfsuUWD13aRqdtc/km/usvf1V6ccvjg/LafkhraP7z4stL0gXNxEmLAGEd9QTu1pa3ku0LCNXn8cVWCbSoM/HbsSPCClOJWCdrSi4xZDGLR2GJLQ2ATlAQl+mTt+R1mSX3CXUFn9Xh1DAAYz1F+YTxpNRiG468LvJHSDlwBfDlb5/UoZyDUNhl7OHgarkzZFMjLQM7M2s7Wp244dIsGr3g+Q0FL8Z+DoT3OqsybdOXQi4VTTSle0b7XiuOCrnC2Jo1vmIjiIgt1D1cLSEJIhnzy0wapLWojXp3H74iJ+wqY9n5aD3gUqeSRoI5zBhFFnckYCFvV1a6PF03/nFux263q7LcQXdqwJEptpY+LzPA3e0sA7wbgI9nzJRl44TvTeCM6+MAFiHwt+/kxcF9B5eX1RA2XeDWD59zIdxraEHMzyBGgCcKP9Mkbz9iiwStdyIDRju8Fvfv+uLqke1Z1iT3YHxM+WZg6cJwN0j8MmwYFjFWSike4LqCwdZWoN0+4oogEigkSiD1Ma42Wg+5FvDpmoDM78x4pLqkd57z6SbJ2l3npwu3XpJHeiPD7YfCqKwu4cIsBb1oG3t9mzzNRLmqAl+UD1bw18HQTUwvC4YjeCHQBa1D9OspPNlS4bAY80UbAiwz7e4fjmWtQJoCWMemJ+uTNP93Cgwuv30WlK7w1H7q8vqT7hupK90WvPXnca0mOR5m3cQcXC+JHFLatEa/LUV3QgSkm2JYYrft4nUO0fPx2qPqYwQ6cMyQuxFMQsaiRGUr9UpXcbLdil5dkbfceYpNbgVcBxFMGXmwnrfiNI7j/ffC2yaLUYdN1I8qZWQXM2wNv8/BNxOhNG47k9FGUExHmr0+dsBF42dFk4/c6GeEajPm/ma/3BLi/16duemWLBm/MJS1z8OM5Xnm30yvP7vSN6nLvUq8jfDI3tnasGJ3XXDnrYqG2NkfQHlLYJgPv1XZMSzoSvENuh/rG4CkitsfBeX5Rw6Sv54Jo4fYHeNb6tq32nzgJAYzv6JvbQzIY/O+CN6xxIAd6Hsg3s3FeAnc8yKPrx3nb4G1ink9AuCJLr6Q4nYVwDiKvvS54CaCyDZ7MRDgKRYAEdaeD/lifuindosEbe3nXOU6HDnFecKh7ae/dh34z/QHJD81vnb7uSCkkz64/4kFIBn2GFhYJxrwj8DoVzvRa3Fhd13PJutsOWdG6/+IDxMavidXfDm+KzaX0PdtDUt4CwNugmT6O4SagqwGA+09UrgaeQ7SCmijL9r4z8HBbAVdhzGeyNg7Re0GuRHUuSK3xOwC1GC2QsDvIKXjyaXR9vc2DqH4FWKpP3biFV6dctdWMKCpfF7ro+63J7vfX5u561+Ay/VBx+94ve8X0Fk0FTQyoEK8pEtYdQVv09sBLo/G+MUebQHr87v4bK8/sOq/66J7j2z/66lgXektIpTzs36UVj/K8LtK61xzVHpGBNz5LIJ8A3PsWwLt8w8kFv6BRG/dG4N2blUVtBJ52I/wAkc9vYIXFGJ4B/QMq/55VqGwM3mYSyFmAMSI+cKA6A8+7Ftir6Q+LUL0PZD7KUgQBnYRhOk4ORpja1PZZlFNBfwXwLgBvglUXnxLWanvlC+0npq9N3bvvV39zt22r/CbXHX7GiFsW9hYwfqNYMazoWwevPMakqlMlJwcXu5xxdbk97G9Z3nvX/vgmmVzatb9CoGvWB2dBSt9T40n7cuC55mTBp7PNHIPSS6Pw8s63AN4PEU7OtMh/A58DljclZPMgV6N8JSPiwQzqRQ0emi/p6A6IXJkVoDZfp0syoH/adHLxz1lyeGV2yjJSOzsHLmWThJyVvXHmPKwckEW5DUhFyDQ9oFs1+Z80ih74b1QvbJj/xoTfDeBhjenGupOiZNUqwvZbouf2//bQy93fzPUMXeV31M+KevOR8Rzq7JuD1zVI6dN3oAPGymDbVLHe7oWJdjB6ccLj9T47WF8zwSarC3ljElq2jkJTSpNh82OLMf1ze4hWFcBqM3ify04D2lHKwDHA7W+yNg+4FuG4DJA5wGeApU3gFTLwjs1up/0iA2/BBkiab67p9ggnYcwhNO5BWJT+DK6fZbP9bBaMdAKrs/Hu3FTjuU3zcUZAzbZ4cjSqXwDZ8U3W+AowC+VW0FeaL128O8CzBgpedzTYf5gYtyDwt32u+sK071Vf7jjaL4YXJnV7ufFdTZ0lHNJi0B7t1DKxum20LhdVF3TMM8V0qW2Ja1r3sIUUO3k++Z1+VzLVtrFp76RIq53Lh+ZOgFIy1p9QnpIO2V6J7cKWnkRNa9oIDq0jXNnC0IsdzTm8Ydk+S2d0ZRv9wAg4Xj/P/7fZRSC/UcnLfQhDJApVB4KH4QB8fT8GJZT5pDwIjWR2I9MmkLOsv4QDRazZE/gb0I4skXwv8FLj3gTbAQfSqMvrA34xHLFvqvXcZsAT8EVI2QdkX0T3QmWHhlsACHWUV0GeQdzDIE82tO5w9PFuAy9viYaGxpAm2xUnlV50ayfLukcmXphWvE+5lDtskF6Lsy+lEvfkOmt7+23pTrUVpR1qi0tdJnBtphA/L2J+ajwzP1rctdbme2MpJEbGOM/VgglE6c65rSvTJZ/8PqnzBCplm1PEy26X+SnVBZ2ka4sQvEFApm93hRv1EyBSWJc2akMskHcN/VgRiGWE60VBoM1rlB8NayhrmhoMZ3kzn1De4rzeCDxPmofeGpWtGL7MJMQoa1B5rVHFLBvu2r57wRtEk8QrTiwFrm9qtfzEhKIpRJ+Ky7kjiCVWZ+43heQxv61eTau+F1e8Dhu4PUnM4cmQ9z6HTcXowmRt6TG3rOMVWurWn7Zmqj+2Nsnglnv55KE0MnPiRAfWJ/GHz749R7yynbS/CNb9+cEbyK4DDoNngepG4CmQF2h9Q/A2vOTPA97IvJ805QKN468JPIoTW3G9Uyg/MQFvTFWScv49LmVfcNOtJzUb6KK4Yl4Gft++c185P7ncMfRq2+7lX/fsE/UF2wTdtW2xzkXLS/ODrtpA0FNZICJzRXVhUrVpnGxKj3iOaEU76bpR8EbBe2ICXkeNZDCHc1gTpONMYrazbTFmbNWEr5VKbdPKq9r2Wbm0/HznynWPTvSitblcaefVPbYYBgO/3nqdX4qGcuOHajhxIkpStYyCNwqeqCqjMiqj4I3KKHijMiqj4I3KKHijMiqj4I3KKHijMiqj4I3KKHijMiqj4I3KFiH/MwDc7lhy7eaCTQAAAABJRU5ErkJggg==';

    documento.addImage(imgData, 'JPEG', 15, 10, 0, 0);
    documento.rect(15, 20, 183, 124);
    documento.line(128, 20, 128, 129); // vertical line

    documento.setFont('courier', 'bold');
    documento.setFontSize(10);
    documento.text('Safra', 20, 25);
    documento.text('Data', 85, 25);
    documento.line(83, 20, 83, 34); // vertical line
    documento.text('Pedido', 130, 25);
    documento.line(15, 27, 198, 27); // horizontal line

    documento.line(15, 34, 198, 34); // horizontal line
    documento.text('Cliente', 20, 38);
    documento.text('Tipo de Venda', 130, 38);
    documento.line(15, 40, 198, 40); // horizontal line

    documento.line(15, 47, 198, 47); // horizontal line
    documento.text('CNPJ/CPF', 20, 51);
    documento.line(83, 47, 83, 60); // vertical line
    documento.text('Inscr. Est.', 85, 51);
    documento.text('Cel', 130, 51);
    documento.line(15, 53, 198, 53); // horizontal line

    documento.line(15, 60, 198, 60); // horizontal line
    documento.text('Endereço', 20, 64);
    documento.text('Tel./Cel.', 130, 64);
    documento.line(15, 66, 198, 66); // horizontal line

    documento.line(15, 73, 198, 73); // horizontal line
    documento.text('Bairro', 20, 77);
    documento.line(97, 73, 97, 86); // vertical line  documento.line(72, 73, 72, 86); // vertical line
    documento.text('UF', 100, 77); //documento.text('UF', 75, 77);
    documento.text('Cidade', 130, 77);
    documento.line(15, 79, 198, 79); // horizontal line

    documento.line(15, 86, 198, 86); // horizontal line
    documento.text('Cep', 20, 90);
    documento.text('Vendedor', 130, 90);
    documento.line(15, 92, 198, 92); // horizontal line

    documento.line(15, 99, 198, 99); // horizontal line
    documento.text('E-mail', 20, 103);
    documento.text('Cultura', 130, 103);
    documento.line(15, 105, 198, 105); // horizontal line

    documento.line(15, 114, 198, 114); // horizontal line
    documento.text('Frete', 20, 118);
    documento.line(53, 114, 53, 129); // vertical line
    documento.text('Tipo Carregamento', 55, 118);
    documento.line(93, 114, 93, 129); // vertical line
    documento.text('Data Entrega', 95, 118);
    documento.text('Vencimento', 130, 118);
    documento.line(15, 120, 198, 120); // horizontal line

    documento.line(15, 129, 198, 129); // horizontal line
    documento.text('Endereço de Cobrança', 20, 133);
    documento.line(15, 135, 198, 135); // horizontal line

    /***** ***/
    documento.setFont('courier', 'normal');
    documento.setFontSize(9);
    documento.text(impresaoDadosPdf.Safra.substr(0, 30), 20, 32);
    documento.text(impresaoDadosPdf.dat_criacao, 85, 32);
    documento.text(impresaoDadosPdf.nr_pedido_web, 130, 32);
    documento.text(impresaoDadosPdf.nomeabrev.substr(0, 55), 20, 45);
    documento.text(impresaoDadosPdf.tipoVenda, 130, 45);
    documento.text(impresaoDadosPdf.cgc, 20, 58);
    documento.text(impresaoDadosPdf.insestadual, 85, 58);
    documento.text(impresaoDadosPdf.telefone2, 130, 58);
    documento.text(impresaoDadosPdf.endereco.substr(0, 55), 20, 71);
    documento.text(impresaoDadosPdf.telefone1, 130, 71);
    documento.text(impresaoDadosPdf.bairro.substr(0, 55), 20, 84);
    documento.text(impresaoDadosPdf.estado, 100, 84);
    documento.text(impresaoDadosPdf.cidade.substr(0, 35), 130, 84);
    documento.text(impresaoDadosPdf.cep, 20, 97);
    documento.text(impresaoDadosPdf.nomeVendedor, 130, 97);
    documento.text(impresaoDadosPdf.email, 20, 110);
    documento.text(impresaoDadosPdf.cod_cultura.substr(0, 55), 130, 110);
    documento.text(impresaoDadosPdf.tipo_frete, 20, 125);
    documento.text(impresaoDadosPdf.tipo_carregamento, 55, 125);
    documento.text(impresaoDadosPdf.dt_entrega, 95, 125);
    documento.text(impresaoDadosPdf.dt_vencimento? impresaoDadosPdf.dt_vencimento : '', 130, 125);
    documento.text(impresaoDadosPdf.enderecocob.substr(0, 150), 20, 140);

    if (tipoImp == '1') {
      /****itens Detalhados *****/

      linhaItem = 5;
      console.log('linha', linhaItem);

      for (var i = 0; i <= itot; i++) {
        totalpedido +=
          impresaoitensPdf.items[i].quantidade *
          impresaoitensPdf.items[i].dePrecoNF;

        documento.setFont('helvetica', 'normal');
        documento.setFontSize(8);
        //documento.text("Item", 18, linhaItem);
        documento.text(
          '' + ("00000" + impresaoitensPdf.items[i].seq_item).slice(-5),
          17,
          162 + linhaItem * i
        );

        //documento.text("Quantidade", 62, 154);
        documento.text(
          '' + impresaoitensPdf.items[i].quantidade.toFixed(0),
          52,
          162 + linhaItem * i,
          { align: 'right' }
        );

        //documento.text("Unid.", 82, 153);
        documento.text(impresaoitensPdf.items[i].un, 67, 162 + linhaItem * i);

        //documento.text("Descricao", 102, 154);
        documento.text(
          impresaoitensPdf.items[i].desc_item.substr(0, 29),
          83,
          162 + linhaItem * i
        );

        //documento.text("PIS/COFINS", 134, 153);
        let intFormat = Intl.NumberFormat('pt-BR', {
          minimumFractionDigits: 2
        })
        documento.text(
          '' + intFormat.format(impresaoitensPdf.items[i].deValorPisCofins).toLocaleString(),
          147,
          162 + linhaItem * i,
          { align: 'right' }
        );

        //documento.text("Preço Unit.", 155, 153);
        documento.text(
          '' + intFormat.format(impresaoitensPdf.items[i].dePrecoNF).toLocaleString(),
          167,
          162 + linhaItem * i,
          { align: 'right' }
        );

        //documento.text("Preço Total(R$)", 174, 153);
        documento.text(
          intFormat.format((impresaoitensPdf.items[i].dePrecoNF * impresaoitensPdf.items[i].quantidade)).toLocaleString(),
          195,
          162 + linhaItem * i,
          { align: 'right' }
        );

        if (i == 17) {
          break;
        }
      }

      documento.rect(15, 150, 183, 80);
      documento.line(15, 157, 198, 157); // horizontal line

      documento.setFont('courier', 'bold');
      documento.setFontSize(8);
      documento.text('Item', 18, 154);
      documento.line(28, 150, 28, 230); // vertical line

      documento.text('Quantidade', 35, 154);
      //documento.text('Embalagem x Caixa', 30, 156);
      documento.line(60, 150, 60, 230); // vertical line

      //documento.text('Quantidade', 62, 154);
      documento.text('Unid.', 61, 154);
      documento.text('(kg,L)', 69, 154);
      documento.line(80, 150, 80, 230); // vertical line

      documento.text('Descricao', 96, 154);
      documento.line(132, 150, 132, 230); // vertical line

      documento.text('PIS/COFINS', 134, 153);
      documento.text('(%)', 139, 156);
      documento.line(152, 150, 152, 230); // vertical line

      documento.text('Preço Unit.', 154, 153);
      documento.text('(R$)', 161, 156);
      documento.line(172, 150, 172, 237); // vertical line

      documento.text('Preço Total(R$)', 173, 153);

      /********************* */
      //documento.rect(15, 250, 183, 30);
      //documento.line(92, 250, 92, 256); // vertical line
      //documento.line(132, 250, 132, 256); // vertical line

      documento.text('Total', 125, 235);
      documento.text('R$', 160, 235);
      documento.text('' + intFormat.format(totalpedido).toLocaleString(), 189, 235, { align: 'right' });
      documento.line(15, 230, 15, 237); // vertical line
      documento.line(198, 230, 198, 237); // vertical line
      documento.line(15, 237, 198, 237); // horizontal line

      /***********/

      documento.setFont('helvetica', 'normal');
      documento.setFontSize(8);
      documento.text(impresaoDadosPdf.obs.substr(0, 95), 20, 250);
      documento.text(impresaoDadosPdf.obs.substr(95, 95), 20, 255);
      documento.text(impresaoDadosPdf.obs.substr(190, 95), 20, 260);

      documento.setFont('courier', 'bold');
      documento.setFontSize(8);
      documento.rect(15, 239, 183, 41);
      documento.text('Observações', 20, 243);
      documento.line(15, 246, 198, 245); // horizontal line
      documento.line(15, 264, 198, 264); // horizontal line
      documento.text(
        'CONCORDO(AMOS) COM AS CONDIÇÕES DE VENDA DESTE PEDIDO',
        20,
        272
      );
      documento.line(136, 264, 136, 280); // vertical line
      documento.line(136, 276, 198, 276); // horizontal line
      documento.text('ASSINATURA DO CLIENTE', 149, 279);

      /***************/
      documento.line(15, 282, 198, 282); // horizontal line
      documento.text('Pagina ' + pagina, 165, 285);

      /********************************************/
      intFormat = Intl.NumberFormat('pt-BR', {
        minimumFractionDigits: 2
      })
      if (itot > 17) {
        /****Nova Pagina ****/

        documento.addPage();
        pagina += 1;

        for (var i = 18; i <= itot; i++) {
          linhaloop += 1;
          totalpedido +=
            impresaoitensPdf.items[i].quantidade *
            impresaoitensPdf.items[i].dePrecoNF;

          documento.setFont('helvetica', 'normal');
          documento.setFontSize(8);
          //documento.text("Item", 18, linhaItem);
          documento.text(
            '' + ("00000" + impresaoitensPdf.items[i].seq_item).slice(-5),
            17,
            28 + linhaItem * linhaloop
          );

          //documento.text("Quantidade", 62, 154);
          documento.text(
            '' + impresaoitensPdf.items[i].quantidade.toFixed(0),
            52,
            28 + linhaItem * linhaloop,
            { align: 'right' }
          );

          //documento.text("Unid.", 82, 153);
          documento.text(
            impresaoitensPdf.items[i].un,
            67,
            28 + linhaItem * linhaloop
          );

          //documento.text("Descricao", 102, 154);
          documento.text(
            impresaoitensPdf.items[i].desc_item.substr(0, 29),
            83,
            28 + linhaItem * linhaloop
          );

          //documento.text("PIS/COFINS", 134, 153);
          documento.text(
            '' + intFormat.format(impresaoitensPdf.items[i].deValorPisCofins).toLocaleString(),
            136,
            28 + linhaItem * linhaloop,
            { align: 'right' }
          );

          //documento.text("Preço Unit.", 155, 153);
          documento.text(
            '' + intFormat.format(impresaoitensPdf.items[i].dePrecoNF).toLocaleString(),
            167,
            28 + linhaItem * linhaloop,
            { align: 'right' }
          );

          //documento.text("Preço Total(R$)", 174, 153);
          documento.text(
            intFormat.format((impresaoitensPdf.items[i].quantidade * impresaoitensPdf.items[i].dePrecoNF)).toLocaleString(),
            195,
            28 + linhaItem * linhaloop,
            { align: 'right' }
          );
        }

        documento.addImage(imgData, 'JPEG', 15, 10, 0, 0);
        documento.rect(15, 20, 183, 228);
        documento.line(15, 29, 198, 29); // horizontal line

        documento.setFont('courier', 'bold');
        documento.setFontSize(8);
        documento.text('Item', 18, 25);
        documento.line(28, 20, 28, 248); // vertical line

        documento.text('Quantidade', 35, 24);
        documento.line(60, 20, 60, 248); // vertical line*/

        documento.text('Unid.(kg,L)', 61, 24);
        documento.line(80, 20, 80, 248); // vertical line

        //documento.line(92, 20, 92, 248); // vertical line

        documento.text('Descricao', 96, 25);
        documento.line(132, 20, 132, 248); // vertical line

        documento.text('PIS/COFINS', 134, 24);
        documento.text('(%)', 139, 27);
        documento.line(152, 20, 152, 248); // vertical line

        documento.text('Preço Unit.', 154, 24);
        documento.text('(R$)', 160, 27);
        documento.line(172, 20, 172, 248); // vertical line

        documento.text('Preço Total(R$)', 173, 25);

        /**********/
        documento.setFont('helvetica', 'normal');
        documento.setFontSize(8);

        documento.text('' + intFormat.format(totalpedido).toLocaleString(), 195, 254,
        { align: 'right' });
        documento.text(impresaoDadosPdf.obs.substr(0, 95), 20, 267);
        documento.text(impresaoDadosPdf.obs.substr(95, 95), 20, 272);
        documento.text(impresaoDadosPdf.obs.substr(190, 95), 20, 277);

        documento.rect(15, 250, 183, 30);
        documento.line(92, 250, 92, 256); // vertical line
        documento.line(132, 250, 132, 256); // vertical line
        documento.setFont('courier', 'bold');
        documento.setFontSize(8);

        documento.text('Total', 107, 254);
        documento.text('R$', 134, 254);
        documento.line(15, 256, 198, 256); // horizontal line

        documento.text('Observações', 20, 260);
        documento.line(15, 262, 198, 262); // horizontal line

        /****************/

        documento.line(15, 282, 198, 282); // horizontal line
        documento.text('Pagina ' + pagina, 165, 285);
      }
    } else {
      /****SIMPLES ****/
      linhaItem = 5;
      pagina = 1;
      console.log('linha2', linhaItem);

      for (var i = 0; i <= itot; i++) {
        totalpedido +=
          impresaoitensPdf.items[i].quantidade *
          impresaoitensPdf.items[i].dePrecoNF;

        documento.setFont('helvetica', 'normal');
        documento.setFontSize(8);

        //documento.text("Item", 18, linhaItem);
        documento.text(
          '' + ("00000" + impresaoitensPdf.items[i].seq_item).slice(-5),
          17,
          162 + linhaItem * i
        );

        //documento.text("Quantidade", 62, 154);
        documento.text(
          '' + impresaoitensPdf.items[i].quantidade.toFixed(0),
          55,
          162 + linhaItem * i,
          { align: 'right' }
        );

        //documento.text("Unid.", 82, 153);
        documento.text(impresaoitensPdf.items[i].un, 74, 162 + linhaItem * i);

        //documento.text("Descricao", 102, 154);
        documento.text(
          impresaoitensPdf.items[i].desc_item.substr(0, 38),
          93,
          162 + linhaItem * i
        );

        //documento.text("PIS/COFINS", 134, 153);
        documento.text(
          '' + intFormat.format(impresaoitensPdf.items[i].deValorPisCofins).toLocaleString(),
          189,
          162 + linhaItem * i,
          { align: 'right' }
        );

        if (i == 13) {
          break;
        }
      }

      /***** ITENS Simples ****/
      documento.rect(15, 150, 183, 80);

      documento.setFont('courier', 'bold');
      documento.setFontSize(8);
      documento.text('Item', 18, 154);
      documento.line(28, 150, 28, 230); // vertical line

      documento.text('Quantidade', 38, 154);
      documento.line(64, 150, 64, 230); // vertical line

      documento.text('Unid. (kg,L)', 68, 154);
      documento.line(90, 150, 90, 230); // vertical line

      documento.text('Descricao', 110, 154);
      documento.line(158, 150, 158, 237); // vertical line

      documento.text('PIS/COFINS (R$)', 165, 154);
      documento.line(15, 157, 198, 157); // horizontal line

      documento.text('Total', 125, 235);
      documento.text('R$', 160, 235);
      documento.text('' + intFormat.format(totalpedido).toLocaleString(), 189, 235, { align: 'right' });
      documento.line(15, 230, 15, 237); // vertical line
      documento.line(198, 230, 198, 237); // vertical line
      documento.line(15, 237, 198, 237); // horizontal line

      /***********/

      documento.setFont('helvetica', 'normal');
      documento.setFontSize(8);
      documento.text(impresaoDadosPdf.obs.substr(0, 95), 20, 250);
      documento.text(impresaoDadosPdf.obs.substr(95, 95), 20, 255);
      documento.text(impresaoDadosPdf.obs.substr(190, 95), 20, 260);

      documento.setFont('courier', 'bold');
      documento.setFontSize(8);
      documento.rect(15, 239, 183, 41);
      documento.text('Observações', 20, 243);
      documento.line(15, 246, 198, 245); // horizontal line
      documento.line(15, 264, 198, 264); // horizontal line
      documento.text(
        'CONCORDO(AMOS) COM AS CONDIÇÕES DE VENDA DESTE PEDIDO',
        20,
        272
      );
      documento.line(136, 264, 136, 280); // vertical line
      documento.line(136, 276, 198, 276); // horizontal line
      documento.text('ASSINATURA DO CLIENTE', 149, 279);

      /***************/
      documento.line(15, 282, 198, 282); // horizontal line
      documento.text('Pagina ' + pagina, 165, 285);

      if (itot > 13) {
        /****Nova Pagina ****/
        documento.addPage();
        pagina += 1;

        for (var i = 14; i <= itot; i++) {
          totalpedido +=
            impresaoitensPdf.items[i].quantidade *
            impresaoitensPdf.items[i].dePrecoNF;
          linhaloop += 1;

          documento.setFont('helvetica', 'normal');
          documento.setFontSize(8);
          //documento.text("Item", 18, linhaItem);
          documento.text(
            '' + ("00000" + impresaoitensPdf.items[i].seq_item).slice(-5),
            17,
            26 + linhaItem * linhaloop
          );

          //documento.text("Quantidade", 62, 154);
          documento.text(
            '' + impresaoitensPdf.items[i].quantidade.toFixed(0),
            55,
            26 + linhaItem * linhaloop,
            { align: 'right' }
          );

          //documento.text("Unid.", 82, 153);
          documento.text(
            impresaoitensPdf.items[i].un,
            74,
            26 + linhaItem * linhaloop
          );

          //documento.text("Descricao", 102, 154);
          documento.text(
            impresaoitensPdf.items[i].desc_item.substr(0, 38),
            93,
            26 + linhaItem * linhaloop
          );

          //documento.text("PIS/COFINS", 134, 153);
          documento.text(
            '' + intFormat.format(impresaoitensPdf.items[i].deValorPisCofins).toLocaleString(),
            188,
            26 + linhaItem * linhaloop,
            { align: 'right' }
          );

          if (i == 35) {
            break;
          }
        }

        documento.addImage(imgData, 'JPEG', 15, 10, 0, 0);
        documento.rect(15, 20, 183, 211);
        documento.line(15, 27, 198, 27); // horizontal line

        documento.setFont('courier', 'bold');
        documento.setFontSize(8);
        documento.text('Item', 18, 24);
        documento.line(28, 20, 28, 231); // vertical line

        documento.text('Quantidade', 38, 24);
        documento.line(64, 20, 64, 231); // vertical line

        documento.text('Unid.', 68, 24);
        documento.text('(kg,L)', 77, 24);
        documento.line(90, 20, 90, 231); // vertical line

        documento.text('Descricao', 110, 24);
        documento.line(158, 20, 158, 237); // vertical line

        documento.text('PIS/COFINS (R$)', 165, 24);

        documento.text('Total', 125, 235);
        documento.text('R$', 160, 235);
        documento.line(15, 230, 15, 237); // vertical line
        documento.line(198, 230, 198, 237); // vertical line
        documento.line(15, 237, 198, 237); // horizontal line
        documento.text('' + intFormat.format(totalpedido).toLocaleString()
        , 188, 235, { align: 'right' });

        /***********************/
        documento.setFont('helvetica', 'normal');
        documento.setFontSize(8);
        documento.text(impresaoDadosPdf.obs.substr(0, 95), 20, 250);
        documento.text(impresaoDadosPdf.obs.substr(95, 95), 20, 255);
        documento.text(impresaoDadosPdf.obs.substr(190, 95), 20, 260);

        documento.setFont('courier', 'bold');
        documento.setFontSize(8);
        documento.rect(15, 239, 183, 41);
        documento.text('Observações', 20, 243);
        documento.line(15, 246, 198, 245); // horizontal line
        documento.line(15, 264, 198, 264); // horizontal line
        documento.text(
          'CONCORDO(AMOS) COM AS CONDIÇÕES DE VENDA DESTE PEDIDO',
          20,
          272
        );
        documento.line(136, 264, 136, 280); // vertical line
        documento.line(136, 276, 198, 276); // horizontal line
        documento.text('ASSINATURA DO CLIENTE', 149, 279);

        /***************/
        documento.line(15, 282, 198, 282); // horizontal line
        documento.text('Pagina ' + pagina, 165, 285);
      }
    }

    /****** TERMO ****/

    if (tipoImp == '1') {
      documento.addPage();
      pagina += 1;

      documento.addImage(imgData, 'JPEG', 15, 10, 0, 0);
      documento.rect(15, 20, 183, 260);

      documento.setFont('courier', 'bold');
      documento.setFontSize(11);
      documento.text('Termos e condições de compra e venda', 18, 26);

      documento.setFont('helvetica', 'normal');
      documento.setFontSize(8);
      documento.text(
        'Cláusula Primeira - Os pedidos de compra podem ser alterados ou corrigidos mediante acordo por escrito firmado pelo comprador e pelo ',
        18,
        33
      );
      documento.text(
        'vendedor, descrevendo as mudanças especificadas a serem realizadas e para cada caso, as modificações de preço e prazo de entrega. ',
        18,
        37
      );
      documento.text(
        'O comprador somente poderá cancelar seu pedido em um prazo máximo de 7 dias após a emissão do mesmo.',
        18,
        41
      );

      documento.text(
        'Cláusula Segunda - Os termos de pagamento são acertados com base na cotação de preços e após isto transferido para a Nota Fiscal.',
        18,
        49
      );
      documento.text(
        'Se a situação financeira do comprador resultar em incerteza de pagamento para o vendedor, o mesmo poderá sem necessidade de notificar',
        18,
        53
      );
      documento.text(
        'o comprador, atrasar as entregas parciais ainda pendentes e solicitar o pagamento antecipado das mesmas.',
        18,
        57
      );
      documento.text('', 18, 65);

      documento.text(
        'Cláusula Terceira - O não pagamento do boleto ou duplicatas na data do seu vencimento terá como consequência a aplicação de multa ',
        18,
        65
      );
      documento.text(
        'moratória de 2%(dois por cento) sobre o valor total de débitos, mais juros de mora de 1,8%(um ponto oito por cento), contados a partir da',
        18,
        69
      );
      documento.text(
        'data de vencimento do boleto.',
        18,
        73
      );

      documento.text(
        'Cláusula Quarta - A devolução de produtos para crédito de valores ou troca, só poderá ser feita com autorização por escrito do Gerente de ',
        18,
        81
      );
      documento.text(
        'Vendas. O Gerente de Vendas não aceitará devoluções de produtos em prazos maiores do que 7 dias da data de recebimento.',
        18,
        85
      );
      documento.text('', 18, 97);

      documento.text(
        'Cláusula Quinta - O vendedor poderá cancelar um pedido de compra em qualquer momento, sem responsabilidade alguma e mediante simples',
        18,
        93
      );
      documento.text(
        'aviso por escrito ao comprador, nos seguintes casos: o comprador esteja em situação de falência declarada, falta de pagamento de ',
        18,
        97
      );
      documento.text(
        'pedidos anteriores ou falta de pagamento de juros pendentes relativos a pagamentos anteriores, em geral, por qualquer não cumprimento ',
        18,
        101
      );
      documento.text(
        'das obrigações a cargo do comprador conforme esse documento, pela inclusão do nome do comprador aos órgãos de proteção de crédito.',
        18,
        105
      );


      documento.text(
        'Cláusula Sexta - O comprador poderá cancelar um pedido de compra por escrito em um prazo de 7 dias desde que seja comprovado o ',
        18,
        113
      );
      documento.text(
        'recebimento do pedido de cancelamento pelo vendedor.',
        18,
        117
      );

      documento.text(
        'Cláusula Sétima - Os termos e obrigações estabelecidas no presente documento são de cumulativos e adicionais a quaisquer outros direitos',
        18,
        125
      );
      documento.text(
        'que o vendedor possa ter conforme as leis locais. Qualquer denúncia pelo não cumprimento das obrigações do comprador será formalizada',
        18,
        129
      );
      documento.text(
        'expressamente por escrito e não deve ser entendida como uma renúncia a qualquer outro, o não cumprimento presente ou futuro. A invalidez,',
        18,
        133
      );
      documento.text(
        'legalidade e aplicabilidade do restante das cláusulas desde documento não afetará a validez, legalidade e a aplicabilidade das demais ',
        18,
        137
      );
      documento.text(
         'cláusulas.',
        18,
        141
      );

      documento.text(
        'Cláusula Oitava - Para todo o conteúdo relativo a interpretação, execução e cumprimento deste documento, as partes se submetem as leis ',
        18,
        149
      );
      documento.text(
        'aplicáveis e tribunais competentes da República Federativa do Brasil, Comarca de Rondonópolis/MT, renunciando expressamente a qualquer',
        18,
        153
      );
      documento.text(
        'outro foro que pudera corresponder por razão de seus domicílios presente ou futuros ou por qualquer outra causa.',
        18,
        157
      );

      documento.text(
        'Cláusula Nona - Em virtude de operação de crédito, de qualquer natureza, pleiteada junto à empresa CIA NITRO QUIMICA BRASILEIRA,',
        18,
        165
      );
      documento.text(
        'inscrita no CNPJ nº 61.150.348/0008-26, autorizo a NAGRO CRÉDITO AGRO LTDA., inscrita no CNPJ sob o nº 22.165.622/0001-02, ',
        18,
        169
      );
      documento.text(
        'a consultar, a qualquer tempo, débitos e responsabilidades decorrentes de operações com características de crédito e informações',
        18,
        173
      );
      documento.text(
        'de títulos de minha emissão registrados, tal como, mas não se limitando a Cédulas de Produto Rural, e registros de medidas judiciais que em',
        18,
        177
      );
      documento.text(
        'meu nome e de minhas empresas, constem ou venham a constar nos Sistemas de Informações de Crédito (SCR) e em Sistema de registro ou',
        18,
        181
      );
      documento.text(
        'de depósito centralizado operado por entidade registradora ou depositária central autorizada, ambos, ou dos sistemas que venham a ',
        18,
        185
      );
      documento.text(
        'complementá-lo ou a substituí-lo. Autorizo também à NAGRO CRÉDITO AGRO LTDA a compartilhar os dados desta consulta exclusivamente',
        18,
        189
      );
      documento.text(
        'com a CIA NITRO QUIMICA BRASILEIRA, na qual estou pleiteando operação de crédito. O(s) DEVEDOR(ES) e FIADOR(ES) se cientificam',
        18,
        193
      );
      documento.text(
        'que, as consultas ao SCR serão realizadas com base na presente autorização, prestada neste INSTRUMENTO, e que o SCR tem por' ,
        18,
        197
      );
     // a partir daqui é continuação
      documento.text(
        'finalidades prover informações ao Banco Central do Brasil, para fins de monitoramento e supervisão à que estão expostas do crédito no',
        18,
        201
      );
      documento.text(
        'sistema financeiro e para o exercício de suas atividades de fiscalização e propiciar o intercâmbio de informações entre instituições ',
        18,
        205
      );
      documento.text(
        'financeiras com o objetivo de subsidiar decisões de crédito e de negócios, conforme definido no § 1º do art. 1º da Lei Complementar',
        18,
        209
      );
      documento.text(
        'nº 105, de 10 de janeiro de 2001, sobre o montante de responsabilidades de clientes em operações de crédito, e que poderei ter acesso ',
        18,
        213
      );
      documento.text(
        'aos dados constantes em meu nome no SCR por meio do sistema "Registrato" do Banco Central do Brasil.',
        18,
        217
      );

      documento.text(
        "Cláusula Décima - Em função do objeto deste documento, o vendedor poderá ter acesso a dados pessoais do comprador, que venha a ",
        18,
        225
      );

      documento.text(
        "identificar ou permita a identificação de pessoas naturais ('Dados Pessoais'), e que, portanto, serão submetidos às medidas adequadas",
        18,
        229
      );

      documento.text(
        "para proteção de sua confidencialidade, bem como ao cumprimento das obrigações legais previstas na legislação aplicável à proteção",
        18,
        233
      );

      documento.text(
        "de Dados Pessoais, em especial a Lei nº 13.709/2018 (LGPD), suas alterações e regulamentos subsequentes, que disponham sobre ",
        18,
        237
      );

      documento.text(
        "privacidade e proteção de Dados Pessoais, eventualmente criados pela autoridade competente. ",
        18,
        241
      );

      //244
      documento.setFont('courier', 'bold');
      documento.text(
        'CONCORDO(AMOS) COM OS TERMOS E CONDIÇÕES DE VENDA',
        53,
        251
      );
      documento.text(
        '_________________________________________________',
        60,
        261
      );
      documento.text('ASSINATURA DO CLIENTE', 85, 265);

      documento.setFontSize(8);
      documento.line(15, 282, 198, 282); // horizontal line
      documento.text('Pagina ' + pagina, 165, 285);
    } else {
      documento.addPage();
      pagina += 1;

      documento.addImage(imgData, 'JPEG', 15, 10, 0, 0);
      documento.rect(15, 20, 183, 260);

      documento.setFont('courier', 'bold');
      documento.setFontSize(11);
      documento.text('Termos e condições de compra e venda', 18, 26);

      documento.setFont('helvetica', 'normal');
      documento.setFontSize(8);
      documento.text(
        'Cláusula Primeira - Os pedidos de compra podem ser alterados ou corrigidos mediante acordo por escrito firmado pelo comprador e pelo ',
        18,
        33
      );
      documento.text(
        'vendedor, descrevendo as mudanças especificadas a serem realizadas e para cada caso, as modificações de preço e prazo de entrega. ',
        18,
        37
      );
      documento.text(
        'O comprador somente poderá cancelar seu pedido em um prazo máximo de 7 dias após a emissão do mesmo.',
        18,
        41
      );

      documento.text(
        'Cláusula Segunda - Os termos de pagamento são acertados com base na cotação de preços e após isto transferido para a Nota Fiscal.',
        18,
        49
      );
      documento.text(
        'Se a situação financeira do comprador resultar em incerteza de pagamento para o vendedor, o mesmo poderá sem necessidade de notificar',
        18,
        53
      );
      documento.text(
        'o comprador, atrasar as entregas parciais ainda pendentes e solicitar o pagamento antecipado das mesmas.',
        18,
        57
      );
      documento.text('', 18, 65);

      documento.text(
        'Cláusula Terceira - O não pagamento do boleto ou duplicatas na data do seu vencimento terá como consequência a aplicação de multa ',
        18,
        65
      );
      documento.text(
        'moratória de 2%(dois por cento) sobre o valor total de débitos, mais juros de mora de 1,8%(um ponto oito por cento), contados a partir da',
        18,
        69
      );
      documento.text(
        'data de vencimento do boleto.',
        18,
        73
      );

      documento.text(
        'Cláusula Quarta - A devolução de produtos para crédito de valores ou troca, só poderá ser feita com autorização por escrito do Gerente de ',
        18,
        81
      );
      documento.text(
        'Vendas. O Gerente de Vendas não aceitará devoluções de produtos em prazos maiores do que 7 dias da data de recebimento.',
        18,
        85
      );
      documento.text('', 18, 97);

      documento.text(
        'Cláusula Quinta - O vendedor poderá cancelar um pedido de compra em qualquer momento, sem responsabilidade alguma e mediante simples',
        18,
        93
      );
      documento.text(
        'aviso por escrito ao comprador, nos seguintes casos: o comprador esteja em situação de falência declarada, falta de pagamento de ',
        18,
        97
      );
      documento.text(
        'pedidos anteriores ou falta de pagamento de juros pendentes relativos a pagamentos anteriores, em geral, por qualquer não cumprimento ',
        18,
        101
      );
      documento.text(
        'das obrigações a cargo do comprador conforme esse documento, pela inclusão do nome do comprador aos órgãos de proteção de crédito.',
        18,
        105
      );


      documento.text(
        'Cláusula Sexta - O comprador poderá cancelar um pedido de compra por escrito em um prazo de 7 dias desde que seja comprovado o ',
        18,
        113
      );
      documento.text(
        'recebimento do pedido de cancelamento pelo vendedor.',
        18,
        117
      );

      documento.text(
        'Cláusula Sétima - Os termos e obrigações estabelecidas no presente documento são de cumulativos e adicionais a quaisquer outros direitos',
        18,
        125
      );
      documento.text(
        'que o vendedor possa ter conforme as leis locais. Qualquer denúncia pelo não cumprimento das obrigações do comprador será formalizada',
        18,
        129
      );
      documento.text(
        'expressamente por escrito e não deve ser entendida como uma renúncia a qualquer outro, o não cumprimento presente ou futuro. A invalidez,',
        18,
        133
      );
      documento.text(
        'legalidade e aplicabilidade do restante das cláusulas desde documento não afetará a validez, legalidade e a aplicabilidade das demais ',
        18,
        137
      );
      documento.text(
         'cláusulas.',
        18,
        141
      );

      documento.text(
        'Cláusula Oitava - Para todo o conteúdo relativo a interpretação, execução e cumprimento deste documento, as partes se submetem as leis ',
        18,
        149
      );
      documento.text(
        'aplicáveis e tribunais competentes da República Federativa do Brasil, Comarca de Rondonópolis/MT, renunciando expressamente a qualquer',
        18,
        153
      );
      documento.text(
        'outro foro que pudera corresponder por razão de seus domicílios presente ou futuros ou por qualquer outra causa.',
        18,
        157
      );

      documento.text(
        'Cláusula Nona - Em virtude de operação de crédito, de qualquer natureza, pleiteada junto à empresa CIA NITRO QUIMICA BRASILEIRA,',
        18,
        165
      );
      documento.text(
        'inscrita no CNPJ nº 61.150.348/0008-26, autorizo a NAGRO CRÉDITO AGRO LTDA., inscrita no CNPJ sob o nº 22.165.622/0001-02, ',
        18,
        169
      );
      documento.text(
        'a consultar, a qualquer tempo, débitos e responsabilidades decorrentes de operações com características de crédito e informações',
        18,
        173
      );
      documento.text(
        'de títulos de minha emissão registrados, tal como, mas não se limitando a Cédulas de Produto Rural, e registros de medidas judiciais que em',
        18,
        177
      );
      documento.text(
        'meu nome e de minhas empresas, constem ou venham a constar nos Sistemas de Informações de Crédito (SCR) e em Sistema de registro ou',
        18,
        181
      );
      documento.text(
        'de depósito centralizado operado por entidade registradora ou depositária central autorizada, ambos, ou dos sistemas que venham a ',
        18,
        185
      );
      documento.text(
        'complementá-lo ou a substituí-lo. Autorizo também à NAGRO CRÉDITO AGRO LTDA a compartilhar os dados desta consulta exclusivamente',
        18,
        189
      );
      documento.text(
        'com a CIA NITRO QUIMICA BRASILEIRA, na qual estou pleiteando operação de crédito. O(s) DEVEDOR(ES) e FIADOR(ES) se cientificam',
        18,
        193
      );
      documento.text(
        'que, as consultas ao SCR serão realizadas com base na presente autorização, prestada neste INSTRUMENTO, e que o SCR tem por' ,
        18,
        197
      );
     // a partir daqui é continuação
      documento.text(
        'finalidades prover informações ao Banco Central do Brasil, para fins de monitoramento e supervisão à que estão expostas do crédito no',
        18,
        201
      );
      documento.text(
        'sistema financeiro e para o exercício de suas atividades de fiscalização e propiciar o intercâmbio de informações entre instituições ',
        18,
        205
      );
      documento.text(
        'financeiras com o objetivo de subsidiar decisões de crédito e de negócios, conforme definido no § 1º do art. 1º da Lei Complementar',
        18,
        209
      );
      documento.text(
        'nº 105, de 10 de janeiro de 2001, sobre o montante de responsabilidades de clientes em operações de crédito, e que poderei ter acesso ',
        18,
        213
      );
      documento.text(
        'aos dados constantes em meu nome no SCR por meio do sistema "Registrato" do Banco Central do Brasil.',
        18,
        217
      );

      documento.text(
        "Cláusula Décima - Em função do objeto deste documento, o vendedor poderá ter acesso a dados pessoais do comprador, que venha a ",
        18,
        225
      );

      documento.text(
        "identificar ou permita a identificação de pessoas naturais ('Dados Pessoais'), e que, portanto, serão submetidos às medidas adequadas",
        18,
        229
      );

      documento.text(
        "para proteção de sua confidencialidade, bem como ao cumprimento das obrigações legais previstas na legislação aplicável à proteção",
        18,
        233
      );

      documento.text(
        "de Dados Pessoais, em especial a Lei nº 13.709/2018 (LGPD), suas alterações e regulamentos subsequentes, que disponham sobre ",
        18,
        237
      );

      documento.text(
        "privacidade e proteção de Dados Pessoais, eventualmente criados pela autoridade competente. ",
        18,
        241
      );

      //244
      documento.setFont('courier', 'bold');
      documento.text(
        'CONCORDO(AMOS) COM OS TERMOS E CONDIÇÕES DE VENDA',
        53,
        251
      );
      documento.text(
        '_________________________________________________',
        60,
        261
      );
      documento.text('ASSINATURA DO CLIENTE', 85, 265);

      documento.setFontSize(8);
      documento.line(15, 282, 198, 282); // horizontal line
      documento.text('Pagina ' + pagina, 165, 285);
    }

    documento.output('dataurlnewwindow');
  }

  downloadExcel(base64String: string, fileName: string) {
    const source = `data:application/excel;base64,${base64String}`;
    const link = document.createElement('a');
    link.href = source;
    link.download = `${fileName}.csv`;
    link.click();
  }
}
