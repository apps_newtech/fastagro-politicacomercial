import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PoFieldModule } from '@po-ui/ng-components';
import { SharedModule } from '../shared/shared.module';
import { PedidoRoutingModule } from './pedido-routing.module';
import { PedidoComponent } from './pedido.component';
import { FormPedidoComponent } from './form-pedido/form-pedido.component';
import { VisualizarPedidoComponent } from './visualizar-pedido/visualizar-pedido.component';

@NgModule({
  declarations: [
    PedidoComponent,
    FormPedidoComponent,
    VisualizarPedidoComponent,
  ],
  imports: [
    CommonModule,
    PedidoRoutingModule,
    PoFieldModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [PedidoComponent],
})
export class PedidoModule {}
