import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormPedidoComponent } from './form-pedido/form-pedido.component';
import { PedidoComponent } from './pedido.component';
import { VisualizarPedidoComponent } from './visualizar-pedido/visualizar-pedido.component';

const routes: Routes = [
  { path: '', component: PedidoComponent },
  { path: 'new', component: FormPedidoComponent },
  { path: 'edit/:nr_pedido_web', component: FormPedidoComponent },
  { path: 'view/:nr_pedido_web', component: VisualizarPedidoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PedidoRoutingModule {}
