import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteLookupFilter } from 'src/app/shared/lookup/cliente.lookup';
import { representanteLookupFilter } from 'src/app/shared/lookup/repres.lookup';
import { ItemLookupFilter } from 'src/app/shared/lookup/item.lookup';
import { PoLookupColumn, PoSelectOption } from '@po-ui/ng-components';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {
  PoModalAction,
  PoPageAction,
  PoTableColumn,
  PoTableAction,
  PoPageFilter,
  PoNotificationService,
  PoModalComponent,
  PoComboOption,
} from '@po-ui/ng-components';
import { observable, Observable, of } from 'rxjs';
import { ItemPedidoAPI, ItemPedidoWeb, ItemsFaixa, ItemsPedidoWeb, Parametros, Pedido, PedidoWeb } from '../pedido.model';
import { PedidoService } from '../pedido.service';
import { CulturaLookupFilter } from 'src/app/shared/lookup/cultura.lookup';
import { CampanhaLookupFilter } from 'src/app/shared/lookup/campanha.lookup';
import { TipoCargaLookupFilter } from 'src/app/shared/lookup/tipo-carga.lookup';
import { AgenteLookupFilter } from 'src/app/shared/lookup/representante.lookup';
import { map, finalize, switchMap, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { cidadeLookupFilter } from 'src/app/shared/lookup/cidade.lookup';
import { regiaoLookupFilter } from 'src/app/shared/lookup/regiao.lookup';
import { cpf,cnpj } from 'cpf-cnpj-validator';
import { tipoOperacaoLookupFilter } from 'src/app/shared/lookup/tipoOperacao.lookup';
import { entregaLookupFilter } from 'src/app/shared/lookup/entrega.lookup';
import { analyzeAndValidateNgModules } from '@angular/compiler';


@Component({
  selector: 'app-form-pedido',
  templateUrl: './form-pedido.component.html',
  styleUrls: ['./form-pedido.component.scss'],
})

export class FormPedidoComponent implements OnInit {
  customerLookupColumns: PoLookupColumn[] = [
    { label: 'Codigo Cliente', property: 'customerCode', fieldLabel: true },
    { label: 'Nome Abrev', property: 'shortName', fieldLabel: true },
    { label: 'Nome', property: 'customerName', fieldLabel: true },
    { label: 'CNPJ/CPF', property: 'personalId', fieldLabel: true },
  ];

  representanteLookupColumns: PoLookupColumn[] = [
    { label: 'Representante', property: 'cod_rep', fieldLabel: true },
    { label: 'Nome', property: 'nome', fieldLabel: true },
    { label: 'Usuário', property: 'cod_usuario', fieldLabel: true }
  ];

  entregaLookupColumns: PoLookupColumn[] = [
    { label: 'Local Entrega', property: 'localEntrega', fieldLabel: true },
    { label: 'IE', property: 'ins-estadual', fieldLabel: true },
    { label: 'Endereco', property: 'endereco', fieldLabel: true },
    { label: 'Bairro', property: 'bairro', fieldLabel: true },
    { label: 'Cidade', property: 'cidade', fieldLabel: true },
    { label: 'UF', property: 'estado', fieldLabel: true },
    { label: 'CEP', property: 'cep', fieldLabel: true },
  ];

  itemLookupColumns: PoLookupColumn[] = [
    { label: 'Item', property: 'product', fieldLabel: true },
    { label: 'Descrição', property: 'productDescription', fieldLabel: true },
  ];

  culturaLookupColumns: PoLookupColumn[] = [
    { label: 'Código', property: 'cod_cultura', fieldLabel: true },
    { label: 'Descrição', property: 'desc_cultura', fieldLabel: true },
  ];
  cidadeLookupColumns: PoLookupColumn[] = [
    { label: 'Cidade', property: 'cidade', fieldLabel: true },
    { label: 'Estado', property: 'estado', fieldLabel: true },
    { label: 'País', property: 'pais', fieldLabel: true },
  ];
  regiaoLookupColumns: PoLookupColumn[] = [
    { label: 'Região', property: 'nome-ab-reg', fieldLabel: true },
    { label: 'Descrição', property: 'nome-regiao', fieldLabel: true },
  ];

  tipoOperacaoLookupColumns: PoLookupColumn[] = [
    { label: 'Codigo', property: 'codTipoOperacao', fieldLabel: true },
    { label: 'Descrição', property: 'descricao', fieldLabel: true },
  ];
  agenteLookupColumns: PoLookupColumn[] = [
    { label: 'Código', property: 'code', fieldLabel: true },
    { label: 'Nome Abrev', property: 'shortName', fieldLabel: true },
    { label: 'Nome', property: 'name', fieldLabel: true },
  ];

  campanhaLookupColumns: PoLookupColumn[] = [
    { label: 'Código', property: 'cod_campanha', fieldLabel: true },
    { label: 'Estabelecimento', property: 'cod_estabel' },
    { label: 'Início Validade', property: 'data_valid_ini' },
    { label: 'Fim Validade', property: 'data_valid_fim' },
    { label: 'Data Pagamento', property: 'data_pagto' },
  ];

  tipoCargaLookupColumns: PoLookupColumn[] = [
    { label: 'Código', property: 'cod_tipo_carga', fieldLabel: true },
    { label: 'Descricao', property: 'desc_tipo_carga' },
  ];

  tipoFreteOptions: PoComboOption[] = [
    { label: 'CIF', value: 1 },
    { label: 'FOB', value: 2 },
  ];

  pageActions: PoPageAction[] = [
    { label: 'Incluir', action: this.openFormItem.bind(this) },
  ];

  public tipoVendaOptions: Array<PoSelectOption> = [
    { label: 'Direta', value: '1' },
    { label: 'Distribuição', value: '2' },
    { label: 'Agenciamento', value: '3' }
  ];

  private disclaimers = [];
  tableColumnsFaixa: PoTableColumn[] = [
    {property: 'cCorFaixa',
    label: 'Faixa',
    type: 'label',
    labels: [
      {
        value: 'Vermelho',
        label: ' ',
        color: 'color-07',
        tooltip: 'Vermelho'
      },
      {
        value: 'Amarelo',
        label: ' ',
        color: 'color-06',
        tooltip: 'Amarelo'
      },
      {
        value: 'Azul',
        label: ' ',
        color: 'color-03',
        tooltip: 'Azul'
      },
      {
        value: 'Verde',
        label: ' ',
        color: 'color-09',
        tooltip: 'Verde'
      },
      {
        value: 'Preto',
        label: ' ',
        color: 'color-05',
        tooltip: 'Preto'
      },
     ],
    },
    { label: 'Valor', property: 'dePrecoTabela' },
    ];

  tableColumns: PoTableColumn[] = [
    { label: 'Seq', property: 'seq_item' },
    { label: 'Cod. Produto', property: 'it_codigo' },
    { label: 'Faixa de Cor',  property: 'cCorFaixa',
    type: 'label',
    labels: [
      {
        value: 'Vermelho',
        label: 'Vermelho',
        color: 'color-07',
        tooltip: 'Vermelho'
      },
      {
        value: 'Amarelo',
        label: 'Amarelo',
        color: 'color-06',
        tooltip: 'Amarelo'
      },
      {
        value: 'Azul',
        label: 'Azul',
        color: 'color-03',
        tooltip: 'Azul'
      },
      {
        value: 'Verde',
        label: 'Verde',
        color: 'color-09',
        tooltip: 'Verde'
      },
      {
        value: 'Preto',
        label: 'Preto',
        color: 'color-05',
        tooltip: 'Preto'
      },
     ],
    },
    { label: 'Quantidade', property: 'quantidade' },
    {label: 'Hectares', property: 'deHectares'},
    { label: 'UN', property: 'un' },
    { label: 'Descrição', property: 'desc_item' },
    { label: 'Nat Oper', property: 'cNatOperacao'},
    { label: 'Origem', property: 'cOrigem' },
    { label: 'Preço NET/FOB Target', property: 'dePrecoNetFobTarget', type: 'number', format: '1.2-5'},
    { label: 'Frete R$/Kg', property: 'deValorFrete', type: 'number' , format: '1.2-5'},
    { label: 'Preço NET/FOB (R$/Kg.L)', property: 'dePrecoNetFob'},
    { label: 'Preço NET/FOB C/ EF', property: 'dePrecoNetFobEF', type: 'number', format: '1.2-5' },
    { label: 'Valor EF', property: 'dePrecoEF', type: 'number', format: '1.2-5', visible: false },
    { label: 'Preço NF', property: 'dePrecoNF', type: 'number', format: '1.2-5' },
    { label: 'Valor Total', property: 'deValorTotal', type: 'number', format: '1.2-2' },
    { label: 'Desconto Campanha', property: 'deDescontoCamp', type: 'number', format: '1.2-5' },
  ];
  comissaoColumns: PoTableColumn[] = [
    { label: 'Seq', property: 'seq_item' },
    { label: 'Cod. Produto', property: 'it_codigo' },
    { label: 'Descrição', property: 'desc_item' },
    { label: 'Comissão Total(R$)', property: 'deComissaoTotal', type: 'number', format: '1.2-2' },
    { label: 'Comissão %', property: 'dePercComissao' },
    { label: 'Preço NET/FOB com EF', property: 'dePrecoNetFobEF', type: 'number', format: '1.2-5' },
    { label: 'Comissão MAX Faixa', property: 'deComissaoMaxFaixa' },
    { label: '% Agente', property: 'dePercComissaoAgt' , type: 'number', format: '1.2-2' },
    { label: 'Comissão Agente(R$)', property: 'deComissaoAgt', type: 'number', format: '1.2-2' },
    { label: 'Comissao Agt Total(R$)', property: 'deComissaoAgtTotal', type: 'number', format: '1.2-2' },
  ];
  gerencialColumns: PoTableColumn[] = [
    { label: 'Cod. Produto', property: 'it_codigo' },
    { label: 'Descrição', property: 'desc_item' },
    { label: 'Preço NET/FOB com EF', property: 'dePrecoNetFobEF', type: 'number', format: '1.2-5' },
    { label: 'Comissão Ajustada', property: 'dePercComisAjust' },
    { label: 'Prêmio Distribuição', property: 'dePremioDistrib', type: 'number', format: '1.2-5' },
    { label: 'Comissão(R$/Unid)', property: 'deComissaoUnit', type: 'number', format: '1.2-2' },
    { label: 'Custo', property: 'deCusto', type: 'number', format: '1.1-3' },
    { label: 'Comissão Total(R$)', property: 'deComissaoTotal', type: 'number', format: '1.2-2' },
    { label: 'Comissão %', property: 'dePercComissao' },
    { label: '% Agente', property: 'dePercComissaoAgt' , type: 'number', format: '1.2-2' },
    { label: 'Comissão Agente(R$)', property: 'deComissaoAgt', type: 'number', format: '1.2-2' },
    { label: 'Comissao Agt Total(R$)', property: 'deComissaoAgtTotal', type: 'number', format: '1.2-2' },
    { label: 'MB à Vista %', property: 'dePercMbVista',type: 'number', format: '1.2-2' },
    { label: 'MB à Prazo %', property: 'dePercMbPrazo', type: 'number', format: '1.2-2' },
    { label: 'MB à Vista(R$/Unid)', property: 'deMbVistaUnit', type: 'number', format: '1.2-2' },
    { label: 'Total MB à Vista R$', property: 'deMbVistaTotal', type: 'number', format: '1.2-2' },
    { label: 'MB à Prazo(R$/Unid)', property: 'deMbPrazoUnit', type: 'number', format: '1.2-2' },
    { label: 'Total MB à Prazo R$', property: 'deMbPrazoTotal', type: 'number', format: '1.2-2' },
    { label: 'Margem Comissão % à vista', property: 'dePercMargemVista' },
    { label: 'Margem Comissão R$ à vista', property: 'deMargemVista' },
    { label: 'Financeiro por Produto', property: 'deFinanceiro' },
    { label: 'ICMS', property: 'deValorICMS' },
    { label: 'PIS/COFINS', property: 'deValorPisCofins' },
    { label: 'Qtde Midas', property: 'deQtdeMidas' },
   // { label: 'Foliar', property: 'deFoliar' },
    //{ label: 'Qtde Foliares', property: 'deQtdeFoliar' },
   // { label: 'Check Foliares', property: 'cCheckFoliares' },
    //{ label: 'Qtde ULEXITA', property: 'deQtdeUlexita' },
  //  { label: 'Check ULEXITA', property: 'cCheckUlexita' },
   // { label: 'F Preto', property: 'deFaixaPreto' },
  //  { label: 'F Vermelho', property: 'deFaixaVermelho' },
  //  { label: 'F Amarelo', property: 'deFaixaAmarelo' },
  //  { label: 'F Verde', property: 'deFaixaVerde' },
  //  { label: 'Mínimo', property: 'deMinimo' },
    { label: 'Faixa', property: 'cFaixa',
    type: 'label',
    labels: [
      {
        value: 'Vermelho',
        label: 'Vermelho',
        color: 'color-07',
        tooltip: 'Vermelho'
      },
      {
        value: 'Amarelo',
        label: 'Amarelo',
        color: 'color-06',
        tooltip: 'Amarelo'
      },
      {
        value: 'Azul',
        label: 'Azul',
        color: 'color-03',
        tooltip: 'Azul'
      },
      {
        value: 'Verde',
        label: 'Verde',
        color: 'color-09',
        tooltip: 'Verde'
      },
      {
        value: 'Preto',
        label: 'Preto',
        color: 'color-05',
        tooltip: 'Preto'
      },
     ],
    },
  //  { label: 'Faixa Acima %', property: 'cPercFaixaAcima' },
    { label: 'Comissão Base Faixa %', property: 'dePercComisBaseFaixa' },
   // { label: 'Delta Preço Faixa R$', property: 'deDeltaPreco' },
   // { label: 'Var % Preço Faixa', property: 'dePercVarPrecoFaixa' },
    //{ label: 'Delta Comis. Máx. Faixa', property: 'deDeltaComisMaxFaixa' },
  //  { label: 'Var Comis. Faixa', property: 'deVarComisFaixa' },
   // { label: 'Formula Base p/ Frete', property: 'deFormulaBaseFrete' },
  ];

  customerData: any = {};
  dataItem: ItemPedidoWeb[] = [];

  tableActions: PoTableAction[] = [
    { label: 'Editar', action: this.alteraItem.bind(this)},
    { label: 'Apagar', action: this.excluiItem.bind(this)}
  ];
  items$!: Observable<ItemsPedidoWeb>;
  itemsFaixa$!: Observable<ItemsFaixa>;
  iseq_item$!: Observable<any>;
  hasMore$!: Observable<boolean>;
  show!:   boolean;
  percInformado!: boolean;
  btSalvar!: string;
  titulo!: string;
  altera = false;
  inr_pedido_web!: number;
  p_carrega!: boolean;
  p_alteraItem!: boolean;
  itemInformado!:boolean;
  total: number = 0;
  totalComissao: number = 0;
  totalMargem: number = 0;
  totalDescontoCamp: number = 0;
  precoDecimal: number = 0;
  dComissaoMax!: string;
  nextLabelWidget: string = 'Avançar';
  nextLabelWidgetComissao: string = 'Avançar';
  nextLabelWidgetGerencial: string = 'Salvar'
  previousLabelWidget: string = 'Voltar';
  ClienteCadastrado!: boolean;
  AgenteInformado!: boolean;
  habilitaAgente!: boolean;
  cpfcnpjMask = "";
  ErroCPFCNPJ!: boolean;
  shortName!: string;
  customerCode!: number;
  p_itemSelecionado!: string;
  atualizaValor!: boolean;
  carregaPrecoNF!: number;
  carregaPrecoNetFob!:number;
  carregaQuantidade!: number;
  columnsDefault!: Array<PoTableColumn>;
  validaParametros!: boolean;
  dataPagtoCampanha!: Date;



  pedidoForm!: FormGroup;
  itemPedidoForm!: FormGroup;

  close: PoModalAction = {
    action: () => {
      this.closeModal();
    },
    label: 'Cancelar',
    danger: true,
  };

  confirm: PoModalAction = {
    action: () => {
      this.incluiItem();
    },
    label: 'Salvar',
  };

  @ViewChild(PoModalComponent, { static: true })
  poModal!: PoModalComponent;

  @ViewChild('optionsForm', { static: true }) form!: NgForm;

  culturaFormat(value: any) {
    return `${value.cod_cultura} - ${value.desc_cultura}`;
  }

  tipoCargaFormat(value: any) {
    return `${value.cod_tipo_carga} - ${value.desc_tipo_carga}`;
  }

  agenteFormat(value: any) {
    return `${value.code} - ${value.name}`;
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private service: PedidoService,
    private formBuilder: FormBuilder,
    private poNotification: PoNotificationService,
    public customerLookup: ClienteLookupFilter,
    public representanteLookup: representanteLookupFilter,
    public entregaLookup: entregaLookupFilter,
    public culturaLookup: CulturaLookupFilter,
    public cidadeLookup: cidadeLookupFilter,
    public regiaoLookup: regiaoLookupFilter,
    public tipoOperacaoLookup: tipoOperacaoLookupFilter,
    public campanhaLookup: CampanhaLookupFilter,
    public tipoCargaLookup: TipoCargaLookupFilter,
    public itemLookup: ItemLookupFilter,
    public agenteLookup: AgenteLookupFilter,
  ) {}

  ngOnInit(): void {
    //this.altera = true;
    this.precoDecimal = 2;
    this.ClienteCadastrado = true;
    this.p_carrega = false;
    this.atualizaValor = false;
    this.btSalvar = "Avançar";
    this.show = true;
    this.percInformado = true;
    this.AgenteInformado = true;
    this.habilitaAgente  = true;
    this.itemInformado   = false;
    this.atualizaPerfil();
    this.configForm();
    this.configFormItem();
    this.updateListItem();

  }



  private configForm() {
    this.pedidoForm = this.formBuilder.group({
      customerCode: ['', [Validators.required, Validators.maxLength(12)]],
      personalId: [''],
      customerName: [''],
      email: [''],
      phone1: [''],
      city: [''],
      cidade: [''],
      estado: [''],
      bairro: [''],
      localEntrega: [''],
      cep: [''],
      endereco: [''],
      state: [''],
      stateRegistration: [''],
      codCampanha: [''],
      codCultura: ['', [Validators.required]],
      tipoFrete: ['', [Validators.required]],
      tipoCarregamento: ['', [Validators.required]],
      dataEntrega: ['', [Validators.required]],
      dataVencimento: ['', [Validators.required]],
      codAgente: [''],
      nr_pedido_web: [''],
      cod_rep: [''],
      cod_usuario_trans: [''],
      nomeUsuario: [''],
      nome_usuario: [''],
      rotaDetalhada: [''],
      observacao: [''],
      codTipoVenda: ['', [Validators.required]],
      nomeVendedor: [''],
      dataCriacao: [''],
      completeAddress: [''],
      Safra: [''],
      regiao: [''],
      ClienteCadastrado: ['true'],
      codTipoOperacao: ['']
    });

    this.activatedRoute.params
      .pipe(
        switchMap((param) => {
          const { nr_pedido_web } = param;
          if (nr_pedido_web) {
            this.altera = true;
            this.inr_pedido_web = nr_pedido_web;
            return this.service.getPedido(nr_pedido_web);
          }
          return of({} as Pedido);
        })
      )
      .subscribe((pedido) => {
        if (this.altera) {
          let novoCliente = false;
          if (pedido.customerCode == "" && pedido.customerName || ""){
            novoCliente = true;
            this.changeEvent('true');
            console.debug(this.ClienteCadastrado)
          }

          let jsonPedido: any;
          this.titulo = `Editando Pedido Nr: ${pedido.nr_pedido_web}`;
          jsonPedido = pedido;
          console.debug(pedido.localEntrega)
          console.debug(pedido)
          this.shortName = pedido.shortName
          this.customerCode = Number(pedido.customerCode)
          this.entregaLookup.setShortName(this.shortName);
          //this.pedidoForm.patchValue(pedido);
          this.pedidoForm.patchValue({
            customerCode: pedido.customerCode,
            customerName: pedido.customerName,
            personalId: pedido.personalId,
            codCampanha: pedido.codCampanha,
            codCultura: pedido.codCultura,
            tipoFrete: pedido.tipoFrete,
            tipoCarregamento: pedido.tipoCarregamento,
            dataEntrega: pedido.dataEntrega,
            dataVencimento: pedido.dataVencimento,
            codAgente: pedido.codAgente,
            nr_pedido_web: pedido.nr_pedido_web,
            cod_rep: pedido.cod_usuario,
            nome_usuario: pedido.nome_usuario,
            nomeUsuario: pedido.nome_usuario,
            cod_usuario_trans: pedido.cod_usuario_trans,
            rotaDetalhada: pedido.rotaDetalhada,
            observacao:pedido.observacao,
            codTipoVenda: pedido.codTipoVenda,
            nomeVendedor: pedido.nomeVendedor,
            dataCriacao: pedido.dataCriacao,
            Safra:        pedido.Safra,
            regiao:       pedido.regiao,
            cidade:       pedido.cidade,
            estado:       pedido.estado,
            ClienteCadastrado: novoCliente,
            codTipoOperacao: pedido.codTipoOperacao,
            localEntrega: pedido.localEntrega,
            stateRegistration: pedido.stateRegistration
          })
          this.p_carrega = true;;
        } else {

          this.service.getSequenciaPedido().subscribe((sequencia) => {
          let iseq_pedido: any;
          iseq_pedido = sequencia;

          this.titulo = `Novo Pedido Nr: ${iseq_pedido['nr_pedido_web']}`;
          this.inr_pedido_web = iseq_pedido['nr_pedido_web'];
          this.pedidoForm.patchValue({
            nr_pedido_web: iseq_pedido['nr_pedido_web'],
            nomeVendedor:  iseq_pedido['nomeVendedor'],
            cod_rep:   iseq_pedido['cod_usuario'],
            nomeUsuario: iseq_pedido['nome_usuario'],
            dataCriacao:   iseq_pedido['dataCriacao'],
            regiao:        iseq_pedido['regiao'],
            cod_usuario_trans: iseq_pedido['cod_usuario_trans']
          })
          this.p_carrega = true;
              this.itemPedidoForm.patchValue({
               nr_pedido_web: iseq_pedido['nr_pedido_web'],
              });
          });
         }
      });
  }

  changeColumnVisible(event: any) {
    localStorage.setItem('initial-columns', event);
  }

  restoreColumn() {
    this.tableColumns = this.columnsDefault;
  }

  private atualizaPerfil() {
    if (environment.perfil == "RTV" || environment.perfil == "") {
      this.show = false;
      this.nextLabelWidgetComissao = 'Salvar';
    }
   /*if (environment.perfil || "RTV"){this.show = true
      console.debug(`perfil que pode visualizar comissao ${environment.perfil}`);
      this.btSalvar = 'Avançar';} */

  }

  private configFormItem() {
    this.itemPedidoForm = this.formBuilder.group({
        itemCode          : [''],
        cCodEstabel       : [''],
        iCodEmitente      : [''],
        cItCodigo         : [''],
        desc_item         : [''],
        DtImplant         : [''],
        DtVencto          : [''],
        DtEntrega         : [''],
        cUsuario          : [''],
        iCodAgente        : [''],
        dePrecoNetFob     : ['0'],
        dePrecoNF         : ['0'],
        dePercComissao    : ['0'],
        deComissaoTotal   : ['0'],
        deQuantidade      : ['0'],
        deValorFrete      : ['0,00000'],
        dePercComisAgent  : [''],
        iTipoVenda        : [''],
        iTipoCarregamento : [''],
        iTipoFrete        : [''],
        cCidade           : [''],
        cUf               : [''],
        cCodCampanha      : [''],
        cCodCultura       : [''],
        nr_pedido_web     : [''],
        seq_item          : [''],
        deValorTotal      : [''],
        cRegiao            : [''],
        codTipoOperacao   : [''],
        localEntrega      : ['']
    });

    //this.poModal.open();
  }


  private openFormItem() {
    console.debug('Incluir item')
    console.debug(this.pedidoForm.controls.codAgente.value )

    if (this.pedidoForm.controls.codAgente.value == "" || typeof this.pedidoForm.controls.codAgente.value === "undefined"){
      this.AgenteInformado = false;
      this.dComissaoMax    = '0';
    }
    else{this.AgenteInformado = true
          }
    let iseq_item: any;
    this.p_alteraItem = false;

    this.service.getSequenciaItem(this.inr_pedido_web).subscribe((sequencia) => {
        iseq_item = sequencia;
        this.itemPedidoForm.patchValue({
         //dePercComisAgent  : this.dComissaoMax,
         seq_item: iseq_item['seq_item'],
        });
    });
    this.AtualizaFaixa();
    this.poModal.open();
  }

  public readonly filterSettings: PoPageFilter = {
    //action: this.filterAction.bind(this),
    // advancedAction: this.advancedFilterActionModal.bind(this),
    placeholder: 'Pesquisar',
  };


  canActiveNextStep(form: FormGroup) {

        if (this.ClienteCadastrado) {
          if (this.pedidoForm.controls.customerCode.value == "" || typeof this.pedidoForm.controls.customerCode.value === "undefined")
        {this.poNotification.error(
          `Informar um cliente válido.`
        );
        return false;}
        else if (this.pedidoForm.controls.localEntrega.value == "" || typeof this.pedidoForm.controls.localEntrega.value === "undefined"){
          {this.poNotification.error(
            `Informar um local de Entrega válido para o Cliente.`
          );
          return false;}
        } else {return true;}

        } else {
          if (this.pedidoForm.controls.customerName.value == "" || typeof this.pedidoForm.controls.customerName.value === "undefined")
        {this.poNotification.error(
          `Informar um nome para o Cliente.`
        );
        return false;}
        else if (this.pedidoForm.controls.personalId.value == "" || typeof this.pedidoForm.controls.personalId.value === "undefined" || this.ErroCPFCNPJ){
          {this.poNotification.error(
            `Informar um CNPJ/CPF válido para o Cliente.`
          );
          return false;}
        }
        else if (this.pedidoForm.controls.cidade.value == "" || typeof this.pedidoForm.controls.cidade.value === "undefined"){
          {this.poNotification.error(
            `Informar uma cidade válida para o Cliente.`
          );
          return false;}
        }

        /*else if (this.pedidoForm.controls.regiao.value == ""){
          {this.poNotification.error(
            `Informar uma região válida.`
          );
          return false;}
        }*/

        else {return true;}
        }

  }

  changeEvent(event: string) {
    if (event){
      this.ClienteCadastrado = false
    }
    else{
      this.ClienteCadastrado = true
    }
  }

  changeMask(event: string) {
    if (cpf.isValid(event)){
      this.ErroCPFCNPJ = false;
      this.pedidoForm.patchValue({
        personalId: cpf.format(event),
      });

     } else if(cnpj.isValid(event)) {
      this.ErroCPFCNPJ = false;
       this.pedidoForm.patchValue({
        personalId: cnpj.format(event),
      });
    }
    else {this.ErroCPFCNPJ = true;
      this.cpfcnpjMask= ""}
  }
  canActiveFinishStepComissao(form: FormGroup) {
    if (environment.perfil == "RTV" || environment.perfil == "") {
      this.incluiPedido();

      return of(form.valid).pipe(
        tap(() => (this.show = true)),
        finalize(() => (this.show = false))
      );

    }
    else { return true}

  }

  canActiveFinishStep(form: FormGroup) {
    this.incluiPedido();

    return of(form.valid).pipe(
      tap(() => (this.show = true)),
      finalize(() => (this.show = false)));

  }

  avancaParametros(form: FormGroup) {
    console.log("Vencto : " + this.pedidoForm.controls.dataVencimento.value + " Oper: " + this.pedidoForm.controls.codTipoOperacao.value)
    this.validaParametros = false;
    if (this.pedidoForm.controls.codTipoVenda.value == "" || typeof this.pedidoForm.controls.codTipoVenda.value === "undefined")
    {this.poNotification.error(
      `Informar um Tipo de Venda válido.`
    );
    //return false;
    this.validaParametros = false;
  }
    else if (this.pedidoForm.controls.codTipoOperacao.value == "" || typeof this.pedidoForm.controls.codTipoOperacao.value === "undefined"){
      this.poNotification.error(
        `Informar um Tipo de Operação válida.`
      );
     // return false;
     this.validaParametros = false;
    }
    else if (this.pedidoForm.controls.codCultura.value == "" || typeof this.pedidoForm.controls.codCultura.value === "undefined"){
      this.poNotification.error(
        `Informar uma Cultura válida.`
      );
      //return false;
      this.validaParametros = false;
    }
    /*else if (this.pedidoForm.controls.codCampanha.value == ""){
      {this.poNotification.error(
        `Informar uma Campanha válida.`
      );
      return false;}
    }*/
    else if (this.pedidoForm.controls.tipoFrete.value == "" || typeof this.pedidoForm.controls.tipoFrete.value === "undefined"){
      this.poNotification.error(
        `Informar uma Tipo de Frete válido.`
      );
      //return false;
      this.validaParametros = false;
    }
    else if (this.pedidoForm.controls.tipoCarregamento.value == "" || typeof this.pedidoForm.controls.tipoCarregamento.value === "undefined"){
      this.poNotification.error(
        `Informar uma Tipo Carregamento válido.`
      );
      //return false;
      this.validaParametros = false;
    }
    else if (this.pedidoForm.controls.dataEntrega.value == "" || typeof this.pedidoForm.controls.dataEntrega.value === "undefined"){
      this.poNotification.error(
        `Informar uma Data de Entrega válida.`
      );
     // return false;
    }
    else if ((this.pedidoForm.controls.codTipoOperacao.value != 54 && this.pedidoForm.controls.codTipoOperacao.value != 56)
              && (this.pedidoForm.controls.dataVencimento.value == ""
             || typeof this.pedidoForm.controls.dataVencimento.value === "undefined" || this.pedidoForm.controls.dataVencimento.value == null)){

        this.poNotification.error(`Informar uma Data de Vencimento válida.`);
        //return false;
        this.validaParametros = false;

    }
    else if ((this.pedidoForm.controls.dataVencimento.value < this.pedidoForm.controls.dataCriacao.value  || this.pedidoForm.controls.dataEntrega.value < this.pedidoForm.controls.dataCriacao.value) && this.pedidoForm.controls.codTipoOperacao.value != '54' && this.pedidoForm.controls.codTipoOperacao.value != '56'){
      this.poNotification.error(
        `As datas de vencimento ou entrega devem ser maiores que a Data de Criação do Pedido.`
      );
     // return false;
      this.validaParametros = false;
    }
    else if (this.pedidoForm.controls.cod_rep.value == this.pedidoForm.controls.codAgente.value){
      this.poNotification.error(
        `Agente informado não pode ser igual ao Representante da capa do Pedido.`
      );
      //return false;
      this.validaParametros = false;
    }
    else if (this.pedidoForm.controls.codCampanha.value){
        if (this.pedidoForm.controls.dataVencimento.value > this.dataPagtoCampanha){
          this.poNotification.error(
            `Data de vencimento deve ser menor ou igual a Data de Pagamento da Campanha: ` + this.dataPagtoCampanha
          );
         // return false;
         this.validaParametros = false;
        }
       else {
          this.validaParametros = true;
        }
    }
    else {
      this.validaParametros = true;
    }
    console.debug(this.validaParametros)
    if (this.validaParametros){
      console.debug("antes de AvancaParametros")
      this.onAvancaParametros()
      return true;
    }
    {return false}
  }

  avancaRota(form: FormGroup) {
    if (/*Se mesmo removendo espacos, continua vazio */ !this.pedidoForm.controls.rotaDetalhada.value.trim() || /* Verifica tamannho do campo */ this.pedidoForm.controls.rotaDetalhada.value.length === 0 || typeof this.pedidoForm.controls.rotaDetalhada.value === "undefined")
    {this.poNotification.error(
      `Informe uma Rota para o Pedido.`
    );
    return false;}

    else {return true;}

  }

  onCancel(form: FormGroup) {
    console.debug(`Pedido Nr. ${this.pedidoForm.controls.nr_pedido_web.value}`)
      this.service.cancelaPedido(this.pedidoForm.controls.nr_pedido_web.value).subscribe((data) => {

    });

    this.router.navigate(['pedido']);
  }

  onSelectCustomer(args: any) {
    this.p_carrega = false;
    console.debug(args.shortName)
    this.entregaLookup.setShortName(args.shortName);
    this.pedidoForm.patchValue({
      personalId: args.personalId,
      customerName: args.customerName,
      shortName: args.shortName,
      stateRegistration: args.stateRegistration,
      email: args.email,
      phone1: args.phone1,
      city: args.city,
      state: args.state,
      completeAddress: args.completeAddress,
      //regiao: args.shortRegion
    });
    this.shortName = args.shortName
    if (this.altera && this.customerCode == args.customerCode) {

      console.debug('alteração não busca Rota')
      this.p_carrega = true;
    }

    else {    //Busca rota do cliente
      console.debug(`busca Rota ${this.pedidoForm.controls.customerCode.value}`)
      this.service.getRota(this.pedidoForm.controls.customerCode.value).subscribe((rota) => {
        console.debug(rota['localEntrega'])
        this.p_carrega = true;
        this.pedidoForm.patchValue({
          localEntrega:  rota['localEntrega'],
          rotaDetalhada: rota['rotaDetalhada'],      })

      });
      //this.p_carrega = true;
    }
  }

  onSelectRepresentante(args: any) {
    this.p_carrega = false;
    console.debug(args.cod_rep)
    console.debug(args.nome_ab_reg)
    this.pedidoForm.patchValue({
      nomeVendedor: args.nome,
      regiao: args.nome_ab_reg,
    });
    this.p_carrega = true;
  }

  onSelectEntrega(args: any) {
    console.debug(`selecEntrega ${this.shortName}`)
    this.entregaLookup.setShortName(this.shortName);
    this.pedidoForm.patchValue({
      endereco: args.endereco,
      estado: args.estado,
      cidade: args.cidade,
      cep: args.cep,
      bairro: args.bairro,
      stateRegistration: args['ins-estadual']
    });
    this.p_carrega= true;
  }

  carregaValores(){
    this.carregaPrecoNF = this.itemPedidoForm.controls.dePrecoNF.value
    this.carregaPrecoNetFob = this.itemPedidoForm.controls.dePrecoNetFob.value
    this.carregaQuantidade= this.itemPedidoForm.controls.deQuantidade.value;
  }

  onSelectItem(args: any) {
    this.p_carrega = false;

    this.itemPedidoForm.patchValue({
      DtImplant         : this.pedidoForm.controls.dataCriacao.value,
      DtVencto          : this.pedidoForm.controls.dataVencimento.value ? this.pedidoForm.controls.dataVencimento.value : '',
      DtEntrega         : this.pedidoForm.controls.dataEntrega.value,
      iCodAgente        : this.pedidoForm.controls.codAgente.value,
      iCodEmitente      : this.pedidoForm.controls.customerCode.value,
      cUsuario          : '',
      iTipoVenda        : this.pedidoForm.controls.codTipoVenda.value,
      iTipoCarregamento : this.pedidoForm.controls.tipoCarregamento.value,
      iTipoFrete        : this.pedidoForm.controls.tipoFrete.value,
      cCidade           : this.pedidoForm.controls.cidade.value,
      cUf               : this.pedidoForm.controls.estado.value,
      cCodCampanha      : this.pedidoForm.controls.codCampanha.value,
      cCodCultura       : this.pedidoForm.controls.codCultura.value,
      nr_pedido_web     : this.pedidoForm.controls.nr_pedido_web.value,
      cRegiao           : this.pedidoForm.controls.regiao.value,
      codTipoOperacao   : this.pedidoForm.controls.codTipoOperacao.value,
      localEntrega      : this.pedidoForm.controls.localEntrega.value
       //this.pedidoForm.controls.nr_pedido_web.value
    });
    const parametros: Parametros = { ...this.itemPedidoForm.value };
    this.itemPedidoForm.patchValue({
      desc_item: args.productDescription,
    });
    this.service.postItemDecimal(parametros).subscribe((data: any)=>{
      console.debug("Apenas atualiza Casa Decimal")
      console.debug(data)
      this.precoDecimal = data.qtdeDecimal

    });
    console.debug('Selecionou Item')
    console.debug(this.itemPedidoForm)
    console.debug(this.itemPedidoForm.controls.dePrecoNetFob.value)
    if (this.p_alteraItem == false || this.itemPedidoForm.controls.cItCodigo.value != this.p_itemSelecionado /*
      || this.itemPedidoForm.controls.dePrecoNetFob.value == ""
    || this.itemPedidoForm.controls.dePrecoNetFob.value == null
    || this.itemPedidoForm.controls.dePrecoNetFob.value == "0,00000"*/ ) {
      this.habilitaAgente = false;
    this.service.postItemValor(parametros).subscribe((result: any)=>{
      console.debug('Entra no Success')
      console.debug(result)

      this.precoDecimal = result.qtdeDecimal
      this.itemPedidoForm.patchValue({
        dePrecoNetFob: result.dePrecoNetFob,
        deValorFrete:  result.deValorFrete,
        //dePercComisAgent: this.dComissaoMax,
        deComissaoTotal: result.deComissaoTotal,
        dePercComissao: result.dePercComissao,
        dePrecoNF: result.dePrecoNF
      });
      console.debug('perc ' + result.dePercComissaoAgt)
      if (result.dePercComissaoAgt > 0){
        this.percInformado = false;
        this.itemPedidoForm.patchValue({
          dePercComisAgent: result.dePercComissaoAgt,
        });
      }
      else{this.percInformado = true}
      this.carregaValores()
      this.atualizaValor = true;
      this.AtualizaFaixa(
      );

      },(error) => {
        console.debug('Entra no Erro')
        this.itemPedidoForm.patchValue({
          cItCodigo: '',
          desc_item: '',
          dePrecoNetFob: '0',
          dePrecoNF: '0',
          deValorFrete: '0'
        });

        this.p_carrega = true

        console.log(error);
        this.AtualizaFaixa;
      });
      }
    else {
      this.p_carrega = true}
  }


  atualizaPrecoNF() {
    console.debug(this.carregaPrecoNF + '<>' +  this.itemPedidoForm.controls.dePrecoNF.value)
    if (this.atualizaValor == true) {

      if (this.carregaPrecoNF != this.itemPedidoForm.controls.dePrecoNF.value){

        this.itemPedidoForm.patchValue({
          DtImplant         : this.pedidoForm.controls.dataCriacao.value,
          DtVencto          : this.pedidoForm.controls.dataVencimento.value ? this.pedidoForm.controls.dataVencimento.value : '',
          DtEntrega         : this.pedidoForm.controls.dataEntrega.value,
          iCodAgente        : this.pedidoForm.controls.codAgente.value,
          iCodEmitente      : this.pedidoForm.controls.customerCode.value,
          cUsuario          : '',
          iTipoVenda        : this.pedidoForm.controls.codTipoVenda.value,
          iTipoCarregamento : this.pedidoForm.controls.tipoCarregamento.value,
          iTipoFrete        : this.pedidoForm.controls.tipoFrete.value,
          cCidade           : this.pedidoForm.controls.cidade.value,
          cUf               : this.pedidoForm.controls.estado.value,
          cCodCampanha      : this.pedidoForm.controls.codCampanha.value,
          cCodCultura       : this.pedidoForm.controls.codCultura.value,
          nr_pedido_web     : this.pedidoForm.controls.nr_pedido_web.value,
          cRegiao           : this.pedidoForm.controls.regiao.value,
          codTipoOperacao   : this.pedidoForm.controls.codTipoOperacao.value,
          localEntrega      : this.pedidoForm.controls.localEntrega.value
           //this.pedidoForm.controls.nr_pedido_web.value
        });
        const parametros: Parametros = { ...this.itemPedidoForm.value };
        parametros.dePrecoNetFob = '0';
        console.debug('atualizaPrecoNF valores')
        console.debug(parametros)
        this.carregaValores()
        this.service.postItemValor(parametros).subscribe((result: any)=>{
          console.debug('Entra no Success')
          console.debug(result)
          //this.atualizaValor = false;
          this.precoDecimal = result.qtdeDecimal
          this.itemPedidoForm.patchValue({
            dePrecoNetFob: result.dePrecoNetFob,
            deValorFrete:  result.deValorFrete,
            //dePercComisAgent: this.dComissaoMax,
            deComissaoTotal: result.deComissaoTotal,
            dePercComissao: result.dePercComissao,
            dePrecoNF: result.dePrecoNF

          });

        });
      }
    }
  }

  atualizaQuantidade() {

    if (this.atualizaValor == true) {
      if (this.carregaQuantidade != this.itemPedidoForm.controls.deQuantidade.value){
        this.itemPedidoForm.patchValue({
          DtImplant         : this.pedidoForm.controls.dataCriacao.value,
          DtVencto          : this.pedidoForm.controls.dataVencimento.value ? this.pedidoForm.controls.dataVencimento.value : '',
          DtEntrega         : this.pedidoForm.controls.dataEntrega.value,
          iCodAgente        : this.pedidoForm.controls.codAgente.value,
          iCodEmitente      : this.pedidoForm.controls.customerCode.value,
          cUsuario          : '',
          iTipoVenda        : this.pedidoForm.controls.codTipoVenda.value,
          iTipoCarregamento : this.pedidoForm.controls.tipoCarregamento.value,
          iTipoFrete        : this.pedidoForm.controls.tipoFrete.value,
          cCidade           : this.pedidoForm.controls.cidade.value,
          cUf               : this.pedidoForm.controls.estado.value,
          cCodCampanha      : this.pedidoForm.controls.codCampanha.value,
          cCodCultura       : this.pedidoForm.controls.codCultura.value,
          nr_pedido_web     : this.pedidoForm.controls.nr_pedido_web.value,
          cRegiao           : this.pedidoForm.controls.regiao.value,
          codTipoOperacao   : this.pedidoForm.controls.codTipoOperacao.value,
          localEntrega      : this.pedidoForm.controls.localEntrega.value
           //this.pedidoForm.controls.nr_pedido_web.value
        });
        const parametros: Parametros = { ...this.itemPedidoForm.value };
        console.debug('atualizaQuantidade valores')
        console.debug(parametros)
        this.service.postItemValor(parametros).subscribe((result: any)=>{
          console.debug('Entra no Success')
          console.debug(result)
          this.precoDecimal = result.qtdeDecimal
          //this.atualizaValor = false;
          this.itemPedidoForm.patchValue({
            dePrecoNetFob: result.dePrecoNetFob,
            deValorFrete:  result.deValorFrete,
           // dePercComisAgent: this.dComissaoMax,
            deComissaoTotal: result.deComissaoTotal,
            dePercComissao: result.dePercComissao,
            dePrecoNF: result.dePrecoNF

          });
          this.carregaValores()
        });
      }
    }
  }

  atualizaPrecoNetFob() {
    if (this.atualizaValor == true) {
       if (this.carregaPrecoNetFob != this.itemPedidoForm.controls.dePrecoNetFob.value){
        this.itemPedidoForm.patchValue({
          DtImplant         : this.pedidoForm.controls.dataCriacao.value,
          DtVencto          : this.pedidoForm.controls.dataVencimento.value ? this.pedidoForm.controls.dataVencimento.value : '',
          DtEntrega         : this.pedidoForm.controls.dataEntrega.value,
          iCodAgente        : this.pedidoForm.controls.codAgente.value,
          iCodEmitente      : this.pedidoForm.controls.customerCode.value,
          cUsuario          : '',
          iTipoVenda        : this.pedidoForm.controls.codTipoVenda.value,
          iTipoCarregamento : this.pedidoForm.controls.tipoCarregamento.value,
          iTipoFrete        : this.pedidoForm.controls.tipoFrete.value,
          cCidade           : this.pedidoForm.controls.cidade.value,
          cUf               : this.pedidoForm.controls.estado.value,
          cCodCampanha      : this.pedidoForm.controls.codCampanha.value,
          cCodCultura       : this.pedidoForm.controls.codCultura.value,
          nr_pedido_web     : this.pedidoForm.controls.nr_pedido_web.value,
          cRegiao           : this.pedidoForm.controls.regiao.value,
          codTipoOperacao   : this.pedidoForm.controls.codTipoOperacao.value,
          localEntrega      : this.pedidoForm.controls.localEntrega.value
           //this.pedidoForm.controls.nr_pedido_web.value
        });
        const parametros: Parametros = { ...this.itemPedidoForm.value };
        parametros.dePrecoNF = '0';
        console.debug('atualizaPrecoNetFob valores')
        console.debug(parametros)
        this.service.postItemValor(parametros).subscribe((result: any)=>{
          console.debug('Entra no Success')
          console.debug(result)
          this.precoDecimal = result.qtdeDecimal
        //  this.atualizaValor = false;
          this.itemPedidoForm.patchValue({
            dePrecoNetFob: result.dePrecoNetFob,
            deValorFrete:  result.deValorFrete,
           // dePercComisAgent: this.dComissaoMax,
            deComissaoTotal: result.deComissaoTotal,
            dePercComissao: result.dePercComissao,
            dePrecoNF: result.dePrecoNF

          });
        });
        this.carregaValores()
      }
    }
  }

  onAvancaParametros() {
    this.p_carrega=true;
    console.debug('Alterou Parametros')

    //if (this.itemPedidoForm.controls.dePrecoNetFob.value == "" || this.itemPedidoForm.controls.dePrecoNetFob.value == null ) {
    this.itemPedidoForm.patchValue({
      DtImplant         : this.pedidoForm.controls.dataCriacao.value,
      DtVencto          : this.pedidoForm.controls.dataVencimento.value ? this.pedidoForm.controls.dataVencimento.value : '',
      DtEntrega         : this.pedidoForm.controls.dataEntrega.value,
      iCodAgente        : this.pedidoForm.controls.codAgente.value,
      iCodEmitente      : this.pedidoForm.controls.customerCode.value,
      cUsuario          : '',
      iTipoVenda        : this.pedidoForm.controls.codTipoVenda.value,
      iTipoCarregamento : this.pedidoForm.controls.tipoCarregamento.value,
      iTipoFrete        : this.pedidoForm.controls.tipoFrete.value,
      cCidade           : this.pedidoForm.controls.cidade.value,
      cUf               : this.pedidoForm.controls.estado.value,
      cCodCampanha      : this.pedidoForm.controls.codCampanha.value,
      cCodCultura       : this.pedidoForm.controls.codCultura.value,
      nr_pedido_web     : this.pedidoForm.controls.nr_pedido_web.value,
      cRegiao           : this.pedidoForm.controls.regiao.value,
      codTipoOperacao   : this.pedidoForm.controls.codTipoOperacao.value,
      localEntrega      : this.pedidoForm.controls.localEntrega.value
       //this.pedidoForm.controls.nr_pedido_web.value
    });
    console.dir(this.itemPedidoForm.value);
    const parametros: Parametros = { ...this.itemPedidoForm.value };
    this.service.postItemVencimento(parametros).subscribe((data)=>{
      console.debug(this.itemPedidoForm.controls.DtVencto.value)
      this.updateListItem();
      this.p_carrega=false;
      });
    //}

  }

  onSelectCultura(args: any) {
    this.campanhaLookup.setCodCultura(args['cod_cultura']);
  }

  onSelectCampanha(args: any) {
    this.dataPagtoCampanha = args.data_pagto
  }

  onSelectCidade(args: any) {
    if (this.pedidoForm.controls.customerCode.value == "" || this.pedidoForm.controls.customerCode.value == null){
      this.pedidoForm.patchValue({
        estado: args.estado,
      });
    }
  }

  onSelectTipoVenda(args: any) {
    console.log(`TipoVenda ` + this.pedidoForm.controls.codTipoVenda.value)

      if (this.itemInformado == false) {
        switch(this.pedidoForm.controls.codTipoVenda.value) {
          case "3":
            this.pedidoForm.controls.codAgente.enable();
            break;
          default:
            this.pedidoForm.controls.codAgente.disable();
            this.pedidoForm.patchValue({
              codAgente: '',
            });
                }

    }
    /*if (this.pedidoForm.controls.tipoVendaOptions.value == 3){
      this.itemInformado = false;
    }*/
  }


  onSelectAgente(args: any) {
    this.dComissaoMax = args.dComissaoMax
    /*this.pedidoForm.patchValue({
      codAgente: args.code,
    });*/
  }

  onShowMore() {
    //this.page++;
    // this.updateListItem();
  }

  onNew(args: any) {
    this.router.navigate(['pedido/new']);
  }
  closeModal() {
    this.itemPedidoForm.reset();
    this.poModal.close();
    this.p_carrega = true;
    this.atualizaValor = false;
  }

  incluiItem() {
    console.debug("Aciona inclui item")
    if (this.itemPedidoForm.controls.cItCodigo.value == "" || typeof this.itemPedidoForm.controls.cItCodigo.value === "undefined")
    {this.poNotification.error(
      `Informar um item válido.`
    )}
    else if (this.itemPedidoForm.controls.deQuantidade.value <=  '0' || this.itemPedidoForm.controls.deQuantidade.value == "undefined" || typeof this.itemPedidoForm.controls.deQuantidade.value === "undefined")
    {this.poNotification.error(
      `Informar uma quantidade válida.`
    )}
    else if(this.pedidoForm.controls.codAgente.value != "" && ( this.itemPedidoForm.controls.dePercComisAgent.value > this.dComissaoMax || typeof this.itemPedidoForm.controls.dePercComisAgent.value === "undefined" || this.itemPedidoForm.controls.dePercComisAgent.value > this.itemPedidoForm.controls.dePercComissao.value))
    {this.poNotification.error(
      `Informar um percentual de comissão valido, dentro do Limite máximo para o Agente e menor/igual a comissão do Representante informado para o Pedido: ` + this.itemPedidoForm.controls.dePercComissao.value
    )}
    else {this.p_alteraItem = false;
      this.p_carrega = false;
      this.itemPedidoForm.patchValue({
        DtImplant         : this.pedidoForm.controls.dataCriacao.value,
        DtVencto          : this.pedidoForm.controls.dataVencimento.value,
        iCodAgente        : this.pedidoForm.controls.codAgente.value,
        iCodEmitente      : this.pedidoForm.controls.customerCode.value,
        cUsuario          : '',
        iTipoVenda        : this.pedidoForm.controls.codTipoVenda.value,
        iTipoCarregamento : this.pedidoForm.controls.tipoCarregamento.value,
        cCidade           : this.pedidoForm.controls.cidade.value,
        cUf               : this.pedidoForm.controls.estado.value,
        cCodCampanha      : this.pedidoForm.controls.codCampanha.value,
        cCodCultura       : this.pedidoForm.controls.codCultura.value,
        nr_pedido_web     : this.pedidoForm.controls.nr_pedido_web.value,
        cRegiao           : this.pedidoForm.controls.regiao.value,
        precoDecimal      : this.precoDecimal,
        deValorTotal      : '',
        codTipoOperacao   : this.pedidoForm.controls.codTipoOperacao.value,
        localEntrega      : this.pedidoForm.controls.localEntrega.value
         //this.pedidoForm.controls.nr_pedido_web.value
      });
      const parametros: Parametros = { ...this.itemPedidoForm.value };
      console.debug("Salva item")
      this.service.postItem(parametros).subscribe((data)=>{

        console.debug(data)
        console.debug("update list item")
        this.updateListItem();
        console.debug("close.modal")
        this.poModal.close();
        console.debug("reset form")
        this.itemPedidoForm.reset();
        },
        (error) => {
          console.log(error);
          //this.updateListItem();
          this.p_carrega = true;

        });
      }

  }

  incluiPedido() {
    this.p_carrega = false;
    this.itemsFaixa$;
    const pedido: Pedido = { ...this.pedidoForm.value };
    this.service.postPedido(pedido).subscribe((data)=>{
      console.log("success");
      this.p_carrega = true;
      this.router.navigate(['pedido']);
      },
      (error) => {
        console.log(error);
        this.p_carrega = true;
      })
      ;
  }

  alteraItem(item: ItemPedidoWeb) {
    console.debug('altera item')
    console.debug(String(item.dePrecoNetFob).split(".")[1])
    if ( typeof String(item.dePrecoNetFob).split(".")[1] === "undefined" || String(item.dePrecoNetFob).split(".")[1] === null ){
      this.precoDecimal = 2
    }
    else{this.precoDecimal = String(item.dePrecoNetFob).split(".")[1].length;}
    console.debug(this.pedidoForm.controls.codAgente.value )
    if (this.pedidoForm.controls.codAgente.value == "" || typeof this.pedidoForm.controls.codAgente.value === "undefined"){
      this.AgenteInformado = false
      this.dComissaoMax    = '0'
      this.itemPedidoForm.patchValue({
        dePercComisAgent : 0,
      });}
    else{this.AgenteInformado = true}
    this.p_alteraItem = true;
    this.p_itemSelecionado = item.it_codigo;
    this.itemPedidoForm.patchValue({
      DtImplant         : this.pedidoForm.controls.dataCriacao.value,
      DtVencto          : this.pedidoForm.controls.dataVencimento.value ? this.pedidoForm.controls.dataVencimento.value : '',
      DtEntrega         : this.pedidoForm.controls.dataEntrega.value,
      iCodAgente        : this.pedidoForm.controls.codAgente.value,
      iCodEmitente      : this.pedidoForm.controls.customerCode.value,
      cUsuario          : '',
      iTipoVenda        : this.pedidoForm.controls.codTipoVenda.value,
      iTipoCarregamento : this.pedidoForm.controls.tipoCarregamento.value,
      iTipoFrete        : this.pedidoForm.controls.tipoFrete.value,
      cCidade           : this.pedidoForm.controls.cidade.value,
      cUf               : this.pedidoForm.controls.estado.value,
      cCodCampanha      : this.pedidoForm.controls.codCampanha.value,
      cCodCultura       : this.pedidoForm.controls.codCultura.value,
      nr_pedido_web     : this.pedidoForm.controls.nr_pedido_web.value,
      cRegiao           : this.pedidoForm.controls.regiao.value,
      codTipoOperacao   : this.pedidoForm.controls.codTipoOperacao.value,
      localEntrega      : this.pedidoForm.controls.localEntrega.value
       //this.pedidoForm.controls.nr_pedido_web.value
    });

    const parametros: Parametros = { ...this.itemPedidoForm.value };
    this.service.postItemDecimal(parametros).subscribe((data: any)=>{
      console.debug("Apenas atualiza Casa Decimal")
      console.debug( this.precoDecimal)
      //this.precoDecimal = data.qtdeDecimal
      this.itemPedidoForm.patchValue({
        //itemCode         : item.it_codigo,
        cItCodigo        : item.it_codigo,
        desc_item        : item.desc_item,
        dePrecoNetFob    : item.dePrecoNetFob,
        deQuantidade     : item.quantidade,
        deValorFrete     : item.deValorFrete,
        dePercComisAgent : item.dePercComissaoAgt,
        seq_item         : item.seq_item,
        nr_pedido_web    : item.nr_pedido_web,
        cCodEstabel      : '',
        iCodAgente       : item.iCodAgente,
        dePrecoNF        : item.dePrecoNF,
        deComissaoTotal  : item.deComissaoTotal,
        dePercComissao   : item.dePercComissao,

      });
      this.AtualizaFaixa();
      this.carregaValores()
      this.atualizaValor = true;
      this.poModal.open();
    });
  }

  AtualizaFaixa(){
    this.p_carrega = false;
    console.debug('entra AtualizaFaixa')
    console.debug(this.pedidoForm.controls.dataVencimento.value)
    console.debug('antes de executar postItemFaixa')
    this.itemPedidoForm.patchValue({
      DtImplant         : this.pedidoForm.controls.dataCriacao.value,
      DtVencto          : this.pedidoForm.controls.dataVencimento.value,
      iCodAgente        : this.pedidoForm.controls.codAgente.value,
      iCodEmitente      : this.pedidoForm.controls.customerCode.value,
      cUsuario          : '',
      iTipoVenda        : this.pedidoForm.controls.codTipoVenda.value,
      iTipoCarregamento : this.pedidoForm.controls.tipoCarregamento.value,
      cCidade           : this.pedidoForm.controls.cidade.value,
      cUf               : this.pedidoForm.controls.estado.value,
      cCodCampanha      : this.pedidoForm.controls.codCampanha.value,
      cCodCultura       : this.pedidoForm.controls.codCultura.value,
      nr_pedido_web     : this.pedidoForm.controls.nr_pedido_web.value,
      cRegiao           : this.pedidoForm.controls.regiao.value,
      codTipoOperacao   : this.pedidoForm.controls.codTipoOperacao.value,
      localEntrega      : this.pedidoForm.controls.localEntrega.value
       //this.pedidoForm.controls.nr_pedido_web.value
    });
    const parametros: Parametros = { ...this.itemPedidoForm.value };
    console.debug(parametros)
    this.itemsFaixa$ = this.service.postItemFaixa(parametros)
    console.debug(this.itemsFaixa$)
    this.p_carrega = true;

  }
  excluiItem(item: ItemPedidoWeb) {
    this.p_carrega = false;
    this.itemPedidoForm.patchValue({
      seq_item         : item.seq_item,
      nr_pedido_web    : item.nr_pedido_web
    });
    const parametros: Parametros = { ...this.itemPedidoForm.value };
    this.service.excluiItem(parametros).subscribe((data)=>{
      console.log("success");
      this.p_carrega = true;
      this.poNotification.success(`Item ${item.it_codigo} apagado com sucesso.`);
      this.updateListItem();
      });

  }

  private updateListItem() {
    this.total = 0;
    this.totalComissao = 0;
    this.totalMargem = 0;
    this.totalDescontoCamp = 0;
    this.items$ = this.service.getAllItems(this.inr_pedido_web)
    this.p_carrega = false;
    //this.itemInformado = false;
    this.items$.subscribe(
      (result: ItemPedidoWeb[]) => {
        this.total = 0;
        result.forEach((el: ItemPedidoWeb) => {
          this.total +=  el.deValorTotal
          this.totalComissao += el.deComissaoTotal
          this.totalMargem += el.deMbPrazoTotal
          this.totalDescontoCamp += el.deDescontoCamp
          this.itemInformado = true;
        });
        this.p_carrega = true;
      },
      (error) => {
        console.log(error);
        this.p_carrega = true;
      }
    );
  //  this.items$.forEach(total => this.total += Number(this.items$.deValorTotal) )
    //this.total == this.items$.reduce((total) => total + this.items$.deValorTotal)
  }

  onSelectedTipoOperacao(args:any) {
    console.debug("onSelectedTipoOperacao: " + args['codTipoOperacao'])
    ///switch(args['cdn-tip-ped']) {
      switch(args['codTipoOperacao']) {
      case 54:
        this.pedidoForm.controls.dataVencimento.disable();
        break;
      case 56:
        this.pedidoForm.controls.dataVencimento.disable();
        break;
      default:
        this.pedidoForm.controls.dataVencimento.enable();
    }

  }

}
