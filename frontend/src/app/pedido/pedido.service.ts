import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap, pluck } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { LinhaPedido } from './pedido.component';
import {
  Filtros,
  ItemPedidoAPI,
  ItemPedidoWeb,
  ItemsFaixa,
  ItemsPedidoWeb,
  Parametros,
  Pedido,
  PedidoAPI,
  PedidoWeb,
  Rota,
} from './pedido.model';

const endpoint = `${environment.url}/v1/fastapi001`;
const endpointItem = `${environment.url}/v1/fastapi004`;
const endpointPdf = `${environment.url}/v1/fastapi005`;

@Injectable({
  providedIn: 'root',
})
export class PedidoService {
  options: any = {};

  constructor(private http: HttpClient) {
    if (environment.production == false) {
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Basic ' + btoa(environment.auth),
        }),
      };
    }
  }

  getAll(page: number): Observable<any> {
    return this.http.get<PedidoAPI>(`${endpoint}?page=${page}`, this.options);
  }

  putAll(filtros: Filtros): Observable<any> {
    console.dir(filtros);

    return this.http.put<Filtros>(`${endpoint}`, filtros, this.options);
  }

  getPerfil(): Observable<any> {
    return this.http.get(`${endpoint}/perfil`, this.options);
  }

  getSequenciaPedido() {
    return this.http.get(`${endpoint}/sequencia/`, this.options);
  }

  getPedido(nr_pedido_web: string): Observable<Pedido> {
    return this.http.get<Pedido>(`${endpoint}/pedido/${nr_pedido_web}`);
  }

  copyPedido(nr_pedido_web: number) {
    return this.http.get(`${endpoint}/copyPedido/${nr_pedido_web}`);
  }

  getRota(customerCode: string): Observable<Rota> {
    return this.http.get<Rota>(`${endpoint}/rota/${customerCode}`);
  }

  postPedido(pedido: Pedido) {
    console.debug(pedido);
    return this.http.post<Pedido>(`${endpoint}`, pedido, this.options);
  }

  cotacaoPedido(pedido: LinhaPedido) {
    console.debug(pedido);
    return this.http.post<Pedido>(`${endpoint}/cotacao`, pedido, this.options);
  }

  excluiPedido(nr_pedido_web: number) {
    return this.http.delete<Pedido>(
      `${endpoint}/${nr_pedido_web}`,
      this.options
    );
  }

  cancelaPedido(nr_pedido_web: number) {
    return this.http.delete<Pedido>(
      `${endpoint}/cancela/${nr_pedido_web}`,
      this.options
    );
  }

  getAllItems(pedido: number): Observable<ItemsPedidoWeb> {
    return this.http
      .get<ItemPedidoAPI>(`${endpointItem}/${pedido}`, this.options)
      .pipe(pluck('items'));
  }

  postItemFaixa(parametros: Parametros): Observable<ItemsFaixa> {
   return this.http.post<ItemPedidoWeb>(
      `${endpointItem}/faixa`,
      parametros,
      this.options
    ).pipe(pluck('items'));
  }

  postItem(parametros: Parametros) {
    console.debug(parametros);
    return this.http.post<ItemPedidoWeb>(
      `${endpointItem}`,
      parametros,
      this.options
    );
  }

  postItemVencimento(parametros: Parametros) {
    console.debug(parametros);
    return this.http.post<ItemPedidoWeb>(
      `${endpointItem}/vencimento`,
      parametros,
      this.options
    );
  }
  postItemDecimal(parametros: Parametros) {
    console.debug(parametros);
    return this.http.post<ItemPedidoWeb>(
      `${endpointItem}/idecimal`,
      parametros,
      this.options
    );
  }
  postItemValor(parametros: Parametros) {
    console.debug(parametros);
    return this.http.post(
      `${endpointItem}/valor`,
      parametros,
      this.options
    );
  }

  excluiItem(parametros: Parametros) {
    return this.http.delete<ItemPedidoAPI>(
      `${endpointItem}?nr_pedido_web=${parametros.nr_pedido_web}&seq_item=${parametros.seq_item}`,
      this.options
    );
  }

  getSequenciaItem(pedido: number) {
    return this.http.get(`${endpointItem}/sequencia/${pedido}`, this.options);
  }

  efetivaPedido(pedido: number) {
    return this.http.get(`${endpoint}/efetiva/${pedido}`, this.options);
  }

  getPedidoPdf(pedido: number) {
    return this.http.get(`${endpointPdf}/pedidopdf/${pedido}`);
  }

  getItensPedidoPdf(pedido: number) {
    return this.http.get(`${endpointPdf}/${pedido}`);
  }

  getExcel(filtros: Filtros): Observable<any> {
    return this.http.post<Filtros>(`${endpoint}/excel/`, filtros, this.options);
  }
}
