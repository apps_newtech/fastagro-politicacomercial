export interface PedidoAPI {
  total: number;
  hasNext: boolean;
  items: PedidoWeb[];
}

export interface PedidoWeb {
  dat_criacao: Date;
  cliente: Cliente;
  situacao: number;
  situacaoPedido: number;
  statusAprovacao: number;
  statusAprovacaoMLA: number;
  aprovadorMLA: string;
  pedido: Pedido;
  pedidosDatasul: string;
  nr_pedido_web: number;
  _expandables: any[];
  cod_usuario: string;

}

export interface Cliente {
  nome: string;
  nome_abrev: string;
  cgc: string;
}

export interface Rota {
  rotaDetalhada:          string;
  localEntrega:          string;
}


export interface Pedido {
  situacao:          number;
  nr_pedcli:         string;
  pedidosDatasul:    string;
  nr_pedido:         number;
  nr_pedido_web:     string;
  cod_usuario:       string;
  cod_usuario_trans: string;
  nomeUsuario:       string;
  nome_usuario:      string;
  codCampanha:       string;
  codCultura:        string;
  tipoFrete:         string;
  tipoCarregamento:  string;
  dataEntrega:       string;
  dataVencimento:    string;
  codAgente:         string;
  customerCode:      string;
  customerName:      string;
  rotaDetalhada:     string;
  observacao:        string;
  codTipoVenda:      string;
  dataCriacao:       string;
  nomeVendedor:      string;
  Safra:             string;
  regiao:            string;
  cidade:            string;
  estado:            string;
  personalId:        string;
  codTipoOperacao:   string;
  localEntrega:      string;
  shortName:         string;
  stateRegistration:       string;
}

export interface ItemPedidoAPI {
  total: number;
  hasNext: boolean;
  items: ItemPedidoWeb[];
}

export type ItemsPedidoWeb = Array<ItemPedidoWeb>;

export interface ItemPedidoWeb {
  nr_pedido_web               :number;
  seq_item                    :number;
  it_codigo                   :string;
  quantidade                  :number;
  iCodAgente                  :number;
  cCodTabPreco                :string;
  cCorFaixa                   :string;
  dePrecoTabela               :number;
  dePrecoNetFobTarget         :number;
  dePrecoNetFobEF             :number;
  dePrecoEF                   :number;
  dePrecoNF                   :number;
  cCodTabFrete                :string;
  deValorFrete                :number;
  deTaxaFinanceira            :number;
  cOrigem                     :string;
  desc_item                   :string;
  un                          :string;
  deComissaoTotal             :number;
  dePercComissao              :number;
  deComissaoMaxFaixa          :number;
  dePercComissaoAgt           :number;
  deComissaoAgt               :number;
  deComissaoAgtTotal          :number;
  dePercComisAjust            :number;
  dePremioDistrib             :number;
  deComissaoUnit              :number;
  deCusto                     :number;
  dePercMbVista               :number;
  dePercMbPrazo               :number;
  deMbVistaUnit               :number;
  deMbVistaTotal              :number;
  deMbPrazoUnit               :number;
  deMbPrazoTotal              :number;
  dePercMargemVista           :number;
  deMargemVista               :number;
  _expandables                : any[];
  deFinanceiro                :number;
  deValorICMS                 :number;
  deValorPisCofins            :number;
  deQtdeMidas                 :number;
  deFoliar                    :number;
  deQtdeFoliar                :number;
  cCheckFoliares              :string;
  deQtdeUlexita               :number;
  cCheckUlexita               :string;
  deFaixaPreto                 :number;
  deFaixaVermelho              :number;
  deFaixaAmarelo               :number;
  deFaixaVerde                 :number;
  deMinimo                    :number;
  cFaixa                     :string;
  cPercFaixaAcima            :string;
  dePercComisBaseFaixa        :number;
  deDeltaPreco                :number;
  dePercVarPrecoFaixa         :number;
  deDeltaComisMaxFaixa        :number;
  deVarComisFaixa             :number;
  deFormulaBaseFrete           :number;
  dePrecoNetFob               :number;
  deValorTotal                :number;
  precoDecimal                :number;
  cNatOperacao                :string;
  deDescontoCamp              :number;
  deHectares                  :number;


}

export type ItemsFaixa = Array<ItemFaixa>;
export interface ItemFaixa {
  cCorFaixa                   :string;
  dePrecoTabela               :number;
}

export interface Parametros {
    cCodEstabel   :string;
    iCodEmitente  :string;
    cItCodigo:string;
    DtImplant:string;
    DtVencto:string;
    cUsuario:string;
    iCodAgente:string;
    dePrecoNetFob:string;
    deQuantidade:string;
    dePercComisAgent:string;
    iTipoVenda:string;
    iTipoCarregamento:string;
    cCidade:string;
    cUf:string;
    cCodCampanha:string;
    cCodCultura:string;
    nr_pedido_web:string;
    seq_item:string;
    cRegiao: string;
    dePrecoNF: string;
}

export interface Filtros {
  NrPedidoWebIni: string,
  NrPedidoWebFim: string,
  NrPedidoTotvsIni: string,
  NrPedidoTotvsFim: string,
  dataEmissaoIni: string,
  dataEmissaoFim: string,
  dataEntregaIni: string,
  dataEntregaFim: string,
  codClienteIni: string,
  codClienteFim: string,
  StatusPedidoWEBIni: string,
  StatusPedidoWEBFim: string,
  StatusPedidoIni: string,
  StatusPedidoFim: string,
  StatusAprovacaoIni: string,
  StatusAprovacaoFim: string,
  StatusAprovacaoMLAIni: string,
  StatusAprovacaoMLAFim: string,
}


