import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteLookupFilter } from 'src/app/shared/lookup/cliente.lookup';
import { representanteLookupFilter } from 'src/app/shared/lookup/repres.lookup';
import { ItemLookupFilter } from 'src/app/shared/lookup/item.lookup';
import { PoLookupColumn, PoSelectOption } from '@po-ui/ng-components';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {
  PoModalAction,
  PoPageAction,
  PoTableColumn,
  PoTableAction,
  PoPageFilter,
  PoNotificationService,
  PoModalComponent,
  PoComboOption,
  PoAccordionItemComponent,
  PoDialogService,
} from '@po-ui/ng-components';
import { observable, Observable, of } from 'rxjs';
import { ItemPedidoAPI, ItemPedidoWeb, ItemsPedidoWeb, Parametros, Pedido, PedidoWeb } from '../pedido.model';
import { PedidoService } from '../pedido.service';
import { CulturaLookupFilter } from 'src/app/shared/lookup/cultura.lookup';
import { CampanhaLookupFilter } from 'src/app/shared/lookup/campanha.lookup';
import { TipoCargaLookupFilter } from 'src/app/shared/lookup/tipo-carga.lookup';
import { AgenteLookupFilter } from 'src/app/shared/lookup/representante.lookup';
import { map, finalize, switchMap, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { cidadeLookupFilter } from 'src/app/shared/lookup/cidade.lookup';
import { regiaoLookupFilter } from 'src/app/shared/lookup/regiao.lookup';
import { cpf,cnpj } from 'cpf-cnpj-validator';
import { tipoOperacaoLookupFilter } from 'src/app/shared/lookup/tipoOperacao.lookup';
import { entregaLookupFilter } from 'src/app/shared/lookup/entrega.lookup';


@Component({
  selector: 'app-visualizar-pedido',
  templateUrl: './visualizar-pedido.component.html',
  styleUrls: ['./visualizar-pedido.component.scss'],
})

export class VisualizarPedidoComponent implements OnInit {
  customerLookupColumns: PoLookupColumn[] = [
    { label: 'Codigo Cliente', property: 'customerCode', fieldLabel: true },
    { label: 'Nome Abrev', property: 'shortName', fieldLabel: true },
    { label: 'Nome', property: 'customerName', fieldLabel: true },
    { label: 'CNPJ/CPF', property: 'personalId', fieldLabel: true },
  ];

  representanteLookupColumns: PoLookupColumn[] = [
    { label: 'Representante', property: 'cod_rep', fieldLabel: true },
    { label: 'Nome', property: 'nome', fieldLabel: true },
    { label: 'Usuário', property: 'cod_usuario', fieldLabel: true }
  ];

  entregaLookupColumns: PoLookupColumn[] = [
    { label: 'Local Entrega', property: 'localEntrega', fieldLabel: true },
    { label: 'Endereco', property: 'endereco', fieldLabel: true },
    { label: 'Bairro', property: 'bairro', fieldLabel: true },
    { label: 'Cidade', property: 'cidade', fieldLabel: true },
    { label: 'UF', property: 'estado', fieldLabel: true },
    { label: 'CEP', property: 'cep', fieldLabel: true },
  ];

  itemLookupColumns: PoLookupColumn[] = [
    { label: 'Item', property: 'product', fieldLabel: true },
    { label: 'Descrição', property: 'productDescription', fieldLabel: true },
  ];

  culturaLookupColumns: PoLookupColumn[] = [
    { label: 'Código', property: 'cod_cultura', fieldLabel: true },
    { label: 'Descrição', property: 'desc_cultura', fieldLabel: true },
  ];
  cidadeLookupColumns: PoLookupColumn[] = [
    { label: 'Cidade', property: 'cidade', fieldLabel: true },
    { label: 'Estado', property: 'estado', fieldLabel: true },
    { label: 'País', property: 'pais', fieldLabel: true },
  ];
  regiaoLookupColumns: PoLookupColumn[] = [
    { label: 'Região', property: 'nome-ab-reg', fieldLabel: true },
    { label: 'Descrição', property: 'nome-regiao', fieldLabel: true },
  ];

  tipoOperacaoLookupColumns: PoLookupColumn[] = [
    { label: 'Codigo', property: 'codTipoOperacao', fieldLabel: true },
    { label: 'Descrição', property: 'descricao', fieldLabel: true },
  ];
  agenteLookupColumns: PoLookupColumn[] = [
    { label: 'Código', property: 'code', fieldLabel: true },
    { label: 'Nome Abrev', property: 'shortName', fieldLabel: true },
    { label: 'Nome', property: 'name', fieldLabel: true },
  ];

  campanhaLookupColumns: PoLookupColumn[] = [
    { label: 'Código', property: 'cod_campanha', fieldLabel: true },
    { label: 'Estabelecimento', property: 'cod_estabel' },
    { label: 'Início Validade', property: 'data_valid_ini' },
    { label: 'Fim Validade', property: 'data_valid_fim' },
  ];

  tipoCargaLookupColumns: PoLookupColumn[] = [
    { label: 'Código', property: 'cod_tipo_carga', fieldLabel: true },
    { label: 'Descricao', property: 'desc_tipo_carga' },
  ];

  tipoFreteOptions: PoComboOption[] = [
    { label: 'CIF', value: 1 },
    { label: 'FOB', value: 2 },
  ];

  public tipoVendaOptions: Array<PoSelectOption> = [
    { label: 'Direta', value: '1' },
    { label: 'Distribuição', value: '2' },
    { label: 'Agenciamento', value: '3' }
  ];

  private disclaimers = [];

  tableColumns: PoTableColumn[] = [
    { label: 'Seq', property: 'seq_item' },
    { label: 'Cod. Produto', property: 'it_codigo' },
    { label: 'Faixa de Cor',  property: 'cCorFaixa',
    type: 'label',
    labels: [
      {
        value: 'Vermelho',
        label: 'Vermelho',
        color: 'color-07',
        tooltip: 'Vermelho'
      },
      {
        value: 'Amarelo',
        label: 'Amarelo',
        color: 'color-06',
        tooltip: 'Amarelo'
      },
      {
        value: 'Azul',
        label: 'Azul',
        color: 'color-03',
        tooltip: 'Azul'
      },
      {
        value: 'Verde',
        label: 'Verde',
        color: 'color-09',
        tooltip: 'Verde'
      },
      {
        value: 'Preto',
        label: 'Preto',
        color: 'color-05',
        tooltip: 'Preto'
      },
     ],
    },
    { label: 'Quantidade', property: 'quantidade' },
    { label: 'Hectares', property: 'deHectares'},
    { label: 'UN', property: 'un' },
    { label: 'Descrição', property: 'desc_item' },
    { label: 'Nat Oper', property: 'cNatOperacao'},
    { label: 'Origem', property: 'cOrigem' },
    { label: 'Preço NET/FOB Target', property: 'dePrecoNetFobTarget', type: 'number', format: '1.2-5'},
    { label: 'Frete R$/Kg', property: 'deValorFrete', type: 'number' , format: '1.2-5'},
    { label: 'Preço NET/FOB (R$/Kg.L)', property: 'dePrecoNetFob'},
    { label: 'Preço NET/FOB C/ EF', property: 'dePrecoNetFobEF', type: 'number', format: '1.2-5' },
    { label: 'Valor EF', property: 'dePrecoEF', type: 'number', format: '1.2-5', visible: false },
    { label: 'Preço NF', property: 'dePrecoNF', type: 'number', format: '1.2-5' },
    { label: 'Valor Total', property: 'deValorTotal', type: 'number', format: '1.2-2' },
    { label: 'Desconto Campanha', property: 'deDescontoCamp', type: 'number', format: '1.2-5' },

  ];
  comissaoColumns: PoTableColumn[] = [
    { label: 'Seq', property: 'seq_item' },
    { label: 'Cod. Produto', property: 'it_codigo' },
    { label: 'Descrição', property: 'desc_item' },
    { label: 'Comissão Total(R$)', property: 'deComissaoTotal', type: 'number', format: '1.2-2' },
    { label: 'Comissão %', property: 'dePercComissao' },
    { label: 'Preço NET/FOB com EF', property: 'dePrecoNetFobEF', type: 'number', format: '1.2-5' },
    { label: 'Comissão MAX Faixa', property: 'deComissaoMaxFaixa' },
    { label: '% Agente', property: 'dePercComissaoAgt' , type: 'number', format: '1.2-2' },
    { label: 'Comissão Agente(R$)', property: 'deComissaoAgt', type: 'number', format: '1.2-2' },
    { label: 'Comissao Agt Total(R$)', property: 'deComissaoAgtTotal', type: 'number', format: '1.2-2' },
  ];
  gerencialColumns: PoTableColumn[] = [
    { label: 'Cod. Produto', property: 'it_codigo' },
    { label: 'Descrição', property: 'desc_item' },
    { label: 'Preço NET/FOB com EF', property: 'dePrecoNetFobEF', type: 'number', format: '1.2-5' },
    { label: 'Comissão Ajustada', property: 'dePercComisAjust' },
    { label: 'Prêmio Distribuição', property: 'dePremioDistrib', type: 'number', format: '1.2-5' },
    { label: 'Comissão(R$/Unid)', property: 'deComissaoUnit', type: 'number', format: '1.2-2' },
    { label: 'Custo', property: 'deCusto', type: 'number', format: '1.1-3' },
    { label: 'Comissão Total(R$)', property: 'deComissaoTotal', type: 'number', format: '1.2-2' },
    { label: 'Comissão %', property: 'dePercComissao' },
    { label: '% Agente', property: 'dePercComissaoAgt' , type: 'number', format: '1.2-2' },
    { label: 'Comissão Agente(R$)', property: 'deComissaoAgt', type: 'number', format: '1.2-2' },
    { label: 'Comissao Agt Total(R$)', property: 'deComissaoAgtTotal', type: 'number', format: '1.2-2' },
    { label: 'MB à Vista %', property: 'dePercMbVista',type: 'number', format: '1.2-2' },
    { label: 'MB à Prazo %', property: 'dePercMbPrazo', type: 'number', format: '1.2-2' },
    { label: 'MB à Vista(R$/Unid)', property: 'deMbVistaUnit', type: 'number', format: '1.2-2' },
    { label: 'Total MB à Vista R$', property: 'deMbVistaTotal', type: 'number', format: '1.2-2' },
    { label: 'MB à Prazo(R$/Unid)', property: 'deMbPrazoUnit', type: 'number', format: '1.2-2' },
    { label: 'Total MB à Prazo R$', property: 'deMbPrazoTotal', type: 'number', format: '1.2-2' },
    { label: 'Margem Comissão % à vista', property: 'dePercMargemVista' },
    { label: 'Margem Comissão R$ à vista', property: 'deMargemVista' },
    { label: 'Financeiro por Produto', property: 'deFinanceiro' },
    { label: 'ICMS', property: 'deValorICMS' },
    { label: 'PIS/COFINS', property: 'deValorPisCofins' },
    { label: 'Qtde Midas', property: 'deQtdeMidas' },
   // { label: 'Foliar', property: 'deFoliar' },
    //{ label: 'Qtde Foliares', property: 'deQtdeFoliar' },
   // { label: 'Check Foliares', property: 'cCheckFoliares' },
    //{ label: 'Qtde ULEXITA', property: 'deQtdeUlexita' },
  //  { label: 'Check ULEXITA', property: 'cCheckUlexita' },
   // { label: 'F Preto', property: 'deFaixaPreto' },
  //  { label: 'F Vermelho', property: 'deFaixaVermelho' },
  //  { label: 'F Amarelo', property: 'deFaixaAmarelo' },
  //  { label: 'F Verde', property: 'deFaixaVerde' },
  //  { label: 'Mínimo', property: 'deMinimo' },
    { label: 'Faixa', property: 'cFaixa',
    type: 'label',
    labels: [
      {
        value: 'Vermelho',
        label: 'Vermelho',
        color: 'color-07',
        tooltip: 'Vermelho'
      },
      {
        value: 'Amarelo',
        label: 'Amarelo',
        color: 'color-06',
        tooltip: 'Amarelo'
      },
      {
        value: 'Azul',
        label: 'Azul',
        color: 'color-03',
        tooltip: 'Azul'
      },
      {
        value: 'Verde',
        label: 'Verde',
        color: 'color-09',
        tooltip: 'Verde'
      },
      {
        value: 'Preto',
        label: 'Preto',
        color: 'color-05',
        tooltip: 'Preto'
      },
     ],
    },
  //  { label: 'Faixa Acima %', property: 'cPercFaixaAcima' },
    { label: 'Comissão Base Faixa %', property: 'dePercComisBaseFaixa' },
   // { label: 'Delta Preço Faixa R$', property: 'deDeltaPreco' },
   // { label: 'Var % Preço Faixa', property: 'dePercVarPrecoFaixa' },
    //{ label: 'Delta Comis. Máx. Faixa', property: 'deDeltaComisMaxFaixa' },
  //  { label: 'Var Comis. Faixa', property: 'deVarComisFaixa' },
   // { label: 'Formula Base p/ Frete', property: 'deFormulaBaseFrete' },
  ];

  customerData: any = {};
  dataItem: ItemPedidoWeb[] = [];
  items$!: Observable<ItemsPedidoWeb>;
  iseq_item$!: Observable<any>;
  hasMore$!: Observable<boolean>;
  show!:   boolean;
  btSalvar!: string;
  titulo!: string;
  altera = false;
  inr_pedido_web!: number;
  p_carrega!: boolean;
  total: number = 0;
  totalComissao: number = 0;
  totalMargem: number = 0;
  totalDescontoCamp: number = 0;
  nextLabelWidget: string = 'Avançar';
  nextLabelWidgetComissao: string = 'Avançar';
  nextLabelWidgetGerencial: string = 'Salvar'
  previousLabelWidget: string = 'Voltar';
  ClienteCadastrado!: boolean;
  cpfcnpjMask = "";
  ErroCPFCNPJ!: boolean;
  shortName!: string;
  customerCode!: number;



  pedidoForm!: FormGroup;
  itemPedidoForm!: FormGroup;



  @ViewChild(PoModalComponent, { static: true })
  poModal!: PoModalComponent;

  @ViewChild('optionsForm', { static: true }) form!: NgForm;

  culturaFormat(value: any) {
    return `${value.cod_cultura} - ${value.desc_cultura}`;
  }

  tipoCargaFormat(value: any) {
    return `${value.cod_tipo_carga} - ${value.desc_tipo_carga}`;
  }

  agenteFormat(value: any) {
    return `${value.code} - ${value.name}`;
  }
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private service: PedidoService,
    private formBuilder: FormBuilder,
    private poNotification: PoNotificationService,
    public customerLookup: ClienteLookupFilter,
    public representanteLookup: representanteLookupFilter,
    public entregaLookup: entregaLookupFilter,
    public culturaLookup: CulturaLookupFilter,
    public cidadeLookup: cidadeLookupFilter,
    public regiaoLookup: regiaoLookupFilter,
    private poAlert: PoDialogService,
    public tipoOperacaoLookup: tipoOperacaoLookupFilter,
    public campanhaLookup: CampanhaLookupFilter,
    public tipoCargaLookup: TipoCargaLookupFilter,
    public itemLookup: ItemLookupFilter,
    public agenteLookup: AgenteLookupFilter,
  ) {}

  ngOnInit(): void {
    //this.altera = true;
    this.ClienteCadastrado = true;
    this.p_carrega = false;
    this.btSalvar = "Avançar";
    this.show = true;
    this.atualizaPerfil();
    this.configForm();
    this.configFormItem();
    this.updateListItem();

  }



  private configForm() {
    this.pedidoForm = this.formBuilder.group({
      customerCode: ['', [Validators.required, Validators.maxLength(12)]],
      personalId: [''],
      customerName: [''],
      email: [''],
      phone1: [''],
      city: [''],
      cidade: [''],
      estado: [''],
      bairro: [''],
      localEntrega: [''],
      cep: [''],
      endereco: [''],
      state: [''],
      stateRegistration: [''],
      codCampanha: [''],
      codCultura: ['', [Validators.required]],
      tipoFrete: ['', [Validators.required]],
      tipoCarregamento: ['', [Validators.required]],
      dataEntrega: ['', [Validators.required]],
      dataVencimento: ['', [Validators.required]],
      codAgente: [''],
      nr_pedido_web: [''],
      cod_rep: [''],
      cod_usuario_trans: [''],
      nomeUsuario: [''],
      nome_usuario: [''],
      rotaDetalhada: [''],
      observacao: [''],
      codTipoVenda: ['', [Validators.required]],
      nomeVendedor: [''],
      dataCriacao: [''],
      completeAddress: [''],
      Safra: [''],
      regiao: [''],
      ClienteCadastrado: ['true'],
      codTipoOperacao: ['']
    });

    this.activatedRoute.params
      .pipe(
        switchMap((param) => {
          const { nr_pedido_web } = param;
          if (nr_pedido_web) {
            this.altera = true;
            this.inr_pedido_web = nr_pedido_web;
            return this.service.getPedido(nr_pedido_web);
          }
          return of({} as Pedido);
        })
      )
      .subscribe((pedido) => {
        if (this.altera) {
          let novoCliente = false;
          if (pedido.customerCode == "" && pedido.customerName || ""){
            novoCliente = true;
            console.debug(this.ClienteCadastrado)
          }

          this.p_carrega = true;
          let jsonPedido: any;
          this.titulo = `Visualizando Pedido Nr: ${pedido.nr_pedido_web}`;
          jsonPedido = pedido;
          console.debug(pedido.localEntrega)
          console.debug(pedido)
          this.shortName = pedido.shortName
          this.customerCode = Number(pedido.customerCode)
          this.entregaLookup.setShortName(this.shortName);
          //this.pedidoForm.patchValue(pedido);
          this.pedidoForm.patchValue({
            customerCode: pedido.customerCode,
            customerName: pedido.customerName,
            localEntrega: pedido.localEntrega,
            codTipoOperacao: pedido.codTipoOperacao,
            personalId: pedido.personalId,
            codCampanha: pedido.codCampanha,
            codCultura: pedido.codCultura,
            tipoFrete: pedido.tipoFrete,
            tipoCarregamento: pedido.tipoCarregamento,
            dataEntrega: pedido.dataEntrega,
            dataVencimento: pedido.dataVencimento,
            codAgente: pedido.codAgente,
            nr_pedido_web: pedido.nr_pedido_web,
            cod_rep: pedido.cod_usuario,
            nome_usuario: pedido.nome_usuario,
            nomeUsuario: pedido.nome_usuario,
            cod_usuario_trans: pedido.cod_usuario_trans,
            rotaDetalhada: pedido.rotaDetalhada,
            observacao: pedido.observacao,
            codTipoVenda: pedido.codTipoVenda,
            nomeVendedor: pedido.nomeVendedor,
            dataCriacao: pedido.dataCriacao,
            Safra:        pedido.Safra,
            regiao:       pedido.regiao,
            cidade:       pedido.cidade,
            estado:       pedido.estado,
            stateRegistration: pedido.stateRegistration


          });
        } else {

          this.service.getSequenciaPedido().subscribe((sequencia) => {
          let iseq_pedido: any;
          iseq_pedido = sequencia;
          this.p_carrega = true;
          this.titulo = `Novo Pedido Nr: ${iseq_pedido['nr_pedido_web']}`;
          this.inr_pedido_web = iseq_pedido['nr_pedido_web'];
          this.pedidoForm.patchValue({
            nr_pedido_web: iseq_pedido['nr_pedido_web'],
            nomeVendedor:  iseq_pedido['nomeVendedor'],
            cod_rep:   iseq_pedido['cod_usuario'],
            nomeUsuario: iseq_pedido['nome_usuario'],
            dataCriacao:   iseq_pedido['dataCriacao'],
            regiao:        iseq_pedido['regiao'],
            cod_usuario_trans: iseq_pedido['cod_usuario_trans']
          })
          this.p_carrega = true;
              this.itemPedidoForm.patchValue({
               nr_pedido_web: iseq_pedido['nr_pedido_web'],
              });
          });
         }
      });
  }


  private atualizaPerfil() {
    if (environment.perfil == "RTV") {
      this.show = false;
      this.nextLabelWidgetComissao = 'Salvar';
    }
   /*if (environment.perfil || "RTV"){this.show = true
      console.debug(`perfil que pode visualizar comissao ${environment.perfil}`);
      this.btSalvar = 'Avançar';} */

  }

  private configFormItem() {
    this.itemPedidoForm = this.formBuilder.group({
        itemCode          : [''],
        cCodEstabel       : [''],
        iCodEmitente      : [''],
        cItCodigo         : [''],
        desc_item         : [''],
        DtImplant         : [''],
        DtVencto          : [''],
        cUsuario          : [''],
        iCodAgente        : [''],
        dePrecoNetFob     : ['0,00000'],
        deQuantidade      : ['0'],
        deValorFrete      : ['0,00000'],
        dePercComisAgent  : [''],
        iTipoVenda        : [''],
        iTipoCarregamento : [''],
        cCidade           : [''],
        cUf               : [''],
        cCodCampanha      : [''],
        cCodCultura       : [''],
        nr_pedido_web     : [''],
        seq_item          : [''],
        deValorTotal      : [''],
        cRegiao            : ['']
    });

    //this.poModal.open();
  }



  public readonly filterSettings: PoPageFilter = {
    //action: this.filterAction.bind(this),
    // advancedAction: this.advancedFilterActionModal.bind(this),
    placeholder: 'Pesquisar',
  };


  changeMask(event: string) {
    if (cpf.isValid(event)){
      this.ErroCPFCNPJ = false;
      this.pedidoForm.patchValue({
        personalId: cpf.format(event),
      });

     } else if(cnpj.isValid(event)) {
      this.ErroCPFCNPJ = false;
       this.pedidoForm.patchValue({
        personalId: cnpj.format(event),
      });
    }
    else {this.ErroCPFCNPJ = true;
      this.cpfcnpjMask= ""}
  }

  voltar() {
    this.router.navigate(['pedido']);
  }


  onSelectCustomer(args: any) {
    console.debug(args.shortName)
    this.entregaLookup.setShortName(args.shortName);
    this.pedidoForm.patchValue({
      personalId: args.personalId,
      customerName: args.customerName,
      shortName: args.shortName,
      stateRegistration: args.stateRegistration,
      email: args.email,
      phone1: args.phone1,
      city: args.city,
      state: args.state,
      completeAddress: args.completeAddress,
      regiao: args.shortRegion

    });
    this.shortName = args.shortName}

  onSelectRepresentante(args: any) {
    this.p_carrega = false;
    console.debug(args.cod_rep)
    this.pedidoForm.patchValue({
      nomeVendedor: args.nome,
      regiao: args.nome_ab_reg,
    });
    this.p_carrega = true;
  }

  onSelectEntrega(args: any) {
    console.debug(`selecEntrega ${this.shortName}`)
    this.entregaLookup.setShortName(this.shortName);
    this.pedidoForm.patchValue({
      endereco: args.endereco,
      estado: args.estado,
      cidade: args.cidade,
      cep: args.cep,
      bairro: args.bairro,
      stateRegistration: args['ins-estadual']
    });
  }

  onSelectItem(args: any) {
    this.itemPedidoForm.patchValue({
      desc_item: args.productDescription,
    });
  }

  onSelectCultura(args: any) {
    this.campanhaLookup.setCodCultura(args['cod_cultura']);
  }

  onSelectCidade(args: any) {
    /*this.pedidoForm.patchValue({
      estado: args.estado,
    });*/
  }


  onSelectAgente(args: any) {
    /*this.pedidoForm.patchValue({
      codAgente: args.code,
    });*/
  }

  onShowMore() {
    //this.page++;
    // this.updateListItem();
  }


  private updateListItem() {
    this.total = 0;
    this.totalComissao = 0;
    this.totalMargem   = 0;
    this.totalDescontoCamp = 0;
    this.items$ = this.service.getAllItems(this.inr_pedido_web)
    this.p_carrega = true;
    this.items$.subscribe(
      (result: ItemPedidoWeb[]) => {
        this.total = 0;
        result.forEach((el: ItemPedidoWeb) => {
          this.total +=  el.deValorTotal
          this.totalComissao += el.deComissaoTotal
          this.totalMargem += (el.deMbPrazoTotal)
          this.totalDescontoCamp += el.deDescontoCamp
        });
        this.p_carrega = true;
      },
      (error) => {
        console.log(error);
        this.p_carrega = true;
      }
    );
  //  this.items$.forEach(total => this.total += Number(this.items$.deValorTotal) )
    //this.total == this.items$.reduce((total) => total + this.items$.deValorTotal)
  }


}
