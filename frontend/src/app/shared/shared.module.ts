import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoTemplatesModule } from '@po-ui/ng-templates';
import { PoModule } from '@po-ui/ng-components';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArchwizardModule } from 'angular-archwizard';

@NgModule({
  imports: [
    CommonModule,
    PoModule,
    PoTemplatesModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ArchwizardModule,
  ],
  exports: [
    PoModule,
    PoTemplatesModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ArchwizardModule,
  ],
})
export class SharedModule {}
