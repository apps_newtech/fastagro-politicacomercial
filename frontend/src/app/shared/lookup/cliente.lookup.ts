import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { from, Observable } from 'rxjs';

import {
  PoLookupFilter,
  PoLookupFilteredItemsParams,
  PoLookupResponseApi,
} from '@po-ui/ng-components';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ClienteLookupFilter implements PoLookupFilter {
  private url = '/dts/datasul-rest/resources/prg/esp/v1/customers';

  constructor(private httpClient: HttpClient) {}

  getFilteredItems?(
    filteredParams: PoLookupFilteredItemsParams
  ): Observable<any> {
    const {
      filterParams,
      advancedFilters,
      ...restFilteredItemsParams
    } = filteredParams;
    const params = {
      ...restFilteredItemsParams,
      ...filterParams,
      ...advancedFilters,
    };

    params.quickSearch = params.filter;

    return this.httpClient.get(this.url, { params });
  }
  getObjectByValue(value: string, filterParams?: any): Observable<any> {
    return this.httpClient.get(`${this.url}/${value}`);
  }
}
