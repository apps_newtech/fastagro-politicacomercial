import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  PoComboFilter,
  PoComboOption,
  PoLookupFilter,
  PoLookupFilteredItemsParams,
} from '@po-ui/ng-components';
import { interval, Observable } from 'rxjs';
import { delay, map, pluck, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class regiaoLookupFilter implements PoLookupFilter {
  private url = '/dts/datasul-rest/resources/prg/esp/v1/fastapi007/regiao';

  constructor(private httpClient: HttpClient) {}

  getFilteredItems?(
    filteredParams: PoLookupFilteredItemsParams
  ): Observable<any> {
    const {
      filterParams,
      advancedFilters,
      ...restFilteredItemsParams
    } = filteredParams;
    const params = {
      ...restFilteredItemsParams,
      ...filterParams,
      ...advancedFilters,
    };

    params.quickSearch = params.filter;

    return this.httpClient.get(this.url, { params });
  }
  getObjectByValue(value: string, filterParams?: any): Observable<any> {
    return this.httpClient.get(`${this.url}/${value}`);
  }
}
