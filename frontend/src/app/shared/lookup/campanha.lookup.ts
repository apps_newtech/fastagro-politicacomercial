import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  PoLookupFilter,
  PoLookupFilteredItemsParams,
} from '@po-ui/ng-components';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CampanhaLookupFilter implements PoLookupFilter {
  private url = '/dts/datasul-rest/resources/prg/esp/v1/fastapi003';

  codCultura!: number;

  constructor(private httpClient: HttpClient) {}

  setCodCultura(codigo: number) {
    this.codCultura = codigo;
  }

  getFilteredItems?(
    filteredParams: PoLookupFilteredItemsParams
  ): Observable<any> {
    const {
      filterParams,
      advancedFilters,
      ...restFilteredItemsParams
    } = filteredParams;
    const params = {
      ...restFilteredItemsParams,
      ...filterParams,
      ...advancedFilters,
    };

    if (this.codCultura) {
      params.codCultura = this.codCultura;
    }

    return this.httpClient.get(this.url, { params });
  }
  getObjectByValue(value: string, filterParams?: any): Observable<any> {
    return this.httpClient.get(`${this.url}/${value}`);
  }
}
