import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  PoComboFilter,
  PoComboOption,
  PoLookupFilter,
  PoLookupFilteredItemsParams,
} from '@po-ui/ng-components';
import { interval, Observable } from 'rxjs';
import { delay, map, pluck, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class entregaLookupFilter implements PoLookupFilter {
  private url = '/dts/datasul-rest/resources/prg/esp/v1/fastapi007/entrega';
  shortName!: string;
  constructor(private httpClient: HttpClient) {}

  setShortName(nome: string) {
    console.debug('setShortName')
    this.shortName = nome;
  }

  getFilteredItems?(
    filteredParams: PoLookupFilteredItemsParams
  ): Observable<any> {
    const {
      filterParams,
      advancedFilters,
      ...restFilteredItemsParams
    } = filteredParams;
    const params = {
      ...restFilteredItemsParams,
      ...filterParams,
      ...advancedFilters,
    };

    params.quickSearch = params.filter;

    return this.httpClient.get(`${this.url}/${this.shortName}`, { params });
  }
  getObjectByValue(value: string, filterParams?: any): Observable<any> {
    console.debug(`${this.url}/${this.shortName}/${value}`)
    return this.httpClient.get(`${this.url}/${this.shortName}/${value}`);
  }
}
