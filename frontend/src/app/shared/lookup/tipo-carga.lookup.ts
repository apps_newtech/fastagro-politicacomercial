import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  PoComboFilter,
  PoComboOption,
  PoLookupFilter,
  PoLookupFilteredItemsParams,
} from '@po-ui/ng-components';
import { interval, Observable } from 'rxjs';
import { delay, map, pluck, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TipoCargaLookupFilter implements PoLookupFilter, PoComboFilter {
  private url = '/dts/datasul-rest/resources/prg/esp/v1/fastapi006';

  constructor(private httpClient: HttpClient) {}

  getFilteredData(
    params: any,
    filterParams?: any
  ): Observable<PoComboOption[]> {
    return this.httpClient.get(this.url, { params }).pipe(
      map((data: any) => data.items),
      tap((r) => console.dir(r)),
      map((items: any) =>
        items.map((item: any) => {
          const label = item['desc_tipo_carga'];
          const value = item['cod_tipo_carga'];
          return { label, value };
        })
      ),
      tap((r) => console.dir(r))
    );
  }

  getFilteredItems?(
    filteredParams: PoLookupFilteredItemsParams
  ): Observable<any> {
    const {
      filterParams,
      advancedFilters,
      ...restFilteredItemsParams
    } = filteredParams;
    const params = {
      ...restFilteredItemsParams,
      ...filterParams,
      ...advancedFilters,
    };

    return this.httpClient.get(this.url, { params });
  }
  getObjectByValue(value: string, filterParams?: any): Observable<any> {
    return this.httpClient.get(`${this.url}/${value}`);
  }
}
