import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';


const routes: Routes = [
  {  path:'',    
     component: HomeComponent,
     children: [
       {
         path: 'pedido',
         loadChildren: () =>
         import('../pedido/pedido.module').then(
           (file) => file.PedidoModule
         )         
       },
       {
        path: 'browser',
        loadChildren: () =>
        import('../browser/browser.module').then(
          (file) => file.BrowserModule
        )         
      },
      {
        path: 'filtroexportacao',
        loadChildren: () =>
        import('../filtroexportacao/filtroexportacao.module').then(
          (file) => file.FiltroexportacaoModule
        )         
      },
      {
        path: 'pedidoexcel',
        loadChildren: () =>
        import('../pedidoexcel/pedidoexcel.module').then(
          (file) => file.PedidoexcelModule
        )         
      },
      {
        path: 'peditens',
        loadChildren: () =>
        import('../peditens/peditens.module').then(
          (file) => file.PeditensModule
        )         
      }
     ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
