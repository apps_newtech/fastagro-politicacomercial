import { Component, OnInit } from '@angular/core';
import { PoMenuItem }  from '@po-ui/ng-components';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  menu: Array<PoMenuItem> = [];
  
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.menu = this.getMenu();
  }
  

  getMenu(): Array<PoMenuItem> {
    return [
      {
        label: 'Pedido',
        link: '/home/pedido',
      },      
      {
        label: 'Pedido Venda',        
        //icon: 'po-icon-share',
        //shortLabel: 'Links',
        subItems: [
          { label: 'Itens',     link: '/home/peditens'},                 
          { label: 'Pesquisar', link: '/home/browser'},             
        ]
      },
      {
        label: 'Filtro Exportação',
        link: '/home/filtroexportacao',
      },      
      {
        label: 'Exporta Excel',
        link: '/home/pedidoexcel',
      },      
    ];
  }




/*
  readonly menus: Array<PoMenuItem> = [
    { label: 'Home',            action: this.onClick.bind(this) },
    { label: 'Pedido de Venda', action: this.pagePedido.bind(this) },
    { label: 'Pesquisa',        action: this.onClick.bind(this) },
    { label: 'Exporta Excel',   action: this.onClick.bind(this) },
  ];*/

  public onClick() {
    alert('Clicked in menu item2')
  } 

  public pagePedido() {
    this.router.navigate(['pedido'])
  } 
  public pageBrowser() {
    this.router.navigate(['browser'])
  } 
  public pagrfiltro() {
    this.router.navigate(['itens'])
  } 


}
