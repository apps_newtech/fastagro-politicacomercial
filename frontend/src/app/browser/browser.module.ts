import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowserRoutingModule } from './browser-routing.module';
import { BrowserComponent } from './browser.component';
import { PoFieldModule, PoContainerModule,
         PoButtonModule, PoPageModule  } from '@po-ui/ng-components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [BrowserComponent],
  imports: [
    CommonModule,
    BrowserRoutingModule,
    PoFieldModule,
    FormsModule,
    ReactiveFormsModule,
    PoContainerModule,
    PoButtonModule,
    PoPageModule
    
  ],
  exports: [BrowserComponent]
})
export class BrowserModule { }
