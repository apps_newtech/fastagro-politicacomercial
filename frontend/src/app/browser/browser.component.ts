import { Component, OnInit } from '@angular/core';
import {  PoBreadcrumb,  PoPageAction} from '@po-ui/ng-components';


@Component({
  selector: 'app-browser',
  templateUrl: './browser.component.html',
  styleUrls: ['./browser.component.scss']
})
export class BrowserComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public onClick() {
    alert('Clicked in menu item2')
  } 

  breadcrumb: PoBreadcrumb = {
    items: [{ label: 'Pedido', link: '/pedido' }, { label: 'Pesquisa' }],
  };  
}


