import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'pedido' },
  /*{
    path: 'home',
    loadChildren: () =>
      import('../app/home/home.module').then((m) => m.HomeModule),
  },*/
  {
    path: 'pedido',
    loadChildren: () =>
      import('../app/pedido/pedido.module').then((m) => m.PedidoModule),
  }/*,
  {
    path: 'browser',
    loadChildren: () =>
      import('../app/browser/browser.module').then((m) => m.BrowserModule),
  },
  {
    path: 'pedidoitens',
    loadChildren: () =>
      import('../app/peditens/peditens.module').then((m) => m.PeditensModule),
  },
  {
    path: 'filtroexportacao',
    loadChildren: () =>
      import('../app/filtroexportacao/filtroexportacao.module').then((m) => m.FiltroexportacaoModule),
  },*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
