import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PoTemplatesModule } from '@po-ui/ng-templates';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PoTemplatesModule,
    //    PedidoModule
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: window.location.pathname,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
