ADD TABLE "es_politica_tabela_preco_item"
  AREA "Schema Area"
  LABEL "Itens da tabela de preco."
  DESCRIPTION "Itens da tabela de preco."
  DUMP-NAME "es_politica_tabela_preco_item"

ADD FIELD "it_codigo" OF "es_politica_tabela_preco_item" AS character 
  FORMAT "x(16)"
  INITIAL ""
  LABEL "Item"
  POSITION 2
  MAX-WIDTH 32
  COLUMN-LABEL "Item"
  ORDER 50

ADD FIELD "cod_tab_preco" OF "es_politica_tabela_preco_item" AS character 
  FORMAT "x(8)"
  INITIAL ""
  LABEL "Cod. Tabela"
  POSITION 3
  MAX-WIDTH 16
  COLUMN-LABEL "Cod.Tabela"
  ORDER 10

ADD FIELD "cod_estabel" OF "es_politica_tabela_preco_item" AS character 
  FORMAT "x(6)"
  INITIAL ""
  LABEL "Estab"
  POSITION 4
  MAX-WIDTH 12
  COLUMN-LABEL "Estab"
  ORDER 20

ADD FIELD "data_valid_ini" OF "es_politica_tabela_preco_item" AS date 
  DESCRIPTION "Data inicial de validade da tabela de preco."
  FORMAT "99/99/9999"
  INITIAL ?
  LABEL "Dt.Inicial"
  POSITION 5
  MAX-WIDTH 4
  COLUMN-LABEL "Dt.Inicial"
  HELP "Data inicial de validade da tabela de preco."
  ORDER 30

ADD FIELD "data_valid_fim" OF "es_politica_tabela_preco_item" AS date 
  DESCRIPTION "Data final de validade da tabela de preco."
  FORMAT "99/99/9999"
  INITIAL ?
  LABEL "Dt.Final"
  POSITION 6
  MAX-WIDTH 4
  COLUMN-LABEL "Dt.Final"
  HELP "Data final de validade da tabela de preco."
  ORDER 40

ADD FIELD "cod_faixa" OF "es_politica_tabela_preco_item" AS character 
  FORMAT "x(8)"
  INITIAL ""
  LABEL "Cod. Faixa"
  POSITION 7
  MAX-WIDTH 16
  COLUMN-LABEL "Cod.Faixa"
  ORDER 60

ADD FIELD "perc_margem" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Campo para informar a margem bruta em percentual para venda direta/distribuidor, ser�o valores informados."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "Margem"
  POSITION 8
  MAX-WIDTH 17
  COLUMN-LABEL "Margem"
  HELP "Margem bruta em percentual para venda direta/distribuidor"
  DECIMALS 2
  ORDER 70

ADD FIELD "perc_desconto" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Campo desconto de venda que sera informado e servir� como base para o calculo do pre�o final."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "Desconto"
  POSITION 9
  MAX-WIDTH 17
  COLUMN-LABEL "Desc."
  HELP "Desconto de venda"
  DECIMALS 2
  ORDER 80

ADD FIELD "perc_premio" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Campo premio que sera informado e servir� como base para o calculo do pre�o final."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "Premio"
  POSITION 10
  MAX-WIDTH 17
  COLUMN-LABEL "Premio"
  HELP "Premio distribui��o"
  DECIMALS 2
  ORDER 90

ADD FIELD "perc_comissao" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Percentual de comiss�o por vendedor, valor de comiss�o padr�o da faixa de pre�o."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "Comissao"
  POSITION 11
  MAX-WIDTH 17
  COLUMN-LABEL "Comissao"
  HELP "Percentual de comiss�o por vendedor."
  DECIMALS 2
  ORDER 100

ADD FIELD "perc_comissao_max" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Percentual de comiss�o m�xima para faixa, o percentual m�ximo n�o pode ser maior que o percentual m�nimo da pr�xima faixa."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "Comissao"
  POSITION 12
  MAX-WIDTH 17
  COLUMN-LABEL "Comissao"
  HELP "Percentual de comiss�o m�xima para faixa."
  DECIMALS 2
  ORDER 110

ADD FIELD "perc_comissao_min" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Percentual de comiss�o m�nima para faixa."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "Comissao"
  POSITION 13
  MAX-WIDTH 17
  COLUMN-LABEL "Comissao"
  HELP "Percentual de comiss�o m�nima para faixa."
  DECIMALS 2
  ORDER 120

ADD FIELD "preco_netfob_venda_direta" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Pre�o NET/FOB para venda direta, formula:
Pre�o NET/FOB Direta = ( (valor de custo do produto + valor de desconto direta * Compensa��o do premio) / 1-Margem bruta direta))"
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "NET/FOB V.Direta"
  POSITION 14
  MAX-WIDTH 17
  COLUMN-LABEL "NET/FOB V.Direta"
  HELP "Pre�o NET/FOB para venda direta"
  DECIMALS 2
  ORDER 130

ADD FIELD "preco_netfob_venda_distrib" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Pre�o NET/FOB para venda distribuidor, formula:
Pre�o NET/FOB Distribuidor = ( (valor de custo do produto + % premia��o distribuidor da empresa * Pr�mio de distribui��o) / 1-Margem bruta distribuidor))"
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "NET/FOB V.Dist."
  POSITION 15
  MAX-WIDTH 17
  COLUMN-LABEL "NET/FOB V.Dist."
  HELP "Pre�o NET/FOB para venda distribuidor"
  DECIMALS 2
  ORDER 140

ADD FIELD "perc_margem_bruta_direta" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Margem bruta direta comiss�o, valor composto por margem bruta direta menos a comiss�o do vendedor."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "M.Bruta Direta"
  POSITION 16
  MAX-WIDTH 17
  COLUMN-LABEL "M.Bruta Direta"
  HELP "Margem bruta direta comiss�o"
  DECIMALS 2
  ORDER 150

ADD FIELD "perc_margem_bruta_unit" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Margem bruta unit�ria, margem bruta direta comiss�o vezes o pre�o da venda direta."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "M.Bruta Unit"
  POSITION 17
  MAX-WIDTH 17
  COLUMN-LABEL "M.Bruta Unit"
  HELP "Margem bruta unit�ria"
  DECIMALS 2
  ORDER 160

ADD FIELD "ativo" OF "es_politica_tabela_preco_item" AS logical 
  FORMAT "Sim/Nao"
  INITIAL "Sim"
  LABEL "Ativo"
  POSITION 18
  MAX-WIDTH 1
  COLUMN-LABEL "Ativo"
  ORDER 75
  
ADD FIELD "perc_margem_distrib" OF "es_politica_tabela_preco_item" AS decimal 
  DESCRIPTION "Campo para informar a margem bruta em percentual para venda distribuidor, ser�o valores informados."
  FORMAT "->>,>>9.99"
  INITIAL "0"
  LABEL "Margem Distrib"
  POSITION 18
  MAX-WIDTH 17
  COLUMN-LABEL "Margem Distrib"
  HELP "Margem bruta em percentual para venda distribuidor"
  DECIMALS 2
  ORDER 170

ADD INDEX "Ind01" ON "es_politica_tabela_preco_item" 
  AREA "Schema Area"
  UNIQUE
  PRIMARY
  INDEX-FIELD "cod_tab_preco" ASCENDING 
  INDEX-FIELD "cod_estabel" ASCENDING 
  INDEX-FIELD "data_valid_ini" ASCENDING 
  INDEX-FIELD "data_valid_fim" ASCENDING 
  INDEX-FIELD "it_codigo" ASCENDING 
  INDEX-FIELD "cod_faixa" ASCENDING 

.
PSC
cpstream=ibm850
.
0000006103
