/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i MLAHTML0511E 2.06.00.000}  /*** 010002 ***/
/********************************************************************************
** Copyright DATASUL S.A. 
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
** 22/04/2021 Anderson Luchini
**            Aprova��o de Pedido de Venda - Fast Agro.
*******************************************************************************/

DEF BUFFER usuar_mestre FOR usuar_mestre.
def var i-empresa LIKE mg.empresa.ep-codigo no-undo.

def temp-table tt-mla-chave
    field valor as char format "x(20)" extent 10.

DEFINE TEMP-TABLE tt-html
    FIELD seq-html     AS INT
    FIELD html-doc     AS CHAR 
    INDEX i-seq-mensagem
          seq-html        ASCENDING.

{cdp/cdcfgdis.i}
{utp/ut-glob.i}

DEF INPUT  PARAMETER p-cod-tip-doc   as integer no-undo.
DEF INPUT  PARAMETER p-cod-aprovador as char    no-undo.
DEF INPUT  PARAMETER table for tt-mla-chave.
DEF OUTPUT PARAMETER TABLE FOR tt-html.

DEF BUFFER bf-es_natur_oper FOR es_natur_oper.
DEF BUFFER bf-ped-venda     FOR ped-venda.
DEF BUFFER bf-ped-venda-cex FOR ped-venda-cex.
DEF BUFFER bf-ped-item      FOR ped-item.

DEF BUFFER empresa FOR mg.empresa.

DEF VAR c-class         AS CHAR NO-UNDO.
DEF VAR i-cont          AS INTE NO-UNDO.
DEF VAR c-chave         AS CHAR NO-UNDO.
DEF VAR i-chave         AS INTE NO-UNDO.
DEF VAR c-cod-estabel   AS CHAR NO-UNDO.
DEF VAR c-desc-empresa  AS CHAR NO-UNDO.
DEF VAR c-desc-estab    AS CHAR NO-UNDO.
DEF VAR c-desc-fornec   AS CHAR NO-UNDO.
DEF VAR c-pais          AS CHAR NO-UNDO.
DEF VAR c-nom-usuario   AS CHAR NO-UNDO.
DEF VAR c-segmento      AS CHAR NO-UNDO.
DEF VAR c-incoterm      AS CHAR NO-UNDO.
DEF VAR i-seq-html      AS INTE NO-UNDO.
DEF VAR c-desc-item     AS CHAR NO-UNDO.
DEF VAR c-arquivo       AS CHAR NO-UNDO.
DEF VAR d-preco-negoc   AS DEC  NO-UNDO.
DEFINE VARIABLE c-tipo-venda AS CHARACTER   NO-UNDO.

DEF TEMP-TABLE tt-ped-item NO-UNDO LIKE ped-item  
    FIELD cod_faixa  AS CHAR
    FIELD cor_faixa  AS CHAR
    FIELD deValorNetFob AS DEC
    FIELD dePrecoTarget AS DEC.

DEFINE VARIABLE c-motivo AS CHARACTER   NO-UNDO.

DEF VAR i-historico AS INT NO-UNDO.

DEF TEMP-TABLE tt-ult-ped NO-UNDO
    FIELD seq        AS INT
    FIELD pedido     AS CHAR
    FIELD dt-entrega AS CHAR
    FIELD preco      AS DEC
    FIELD quantidade AS DEC
    FIELD incoterm   AS CHAR.

DEF BUFFER b-es_politica_tabela_preco_item FOR es_politica_tabela_preco_item.

DEF STREAM str-html.

/**   Monta chave do documento
**/

assign c-chave = "".
find first tt-mla-chave no-lock no-error.

for each mla-chave-doc-aprov where
         mla-chave-doc-aprov.cod-tip-doc = p-cod-tip-doc no-lock:
    assign i-chave = i-chave + 1
           c-chave = c-chave + tt-mla-chave.valor[i-chave] + substring("                    ",1,  
                                        (((mla-chave-doc-aprov.posicao-fim - mla-chave-doc-aprov.posicao-ini) + 1) - length(tt-mla-chave.valor[i-chave]))). 
end.

/****NAO RETIRAR ESTE "ASSIGN c-chave = trim(c-chave)" , EM BASE ORACLE N�O � POSS�VEL 
     ENCONTRAR UM REGISTRO COM ESPA�OS EM BRANCO NO FINAL DE UM CAMPO ***********/

ASSIGN c-chave = RIGHT-TRIM(c-chave).

/*
FIND FIRST ped-item NO-LOCK 
     WHERE ped-item.nome-abrev   = TRIM(tt-mla-chave.valor[1])
       AND ped-item.nr-pedcli    = TRIM(tt-mla-chave.valor[2])
       AND ped-item.nr-sequencia = INT(tt-mla-chave.valor[3]) NO-ERROR.*/

FIND FIRST ped-venda WHERE 
           ped-venda.nome-abrev = TRIM(tt-mla-chave.valor[1]) AND 
           ped-venda.nr-pedcli  = TRIM(tt-mla-chave.valor[2]) NO-LOCK NO-ERROR.

FIND FIRST es_natur_oper NO-LOCK
     WHERE es_natur_oper.nat_operacao = ped-venda.nat-operacao NO-ERROR.

FIND FIRST ped-venda-cex NO-LOCK 
     WHERE ped-venda-cex.nome-abrev = ped-venda.nome-abrev
       AND ped-venda-cex.nr-pedcli  = ped-venda.nr-pedcli NO-ERROR.
IF AVAIL ped-venda-cex THEN
    ASSIGN c-incoterm = ped-venda-cex.cod-incoterm.
ELSE
    ASSIGN c-incoterm = "".

FIND FIRST canal-venda NO-LOCK
     WHERE canal-venda.cod-canal-venda = ped-venda.cod-canal-venda NO-ERROR.
IF AVAIL canal-venda THEN
    ASSIGN c-segmento = canal-venda.descricao.
ELSE
    ASSIGN c-segmento = "".

FIND FIRST moeda NO-LOCK
     WHERE moeda.mo-codigo = ped-venda.mo-codigo NO-ERROR.

FIND estabelec NO-LOCK
    WHERE estabelec.cod-estabel = ped-venda.cod-estabel NO-ERROR.

ASSIGN i-empresa = estabelec.ep-codigo.

FIND emitente WHERE
    emitente.nome-abrev = ped-venda.nome-abrev NO-LOCK NO-ERROR.
IF  AVAILABLE emitente THEN
    ASSIGN c-desc-fornec = emitente.nome-emit
           c-pais        = emitente.pais.
ELSE
    ASSIGN c-desc-fornec = ""
           c-pais        = "".

FIND FIRST repres WHERE
           repres.nome-abrev = ped-venda.no-ab-reppri NO-LOCK NO-ERROR.
    
FIND FIRST es_ped_venda WHERE
           es_ped_venda.nr_pedido = ped-venda.nr-pedido NO-LOCK NO-ERROR.
IF AVAIL es_ped_venda THEN do:
    CASE es_ped_venda.tipo-venda:
        WHEN 1 THEN c-tipo-venda = "Direta".
        WHEN 2 THEN c-tipo-venda = "Distribui��o".
        WHEN 3 THEN c-tipo-venda = "Agenciamento".
        WHEN 4 THEN c-tipo-venda = "Bonifica��o".
    END CASE.
END.

FOR EACH ped-item OF ped-venda NO-LOCK:

    CREATE tt-ped-item.
    BUFFER-COPY ped-item TO tt-ped-item.
    
    FIND FIRST es_ped_item WHERE
               es_ped_item.nome_abrev   = ped-item.nome-abrev AND
               es_ped_item.nr_pedcli    = ped-item.nr-pedcli AND
               es_ped_item.nr_sequencia = ped-item.nr-sequencia NO-LOCK NO-ERROR.
    IF AVAIL es_ped_item THEN DO:

        IF es_ped_venda.tipo-venda = 1 THEN
            FIND FIRST es_politica_tabela_preco_item WHERE
                       es_politica_tabela_preco_item.cod_estabel     = ped-venda.cod-estabel AND
                       es_politica_tabela_preco_item.it_codigo       = ped-item.it-codigo AND
                       es_politica_tabela_preco_item.data_valid_ini <= ped-venda.dt-implant AND
                       es_politica_tabela_preco_item.data_valid_fim >= ped-venda.dt-implant AND 
                       es_politica_tabela_preco_item.preco_netfob_venda_direta = es_ped_item.faixa_preco AND
                       es_politica_tabela_preco_item.ativo NO-LOCK NO-ERROR.
        ELSE IF es_ped_venda.tipo-venda = 2 THEN
            FIND FIRST es_politica_tabela_preco_item WHERE
                       es_politica_tabela_preco_item.cod_estabel     = ped-venda.cod-estabel AND
                       es_politica_tabela_preco_item.it_codigo       = ped-item.it-codigo AND
                       es_politica_tabela_preco_item.data_valid_ini <= ped-venda.dt-implant AND
                       es_politica_tabela_preco_item.data_valid_fim >= ped-venda.dt-implant AND 
                       es_politica_tabela_preco_item.preco_netfob_venda_distrib = es_ped_item.faixa_preco AND
                       es_politica_tabela_preco_item.ativo NO-LOCK NO-ERROR.

        IF AVAIL es_politica_tabela_preco_item THEN DO:

            ASSIGN tt-ped-item.cod_faixa     = es_politica_tabela_preco_item.cod_faixa
                   tt-ped-item.deValorNetFob = es_ped_item.vlr_net_fob_ef.
    
            FIND FIRST es_politica_faixa WHERE
                       es_politica_faixa.cod_faixa = string(es_politica_tabela_preco_item.cod_faixa) NO-LOCK NO-ERROR.
            IF AVAIL es_politica_faixa THEN
                ASSIGN tt-ped-item.cor_faixa = es_politica_faixa.cor_faixa.

            IF es_ped_venda.tipo-venda = 1 THEN DO:
                FOR EACH b-es_politica_tabela_preco_item WHERE
                         b-es_politica_tabela_preco_item.cod_tab_preco  = es_politica_tabela_preco_item.cod_tab_preco  AND
                         b-es_politica_tabela_preco_item.cod_estabel    = es_politica_tabela_preco_item.cod_estabel    AND
                         b-es_politica_tabela_preco_item.data_valid_ini = es_politica_tabela_preco_item.data_valid_ini AND
                         b-es_politica_tabela_preco_item.data_valid_fim = es_politica_tabela_preco_item.data_valid_fim AND
                         b-es_politica_tabela_preco_item.it_codigo      = es_politica_tabela_preco_item.it_codigo      AND
                         b-es_politica_tabela_preco_item.ativo NO-LOCK BY b-es_politica_tabela_preco_item.preco_netfob_venda_direta:
                    ASSIGN tt-ped-item.dePrecoTarget = b-es_politica_tabela_preco_item.preco_netfob_venda_direta.
                END.    
            END.
            ELSE IF es_ped_venda.tipo-venda = 2 THEN DO:
                FOR EACH b-es_politica_tabela_preco_item WHERE
                         b-es_politica_tabela_preco_item.cod_tab_preco  = es_politica_tabela_preco_item.cod_tab_preco  AND
                         b-es_politica_tabela_preco_item.cod_estabel    = es_politica_tabela_preco_item.cod_estabel    AND
                         b-es_politica_tabela_preco_item.data_valid_ini = es_politica_tabela_preco_item.data_valid_ini AND
                         b-es_politica_tabela_preco_item.data_valid_fim = es_politica_tabela_preco_item.data_valid_fim AND
                         b-es_politica_tabela_preco_item.it_codigo      = es_politica_tabela_preco_item.it_codigo      AND
                         b-es_politica_tabela_preco_item.ativo NO-LOCK BY b-es_politica_tabela_preco_item.preco_netfob_venda_distrib:
                    ASSIGN tt-ped-item.dePrecoTarget = b-es_politica_tabela_preco_item.preco_netfob_venda_distrib.
                END. 
            END.

        END.
    END.

END.
    
EMPTY TEMP-TABLE tt-ult-ped.

ASSIGN i-historico = 0.

FOR EACH bf-ped-venda NO-LOCK USE-INDEX ch-credito WHERE
         bf-ped-venda.nome-abrev   = ped-venda.nome-abrev
     AND bf-ped-venda.cod-sit-ped <= 3
     AND bf-ped-venda.dt-entrega  <= TODAY + 365
     AND bf-ped-venda.completo,
   FIRST bf-es_natur_oper NO-LOCK
   WHERE bf-es_natur_oper.nat_operacao = bf-ped-venda.nat-operacao
     AND bf-es_natur_oper.tip_oper_exp = es_natur_oper.tip_oper_exp,
    EACH bf-ped-item NO-LOCK OF bf-ped-venda 
    BY bf-ped-venda.dt-entrega DESC:

    IF CAN-FIND(FIRST es_pedido_ontime
                WHERE es_pedido_ontime.nome_abrev = bf-ped-venda.nome-abrev
                  AND es_pedido_ontime.nr_pedcli  = bf-ped-venda.nr-pedcli
                  AND es_pedido_ontime.CHAR_1     = "S") THEN NEXT.

    ASSIGN i-historico = i-historico + 1.

    CREATE tt-ult-ped.
    ASSIGN tt-ult-ped.seq        = i-historico
           tt-ult-ped.pedido     = bf-ped-venda.nr-pedcli
           tt-ult-ped.dt-entrega = STRING(bf-ped-venda.dt-entrega,"99/99/9999")
           tt-ult-ped.preco      = bf-ped-item.vl-preori
           tt-ult-ped.quantidade = bf-ped-item.qt-pedida.

    FIND FIRST bf-ped-venda-cex NO-LOCK 
     WHERE bf-ped-venda-cex.nome-abrev = bf-ped-venda.nome-abrev
       AND bf-ped-venda-cex.nr-pedcli  = bf-ped-venda.nr-pedcli NO-ERROR.
    IF AVAIL bf-ped-venda-cex THEN
        ASSIGN tt-ult-ped.incoterm = bf-ped-venda-cex.cod-incoterm.

    IF i-historico >= 4 THEN LEAVE.
END.                
    
FIND estabelec WHERE
     estabelec.cod-estabel = ped-venda.cod-estabel
     NO-LOCK NO-ERROR.
IF AVAIL estabelec THEN
    ASSIGN c-cod-estabel = estabelec.cod-estabel
           c-desc-estab  = estabelec.nome.

FIND FIRST empresa 
     WHERE empresa.ep-codigo = estabelec.ep-codigo
     NO-LOCK NO-ERROR.
IF AVAIL empresa THEN
   ASSIGN c-desc-empresa = empresa.nome.
ELSE
   ASSIGN c-desc-empresa = "".

FIND FIRST mla-param-aprov  where
    mla-param-aprov.ep-codigo   = estabelec.ep-codigo and
    mla-param-aprov.cod-estabel = ped-venda.cod-estabel
    NO-LOCK NO-ERROR.

FIND LAST mla-doc-pend-aprov 
    WHERE mla-doc-pend-aprov.ep-codigo    = estabelec.ep-codigo
      AND mla-doc-pend-aprov.cod-estabel  = c-cod-estabel
      AND mla-doc-pend-aprov.cod-tip-doc  = p-cod-tip-doc
      AND mla-doc-pend-aprov.chave-doc    = c-chave  
      AND mla-doc-pend-aprov.ind-situacao = 1
      AND mla-doc-pend-aprov.historico    = NO NO-LOCK NO-ERROR. /* pend�ncia atual */

FIND FIRST usuar_mestre NO-LOCK
     WHERE usuar_mestre.cod_usuario = mla-doc-pend-aprov.cod-usuar-doc NO-ERROR.
IF AVAIL usuar_mestre THEN
    ASSIGN c-nom-usuario = usuar_mestre.nom_usuario.
ELSE
    ASSIGN c-nom-usuario = "".

ASSIGN c-motivo = mla-doc-pend-aprov.motivo-doc.

CREATE tt-html.
ASSIGN i-seq-html       = i-seq-html + 1
       tt-html.seq-html = i-seq-html.
ASSIGN tt-html.html-doc = tt-html.html-doc +
'<html>' +
'<head>' +
'<style>' +
'    BODY ~{ ' +
'      FONT-SIZE: small; FONT-FAMILY: Times; BACKGROUND-COLOR: #cccccc ' +
'    ~}' +
'    .campo ~{ ' +
'        BACKGROUND: black ' +
'    } ' +
'    .destaq ~{ ' +
'        BACKGROUND: #cccccc ' +
'    } ' +
'    .linhaBrowse ~{ ' +
'        FONT-SIZE: 8pt; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif ' +
'    } ' +
'    .linhaForm ~{ ' +
'        FONT-SIZE: 8pt; LINE-HEIGHT: normal;; COLOR:black; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif, ' +
'    } ' +
'    .fill-in ~{ ' +
'     font-family:Tahoma, Arial;font-size:xx-small;font-weight:bold;color:black;background:white; ' +
'    } ' +
'    .tableForm ~{ ' +
'        BORDER-RIGHT: 1px inset; BORDER-TOP: 1px inset; BORDER-LEFT: 1px inset; BORDER-BOTTOM: 1px inset;width:90%  ' +
'    } ' +
'    .tableForm2 ~{ ' +
'        BORDER-width: 0px inset;padding:0;  ' +
'    } ' +
'    .barraTitulo ~{ ' +
'        FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR:white; FONT-FAMILY: Arial; BACKGROUND-COLOR: #6699CC; TEXT-ALIGN: center; vertical-alignment: middle ' +
'    } ' +
'    .barraTituloBrowse ~{ ' +
'        FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: Arial; BACKGROUND-COLOR: #808080; TEXT-ALIGN: center; vertical-alignment: middle ' +
'    } ' +
'    .selectedFolder ~{ ' +
'        BACKGROUND-COLOR: white ' +
'    } ' +
'    .unselectedFolder ~{ ' +
'        BACKGROUND-COLOR: #dcdcdc ' +
'    } ' +
'    .button ~{ ' +
'    font-family:Verdana, Arial;font-size:xx-small;font-weight:bold;color:white;background:#e0b050;cursor:hand;width:80 ' +
'    } ' +
'    .linhaBrowsepar ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#d2eef4 ' +
'        } ' +
'    .linhaBrowseimpar ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#ffffff ' +
'        } ' +
'    .linhaBrowseAmarelo ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#FFFF00 ' +
'        } ' +
'    .linhaBrowsePreto ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#000000 ' +
'        } ' +
'    .linhaBrowseCinza ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#A9A9A9 ' +
'        } ' +
'    .linhaBrowseAzul ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#00008B ' +
'        } ' +
'    .linhaBrowseVerde ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#008000 ' +
'        } ' +
'    .linhaBrowseMarron ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#8B4513 ' +
'        } ' +
'    .linhaBrowseRoxo ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#9400D3 ' +
'        } ' +
'    .linhaBrowseRosa ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#FFC0CB ' +
'        } ' +
'    .linhaBrowseVermelho ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#FF0000 ' +
'        } ' +
'    .linhaBrowseLaranja ~{ ' +
'        FONT-SIZE: 8pt;color:black; LINE-HEIGHT: normal; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;BACKGROUND-COLOR:#FFA500 ' +
'        } ' +
'    .columnLabel ~{ ' +
'        FONT-SIZE: 8pt; LINE-HEIGHT: normal;font-weight:bold; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;color:#FFFFFF;BACKGROUND-COLOR:#107f80 ' +
'        } ' +
'    .columnLabelSelected ~{ ' +
'        FONT-SIZE: 8pt; LINE-HEIGHT: normal;font-weight:bold; FONT-STYLE: normal; FONT-FAMILY: Arial, Helvetica, sans-serif;color:#FFFFFF;BACKGROUND-COLOR:#20b0b0 ' +
'        } ' +
'    .linkOrdena ~{ ' +
'        COLOR:white;text-decoration:none;  ' +
'        } ' +
'    .aBrowser ~{ ' +
'       color:red;font-family:Arial;font-size:8pt;font-weight:normal;text-decoration:none;  ' +
'       }  ' +
'    .aBrowser:hover ~{ ' +
'       color:red;font-family:Arial;font-size:8pt;font-weight:normal;text-decoration:underline ;  ' +
'      }  ' +
'   .aBrowserAcom ~{ ' +
'      color:blue;font-family:Arial;font-size:8pt;font-weight:normal;text-decoration:none;  ' +
'       }  ' +
'    .aBrowserAcom:hover ~{ ' +
'       color:blue;font-family:Arial;font-size:8pt;font-weight:normal;text-decoration:underline;  ' +
'       } ' +
'</style>'.

ASSIGN tt-html.html-doc = tt-html.html-doc +
'   <meta http-equiv="Cache-Control" content="No-Cache"> ' +
'   <meta http-equiv="Pragma"        content="No-Cache"> ' +
'   <meta http-equiv="Expires"       content="0"> ' +
' </head> ' +
' <body topmargin="0" leftmargin="0"> ' +
'<form method="GET" action="' + trim(mla-param-aprov.servidor-asp) + '">' +
' <input class="fill-in" type="hidden" name="hid_param"> ' +
' <input class="fill-in" type="hidden" name="hid_tp_docum" value="' + string(p-cod-tip-doc) + '"> ' +
' <input class="fill-in" type="hidden" name="hid_chave" value="' + replace(c-chave," ","**") + '"> ' +
' <input class="fill-in" type="hidden" name="hid_cod_usuar"  value="' + string(p-cod-aprovador) + '"> ' +
' <input class="fill-in" type="hidden" name="hid_senha"  value="1">' +

' <input class="fill-in" type="hidden" name="hid_empresa" value="' + string(i-empresa) + '"> ' +
' <input class="fill-in" type="hidden" name="hid_estabel" value="' + estabelec.cod-estabel + '"> ' +

' <a name="evento"> ' +
' <div align="center"><center> ' +
'   <table align="center" border="0" cellpadding="0" cellspacing="3" width="90%" class="tableForm"> ' +
'     <tr> ' +
'       <td class="barratitulo">Pedido</td> ' +
'     </tr> ' +
'    <tr> ' +
'     <td class="linhaForm" align="center"><center> ' +
'      <fieldset> ' +
'        <table align="center" border="0" cellpadding="0" cellspacing="1"> ' +
'          <tr><td height="5"></td></tr> ' +
'         <tr> ' +
'          <th align="right" class="linhaForm" nowrap> ' +
'              Empresa:               ' +
'          </th> ' +
'          <td colspan="2" align="left" class="linhaForm" nowrap> ' +
'              <input type="text" class="fill-in" size="3" name="empresa" value="' + STRING(i-empresa) + '" readonly>' + 
'              <input type="text" class="fill-in" size="40" name="razaosocial" value="' + c-desc-empresa + '" readonly> ' +
'          </td> ' +
'         </tr> ' +
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               Estabelecimento: ' +
'           </th> ' +
'           <td colspan="2" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <input type="text" class="fill-in" size="15" name="w_estab" value="' + estabelec.cod-estabel + '" readonly> ' +
'               <input type="text" class="fill-in" size="40" name="w_desc_estab" value="' + c-desc-estab + '" readonly> ' +
'           </td> ' +
'          </tr> ' +
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               PedCli / Incoterm: ' +
'           </th> ' +
'           <td colspan="2" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <input type="text" class="fill-in" size="15" name="w_pedcli" value="' + ped-venda.nr-pedcli + '" readonly> ' +
'               <input type="text" class="fill-in" size="40" name="w_incoterm" value="' + c-incoterm + '" readonly> ' +
'           </td> ' +
'          </tr> ' +
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               Cliente: ' +
'           </th> ' +
'           <td colspan="2" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <input type="text" class="fill-in" size="15" name="w_emite" value="' + STRING(emitente.cod-emitente) + '" readonly> ' +
'               <input type="text" class="fill-in" size="40" name="w_desc_emite" value="' + c-desc-fornec + '" readonly> ' +
'           </td> ' +
'          </tr> ' +
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               Pais: ' +
'           </th> ' +
'           <td colspan="2" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <input type="text" class="fill-in" size="20" name="w_pais" value="' + c-pais + '" readonly> ' +
'           </td> ' +
'          </tr> ' +
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               Tipo Venda: ' +
'           </th> ' +
'           <td colspan="2" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <input type="text" class="fill-in" size="15" name="w_tipo_venda" value="' + c-tipo-venda + '" readonly> ' +
'           </td> ' +
'          </tr> ' + 
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               Moeda: ' +
'           </th> ' +
'           <td colspan="2" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <input type="text" class="fill-in" size="15" name="w_moeda" value="' + STRING(ped-venda.mo-codigo) + '" readonly> ' +
'               <input type="text" class="fill-in" size="40" name="w_desc_moeda" value="' + moeda.descricao + '" readonly> ' +
'           </td> ' +
'          </tr> ' +
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               Usuario: ' +
'           </th> ' +
'           <td colspan="2" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <input type="text" class="fill-in" size="15" name="w_usuario" value="' + mla-doc-pend-aprov.cod-usuar-doc + '" readonly> ' +
'               <input type="text" class="fill-in" size="40" name="w_nom_usuario" value="' + c-nom-usuario + '" readonly> ' +
'           </td> ' +
'          </tr> ' +
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               Data Emb: ' +
'           </th> ' +
'           <td colspan="2" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <input type="text" class="fill-in" size="15" name="w_data" value="' + STRING(ped-venda.dt-entrega,"99/99/9999") + '" readonly> ' +
'           </td> ' +
'          </tr> ' +
'          <tr> ' +
'           <th align="right" class="linhaForm" nowrap width="30%"> ' +
'               Observa��es: ' +
'           </th> ' +
'           <td colspan="2" valign="left" align="left" class="linhaForm" nowrap width="70%"> ' +
'               <textarea class="fill-in" name="w_motivo" rows="3" cols="40" readonly>' + c-motivo + '</textarea> ' +
'           </td> ' +
'          </tr> '.


ASSIGN tt-html.html-doc = tt-html.html-doc +
'          <tr><td height="5"></td></tr> ' +
'        </table> ' +
'      </fieldset> '.

ASSIGN tt-html.html-doc = tt-html.html-doc +
       '<br>'.

/*
ASSIGN tt-html.html-doc = tt-html.html-doc +
' <div align="center"><center> ' +
'   <table align="center" border="0" cellpadding="0" cellspacing="3" width="90%" class="tableForm"> ' +
'    <tr> ' +
'       <td class="barratitulo"> Pre�o Ponderado do Pedido </td> ' +
'    </tr> ' +
'   </table>' +
'   <table class="tableForm" align="center" width="100%"> ' +
'    <tr> ' +
'     <td align="center"  class="ColumnLabel" width="20%">Volume</td> ' +
'     <td align="center"  class="ColumnLabel" width="20%">Pre�o Pedido</td> ' +
'     <td align="center"  class="ColumnLabel" width="20%">Pre�o Lista</td> ' +
'     <td align="center"  class="ColumnLabel" width="20%">Pre�o Or�ado</td> ' +
'     <td align="center"  class="ColumnLabel" width="20%">% Diferen�a</td> ' +
'    </tr> '.

ASSIGN c-class = ' class="linhaBrowseImpar" '.       

ASSIGN tt-html.html-doc = tt-html.html-doc +
        '    <tr> ' +
        '     <td ' + c-class + '> ' +
        '       ' + trim(string(de-tot-quantidade,">>>>,>>9.99")) +
        '     </td> ' +
        '     <td ' + c-class + '> ' +
        '       ' + trim(string(de-preco-unit-cotacao,">>>,>>9.9999")) +
        '     </td> ' +
        '     <td ' + c-class + '> ' +
        '       ' + trim(string(de-preco-lista-cotacao,">>>,>>9.9999")) +
        '     </td> ' +
        '     <td ' + c-class + '> ' +
        '       ' + trim(string(de-preco-orcado-cotacao,">>>,>>9.9999")) +
        '     </td> ' +
        '     <td ' + c-class + '> ' +
        '       ' + trim(string(de-perc-faixa,">>>,>>9.99")) +
        '     </td> '.


ASSIGN tt-html.html-doc = tt-html.html-doc +
        '    </tr>'.

ASSIGN tt-html.html-doc = tt-html.html-doc +
'   </table>' +
' </div>'.
*/

ASSIGN tt-html.html-doc = tt-html.html-doc +
       '<br>'.

CREATE tt-html.
ASSIGN i-seq-html       = i-seq-html + 1
       tt-html.seq-html = i-seq-html.
ASSIGN tt-html.html-doc = tt-html.html-doc +
' <div align="center"><center> ' +
'   <table align="center" border="0" cellpadding="0" cellspacing="3" width="90%" class="tableForm"> ' +
'    <tr> ' +
'       <td class="barratitulo"> Item </td> ' +
'    </tr> ' +
'   </table>' +
'   <table class="tableForm" align="center" width="100%"> ' +
'    <tr> ' +
'     <td align="center"  class="ColumnLabel" >Faixa</td> ' +
'     <td align="center"  class="ColumnLabel" >Seq</td> ' +
'     <td align="center"  class="ColumnLabel" >Item</td> ' +
'     <td align="center"  class="ColumnLabel" >Descricao</td> ' +
'     <td align="center"  class="ColumnLabel" >Quantidade</td> ' +
'     <td align="center"  class="ColumnLabel" >Preco Target</td> ' +
'     <td align="center"  class="ColumnLabel" >Preco NET/FOB</td> ' +
'     <td align="center"  class="ColumnLabel" >Preco NF</td> ' +
'    </tr> '.

ASSIGN c-class = ' class="linhaBrowseImpar" '.       

FOR EACH tt-ped-item NO-LOCK:
    FIND ITEM WHERE ITEM.it-codigo = tt-ped-item.it-codigo NO-LOCK NO-ERROR.
    ASSIGN c-desc-item = ITEM.desc-item.
    
    ASSIGN tt-html.html-doc = tt-html.html-doc +
            '    <tr> '.

    RUN pi-cor.
    ASSIGN tt-html.html-doc = tt-html.html-doc +
            '     <td ' + c-class + ' width="4%">  ' +
            '       ' + '' +
            '     </td> '.

    ASSIGN c-class = ' class="linhaBrowseImpar" '. 

    ASSIGN tt-html.html-doc = tt-html.html-doc +
            '     <td ' + c-class + '> ' +
            '       ' + STRING(tt-ped-item.nr-sequencia) +
            '     </td> ' +
            '     <td ' + c-class + '> ' +
            '       ' + STRING(tt-ped-item.it-codigo) +
            '     </td> ' +
            '     <td ' + c-class + '> ' +
            '       ' + c-desc-item +
            '     </td> ' +
            '     <td ' + c-class + '> ' +
            '       ' + trim(string(tt-ped-item.qt-pedida,">>>>>,>>>,>>9")) + ' ' + ITEM.un +
            '     </td> ' +
            '     <td ' + c-class + '> ' +
            '       ' + trim(string(tt-ped-item.dePrecoTarget,">>>>>,>>>,>>9.9999")) +
            '     </td> ' +
            '     <td ' + c-class + '> ' +
            '       ' + trim(string(tt-ped-item.deValorNetFob,">>>>>,>>>,>>9.9999")) +
            '     </td> ' +
            '     <td ' + c-class + '> ' +
            '       ' + trim(string(tt-ped-item.vl-preori,">>>>>,>>>,>>9.9999")) +
            '     </td> ' +
            '    </tr>'.
END.

ASSIGN tt-html.html-doc = tt-html.html-doc +
'   </table>' +
' </div>'.

/*
CREATE tt-html.
ASSIGN i-seq-html       = i-seq-html + 1
       tt-html.seq-html = i-seq-html.
ASSIGN tt-html.html-doc = tt-html.html-doc +
' <div align="center"><center> ' +
'   <table align="center" border="0" cellpadding="0" cellspacing="3" width="90%" class="tableForm"> ' +
'    <tr> ' +
'       <td class="barratitulo"> Historico </td> ' +
'    </tr> ' +
'   </table>' +
'   <table class="tableForm" align="center" width="100%"> ' +
'    <tr> ' +
'     <td align="center"  class="ColumnLabel" >Data</td> ' +
'     <td align="center"  class="ColumnLabel" >Pedido</td> ' +
'     <td align="center"  class="ColumnLabel" >Incoterm</td> ' +
'     <td align="center"  class="ColumnLabel" >Quantidade</td> ' +
'     <td align="center"  class="ColumnLabel" >Preco</td> ' +
'    </tr> '.

ASSIGN c-class = ' class="linhaBrowseImpar" '.       

FOR EACH tt-ult-ped BY tt-ult-ped.seq:

ASSIGN tt-html.html-doc = tt-html.html-doc +
        '    <tr> ' +
        '     <td ' + c-class + '> ' +
        '       ' + tt-ult-ped.dt-entrega +
        '     </td> ' +
        '     <td ' + c-class + '> ' +
        '       ' + tt-ult-ped.pedido +
        '     </td> ' +
        '     <td ' + c-class + '> ' +
        '       ' + tt-ult-ped.incoterm +
        '     </td> ' +
        '     <td ' + c-class + '> ' +
        '       ' + trim(string(tt-ult-ped.quantidade,">>>>>,>>>,>>9")) + ' ' + ITEM.un +
        '     </td> ' +
        '     <td ' + c-class + '> ' +
        '       ' + trim(string(tt-ult-ped.preco,">>>>>,>>>,>>9.9999")) +
        '     </td> '.


ASSIGN tt-html.html-doc = tt-html.html-doc +
        '    </tr>'.

END.

ASSIGN tt-html.html-doc = tt-html.html-doc +
'   </table>' +
' </div>'.
*/

/********************************************************************************
** Copyright DATASUL S.A. 
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
DEFINE VARIABLE c-class-notif AS CHARACTER  NO-UNDO.
DEFINE VARIABLE c-informado   AS CHAR       NO-UNDO.
DEFINE VARIABLE i-cont-aprov  AS INTEGER    NO-UNDO.
DEFINE VARIABLE c-checked     AS CHARACTER  NO-UNDO.
DEFINE VARIABLE i-sequencia-substring AS INTEGER    NO-UNDO.

FIND FIRST mla-tipo-doc-aprov 
     WHERE mla-tipo-doc-aprov.ep-codigo   = estabelec.ep-codigo
       AND mla-tipo-doc-aprov.cod-estabel = c-cod-estabel
       AND mla-tipo-doc-aprov.cod-tip-doc = p-cod-tip-doc NO-LOCK NO-ERROR.

/* Altera��o realizada para corre��o na PQU e Orsa-Suzano - N�o retirar */
FOR EACH mla-doc-pend-aprov 
     WHERE mla-doc-pend-aprov.ep-codigo    = estabelec.ep-codigo
       AND mla-doc-pend-aprov.cod-estabel  = c-cod-estabel
       AND mla-doc-pend-aprov.cod-tip-doc  = p-cod-tip-doc
       AND mla-doc-pend-aprov.chave-doc    = c-chave  NO-LOCK: /* pend�ncia atual */
END.
/* Fim altera��o */

FIND LAST mla-doc-pend-aprov 
     WHERE mla-doc-pend-aprov.ep-codigo    = estabelec.ep-codigo
       AND mla-doc-pend-aprov.cod-estabel  = c-cod-estabel
       AND mla-doc-pend-aprov.cod-tip-doc  = p-cod-tip-doc
       AND mla-doc-pend-aprov.chave-doc    = c-chave  
       AND mla-doc-pend-aprov.ind-situacao = 1
       AND mla-doc-pend-aprov.historico    = NO NO-LOCK NO-ERROR. /* pend�ncia atual */

ASSIGN tt-html.html-doc = tt-html.html-doc +           
'<input class="fill-in" type="hidden" name="hid_nr_trans"  value="' + string(mla-doc-pend-aprov.nr-trans) + '"> '.

Find First login_integr_so NO-LOCK NO-ERROR.  /*Essa vari�veis ser�o necess�rios que fazer o login integrado via e-mail*/

if AVAIL login_integr_so AND log_habtdo_login_integr then  
    ASSIGN tt-html.html-doc = tt-html.html-doc +           
           '<input class="fill-in" type="hidden" name="hid_user_dom"  value="' + OS-GETENV('USERNAME') +  '">' +
           '<input class="fill-in" type="hidden" name="hid_dominio"  value="' + Os-getenv('USERDNSDOMAIN':U) + '">' +
           '<input class="fill-in" type="hidden" name="hid_login_integrado"  value=" 1 ">' .
ELSE
    ASSIGN tt-html.html-doc = tt-html.html-doc +           
           '<input class="fill-in" type="hidden" name="hid_login_integrado"  value=" 2 ">' .

ASSIGN tt-html.html-doc = tt-html.html-doc +           
       '<table border=0 width=100%> ' +
       '  <tr> ' +
       '  <td align=center> ' +
       '   <table border=0> ' +
       '     <tr> ' +
       '      <th class=linhaform align=right>Codigo Rejeicao:</td> ' +
       '       <td align=left>' +
       '        <select name="w_cod_rejeicao" class="fill-in">'.

FOR EACH mla-rej-aprov NO-LOCK:
    ASSIGN tt-html.html-doc = tt-html.html-doc + '<option value="' + STRING(mla-REJ-APROV.cod-rejeicao) + '">' + string(mla-REJ-APROV.cod-rejeicao) + '-' + mla-REJ-APROV.des-rejeicao + IF mla-REJ-APROV.LOG-1 AND mla-tipo-doc-aprov.log-2 THEN "(Re-an�lise)</option>" ELSE "</option>".
END.

ASSIGN tt-html.html-doc = tt-html.html-doc +           
       '        </select></td></tr>' +
       '      </td>    ' +
       '      </tr> ' +
       '      <tr> ' +
       '       <th class=linhaform align=right>Narrativa:</td> ' +
       '       <td valign="midlle" align=left><textarea class="fill-in" name="w_narrativa_usuar" rows="3" cols="40"></textarea> ' +
       '      </td></tr> ' +
       '      </tr>' .

ASSIGN tt-html.html-doc = tt-html.html-doc +           
           '      <tr><td align=center colspan=2> '.

ASSIGN tt-html.html-doc = tt-html.html-doc +
       '       <input type="submit" name="action" value="Aprovar"  class="button" >' +
       '       <input type="submit" name="action" value="Rejeitar" class="button">' +
       '      </td></tr>      ' +
       
       '    </table>       ' + 
       '  </td>' +
       '  <td align=center>'.

IF AVAIL mla-tipo-doc-aprov AND 
   (SUBSTRING(mla-tipo-doc-aprov.char-1,1,1) = "Y" OR 
    SUBSTRING(mla-tipo-doc-aprov.char-1,2,1) = "Y") THEN DO:

    ASSIGN tt-html.html-doc = tt-html.html-doc +           
       '<table class="tableForm" align="center" width="100%">' +
       '   <tr>' +
       '      <td align="center"  class="ColumnLabel" colspan=2>Notificar os Seguites Usuarios</td>' +
       '   </tr>' +
       '   <tr>' +
       '      <th align="center"  class="ColumnLabel">Aprovacao</th>' +
       '      <th align="center"  class="ColumnLabel">Rejeicao</th>' +
       '   </tr>' +
       '   <tr >'.
    
     {laphtml/mlahtmlrodape.i2 "aprova"}
    
     FIND FIRST mla-doc-pend-aprov 
          WHERE mla-doc-pend-aprov.ep-codigo    = estabelec.ep-codigo
            AND mla-doc-pend-aprov.cod-estabel  = c-cod-estabel
            AND mla-doc-pend-aprov.cod-tip-doc  = p-cod-tip-doc
            AND mla-doc-pend-aprov.chave-doc    = c-chave  
            AND mla-doc-pend-aprov.ind-situacao = 1
            AND mla-doc-pend-aprov.historico    = NO NO-LOCK NO-ERROR. /* pend�ncia atual */
    
     {laphtml/mlahtmlrodape.i2 "rejeita"}
    
    ASSIGN tt-html.html-doc = tt-html.html-doc +           
                               '</tr>' +
                               '</table>'.
    
    
    ASSIGN tt-html.html-doc = tt-html.html-doc +           
           '  </td>' +
           '  </tr>' + 
           '</table>'.

END.
    ASSIGN tt-html.html-doc = tt-html.html-doc +  
           '  </td>' +
           '  </tr>' +
           '     <tr> ' +
           '      <td align=center>'.

ASSIGN i-cont-aprov = 0.

IF AVAIL mla-tipo-doc-aprov AND mla-tipo-doc-aprov.log-1 = YES THEN DO:

    ASSIGN tt-html.html-doc = tt-html.html-doc +  
           '   <center> ' +
           '   <table border=0 width=80%> ' +
           '     <tr> ' +
           '      <td class=linhaform align=left>' +
           '        <fieldset><legend><b>Aprovacoes</b></legend>' + 
           '   <table border=0>'.
           
   FOR EACH  mla-doc-pend-aprov 
       WHERE mla-doc-pend-aprov.ep-codigo    = estabelec.ep-codigo
         AND mla-doc-pend-aprov.cod-estabel  = c-cod-estabel
         AND mla-doc-pend-aprov.cod-tip-doc  = p-cod-tip-doc
         AND mla-doc-pend-aprov.chave-doc    = c-chave  
         AND mla-doc-pend-aprov.ind-situacao = 2
         AND mla-doc-pend-aprov.historico    = NO NO-LOCK USE-INDEX pend-18 BY mla-doc-pend-aprov.nr-trans: 
       
       FIND mla-tipo-aprov WHERE mla-tipo-aprov.cod-tip-aprov = mla-doc-pend-aprov.cod-tip-aprov NO-LOCK NO-ERROR. 
       

       ASSIGN i-cont-aprov = i-cont-aprov + 1.

       IF mla-doc-pend-aprov.cod-usuar-altern = "" THEN 
           FIND usuar_mestre WHERE usuar_mestre.cod_usuar = mla-doc-pend-aprov.cod-usuar NO-LOCK NO-ERROR.
       ELSE
           FIND usuar_mestre WHERE usuar_mestre.cod_usuar = mla-doc-pend-aprov.cod-usuar-altern NO-LOCK NO-ERROR.
       

       ASSIGN tt-html.html-doc = tt-html.html-doc + 
                                 '<tr> ' +
                                 '  <td colspan=2 class=linhaform align=left>' +
                                 '----------- N�vel ' + STRING(i-cont-aprov) + ' (' + mla-tipo-aprov.des-tip-aprov + ') ------------<br>' +
                                 '<b>Aprovado por: </b>' + usuar_mestre.cod_usuar + ' - ' + usuar_mestre.nom_usuario + '<br>' +
                                 '<b>Quando: </b>' + string(mla-doc-pend-aprov.dt-aprova,'99/99/9999') + ' as ' + substring(mla-doc-pend-aprov.char-1,1,8) +
                                 '  </td> ' +
                                 '</tr> ' +
                                 '<tr> ' +
                                 '  <td class=linhaform align=left valign=top>' +
                                 '      <b>Coment�rio: </b>' + 
                                 '  </td>' +
                                 '  <td class=linhaform align=left>' + replace(mla-doc-pend-aprov.narrativa-apr,CHR(10),'<br>') +
                                 '  </td>' +
                                 '</tr>'. 

   END.

    ASSIGN tt-html.html-doc = tt-html.html-doc +           
               '        </table>' +
               '        </fieldset>' +
               '     </td> ' +
               '     </tr>' +
               '   </table>'.
 
END.

CREATE tt-html.
ASSIGN i-seq-html       = i-seq-html + 1
       tt-html.seq-html = i-seq-html.
ASSIGN tt-html.html-doc = tt-html.html-doc +
'  </form> ' +
' </body>  ' +
'</html> ' .

FOR EACH tt-html:
    ASSIGN tt-html.html-doc = codepage-convert(tt-html.html-doc,"iso8859-1","ibm850").
END.

ASSIGN c-arquivo = SESSION:TEMP-DIRECTORY + "/mlaHTML0511e_" +
                   TRIM(tt-mla-chave.valor[1]) + TRIM(tt-mla-chave.valor[2]) + ".htm.".

OUTPUT TO VALUE(c-arquivo).
    FOR EACH tt-html:
        PUT UNFORMATTED tt-html.html-doc AT 1 SKIP.
    END.
OUTPUT CLOSE.

PROCEDURE pi-cor:

    CASE tt-ped-item.cor_faixa:
        WHEN "Amarelo" OR WHEN "Amarela" THEN ASSIGN c-class = ' class="linhaBrowseAmarelo" '. 
        WHEN "Preto" OR WHEN "Preta" THEN ASSIGN c-class = ' class="linhaBrowsePreto" '.
        WHEN "Cinza" THEN ASSIGN c-class = ' class="linhaBrowseCinza" '.
        WHEN "Azul" THEN ASSIGN c-class = ' class="linhaBrowseAzul" '.
        WHEN "Verde" THEN ASSIGN c-class = ' class="linhaBrowseVerde" '.
        WHEN "Marron" THEN ASSIGN c-class = ' class="linhaBrowseMarron" '.
        WHEN "Roxo" OR WHEN "Roxa" THEN ASSIGN c-class = ' class="linhaBrowseRoxo" '.
        WHEN "Rosa" THEN ASSIGN c-class = ' class="linhaBrowseRosa" '.
        WHEN "Vermelho" OR WHEN "Vermelha" THEN ASSIGN c-class = ' class="linhaBrowseVermelho" '.
        WHEN "Laranja" THEN ASSIGN c-class = ' class="linhaBrowseLaranja" '.
    END CASE.

END PROCEDURE.
