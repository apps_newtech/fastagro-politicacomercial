DEFINE TEMP-TABLE tt-detalhe NO-UNDO
    /* Informar os campos aqui conforme necessidade - estes podem ser retirados*/
    FIELD cod-empresa          LIKE mg.empresa.ep-codigo
    FIELD desc-empresa         AS CHARACTER FORMAT 'X(60)'
    FIELD cod-estab            LIKE ped-venda.cod-estabel
    FIELD desc-estab           AS CHARACTER FORMAT 'X(60)'
    FIELD nr-pedcli            LIKE ped-venda.nr-pedcli
    FIELD c-incoterm           AS CHARACTER 
    FIELD cod-emitente         LIKE ped-venda.cod-emitente
    FIELD des-emitente        AS CHARACTER FORMAT 'X(60)'
    FIELD c-pais               AS CHARACTER FORMAT 'X(60)'
    FIELD c-tipo-venda         AS CHARACTER FORMAT 'X(20)'
    
    FIELD mo-codigo            LIKE ped-venda.mo-codigo
    FIELD desc-moeda           LIKE moeda.descricao
    FIELD cod-usuario          LIKE mla-doc-pend-aprov.cod-usuar-doc
    FIELD desc-usuario         LIKE usuar_mestre.nom_usuario
    FIELD dt-entrega           LIKE ped-venda.dt-entrega
    FIELD observacoes          LIKE ped-venda.observacoes.

DEF TEMP-TABLE tt-ped-item NO-UNDO
    FIELD cor-faixa        LIKE es_politica_faixa.cor_faixa
    FIELD nr-sequencia     LIKE ped-item.nr-sequencia
    FIELD it-codigo        LIKE ped-item.it-codigo
    FIELD desc-item        LIKE ITEM.desc-item
    FIELD quantidade       LIKE ped-item.qt-pedida
    FIELD preco-target     LIKE ped-item.vl-preori
    FIELD preco-net-fob    LIKE ped-item.vl-preori
    FIELD preco-nf         LIKE ped-item.vl-preori.


DEFINE TEMP-TABLE tt-lista NO-UNDO LIKE tt-lista-portal
    /* Incluir aqui os campos do documento em quest�o */
    FIELD valor AS DECIMAL .

