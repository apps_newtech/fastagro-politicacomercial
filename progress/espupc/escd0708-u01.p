/*
**    Programa: escd0708-u00
**    Objetivo: Chamador de UPC espec�fica
**       Autor: Anderson - Newtech
** Atualiza��o: 17/05/2021
*/


/*----- DEFINICAO DE FUNCOES -----*/
{tools/fc-handle-obj.I}



/*----- DEFINICAO DE PARAMETROS -----*/
def input parameter p-ind-event  as char          no-undo.
def input parameter p-ind-object as char          no-undo.
def input parameter p-wgh-object as handle        no-undo.
def input parameter p-wgh-frame  as widget-handle no-undo.
def input parameter p-cod-table  as char          no-undo.
def input parameter p-row-table  as rowid         no-undo.
               
/*----- DEFINICAO DE VARIAVEIS LOCAIS -----*/
DEF VAR c-handle-obj        AS CHAR         NO-UNDO.
DEF VAR h-objeto            AS HANDLE       NO-UNDO.

/*----- DEFINICAO DE VARIAVEIS LOCAIS -----*/
DEF NEW GLOBAL SHARED VAR wh-cd0708-cod-rep AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-cd0708-cep AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-cd0708-f-formula AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-cd0708-cod-usuario     AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-cd0708-cod-usuario-txt AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR h-cd0708-urg-upc AS WIDGET-HANDLE NO-UNDO.
/*
MESSAGE p-ind-object SKIP
    p-ind-event  SKIP
    p-wgh-object:FILE-NAME SKIP
    p-wgh-frame SKIP
    STRING(p-row-table)
    VIEW-AS ALERT-BOX INFO BUTTONS OK. */


IF p-ind-event  = "BEFORE-INITIALIZE" AND 
   p-ind-object = "CONTAINER"         THEN DO:

   c-handle-obj = fc-handle-obj("cod-rep",P-WGH-FRAME).
   wh-cd0708-cod-rep = widget-handle(ENTRY(1,c-handle-obj)) NO-ERROR.

END.

IF p-ind-event  = "BEFORE-INITIALIZE" AND 
   p-ind-object = "VIEWER"    AND
   p-wgh-object:FILE-NAME = "advwr/v10ad229.w"            THEN DO:

   c-handle-obj = fc-handle-obj("f-formula",P-WGH-FRAME).
   wh-cd0708-f-formula = widget-handle(ENTRY(1,c-handle-obj)) NO-ERROR.

END.

IF p-ind-event  = "BEFORE-INITIALIZE" AND 
   p-ind-object = "VIEWER"    AND
   p-wgh-object:FILE-NAME = "advwr/v05ad229.w"     THEN DO:

   c-handle-obj = fc-handle-obj("cep",P-WGH-FRAME).
   wh-cd0708-cep = widget-handle(ENTRY(1,c-handle-obj)) NO-ERROR.

   ASSIGN h-cd0708-urg-upc = p-wgh-object.
          
   create TEXT      wh-cd0708-cod-usuario-txt
   assign col          = wh-cd0708-cep:COL - 6
          row          = wh-cd0708-cep:ROW + 1
          frame        = wh-cd0708-cep:FRAME
          format       = "x(12)"
          width        = 7
          height       = .88
          sensitive    = TRUE
          name         = "cod-usuariotxt"
          screen-value = "Usu�rio:".

   create fill-in      wh-cd0708-cod-usuario
   assign col          = wh-cd0708-cep:col
          row          = wh-cd0708-cep:ROW + 1
          frame        = wh-cd0708-cep:FRAME
          format       = "x(15)"
          width        = 12
          height       = .88
          sensitive    = no
          name         = "cod-usuario"
          screen-value = "".

    wh-cd0708-cod-usuario:LOAD-MOUSE-POINTER ("image\lupa.cur" ).                     
    ON 'F5'  OF wh-cd0708-cod-usuario OR "MOUSE-SELECT-DBLCLICK" OF wh-cd0708-cod-usuario PERSISTENT RUN espupc/escd0708-u01a.p.

END.


IF p-ind-event  = "DISPLAY"  AND
   p-ind-object = "VIEWER"     AND
   p-wgh-object:FILE-NAME = "advwr/v05ad229.w"    THEN DO:

    ASSIGN wh-cd0708-cod-usuario-txt:screen-value = "Usu�rio:"
           wh-cd0708-cod-usuario:screen-value     = "".

    find first repres
         where rowid(repres) = p-row-table no-lock no-error.
    if avail repres then do:
        find first ext_repres
             where ext_repres.cod-rep = repres.cod-rep no-error.
        if avail ext_repres THEN
            ASSIGN wh-cd0708-cod-usuario:screen-value = ext_repres.cod_usuario.
    end.
end.

IF p-ind-event  = "ASSIGN"  AND
   p-ind-object = "VIEWER"    AND
   p-wgh-object:FILE-NAME = "advwr/v05ad229.w"    THEN DO:

    find first repres
         where rowid(repres) = p-row-table
         no-lock no-error.
    if avail repres then do:
        find first ext_repres
             where ext_repres.cod-rep = repres.cod-rep
             no-error.
        if not avail ext_repres then do:
            create ext_repres.
            assign ext_repres.cod-rep = repres.cod-rep.
        end.

        assign ext_repres.cod_usuario = wh-cd0708-cod-usuario:screen-value.
    end.

end.

IF p-ind-event  = "VALIDATE"  AND
   p-ind-object = "VIEWER"    THEN DO:

   IF VALID-HANDLE(wh-cd0708-cod-usuario) AND
      wh-cd0708-cod-usuario:SCREEN-VALUE <> "" THEN DO:
       FIND FIRST usuar_mestre WHERE
                  usuar_mestre.cod_usuar = wh-cd0708-cod-usuario:SCREEN-VALUE NO-LOCK NO-ERROR.
       IF NOT AVAIL usuar_mestre THEN DO:
           RUN utp/ut-msgs.p (INPUT "show":U,
                                       INPUT 17006, 
                                       INPUT "Usu�rio inv�lido." + "~~" + 
                                             'Usu�rio n�o cadastrado.').
           RETURN 'NOK'.  
       END.
   END.    

END.

IF VALID-HANDLE(wh-cd0708-cod-usuario) AND VALID-HANDLE(wh-cd0708-f-formula) THEN DO:
    wh-cd0708-cod-usuario:SENSITIVE = wh-cd0708-f-formula:SENSITIVE.
END.



RETURN "OK".
