DEFINE NEW GLOBAL SHARED VARIABLE wh-cd0708-cod-usuario  AS WIDGET-HANDLE  NO-UNDO.
DEFINE NEW GLOBAL SHARED VARIABLE h-cd0708-urg-upc         AS HANDLE         NO-UNDO.

DEFINE NEW GLOBAL SHARED VARIABLE adm-broker-hdl AS HANDLE NO-UNDO.
DEFINE VARIABLE wh-pesquisa                      AS HANDLE NO-UNDO.
DEFINE VARIABLE l-implanta AS LOGICAL    NO-UNDO.

IF VALID-HANDLE(wh-cd0708-cod-usuario) THEN DO:
    {include/zoomvar.i &prog-zoom=fnzoom/z01fn017.w
                       &campohandle=wh-cd0708-cod-usuario
                       &campozoom=cod_usuario
                       &proghandle=h-cd0708-urg-upc}
END.
