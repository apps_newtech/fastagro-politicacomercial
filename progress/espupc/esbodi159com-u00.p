/****************************************************************************************************
**
**  Programa ........:
**  Data ............:
**  Versao ..........:
**
** 001 - Projeto Exporta��o. Barth criou pi-valida-container para validar
**       se algum Conainer foi informado.
** 002 - Projeto Exporta��o. Barth criou evento afterCompleteOrder para levar
**       containers do pedido para o processo.
** 003 - Projeto Exporta��o. Barth tirou a valida��o do container. Esta valida��o
**       foi colocada no bot�o de completar o pedido.
**       Inserida notifica��o por e-mail quando incoterm do pedido for diferente
**       do incoterm do cadastro do cliente.
** 004 - Projeto Exporta��o. Josias -> gravar dt-entrega e natureza de opera��o na es_processo_esp
** 005 - Projeto Exporta��o. 25/04/2013 Quando natureza = 7949 o pedido � aprovado aqui na upc.
**       Mas logo em seguida a rotina tornava o pedido reprovado.
**       Para os casos de amostra o pedido deve se manter aprovado.
** 
** Data       Autor Marca                         Descri��o
** ---------- ----- ---------------------------- --------------------------------------------------
** 20/05/2020 LuisR 20200520-LuisR-Pedido-DDP-EU Release 006 - Acrescimo do campo texto
** 22/03/21 - GILBERTO RISSATI - NEWELL - VALIDANDO COTACAO DA MOEDA FUNCIONAL ANTES DE COMPLETAR O 
** PEDIDO   

***************************************************************************************************/
{include/i-epc200.i1} /*Definicao tt-EPC*/
{cdp/cdcfgdis.i} /* Include para Pre Processadores */
{cdp/cdapi013.i}
{method/dbotterr.i}
{utp/utapi019.i}

DEFINE INPUT        PARAM p-ind-event AS CHARACTER NO-UNDO.
DEFINE INPUT-OUTPUT PARAM TABLE FOR tt-epc.

/* 20200520-LuisR-Pedido-DDP-EU - ini */
DEFINE VARIABLE wh-esex2573api AS HANDLE                NO-UNDO.
DEFINE VARIABLE c-texto-tx     LIKE texto-tx.texto      NO-UNDO.
/* 20200520-LuisR-Pedido-DDP-EU - fim */

DEFINE VARIABLE cUserName       AS CHARACTER       NO-UNDO.
DEFINE VARIABLE iUserCountryTax AS INTEGER         NO-UNDO.
DEFINE VARIABLE h-bo            AS HANDLE          NO-UNDO.

DEFINE VARIABLE de-valor-acum   AS DECIMAL         NO-UNDO.
DEFINE VARIABLE de-vl-entrega   AS DECIMAL         NO-UNDO.

DEF VAR de-totvlap           AS DECIMAL                   NO-UNDO.
DEF VAR de-totvlre           AS DECIMAL                   NO-UNDO.

DEFINE VARIABLE hPoliticaFastMLA AS HANDLE      NO-UNDO.
DEFINE VARIABLE hValidaPedido AS HANDLE      NO-UNDO.

/*
OUTPUT TO "C:\Temp\NewTech\Barth\2012 11 28 - exp\eventos_bodi19com.txt" APPEND.
FOR EACH tt-epc:
    DISP tt-epc.cod-event           FORMAT "X(25)"
         tt-epc.cod-parameter       FORMAT "X(25)"
         WITH WIDTH 100 STREAM-IO.
END.
OUTPUT CLOSE.
*/

def temp-table tt-frete no-undo
    field cod_container as char 
    field qtd_container as int  
    FIELD l-gera-dvv-7  AS LOG
    .

DEFINE TEMP-TABLE ttParam NO-UNDO
    FIELD pNrPedido    LIKE ped-venda.nr-pedido
    field pCodEstabel  LIKE ped-venda.cod-estabel
    field pNrTabPreco  LIKE ped-venda.nr-tabpre
    field pItCodigo    LIKE ped-item.it-codigo
    field pPrecoItem   LIKE ped-item.vl-preuni
    FIELD pQtdItem     LIKE ped-item.qt-pedida
    field pPercComis   AS DECIMAL
    field pCodRep      LIKE repres.cod-rep
    field pCodCampanha LIKE es_politica_campanha_item.cod_campanha
    FIELD pEstado      LIKE es_politica_tabela_frete_cidade.estado
    FIELD pCidade      LIKE es_politica_tabela_frete_cidade.cidade
    FIELD pPrecoFrete  LIKE es_politica_tabela_frete_cidade.valor_frete.

def new global shared var de-tit-vencido-g            as dec           no-undo.
def new global shared var de-tit-avencer-g            as dec           no-undo.
def new global shared var de-tit-vencido-m            as dec           no-undo.
def new global shared var de-tit-avencer-m            as dec           no-undo.

DEFINE VARIABLE i-qtd-tit       AS INTEGER         NO-UNDO.
define variable i-cont          as integer         no-undo.

def var d-valor-despesa as dec  no-undo.
def var i-frequencia    as int  no-undo.
DEF VAR c_den_local AS CHAR NO-UNDO.
DEF VAR c-container      AS CHAR NO-UNDO.
DEF VAR i_cod_via_transp AS INT NO-UNDO.
DEFINE VARIABLE c-estabel AS CHARACTER   NO-UNDO.

define buffer bf-emitente for emitente.
DEFINE BUFFER b-natur-oper FOR natur-oper.
DEFINE BUFFER bf-ped-venda FOR ped-venda.
DEFINE BUFFER bf-ped-item  FOR ped-item.
define buffer bf-ped-ent  for ped-ent.
DEF BUFFER b-emitente FOR emitente.
DEF BUFFER b-ped-venda FOR ped-venda.

{method/svc/autentic/autentic.i &vUserName="cUserName"
                                &vUserCountryTax="iUserCountryTax"}

    IF p-ind-event = 'beforeCompleteOrder' THEN DO:

        FIND FIRST tt-epc NO-LOCK WHERE
                   tt-epc.cod-event     = p-ind-event     AND
                   tt-epc.cod-parameter = "OBJECT-HANDLE" NO-ERROR.
    
        IF AVAILABLE tt-epc THEN 
            ASSIGN h-bo = WIDGET-HANDLE(tt-epc.val-parameter).
    
        FIND FIRST tt-epc NO-LOCK WHERE
                   tt-epc.cod-event     = p-ind-event   AND
                   tt-epc.cod-parameter = 'table-rowid' NO-ERROR.
    
        
        IF AVAIL tt-epc THEN DO:
    
            FIND FIRST ped-venda EXCLUSIVE-LOCK WHERE
                 ROWID(ped-venda) = TO-ROWID(tt-epc.val-parameter) NO-ERROR.

            IF AVAIL ped-venda THEN DO:

                IF CAN-FIND(FIRST ped-venda-cex WHERE
                                  ped-venda-cex.nr-pedido = ped-venda.nr-pedido)  THEN
                    RETURN "OK":U.

                FIND FIRST emitente NO-LOCK WHERE
                           emitente.cod-emitente = ped-venda.cod-emitente NO-ERROR.

                
                /*------------------------ Validar se foi informado no m�nimo 1 container 22/11/2012 Paulo Barth */
                            /* 
                RUN espupc\esbodi159com-u01.p (INPUT p-ind-event,
                                               INPUT-OUTPUT TABLE tt-epc).
                                                */

                /* Paulo Barth 22/01/2013 Valida��o colocada no bot�o de completar o pedido.
                RUN pi-valida-container.
                IF RETURN-VALUE = "NOK" THEN
                    RETURN "NOK".
                */

                /*------------------------ Verificar se pedido e de acido para explodir a programacao de entrega 03/02/2011 sevarolli */
                do i-cont = 1 to 9.
                   if index(program-name(i-cont),"espd028") > 0 then leave.
                end.
                if i-cont >= 10 then do:
                   find first es_emitente  where
                              es_emitente.cod_emitente  =  ped-venda.cod-emitente no-lock no-error.
                   if avail es_emitente  and 
                            es_emitente.qtd_cliente  > 0 then do:
                      find first ped-ent use-index codigo of ped-venda no-lock no-error.
                      FIND last  bf-ped-ent no-lock USE-INDEX codigo WHERE
                                 bf-ped-ent.nome-abrev    = ped-ent.nome-abrev
                             AND bf-ped-ent.nr-pedcli     = ped-ent.nr-pedcli
                             AND bf-ped-ent.nr-sequencia  = ped-ent.nr-sequencia
                             AND bf-ped-ent.it-codigo     = ped-ent.it-codigo
                             AND bf-ped-ent.cod-refer     = ped-ent.cod-refer NO-ERROR.
                      /* nao houve explos�o da programa de entrega     */
                      if avail bf-ped-ent and 
                         bf-ped-ent.nr-entrega    =   ped-ent.nr-entrega then do:
                         run  esp\espd035.p(input rowid(ped-venda)).
                      end.
                   end.
                end.
                /*------------------------ fim da gera��o automatica de programacao de entrega para acido ---------------------------*/

                RUN pi-avalia-cred-entrega.

                RUN pi-valida-moeda-func. // 22/3/21

                RUN pi-politica-fast-agro.

            END. // IF AVAIL ped-venda THEN DO:
        END. //IF AVAIL tt-epc THEN DO:
    END.
    
    IF p-ind-event = 'afterCompleteOrder' THEN DO:

        FIND FIRST tt-epc NO-LOCK WHERE
                   tt-epc.cod-event     = p-ind-event   AND
                   tt-epc.cod-parameter = 'table-rowid' NO-ERROR.
        
        IF AVAIL tt-epc THEN DO:
    
            FIND FIRST ped-venda EXCLUSIVE-LOCK WHERE
                 ROWID(ped-venda) = TO-ROWID(tt-epc.val-parameter) NO-ERROR.

            IF AVAIL ped-venda THEN DO:

                /* Paulo Barth 22/01/2013 Quando incoterm do pedido for diferente
                                          do incoterm do cliente CD2577 enviar e-mail. */
                FOR FIRST ped-venda-cex NO-LOCK WHERE
                          ped-venda-cex.nome-abrev = ped-venda.nome-abrev
                      AND ped-venda-cex.nr-pedcli  = ped-venda.nr-pedcli,
                    FIRST emitente-cex NO-LOCK WHERE
                          emitente-cex.cod-emitente = ped-venda.cod-emitente:
                    
                    /*
                    ** SOlicita��o Juliana - Retirar regra.
                    */
                    /*
                    IF ped-venda-cex.cod-incoterm <> emitente-cex.cod-incoterm-exp THEN
                        RUN pi-envia-email-incoterm.
                    */                        

                    RUN pi-valida-frete-maritimo.
                END.

                RUN pi-container-pedido-processo.

                RUN pi-texto-processo.
                               
            END.
        END.
    END.

    
/* RUN espupc\esbodi159com-u95.p (INPUT p-ind-event,           */
/*                                INPUT-OUTPUT TABLE tt-epc).  */
/* if return-value = "NOK" then                                */
/*     return "NOK".                                           */

    
        
/* Fim */      
PROCEDURE pi-avalia-cred-entrega:

    DEF VAR l-erro AS LOGICAL INITIAL NO NO-UNDO.

    /******************** TICKET 851 - BLOCO FOI TIRADO DE DENTRO DO FOR EACH DE PED-ITEM E PED-ENT ****************/

    /* 09/11/2009 - Seiki - Tratamento de avalia��o por Matriz */
    def var de-lim-credito as dec no-undo.
    assign de-lim-credito = 0.
    for each bf-emitente no-lock
        where bf-emitente.nome-matriz = emitente.nome-matriz
        and bf-emitente.dt-lim-cred >= today:

        if emitente.ind-abrange-aval = 1 and    /* por cliente */
            bf-emitente.nome-matriz <> emitente.nome-abrev then next.

        if emitente.moeda-libcre = 0 then  /* limite de credito em real sevarolli */
           assign de-lim-credito = de-lim-credito + bf-emitente.lim-credito + bf-emitente.lim-adicional.
        else do:    /* trannsforma o limite em real pela cotacao da moeda do dia corrente sevarolli */
            find first cotacao no-lock
                 where cotacao.mo-codigo   = emitente.moeda-libcre
                    and cotacao.ano-periodo = string(string(year(today),"9999") + string(month(today),"99")) no-error.
            if avail cotacao then
               assign de-lim-credito = de-lim-credito + 
                                       (bf-emitente.lim-credito * cotacao.cotacao[day(today)]) 
                                       + (bf-emitente.lim-adicional *  cotacao.cotacao[day(today)]).

        end.
    end.

    run espupc/espd0802f-u05.p(input  emitente.cod-emitente,
                               input  0,
                               output de-tit-vencido-g,
                               output de-tit-avencer-g,
                               output de-tit-vencido-m,
                               output de-tit-avencer-m).

    run pi-verifica-pd.

    if emitente.ind-abrange-aval = 1 then
        assign de-lim-credito = de-lim-credito - (de-tit-vencido-g + de-tit-avencer-g + de-totvlap + de-totvlre).
    else
        assign de-lim-credito = de-lim-credito - (de-tit-vencido-m + de-tit-avencer-m + de-totvlap + de-totvlre).

    /**************************************************************************************************************/

    FOR EACH ped-item OF ped-venda NO-LOCK,
        EACH ped-ent OF ped-item EXCLUSIVE-LOCK:

        ASSIGN l-erro = NO.

        IF CAN-FIND(FIRST ext-ped-ent 
                    WHERE ext-ped-ent.nome-abrev     = ped-ent.nome-abrev    
                      AND ext-ped-ent.nr-pedcli      = ped-ent.nr-pedcli   
                      AND ext-ped-ent.nr-sequencia   = ped-ent.nr-sequencia
                      AND ext-ped-ent.it-codigo      = ped-ent.it-codigo   
                      AND ext-ped-ent.cod-refer      = ped-ent.cod-refer   
                      AND ext-ped-ent.nr-entrega     = ped-ent.nr-entrega
                      AND ext-ped-ent.ind-aprov-cred = YES) THEN NEXT.
        
        FOR EACH tt-erros-aval: DELETE tt-erros-aval. END.
        FOR EACH tt-param-aval: DELETE tt-param-aval. END.
        ASSIGN de-vl-entrega = 0.

        /**************************
        ** Alteracao 14/07/2009 - Mineiro - NOL
        ** colocado logica para executar avaliacao somente quando o cliente nao for 'CREDITO AUTOMATICO' 
        **************************/
        if not emitente.ind-cre-cli = 2 and can-find(first natur-oper where natur-oper.nat-operacao = ped-venda.nat-operacao and natur-oper.emite-duplic) then do:

            ASSIGN ped-venda.cod-sit-aval = 1. /* Verificar se teremos que manipular este valor*/

            CREATE tt-param-aval.
            ASSIGN tt-param-aval.nr-pedido     = ped-venda.nr-pedido
                   tt-param-aval.param-aval    = IF emitente.ind-aval = 2 THEN 1 ELSE 3
                   tt-param-aval.embarque      = NO
                   tt-param-aval.efetiva       = YES
                   tt-param-aval.retorna       = YES
                   tt-param-aval.reavalia-forc = YES
                   tt-param-aval.vl-a-aval     = (ped-item.vl-liq-abe / ped-item.qt-pedida) * ped-ent.qt-pedida
                   tt-param-aval.usuario       = cUserName
                   tt-param-aval.programa      = "pd4000".

            ASSIGN de-vl-entrega = tt-param-aval.vl-a-aval.

            RUN cdp/cdapi013.p (INPUT-OUTPUT TABLE tt-param-aval,
                                INPUT-OUTPUT TABLE tt-erros-aval).
        end.
        
        /*Cria tabela especifica*/
        FIND FIRST ext-ped-ent EXCLUSIVE-LOCK
            WHERE ext-ped-ent.nome-abrev   = ped-ent.nome-abrev    
              AND ext-ped-ent.nr-pedcli    = ped-ent.nr-pedcli   
              AND ext-ped-ent.nr-sequencia = ped-ent.nr-sequencia
              AND ext-ped-ent.it-codigo    = ped-ent.it-codigo   
              AND ext-ped-ent.cod-refer    = ped-ent.cod-refer   
              AND ext-ped-ent.nr-entrega   = ped-ent.nr-entrega NO-ERROR.

        IF NOT AVAIL ext-ped-ent THEN DO:

            CREATE ext-ped-ent.
            ASSIGN ext-ped-ent.nome-abrev    = ped-ent.nome-abrev  
                   ext-ped-ent.nr-pedcli     = ped-ent.nr-pedcli   
                   ext-ped-ent.nr-sequencia  = ped-ent.nr-sequencia
                   ext-ped-ent.it-codigo     = ped-ent.it-codigo   
                   ext-ped-ent.cod-refer     = ped-ent.cod-refer   
                   ext-ped-ent.nr-entrega    = ped-ent.nr-entrega.
        END.

        /* 01/07/2009  - Diego Mariano - Verifica Limite de Cr�dito */
        /* Qdo retornar e nao tiver erro cria a tabela de extensao como status aprovado 
           O pedido sempre deve estar aprovado */

        FIND FIRST tt-erros-aval NO-LOCK WHERE
                   tt-erros-aval.cod-emitente = emitente.cod-emitente NO-ERROR.
        IF AVAIL tt-erros-aval THEN DO:
            RUN _insertErrorManual IN h-bo (INPUT 0,
                                            INPUT "EPC":U,
                                            INPUT "WARNING":U,
                                            INPUT "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado.", 
                                            INPUT "",
                                            INPUT "":U).
            
            ASSIGN ext-ped-ent.ind-aprov-cred = NO
                   l-erro                     = YES
                   ext-ped-ent.char-1         = "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado.".
        END.
        ELSE 
            ASSIGN ext-ped-ent.ind-aprov-cred = YES
                   ext-ped-ent.usuario        = "Autom�tico"
                   ext-ped-ent.char-1         = "".

        /* Barth 25/04/2013 */
        IF emitente.ind-aval <> 1 AND can-find(first natur-oper where natur-oper.nat-operacao = ped-venda.nat-operacao and natur-oper.emite-duplic) THEN DO:

            IF (emitente.ind-abrange-aval = 1 AND de-tit-vencido-g > 0) OR 
               (emitente.ind-abrange-aval = 2 AND de-tit-vencido-m > 0) THEN DO:
                ASSIGN ped-venda.cod-sit-aval = 4.
                assign ext-ped-ent.ind-aprov-cred = no
                       l-erro                     = YES 
                       ext-ped-ent.usuario        = ""
                       ext-ped-ent.char-1         = "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado. Duplicatas em atraso".
                RUN _insertErrorManual IN h-bo (INPUT 0,
                                                INPUT "EPC":U,
                                                INPUT "WARNING":U,
                                                INPUT "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado. Duplicatas em atraso", 
                                                INPUT "",
                                                INPUT "":U).
            END.

            IF de-lim-credito < de-vl-entrega THEN DO:
                ASSIGN ped-venda.cod-sit-aval = 4
                       ext-ped-ent.usuario        = ""
                       ext-ped-ent.char-1     = "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado. Excedeu limite de cr�dito".
                assign ext-ped-ent.ind-aprov-cred = no
                       l-erro                     = YES.

                RUN _insertErrorManual IN h-bo (INPUT 0,
                                                INPUT "EPC":U,
                                                INPUT "WARNING":U,
                                                INPUT "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado. Excedeu limite de cr�dito", 
                                                INPUT "",
                                                INPUT "":U).
            END.

            IF emitente.dt-lim-cred < today THEN DO:
                ASSIGN ped-venda.cod-sit-aval = 4
                       ext-ped-ent.usuario        = ""
                       ext-ped-ent.char-1     = "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado. Data de limite de cr�dito expirou".
                assign ext-ped-ent.ind-aprov-cred = no
                       l-erro                     = YES.

                RUN _insertErrorManual IN h-bo (INPUT 0,
                                                INPUT "EPC":U,
                                                INPUT "WARNING":U,
                                                INPUT "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado. Data de limite de cr�dito expirou", 
                                                INPUT "",
                                                INPUT "":U).
            END.

        END.

        FIND FIRST param_estab_acr
             WHERE param_estab_acr.cod_estab = ped-venda.cod-estabel
             NO-LOCK NO-ERROR.
        
        ASSIGN i-qtd-tit = 0.
        
        /* 09/11/2009 - Seiki - Nao considera Antecipacao e Nota de credito */
        /*                      nao considera titulo estornado              */
        /*                      abrangencia por matriz                      */
        for each bf-emitente no-lock
            where bf-emitente.nome-matriz = emitente.nome-matriz:
            if emitente.ind-abrange-aval = 1 and    /* por cliente */
               bf-emitente.nome-matriz <> emitente.nome-abrev then next.

            FOR EACH tit_acr
               WHERE tit_acr.cdn_cliente         = bf-emitente.cod-emitente
                 AND tit_acr.val_sdo_tit_acr     > 0
                 AND tit_acr.dat_vencto_tit_acr  < TODAY - param_estab_acr.qtd_dias_carenc_juros_acr
                 and tit_acr.log_tit_acr_estordo = no
                 and NOT tit_acr.ind_tip_espec_docto BEGINS "ANTECIPA"
                 and tit_acr.ind_tip_espec_docto <> "NOTA DE CREDITO".

                ASSIGN i-qtd-tit = i-qtd-tit + 1.
                leave.
            END.
        end.

        /*10072019 - Ticket 749 - n�o alterar a situa��o do pedido para "APROVADO" caso encontre erro "ext-ped-ent.ind-aprov-cred" */
        IF l-erro = NO THEN DO:

            if not can-find(first natur-oper where natur-oper.nat-operacao = ped-venda.nat-operacao and natur-oper.emite-duplic) then do:
               ASSIGN ext-ped-ent.ind-aprov-cred = YES
                      ext-ped-ent.usuario        = "Autom�tico".
               if ped-venda.cod-sit-aval <> 3 then
                   ASSIGN ped-venda.cod-sit-aval = 3.                       
               next.                       
            end.
        END.


/*
        IF i-qtd-tit > 0 THEN DO:
            ASSIGN ped-venda.cod-sit-aval = 4.
            assign ext-ped-ent.ind-aprov-cred = no.
            RUN _insertErrorManual IN h-bo (INPUT 0,
                                            INPUT "EPC":U,
                                            INPUT "WARNING":U,
                                            INPUT "Seq. " + STRING(ped-ent.nr-sequencia) + " Item " + TRIM(ped-ent.it-codigo) + " Ref. " + ped-ent.cod-refer + " Entrega " + STRING(ped-ent.nr-entrega) + " Valor " + trim(string(de-vl-entrega,">>>,>>>,>>9.99")) + " n�o foi aprovado. Cliente possui t�tulos em aberto", 
                                            INPUT "",
                                            INPUT "":U).
        END.
*/
        /* 01/07/2009  - Diego Mariano */

        VALIDATE ext-ped-ent.
        VALIDATE ped-ent.    
    END.

    /**************************
    ** Alteracao 14/07/2009 - Mineiro - NOL
    ** colocado logica para executar avaliacao somente quando o cliente nao for 'CREDITO AUTOMATICO' 
    **************************/

    if not emitente.ind-cre-cli = 2 then
        FOR FIRST ext-ped-ent EXCLUSIVE-LOCK
            WHERE ext-ped-ent.nome-abrev     = ped-venda.nome-abrev    
              AND ext-ped-ent.nr-pedcli      = ped-venda.nr-pedcli
              AND ext-ped-ent.ind-aprov-cred = YES:

            ASSIGN ped-venda.cod-sit-aval = 3. 
        END.

END PROCEDURE.

procedure pi-verifica-pd:

    FOR EACH bf-ped-venda  
        NO-LOCK USE-INDEX ch-credito WHERE 
        bf-ped-venda.nome-abrev        = emitente.nome-abrev AND
        bf-ped-venda.completo          = YES AND 
        bf-ped-venda.cod-sit-ped      <= 2 /* aberto e atend. parcial */ AND 
        bf-ped-venda.nr-pedido        <> ped-venda.nr-pedido,  
        EACH  natur-oper WHERE
              natur-oper.nat-operacao = bf-ped-venda.nat-operacao AND 
              natur-oper.emite-duplic = YES NO-LOCK:

        &IF DEFINED(bf_dis_versao_ems) &THEN 
            &IF STRING({&bf_dis_versao_ems}) >= "2.04":U &THEN 
                /* N�o considera cota��o para a avalia��o de cr�dito */ 
                IF bf-ped-venda.log-cotacao THEN  
                    NEXT.
            &ENDIF 
        &ENDIF

        FOR EACH bf-ped-item NO-LOCK USE-INDEX ch-item-ped WHERE 
            bf-ped-item.nome-abrev    = bf-ped-venda.nome-abrev AND 
            bf-ped-item.nr-pedcli     = bf-ped-venda.nr-pedcli AND 
            bf-ped-item.cod-sit-item <= 2,
            FIRST b-natur-oper NO-LOCK WHERE 
                 b-natur-oper.nat-operacao = bf-ped-item.nat-operacao AND 
                 b-natur-oper.emite-duplic = YES:

            IF  bf-ped-item.ind-componen = 3 THEN 
                NEXT.

            IF  bf-ped-venda.cod-sit-aval = 2 /* Avaliados */ OR 
                bf-ped-venda.cod-sit-aval = 3 /* Aprovados */ THEN DO:
                
                   ASSIGN de-totvlap = de-totvlap + bf-ped-item.vl-liq-abe. 
                
            END.
            ELSE do:
                IF  bf-ped-venda.cod-sit-aval = 4 THEN DO: /* Reprovados */
                    ASSIGN de-totvlre = de-totvlre + bf-ped-item.vl-liq-abe.
                END.
            end.
        END.
    END.
end.

/*
PROCEDURE pi-valida-container:

    DEF VAR l-valida AS LOGICAL NO-UNDO.

    IF AVAIL emitente and
       emitente.natureza = 3 OR emitente.natureza = 4 THEN DO:

        do i-cont = 1 to 9.
           if index(program-name(i-cont),"pd4000") > 0 THEN DO:
               ASSIGN l-valida = YES.
               leave.
           END.
        end.

    END.

    if l-valida then do:

        IF NOT can-find(FIRST es_exp_ped_cont where
                              es_exp_ped_cont.nome-abrev = ped-venda.nome-abrev
                          AND es_exp_ped_cont.nr-pedcli  = ped-venda.nr-pedcli) THEN DO:

            RUN _insertErrorManual IN h-bo (INPUT 0,
                                            INPUT "EPC":U,
                                            INPUT "ERROR":U,
                                            INPUT "N�o foi informado Container para este pedido.",
                                            INPUT "Deve ser informado no m�nimo um container.",
                                            INPUT "":U).

            RETURN "NOK".
        END.
    END.
    
    RETURN "OK".

END PROCEDURE.
*/

PROCEDURE pi-container-pedido-processo:

    RUN EXP/esex3500.p (INPUT ped-venda.nome-abrev,
                        INPUT ped-venda.nr-pedcli).

END PROCEDURE.

PROCEDURE pi-texto-processo:
    
    DEF VAR l-com  AS LOGICAL NO-UNDO.
    DEF VAR l-pack AS LOGICAL NO-UNDO.
    DEF VAR l-emb  AS LOGICAL NO-UNDO.

    FOR FIRST proc-ped-venda NO-LOCK WHERE
              proc-ped-venda.nome-abrev = ped-venda.nome-abrev
          AND proc-ped-venda.nr-pedcli  = ped-venda.nr-pedcli,
        FIRST processo-exp EXCLUSIVE-LOCK
        WHERE processo-exp.cod-estabel = proc-ped-venda.cod-estabel
          AND processo-exp.nr-proc-exp = proc-ped-venda.nr-proc-exp:

        FIND es_processo_exp EXCLUSIVE-LOCK WHERE
             es_processo_exp.cod_estabel = processo-exp.cod-estabel
         AND es_processo_exp.nr_proc_exp = processo-exp.nr-proc-exp NO-ERROR.

        IF NOT AVAIL es_processo_exp THEN DO:
            CREATE es_processo_exp.
            ASSIGN es_processo_exp.cod_estabel = processo-exp.cod-estabel
                   es_processo_exp.nr_proc_exp = processo-exp.nr-proc-exp
                   es_processo_exp.dt_entrega  = ?
                   es_processo_exp.dt_entorig  = ?
                   es_processo_exp.char-2      = string(ped-venda.dt-entrega,"99/99/9999") + ";" + ped-venda.nat-operacao.
        END.


        /* 26/12/2012 Barth */
        IF processo-exp.cod-estabel = "101" OR processo-exp.cod-estabel = "102" THEN DO:
            FIND es_emitente_cex EXCLUSIVE-LOCK WHERE
                 es_emitente_cex.cod_emitente = ped-venda.cod-emitente NO-ERROR.

            ASSIGN processo-exp.cod-despachante = IF AVAIL es_emitente_cex THEN es_emitente_cex.cod_despachante ELSE 1861
                   processo-exp.cod-seguradora  = IF AVAIL es_emitente_cex THEN es_emitente_cex.cod_seguradora ELSE 3696
                   processo-exp.hra-deadline    = "120000"
                   es_processo_exp.hra_deadline[1] = "120000"
                   es_processo_exp.hra_deadline[2] = "120000".
        END.
        ELSE DO:
            ASSIGN processo-exp.cod-despachante = 1861
                   processo-exp.cod-seguradora  = 3696
                   processo-exp.hra-deadline    = "120000"
                   es_processo_exp.hra_deadline[1] = "120000"
                   es_processo_exp.hra_deadline[2] = "120000".
        END.


        ASSIGN l-com  = YES
               l-pack = YES
               l-emb  = YES.
        IF es_processo_exp.dsl_commercial <> "" THEN
            ASSIGN l-com = NO.
        IF es_processo_exp.dsl_packing <> "" THEN
            ASSIGN l-pack = NO.
        IF es_processo_exp.dsl_instr_embarque <> "" THEN
            ASSIGN l-emb = NO.

        FOR EACH es_exp_instr_cli NO-LOCK WHERE
                 es_exp_instr_cli.cod_emitente = ped-venda.cod-emitente:

            IF l-com AND es_exp_instr_cli.log_commercial THEN DO:
                IF es_processo_exp.dsl_commercial <> "" THEN
                    ASSIGN es_processo_exp.dsl_commercial = es_processo_exp.dsl_commercial + " ".

                ASSIGN es_processo_exp.dsl_commercial = es_processo_exp.dsl_commercial + ped-venda.cond-espec /*es_exp_instr_cli.texto - essa informa��o j� � carregada para o Ped-venda, caso o usu�rio altere no Ped-venda, tem que ir a info complementada. */.
            END.
            
            IF l-pack AND es_exp_instr_cli.log_packing THEN DO:
                IF es_processo_exp.dsl_packing <> "" THEN
                    ASSIGN es_processo_exp.dsl_packing = es_processo_exp.dsl_packing + " ".

                ASSIGN es_processo_exp.dsl_packing = es_processo_exp.dsl_packing + ped-venda.cond-espec /*es_exp_instr_cli.texto - essa informa��o j� � carregada para o Ped-venda, caso o usu�rio altere no Ped-venda, tem que ir a info complementada. */.
            END.

            IF l-emb AND substring(es_exp_instr_cli.char-1, 1, 3) = "YES" THEN DO:
                IF es_processo_exp.dsl_instr_embarque <> "" THEN
                    ASSIGN es_processo_exp.dsl_instr_embarque = es_processo_exp.dsl_instr_embarque + " ".

                ASSIGN es_processo_exp.dsl_instr_embarque = es_processo_exp.dsl_instr_embarque + ped-venda.cond-espec /*es_exp_instr_cli.texto - essa informa��o j� � carregada para o Ped-venda, caso o usu�rio altere no Ped-venda, tem que ir a info complementada. */.
            END.
        END.

        /* LuisR 20200514-LuisR-Pedido-DDP-EU -ini */
        ASSIGN c-texto-tx = ''.
        buscando-texto-api-esex2573api:
        DO:
            RUN exp/esex2573api.p PERSISTENT SET wh-esex2573api.
            IF VALID-HANDLE(wh-esex2573api) THEN
                RUN pi-txt-ped-venda IN wh-esex2573api
                    (INPUT ROWID(ped-venda)
                     ,INPUT 'pedido'
                     ,OUTPUT c-texto-tx).
            DELETE PROCEDURE wh-esex2573api.
            ASSIGN wh-esex2573api = ?.
            IF TRIM(c-texto-tx) <> '' THEN DO:
                IF LENGTH(c-texto-tx) > 2000 THEN ASSIGN c-texto-tx = SUBSTRING(c-texto-tx,1,2000).
                IF INDEX(es_processo_exp.dsl_commercial,c-texto-tx) = 0 THEN
                    ASSIGN es_processo_exp.dsl_commercial = es_processo_exp.dsl_commercial 
                                                            + ' ' + c-texto-tx.
            END.
        END.
        /* LuisR 20200514-LuisR-Pedido-DDP-EU -ini */

    END.

END PROCEDURE.

PROCEDURE pi-envia-email-incoterm:

    run utp/utapi019.p persistent set h-utapi019.

    FIND FIRST param-global NO-ERROR.

    create tt-envio2.
    assign tt-envio2.versao-integracao = 1
           tt-envio2.servidor          = param-global.serv-mail
           tt-envio2.porta             = param-global.porta-mail
           tt-envio2.arq-anexo         = ""
           tt-envio2.assunto           = "Incoterm Pedido X Cliente - " +
                                         ped-venda.nome-abrev + " - " + ped-venda.nr-pedcli
           tt-envio2.destino           = "dvv@nitroquimica.com.br"
           tt-envio2.remetente         = "dvv@nitroquimica.com.br".

    create tt-mensagem.
    assign tt-mensagem.seq-mensagem = 1
           tt-mensagem.mensagem     = "O pedido abaixo foi criado com Incoterm diferente do cadastro:"  + CHR(10) + CHR(10) +
                                      "Cliente: " + ped-venda.nome-abrev        + CHR(10) +
                                      "Pedido: "  + ped-venda.nr-pedcli         + CHR(10) +
                                      "Data Implanta��o: "  + string(ped-venda.dt-implant, "99/99/9999") + CHR(10) +
                                      "Incoterm Cadastro: " + emitente-cex.cod-incoterm-exp              + CHR(10) +
                                      "Incoterm Pedido: "   + ped-venda-cex.cod-incoterm.

    output to value(session:temp-directory + "envemail-ped-incoterm.txt").
    FOR EACH tt-envio2.
        DISP tt-envio2 WITH WIDTH 300 1 COL .
    END.
    
    run pi-execute2 in h-utapi019(input  table tt-envio2,
                                  input  table tt-mensagem,
                                  output table tt-erros).

    IF CAN-FIND(FIRST tt-erros) THEN DO:
        for each tt-erros no-lock.
            DISP tt-erros.cod-erro  AT 01
                 tt-erros.desc-erro FORMAT "x(100)" AT 10
                 WITH WIDTH 300 1 COL.
        end.
    END.
    
    output close.

    delete procedure h-utapi019.

END PROCEDURE.

PROCEDURE pi-valida-frete-maritimo:
    DEF VAR r-rowid AS ROWID NO-UNDO.

    /* Foi trazida a mesma regra do programa Gera DVV mas a
       an�lise ser� feita apenas para a despesa 7 Frete Mar�timo.
       A �nica nova regra � que apenas ser� feita esta an�lise caso no pedido
       a via de transporte seja Mar�tima. */

    IF AVAIL ped-venda-cex and
       ped-venda-cex.cod-via-transp <> 3 THEN
        RETURN "OK".

    EMPTY TEMP-TABLE tt-frete.

    FOR EACH es_exp_ped_cont NO-LOCK WHERE
             es_exp_ped_cont.nome-abrev = ped-venda.nome-abrev
         AND es_exp_ped_cont.nr-pedcli  = ped-venda.nr-pedcli:

        FIND embalag NO-LOCK WHERE
             embalag.sigla-emb = es_exp_ped_cont.sigla-emb NO-ERROR.

        find first tt-frete no-lock
            where tt-frete.cod_container = embalag.embalagem no-error.
        if not avail tt-frete then do:
            create tt-frete.
            assign tt-frete.cod_container = embalag.embalagem.
        end.
        ASSIGN tt-frete.qtd_container = tt-frete.qtd_container + es_exp_ped_cont.qt-container.
        
    END.

    FIND es_itinerario
        WHERE es_itinerario.cod_itiner = ped-venda-cex.cod-itiner NO-LOCK NO-ERROR.
    IF AVAIL es_itinerario THEN DO:
         
        IF INT(es_itinerario.CHAR_1) = 2 THEN DO:
            FIND pto-contr
                WHERE pto-contr.cod-pto-contr = ped-venda-cex.pto-chegada NO-LOCK NO-ERROR.
            IF AVAIL pto-contr THEN
                ASSIGN c_den_local = pto-contr.descricao.
         END.
         ELSE DO:
            FIND pto-contr
                 WHERE pto-contr.cod-pto-contr = ped-venda-cex.pto-desembarque NO-LOCK NO-ERROR.
            IF AVAIL pto-contr THEN
                ASSIGN c_den_local = pto-contr.descricao.
         END.
    END.
    ELSE DO:
    
        FIND FIRST pto-contr NO-LOCK
            WHERE pto-contr.cod-pto-contr = ped-venda-cex.pto-desembarque NO-ERROR.
        ASSIGN c_den_local = IF AVAIL pto-contr THEN pto-contr.descricao ELSE "".

    END.

    FIND emitente 
        WHERE emitente.cod-emitente = ped-venda.cod-emitente NO-LOCK.

    /* verifica se o pedido pertence a um processo trading */
    ASSIGN c-estabel = ped-venda.cod-estabel.
    FIND FIRST es_param_estab NO-LOCK
        WHERE es_param_estab.cod_estabel = ped-venda.cod-estabel NO-ERROR.
    IF AVAIL es_param_estab THEN DO:

        IF es_param_estab.cod_estabel_trd <> '' THEN DO:

            FIND FIRST estabelec NO-LOCK
                WHERE estabelec.cod-estabel = es_param_estab.cod_estabel_trd NO-ERROR.
            IF AVAIL estabelec THEN DO:
    
                FIND b-emitente WHERE b-emitente.cod-emitente = estabelec.cod-emitente NO-LOCK NO-ERROR.
                IF AVAIL b-emitente THEN DO:

                    FIND b-ped-venda
                        WHERE b-ped-venda.nome-abrev = b-emitente.nome-abrev
                          AND b-ped-venda.nr-pedcli  = ped-venda.nr-pedcli NO-LOCK NO-ERROR.
                    IF AVAIL b-ped-venda THEN
                        ASSIGN c-estabel = b-ped-venda.cod-estabel.
                END.
            END.
        END.
    END.

    FOR EACH tt-frete:
    
        RUN dvv/dvv0070c.p (INPUT 0,
                            INPUT 7,
                            INPUT tt-frete.cod_container,
                            INPUT c_den_local,
                            INPUT emitente.pais,
                            INPUT ped-venda.dt-entrega,
                            INPUT ped-venda.dt-entrega,
                            INPUT ped-venda.cod-emitente,
                            INPUT "",  /* terminal ret */
                            INPUT "",  /* terminal ent */
                            INPUT "", /* dep�sito */
                            INPUT 2,
                            INPUT c-estabel,
                            OUTPUT r-rowid).

        IF r-rowid <> ? THEN
            ASSIGN tt-frete.l-gera-dvv-7 = YES.

    END.

    FOR EACH tt-frete WHERE
             tt-frete.l-gera-dvv-7 = NO
        BREAK BY tt-frete.l-gera-dvv-7:

        IF c-container <> "" THEN
            ASSIGN c-container = c-container + " - " + tt-frete.cod_container.

        ASSIGN c-container = c-container + tt-frete.cod_container.

        IF  LAST-OF(tt-frete.l-gera-dvv-7) AND emitente.cod-emitente <> 11212 THEN DO:
            RUN pi-envia-email-dvv-7.
        END.
    END.

END PROCEDURE.

PROCEDURE pi-envia-email-dvv-7:

/*     run utp/utapi019.p persistent set h-utapi019.                                                                                 */
/*                                                                                                                                   */
/*     FIND FIRST param-global NO-ERROR.                                                                                             */
/*                                                                                                                                   */
/*     create tt-envio2.                                                                                                             */
/*     assign tt-envio2.versao-integracao = 1                                                                                        */
/*            tt-envio2.servidor          = param-global.serv-mail                                                                   */
/*            tt-envio2.porta             = param-global.porta-mail                                                                  */
/*            tt-envio2.arq-anexo         = ""                                                                                       */
/*            tt-envio2.assunto           = "Pedido sem DVV / Frete Mar�timo - " +                                                   */
/*                                          ped-venda.nome-abrev + " - " + ped-venda.nr-pedcli                                       */
/*            tt-envio2.destino           = "dvv@nitroquimica.com.br"                                                                */
/*            tt-envio2.remetente         = "dvv@nitroquimica.com.br".                                                               */
/*                                                                                                                                   */
/*     create tt-mensagem.                                                                                                           */
/*     assign tt-mensagem.seq-mensagem = 1                                                                                           */
/*            tt-mensagem.mensagem     = "N�o foi encontrada tabela de pre�o para despesa de Frete Mar�timo:"  + CHR(10) + CHR(10) + */
/*                                       "Cliente: "           + ped-venda.nome-abrev          + CHR(10) +                           */
/*                                       "Pedido: "            + ped-venda.nr-pedcli           + CHR(10) +                           */
/*                                       "Data Implanta��o: "  + string(ped-venda.dt-implant, "99/99/9999") + CHR(10) +              */
/*                                       "Tipo Container(s): " + c-container                   + CHR(10) +                           */
/*                                       "Porto Destino: "     + c_den_local                   + CHR(10) +                           */
/*                                       "Pa�s: "              + ped-venda.pais.                                                     */
/*                                                                                                                                   */
/*     output to value(session:temp-directory + "envemail-dvv-7.txt").                                                               */
/*     FOR EACH tt-envio2.                                                                                                           */
/*         DISP tt-envio2 WITH WIDTH 300 1 COL .                                                                                     */
/*     END.                                                                                                                          */
/*                                                                                                                                   */
/*     run pi-execute2 in h-utapi019(input  table tt-envio2,                                                                         */
/*                                   input  table tt-mensagem,                                                                       */
/*                                   output table tt-erros).                                                                         */
/*                                                                                                                                   */
/*     IF CAN-FIND(FIRST tt-erros) THEN DO:                                                                                          */
/*         for each tt-erros no-lock.                                                                                                */
/*             DISP tt-erros.cod-erro  AT 01                                                                                         */
/*                  tt-erros.desc-erro FORMAT "x(100)" AT 10                                                                         */
/*                  WITH WIDTH 300 1 COL.                                                                                            */
/*         end.                                                                                                                      */
/*     END.                                                                                                                          */
/*                                                                                                                                   */
/*     output close.                                                                                                                 */
/*                                                                                                                                   */
/*     delete procedure h-utapi019.                                                                                                  */
/*                                                                                                                                   */
END PROCEDURE.

PROCEDURE pi-valida-moeda-func:

  FOR FIRST es_ped_venda NO-LOCK
      WHERE es_ped_venda.nr_pedido       = ped-venda.nr-pedido
        AND (es_ped_venda.mo_codigo_func = 21 
         OR es_ped_venda.mo_codigo_func  = 22):

     IF es_ped_venda.cotacao_mo_func = 0 THEN DO:
        RUN _insertErrorManual IN h-bo (INPUT 17854,
                                        INPUT "OUTROS":U,
                                        INPUT "ERROR":U,
                                        INPUT "Cota��o moeda funncional inv�lida!",
                                        INPUT "O valor da cota��o da moeda funcional deve ser diferente de zero",
                                        INPUT "":U).
        RETURN "NOK":U.
     END.
  END.

  RETURN "OK":u.

END PROCEDURE.

PROCEDURE pi-politica-fast-agro:

    DEFINE VARIABLE l-erro-fast-agro AS LOGICAL     NO-UNDO.

    // Valida��es espec�ficas da Fast-Agro
    EMPTY TEMP-TABLE ttParam.

    FIND FIRST repres NO-LOCK
        WHERE repres.nome-abrev = ped-venda.no-ab-rep NO-ERROR.
    FIND FIRST ped-repre NO-LOCK OF ped-venda NO-ERROR.

    FIND es_ped_venda NO-LOCK
        WHERE es_ped_venda.nr_pedido = ped-venda.nr-pedido NO-ERROR.

    FOR EACH ped-item NO-LOCK OF ped-venda:
        CREATE ttParam.
        ASSIGN ttParam.pNrPedido     = ped-venda.nr-pedido
               ttParam.pCodEstabel   = ped-venda.cod-estabel
               ttParam.pNrTabPreco   = ped-venda.nr-tabpre
               ttParam.pItCodigo     = ped-item.it-codigo
               ttParam.pPrecoItem    = ped-item.vl-preuni
               ttParam.pQtdItem      = ped-item.qt-pedida
               ttParam.pPercComis    = ped-repre.perc-comis WHEN AVAIL ped-repre
               ttParam.pCodRep       = repres.cod-rep
               ttParam.pCodCampanha  = IF AVAIL es_ped_venda THEN es_ped_venda.campanha ELSE ""
               ttParam.pEstado       = ped-venda.estado
               ttParam.pCidade       = ped-venda.cidade
               ttParam.pPrecoFrete   = ped-venda.val-frete.
    END.
        
    RUN esp/ApiValidaPedido.p PERSISTENT SET hValidaPedido.
    RUN piValidaPedido IN hValidaPedido (INPUT TABLE ttParam, OUTPUT TABLE rowErrors).
    DELETE PROCEDURE hValidaPedido.

    l-erro-fast-agro = NO.
    IF TEMP-TABLE RowErrors:HAS-RECORDS THEN DO:
        l-erro-fast-agro = YES.

        RUN emptyRowErrors IN h-bo.
        FOR EACH rowErrors:
            RUN _insertErrorManual IN h-bo (INPUT rowErrors.ErrorNumber,
                                            INPUT "OUTROS":U,
                                            INPUT "ERROR":U,
                                            INPUT rowErrors.ErrorDescription,
                                            INPUT rowErrors.ErrorHelp,
                                            INPUT "":U).    
        END.
    END.
    // Valida��es espec�ficas da Fast-Agro

    // Valida��es/Gera��o MLA
    IF NOT l-erro-fast-agro THEN DO:
        RUN esp/ApiPoliticaMLA.p PERSISTENT SET hPoliticaFastMLA.
        RUN piGerarMla IN hPoliticaFastMLA (INPUT ped-venda.nr-pedido, OUTPUT TABLE rowErrors).
        DELETE PROCEDURE hPoliticaFastMLA.
    
        IF TEMP-TABLE RowErrors:HAS-RECORDS THEN DO:
            RUN emptyRowErrors IN h-bo.
            FOR EACH rowErrors:
                RUN _insertErrorManual IN h-bo (INPUT rowErrors.ErrorNumber,
                                                INPUT "OUTROS":U,
                                                INPUT "ERROR":U,
                                                INPUT rowErrors.ErrorDescription,
                                                INPUT rowErrors.ErrorHelp,
                                                INPUT "":U).    
            END.
        END.
    END.
    // Valida��es/Gera��o MLA

END PROCEDURE.

