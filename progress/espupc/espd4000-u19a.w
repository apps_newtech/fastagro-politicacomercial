&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mg               ORACLE
          mgnitro          ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*:T*******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESPD4000-U19A 9.99.99.999}
{method/dbotterr.i}
/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
DEF NEW GLOBAL SHARED VAR wh-pd4000u19-nome-abrev     AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-pd4000u19-nr-pedcli      AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-pd4000u19-it-codigo      AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-pd4000u19-nr-sequencia   AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-pd4000u19-nr-sequencia1  AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-pd4000u19-vl-preori      AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR wh-pd4000u19-vl-preori-un-fat AS WIDGET-HANDLE NO-UNDO.
DEF NEW GLOBAL SHARED VAR pd4000u19-row-ped-venda     AS ROWID NO-UNDO.
DEF NEW GLOBAL SHARED VAR pd4000u19-row-ped-item      AS ROWID NO-UNDO.

/* Local Variable Definitions ---                                       */
DEFINE BUFFER bf-ped-item    FOR ped-item.

DEFINE TEMP-TABLE tt-comis NO-UNDO
 FIELD cod-rep    LIKE es_ped_item.cod_repres_1 
 FIELD nome-abrev LIKE repres.nome-abrev
 FIELD vlr-comis LIKE es_ped_item.vlr_net_fob_ef .

{esp/ApiPoliticaPreco.i}

DEFINE VARIABLE h-complem-pedido AS HANDLE      NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ES_PED_ITEM

/* Definitions for DIALOG-BOX D-Dialog                                  */
&Scoped-define QUERY-STRING-D-Dialog FOR EACH ES_PED_ITEM SHARE-LOCK
&Scoped-define OPEN-QUERY-D-Dialog OPEN QUERY D-Dialog FOR EACH ES_PED_ITEM SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-D-Dialog ES_PED_ITEM
&Scoped-define FIRST-TABLE-IN-QUERY-D-Dialog ES_PED_ITEM


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rt-buttom RECT-1 RECT-2 RECT-3 RECT-4 ~
fi-vlr-net-fob-ef fi-vlr-unit-frete PERC_REPRES_1 PERC_REPRES_2 bt-ok ~
bt-cancela 
&Scoped-Define DISPLAYED-OBJECTS fi-nome-abrev fi-nr-pedcli fi-sequencia ~
fi-it-codigo fi-desc-item fi-vlr-net-fob-ef fi-vlr-nf COD_TAB_PRE ~
FAIXA_PRECO COR_FAIXA COD_TAB_FRETE fi-vlr-unit-frete PERC_TX_FINANCEIRA ~
MEDIA_PERC_COMISSAO COD_REPRES_1 COD_REPRES_2 fi-nome-repres-1 ~
fi-nome-repres-2 PERC_REPRES_1 fi-vlr-comis-item-1 PERC_REPRES_2 ~
fi-vlr-comis-item-2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancela AUTO-END-KEY 
     LABEL "&Cancelar" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-ok AUTO-GO 
     LABEL "&OK" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE COD_REPRES_1 LIKE ES_PED_ITEM.COD_REPRES_1
     LABEL "C�digo" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .88 NO-UNDO.

DEFINE VARIABLE COD_REPRES_2 LIKE ES_PED_ITEM.COD_REPRES_2
     LABEL "C�digo" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .88 NO-UNDO.

DEFINE VARIABLE COD_TAB_FRETE AS CHARACTER FORMAT "x(8)" 
     LABEL "Tabela Frete" 
     VIEW-AS FILL-IN 
     SIZE 12.14 BY .88 NO-UNDO.

DEFINE VARIABLE COD_TAB_PRE LIKE ES_PED_ITEM.COD_TAB_PRE
     VIEW-AS FILL-IN 
     SIZE 12.14 BY .88 NO-UNDO.

DEFINE VARIABLE COR_FAIXA AS CHARACTER FORMAT "x(8)" 
     LABEL "Cor Faixa" 
     VIEW-AS FILL-IN 
     SIZE 12.14 BY .88 NO-UNDO.

DEFINE VARIABLE FAIXA_PRECO LIKE ES_PED_ITEM.FAIXA_PRECO
     VIEW-AS FILL-IN 
     SIZE 10 BY .88 NO-UNDO.

DEFINE VARIABLE fi-desc-item AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 42.57 BY .88 NO-UNDO.

DEFINE VARIABLE fi-it-codigo AS CHARACTER FORMAT "X(32)":U 
     LABEL "Item" 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nome-abrev AS CHARACTER FORMAT "X(24)":U 
     LABEL "Cliente" 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nome-repres-1 LIKE repres.nome
     LABEL "Nome" 
     VIEW-AS FILL-IN 
     SIZE 31.29 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nome-repres-2 LIKE repres.nome
     LABEL "Nome" 
     VIEW-AS FILL-IN 
     SIZE 31.29 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nr-pedcli AS CHARACTER FORMAT "X(24)":U 
     LABEL "Pedido Cliente" 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88 NO-UNDO.

DEFINE VARIABLE fi-sequencia AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Seq" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE fi-vlr-comis-item-1 AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Valor Comiss�o" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .88 NO-UNDO.

DEFINE VARIABLE fi-vlr-comis-item-2 AS DECIMAL FORMAT "->>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Valor Comiss�o" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .88 NO-UNDO.

DEFINE VARIABLE fi-vlr-net-fob-ef AS DECIMAL FORMAT "->>>,>>>,>>9.99999":U INITIAL 0 
     LABEL "Valor NET FOB + EF" 
     VIEW-AS FILL-IN 
     SIZE 13.14 BY .88 NO-UNDO.

DEFINE VARIABLE fi-vlr-nf AS DECIMAL FORMAT "->>>,>>>,>>9.99999":U INITIAL 0 
     LABEL "Valor NF" 
     VIEW-AS FILL-IN 
     SIZE 11.72 BY .88 NO-UNDO.

DEFINE VARIABLE fi-vlr-unit-frete AS DECIMAL FORMAT "->>>,>>>,>>9.99999":U INITIAL 0 
     LABEL "Valor Unit�rio Frete" 
     VIEW-AS FILL-IN 
     SIZE 13.14 BY .88 NO-UNDO.

DEFINE VARIABLE MEDIA_PERC_COMISSAO LIKE ES_PED_ITEM.MEDIA_PERC_COMISSAO
     VIEW-AS FILL-IN 
     SIZE 9.14 BY .88 NO-UNDO.

DEFINE VARIABLE PERC_REPRES_1 LIKE ES_PED_ITEM.PERC_REPRES_1
     LABEL "% Comiss�o" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .88 NO-UNDO.

DEFINE VARIABLE PERC_REPRES_2 LIKE ES_PED_ITEM.PERC_REPRES_2
     LABEL "% Comiss�o" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .88 NO-UNDO.

DEFINE VARIABLE PERC_TX_FINANCEIRA LIKE ES_PED_ITEM.PERC_TX_FINANCEIRA
     LABEL "% Tx Financeira" 
     VIEW-AS FILL-IN 
     SIZE 9.14 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84.43 BY 2.75.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84.43 BY 4.29.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42 BY 3.67.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42 BY 3.67.

DEFINE RECTANGLE rt-buttom
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 84.43 BY 1.42
     BGCOLOR 7 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY D-Dialog FOR 
      ES_PED_ITEM SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     fi-nome-abrev AT ROW 1.46 COL 13.29 COLON-ALIGNED WIDGET-ID 32
     fi-nr-pedcli AT ROW 1.46 COL 44.14 COLON-ALIGNED WIDGET-ID 34
     fi-sequencia AT ROW 1.46 COL 66.57 COLON-ALIGNED WIDGET-ID 38
     fi-it-codigo AT ROW 2.46 COL 13.29 COLON-ALIGNED WIDGET-ID 30
     fi-desc-item AT ROW 2.46 COL 30 COLON-ALIGNED NO-LABEL WIDGET-ID 28
     fi-vlr-net-fob-ef AT ROW 4.08 COL 19.86 COLON-ALIGNED WIDGET-ID 42
     fi-vlr-nf AT ROW 4.08 COL 47.72 COLON-ALIGNED WIDGET-ID 78
     COD_TAB_PRE AT ROW 5.04 COL 19.86 COLON-ALIGNED HELP
          "" WIDGET-ID 66
     FAIXA_PRECO AT ROW 5.04 COL 47.72 COLON-ALIGNED HELP
          "" WIDGET-ID 70
     COR_FAIXA AT ROW 5.04 COL 70 COLON-ALIGNED WIDGET-ID 80
     COD_TAB_FRETE AT ROW 6 COL 19.86 COLON-ALIGNED WIDGET-ID 82
     fi-vlr-unit-frete AT ROW 6 COL 47.72 COLON-ALIGNED WIDGET-ID 44
     PERC_TX_FINANCEIRA AT ROW 6.96 COL 19.86 COLON-ALIGNED HELP
          "" WIDGET-ID 74
          LABEL "% Tx Financeira"
     MEDIA_PERC_COMISSAO AT ROW 6.96 COL 47.72 COLON-ALIGNED HELP
          "" WIDGET-ID 72
     COD_REPRES_1 AT ROW 9.25 COL 8.72 COLON-ALIGNED HELP
          "" WIDGET-ID 58
          LABEL "C�digo"
     COD_REPRES_2 AT ROW 9.25 COL 51 COLON-ALIGNED HELP
          "" WIDGET-ID 60
          LABEL "C�digo"
     fi-nome-repres-1 AT ROW 10.21 COL 8.72 COLON-ALIGNED HELP
          "" WIDGET-ID 88
          LABEL "Nome"
     fi-nome-repres-2 AT ROW 10.21 COL 51 COLON-ALIGNED HELP
          "" WIDGET-ID 90
          LABEL "Nome"
     PERC_REPRES_1 AT ROW 11.17 COL 8.72 COLON-ALIGNED HELP
          "" WIDGET-ID 62
          LABEL "% Comiss�o"
     fi-vlr-comis-item-1 AT ROW 11.17 COL 29 COLON-ALIGNED WIDGET-ID 40
     PERC_REPRES_2 AT ROW 11.17 COL 51 COLON-ALIGNED HELP
          "" WIDGET-ID 64
          LABEL "% Comiss�o"
     fi-vlr-comis-item-2 AT ROW 11.17 COL 71.14 COLON-ALIGNED WIDGET-ID 76
     bt-ok AT ROW 12.83 COL 3
     bt-cancela AT ROW 12.83 COL 14
     "Representante Agente" VIEW-AS TEXT
          SIZE 16 BY .54 AT ROW 8.5 COL 45 WIDGET-ID 86
     "Representante Principal" VIEW-AS TEXT
          SIZE 18 BY .54 AT ROW 8.5 COL 3 WIDGET-ID 84
     rt-buttom AT ROW 12.58 COL 1.29
     RECT-1 AT ROW 1.04 COL 1.29 WIDGET-ID 46
     RECT-2 AT ROW 3.83 COL 1.29 WIDGET-ID 48
     RECT-3 AT ROW 8.83 COL 1.29 WIDGET-ID 50
     RECT-4 AT ROW 8.83 COL 43.57 WIDGET-ID 52
     SPACE(0.28) SKIP(1.66)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Comiss�o Item"
         DEFAULT-BUTTON bt-ok WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-dialog.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME L-To-R                                                    */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN COD_REPRES_1 IN FRAME D-Dialog
   NO-ENABLE LIKE = mgnitro.ES_PED_ITEM. EXP-LABEL EXP-SIZE             */
/* SETTINGS FOR FILL-IN COD_REPRES_2 IN FRAME D-Dialog
   NO-ENABLE LIKE = mgnitro.ES_PED_ITEM. EXP-LABEL EXP-SIZE             */
/* SETTINGS FOR FILL-IN COD_TAB_FRETE IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN COD_TAB_PRE IN FRAME D-Dialog
   NO-ENABLE LIKE = mgnitro.ES_PED_ITEM. EXP-SIZE                       */
/* SETTINGS FOR FILL-IN COR_FAIXA IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN FAIXA_PRECO IN FRAME D-Dialog
   NO-ENABLE LIKE = mgnitro.ES_PED_ITEM. EXP-SIZE                       */
/* SETTINGS FOR FILL-IN fi-desc-item IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-it-codigo IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-nome-abrev IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-nome-repres-1 IN FRAME D-Dialog
   NO-ENABLE LIKE = repres.nome EXP-LABEL EXP-SIZE */
/* SETTINGS FOR FILL-IN fi-nome-repres-2 IN FRAME D-Dialog
   NO-ENABLE LIKE = repres.nome EXP-LABEL EXP-SIZE */
/* SETTINGS FOR FILL-IN fi-nr-pedcli IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-sequencia IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-vlr-comis-item-1 IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-vlr-comis-item-2 IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-vlr-nf IN FRAME D-Dialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN MEDIA_PERC_COMISSAO IN FRAME D-Dialog
   NO-ENABLE LIKE = mgnitro.ES_PED_ITEM. EXP-SIZE                       */
/* SETTINGS FOR FILL-IN PERC_REPRES_1 IN FRAME D-Dialog
   LIKE = mgnitro.ES_PED_ITEM. EXP-LABEL EXP-SIZE                       */
/* SETTINGS FOR FILL-IN PERC_REPRES_2 IN FRAME D-Dialog
   LIKE = mgnitro.ES_PED_ITEM. EXP-LABEL EXP-SIZE                       */
/* SETTINGS FOR FILL-IN PERC_TX_FINANCEIRA IN FRAME D-Dialog
   NO-ENABLE LIKE = mgnitro.ES_PED_ITEM. EXP-LABEL EXP-SIZE             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _TblList          = "mgnitro.ES_PED_ITEM"
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Comiss�o Item */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok D-Dialog
ON CHOOSE OF bt-ok IN FRAME D-Dialog /* OK */
DO:
  
    RUN pi-grava-dados.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-vlr-net-fob-ef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-vlr-net-fob-ef D-Dialog
ON LEAVE OF fi-vlr-net-fob-ef IN FRAME D-Dialog /* Valor NET FOB + EF */
DO:                  

    DEFINE VARIABLE i-qtd-rep AS INTEGER     NO-UNDO.

    IF SELF:SCREEN-VALUE = ? OR dec(SELF:SCREEN-VALUE) = 0 THEN RETURN.

    FIND FIRST ped-item NO-LOCK
         WHERE ROWID(ped-item) = pd4000u19-row-ped-item NO-ERROR.
    IF NOT AVAIL ped-item THEN RETURN.

    FIND FIRST ped-venda OF ped-item NO-LOCK NO-ERROR.
    IF NOT AVAIL ped-venda THEN RETURN.
                                    
    IF NOT AVAIL es_ped_venda THEN RETURN.

    FIND FIRST repres NO-LOCK 
         WHERE repres.nome-abrev = ped-venda.no-ab-rep NO-ERROR.

    EMPTY TEMP-TABLE ttParametros2.
    EMPTY TEMP-TABLE ttOutrosItens.

    CREATE ttParametros2.
    ASSIGN ttParametros2.cCodEstabel       = ped-venda.cod-estabel
           ttParametros2.iCodEmitente      = ped-venda.cod-emitente
           ttParametros2.cItCodigo         = ped-item.it-codigo
           ttParametros2.DtImplant         = ped-venda.dt-implant
           ttParametros2.iCodRep           = IF AVAIL repres THEN repres.cod-rep ELSE 0
           ttParametros2.dePrecoNetFob     = DEC(SELF:SCREEN-VALUE)
           ttParametros2.deQuantidade      = ped-item.qt-pedida
           ttParametros2.deValorFrete      = 0
           ttParametros2.iTipoVenda        = es_ped_venda.tipo-venda       
           ttParametros2.iTipoCarregamento = es_ped_venda.cod_tipo_carga
           ttParametros2.cCidade           = emitente.cidade
           ttParametros2.cUf               = emitente.estado
           ttParametros2.cCodCampanha      = es_ped_venda.campanha
           ttParametros2.cCodCultura       = es_ped_venda.cultura.

    FOR EACH bf-ped-item OF ped-venda WHERE
             bf-ped-item.nr-sequencia <> ped-item.nr-sequencia NO-LOCK:
        CREATE ttOutrosItens.
        ASSIGN ttOutrosItens.cItCodigo    = bf-ped-item.it-codigo
               ttOutrosItens.deQuantidade = bf-ped-item.qt-pedida.
    END.
                     
    RUN esp/ApiPoliticaPreco.p PERSISTENT SET h-complem-pedido.
    RUN piBuscaComplemento IN h-complem-pedido (INPUT TABLE ttParametros2, INPUT TABLE ttOutrosItens, OUTPUT TABLE ttRetorno2, OUTPUT TABLE rowErrors).
    DELETE PROCEDURE h-complem-pedido.

    FIND FIRST ttRetorno2 NO-LOCK NO-ERROR.
    IF AVAIL ttRetorno2 THEN DO:
        ASSIGN fi-vlr-nf:SCREEN-VALUE IN FRAME {&FRAME-NAME}          = STRING(ttRetorno2.dePrecoNf)
               cod_tab_pre:SCREEN-VALUE IN FRAME {&FRAME-NAME}        = ttRetorno2.cCodTabPreco
               FAIXA_PRECO:SCREEN-VALUE IN FRAME {&FRAME-NAME}        = STRING(ttRetorno2.deFaixaPreco)
               COR_FAIXA:SCREEN-VALUE IN FRAME {&FRAME-NAME}          = ttRetorno2.cCorFaixa
               COD_TAB_FRETE:SCREEN-VALUE IN FRAME {&FRAME-NAME}      = ttRetorno2.cCodTabFrete
               fi-vlr-unit-frete:SCREEN-VALUE IN FRAME {&FRAME-NAME}  = STRING(ttRetorno2.deValorFrete)
               PERC_TX_FINANCEIRA:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(ttRetorno2.deTaxaFinanceira).

        i-qtd-rep = 0.
        FOR EACH ped-repre NO-LOCK
           WHERE ped-repre.nr-pedido = ped-venda.nr-pedido,
           FIRST repres NO-LOCK
           WHERE repres.nome-abrev = ped-repre.nome-ab-rep:
    
            ASSIGN i-qtd-rep     = i-qtd-rep + 1. 
    
            IF i-qtd-rep = 1 THEN 
                ASSIGN cod_repres_1:SCREEN-VALUE IN FRAME {&FRAME-NAME}        = STRING(repres.cod-rep)
                       fi-nome-repres-1:SCREEN-VALUE IN FRAME {&FRAME-NAME}    = repres.nome
                       perc_repres_1:SCREEN-VALUE IN FRAME {&FRAME-NAME}       = STRING(ttRetorno2.dePercComissao)
                       fi-vlr-comis-item-1:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING((DEC(fi-vlr-net-fob-ef:SCREEN-VALUE IN FRAME {&FRAME-NAME}) * ped-item.qt-pedida) * (DEC(perc_repres_1:SCREEN-VALUE IN FRAME {&FRAME-NAME}) / 100)).
    
            IF i-qtd-rep = 2 THEN 
                ASSIGN cod_repres_2:SCREEN-VALUE IN FRAME {&FRAME-NAME}        = STRING(repres.cod-rep)
                       fi-nome-repres-2:SCREEN-VALUE IN FRAME {&FRAME-NAME}    = repres.nome.
        END.

    END.        

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-vlr-unit-frete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-vlr-unit-frete D-Dialog
ON LEAVE OF fi-vlr-unit-frete IN FRAME D-Dialog /* Valor Unit�rio Frete */
DO:                          

    IF SELF:SCREEN-VALUE = ? OR dec(SELF:SCREEN-VALUE) = 0 THEN RETURN.

    FIND FIRST ped-item NO-LOCK
         WHERE ROWID(ped-item) = pd4000u19-row-ped-item NO-ERROR.
    IF NOT AVAIL ped-item THEN RETURN.

    FIND FIRST ped-venda OF ped-item NO-LOCK NO-ERROR.
    IF NOT AVAIL ped-venda THEN RETURN.
                                    
    IF NOT AVAIL es_ped_venda THEN RETURN.

    FIND FIRST repres NO-LOCK 
         WHERE repres.nome-abrev = ped-venda.no-ab-rep NO-ERROR.
                             
    EMPTY TEMP-TABLE ttParametros2.
    EMPTY TEMP-TABLE ttOutrosItens.

    CREATE ttParametros2.
    ASSIGN ttParametros2.cCodEstabel       = ped-venda.cod-estabel
           ttParametros2.iCodEmitente      = ped-venda.cod-emitente
           ttParametros2.cItCodigo         = ped-item.it-codigo
           ttParametros2.DtImplant         = ped-venda.dt-implant
           ttParametros2.iCodRep           = IF AVAIL repres THEN repres.cod-rep ELSE 0
           ttParametros2.dePrecoNetFob     = DEC(fi-vlr-net-fob-ef:SCREEN-VALUE IN FRAME {&FRAME-NAME})
           ttParametros2.deQuantidade      = ped-item.qt-pedida
           ttParametros2.deValorFrete      = DEC(SELF:SCREEN-VALUE)
           ttParametros2.iTipoVenda        = es_ped_venda.tipo-venda       
           ttParametros2.iTipoCarregamento = es_ped_venda.cod_tipo_carga
           ttParametros2.cCidade           = emitente.cidade
           ttParametros2.cUf               = emitente.estado
           ttParametros2.cCodCampanha      = es_ped_venda.campanha
           ttParametros2.cCodCultura       = es_ped_venda.cultura.

    FOR EACH bf-ped-item OF ped-venda WHERE
             bf-ped-item.nr-sequencia <> ped-item.nr-sequencia NO-LOCK:
        CREATE ttOutrosItens.
        ASSIGN ttOutrosItens.cItCodigo    = bf-ped-item.it-codigo
               ttOutrosItens.deQuantidade = bf-ped-item.qt-pedida.
    END.

    RUN esp/ApiPoliticaPreco.p PERSISTENT SET h-complem-pedido.
    RUN piBuscaComplemento IN h-complem-pedido (INPUT TABLE ttParametros2, INPUT TABLE ttOutrosItens, OUTPUT TABLE ttRetorno2, OUTPUT TABLE rowErrors).
    DELETE PROCEDURE h-complem-pedido.

    FIND FIRST ttRetorno2 NO-LOCK NO-ERROR.
    IF AVAIL ttRetorno2 THEN DO:
        ASSIGN fi-vlr-nf:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(ttRetorno2.dePrecoNf).
    END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME PERC_REPRES_1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL PERC_REPRES_1 D-Dialog
ON LEAVE OF PERC_REPRES_1 IN FRAME D-Dialog /* % Comiss�o */
DO:
    FIND FIRST ped-item NO-LOCK
         WHERE ROWID(ped-item) = pd4000u19-row-ped-item NO-ERROR.
    IF AVAIL ped-item THEN DO:
        IF SELF:SCREEN-VALUE <> ? THEN DO:
    
            ASSIGN fi-vlr-comis-item-1 = (DEC(fi-vlr-net-fob-ef:SCREEN-VALUE IN FRAME {&FRAME-NAME}) * ped-item.qt-pedida) * (DEC(perc_repres_1:SCREEN-VALUE IN FRAME {&FRAME-NAME}) / 100).
                  
            DISPLAY fi-vlr-comis-item-1 WITH FRAME {&FRAME-NAME}.
    
        END.
    END.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME PERC_REPRES_2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL PERC_REPRES_2 D-Dialog
ON LEAVE OF PERC_REPRES_2 IN FRAME D-Dialog /* % Comiss�o */
DO:
    FIND FIRST ped-item NO-LOCK
         WHERE ROWID(ped-item) = pd4000u19-row-ped-item NO-ERROR.
    IF AVAIL ped-item THEN DO:
        IF SELF:SCREEN-VALUE <> ? THEN DO:
    
            ASSIGN fi-vlr-comis-item-2 = (DEC(fi-vlr-net-fob-ef:SCREEN-VALUE IN FRAME {&FRAME-NAME}) * ped-item.qt-pedida) * (DEC(perc_repres_2:SCREEN-VALUE IN FRAME {&FRAME-NAME}) / 100).
                  
            DISPLAY fi-vlr-comis-item-2 WITH FRAME {&FRAME-NAME}.
    
        END.
    END.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fi-nome-abrev fi-nr-pedcli fi-sequencia fi-it-codigo fi-desc-item 
          fi-vlr-net-fob-ef fi-vlr-nf COD_TAB_PRE FAIXA_PRECO COR_FAIXA 
          COD_TAB_FRETE fi-vlr-unit-frete PERC_TX_FINANCEIRA MEDIA_PERC_COMISSAO 
          COD_REPRES_1 COD_REPRES_2 fi-nome-repres-1 fi-nome-repres-2 
          PERC_REPRES_1 fi-vlr-comis-item-1 PERC_REPRES_2 fi-vlr-comis-item-2 
      WITH FRAME D-Dialog.
  ENABLE rt-buttom RECT-1 RECT-2 RECT-3 RECT-4 fi-vlr-net-fob-ef 
         fi-vlr-unit-frete PERC_REPRES_1 PERC_REPRES_2 bt-ok bt-cancela 
      WITH FRAME D-Dialog.
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy D-Dialog 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  {utp/ut9000.i "ESPD4000-U19A" "9.99.99.999"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN pi-carrega-dados.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-carrega-dados D-Dialog 
PROCEDURE pi-carrega-dados :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE i-qtd-rep AS INTEGER     NO-UNDO.

IF  VALID-HANDLE (wh-pd4000u19-nome-abrev) 
AND VALID-HANDLE (wh-pd4000u19-nr-pedcli) 
AND VALID-HANDLE (wh-pd4000u19-it-codigo)  
AND VALID-HANDLE (wh-pd4000u19-nr-sequencia) THEN DO:
 
    FIND FIRST ITEM NO-LOCK
         WHERE ITEM.it-codigo = wh-pd4000u19-it-codigo:SCREEN-VALUE NO-ERROR.
    IF AVAIL ITEM THEN
        ASSIGN fi-desc-item = ITEM.desc-item.

    ASSIGN fi-nome-abrev = wh-pd4000u19-nome-abrev:SCREEN-VALUE         
           fi-nr-pedcli  = wh-pd4000u19-nr-pedcli:SCREEN-VALUE          
           fi-sequencia  = INT(wh-pd4000u19-nr-sequencia:SCREEN-VALUE)
           fi-it-codigo  = wh-pd4000u19-it-codigo:SCREEN-VALUE.
        
    FIND FIRST ped-venda NO-LOCK
         WHERE ROWID(ped-venda) = pd4000u19-row-ped-venda NO-ERROR.
    IF AVAIL ped-venda THEN DO:
        FIND FIRST emitente WHERE
                   emitente.cod-emitente = ped-venda.cod-emitente NO-LOCK NO-ERROR.

        FIND FIRST es_ped_venda WHERE 
                   es_ped_venda.nr_pedido = ped-venda.nr-pedido NO-LOCK NO-ERROR.

        FOR EACH ped-repre NO-LOCK
          WHERE ped-repre.nr-pedido = ped-venda.nr-pedido,
        FIRST repres NO-LOCK
          WHERE repres.nome-abrev = ped-repre.nome-ab-rep:

            ASSIGN i-qtd-rep     = i-qtd-rep + 1. 

            IF i-qtd-rep = 1 THEN 
                ASSIGN cod_repres_1     = repres.cod-rep
                       fi-nome-repres-1 = repres.nome.

            IF i-qtd-rep = 2 THEN 
                ASSIGN cod_repres_2     = repres.cod-rep
                       fi-nome-repres-2 = repres.nome.
        END.
    END.

    DISPLAY fi-nome-abrev
            fi-nr-pedcli 
            fi-sequencia 
            fi-it-codigo 
            fi-desc-item
            cod_repres_1
            cod_repres_2
            fi-nome-repres-1
            fi-nome-repres-2
            WITH FRAME {&FRAME-NAME}.

    /* Desabilitar campo representante 2 quando n�o existir */
    IF i-qtd-rep = 1 THEN
        DISABLE perc_repres_2 WITH FRAME {&FRAME-NAME}.

    FIND FIRST es_ped_item NO-LOCK
         WHERE es_ped_item.nome_abrev   = wh-pd4000u19-nome-abrev:SCREEN-VALUE
           AND es_ped_item.nr_pedcli    = wh-pd4000u19-nr-pedcli:SCREEN-VALUE
           AND es_ped_item.nr_sequencia = INT(wh-pd4000u19-nr-sequencia:SCREEN-VALUE)
           AND es_ped_item.it_codigo    = wh-pd4000u19-it-codigo:SCREEN-VALUE NO-ERROR.
    IF AVAIL es_ped_item THEN DO:

        ASSIGN fi-vlr-net-fob-ef = es_ped_item.vlr_net_fob_ef
               fi-vlr-unit-frete = es_ped_item.vlr_unit_frete.

        /* Politica */
        ASSIGN cod_tab_pre         = es_ped_item.cod_tab_pre        
               faixa_preco         = es_ped_item.faixa_preco        
               media_perc_comissao = es_ped_item.media_perc_comissao
               perc_tx_financeira  = es_ped_item.perc_tx_financeira 
        /* Representate 1 */      
               perc_repres_1       = es_ped_item.perc_repres_1      
        /* Representate 2 */      
               perc_repres_2       = es_ped_item.perc_repres_2 .

        FIND FIRST ped-item NO-LOCK
             WHERE ROWID(ped-item) = pd4000u19-row-ped-item NO-ERROR.
        IF AVAIL ped-item THEN DO:
            ASSIGN fi-vlr-comis-item-1 = (es_ped_item.vlr_net_fob_ef * ped-item.qt-pedida) * (es_ped_item.perc_repres_1 / 100)
                   fi-vlr-comis-item-2 = (es_ped_item.vlr_net_fob_ef * ped-item.qt-pedida) * (es_ped_item.perc_repres_2 / 100).

            DISPLAY fi-vlr-comis-item-1
                    fi-vlr-comis-item-2 WITH FRAME {&FRAME-NAME}.
        END.

        DISPLAY fi-vlr-net-fob-ef 
                fi-vlr-unit-frete 
                cod_tab_pre        
                faixa_preco        
                media_perc_comissao
                perc_tx_financeira 
                cod_repres_1  
                perc_repres_1
                cod_repres_2  
                perc_repres_2 
                WITH FRAME {&FRAME-NAME}.

        IF AVAIL ped-venda THEN DO:
            IF AVAIL es_ped_venda THEN DO:
                FIND FIRST repres NO-LOCK 
                     WHERE repres.nome-abrev = ped-venda.no-ab-rep NO-ERROR.
            
                EMPTY TEMP-TABLE ttParametros2.
                EMPTY TEMP-TABLE ttOutrosItens.

                CREATE ttParametros2.
                ASSIGN ttParametros2.cCodEstabel       = ped-venda.cod-estabel
                       ttParametros2.iCodEmitente      = ped-venda.cod-emitente
                       ttParametros2.cItCodigo         = es_ped_item.it_codigo
                       ttParametros2.DtImplant         = ped-venda.dt-implant
                       ttParametros2.iCodRep           = IF AVAIL repres THEN repres.cod-rep ELSE 0
                       ttParametros2.dePrecoNetFob     = es_ped_item.vlr_net_fob_ef
                       ttParametros2.deQuantidade      = ped-item.qt-pedida
                       ttParametros2.deValorFrete      = es_ped_item.vlr_unit_frete
                       ttParametros2.iTipoVenda        = es_ped_venda.tipo-venda       
                       ttParametros2.iTipoCarregamento = es_ped_venda.cod_tipo_carga
                       ttParametros2.cCidade           = emitente.cidade
                       ttParametros2.cUf               = emitente.estado
                       ttParametros2.cCodCampanha      = es_ped_venda.campanha
                       ttParametros2.cCodCultura       = es_ped_venda.cultura.

                FOR EACH bf-ped-item OF ped-venda WHERE
                         bf-ped-item.nr-sequencia <> ped-item.nr-sequencia NO-LOCK:
                    CREATE ttOutrosItens.
                    ASSIGN ttOutrosItens.cItCodigo    = bf-ped-item.it-codigo
                           ttOutrosItens.deQuantidade = bf-ped-item.qt-pedida.
                END.
            
                RUN esp/ApiPoliticaPreco.p PERSISTENT SET h-complem-pedido.
                RUN piBuscaComplemento IN h-complem-pedido (INPUT TABLE ttParametros2, INPUT TABLE ttOutrosItens, OUTPUT TABLE ttRetorno2, OUTPUT TABLE rowErrors).
                DELETE PROCEDURE h-complem-pedido.
            
                FIND FIRST ttRetorno2 NO-LOCK NO-ERROR.
                IF AVAIL ttRetorno2 THEN DO:
                    ASSIGN fi-vlr-nf:SCREEN-VALUE IN FRAME {&FRAME-NAME}     = STRING(ttRetorno2.dePrecoNf)
                           COR_FAIXA:SCREEN-VALUE IN FRAME {&FRAME-NAME}     = ttRetorno2.cCorFaixa
                           COD_TAB_FRETE:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ttRetorno2.cCodTabFrete .
                END.
            END.
        END.
    END.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-grava-dados D-Dialog 
PROCEDURE pi-grava-dados :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE de-vlr-tot-item     AS DECIMAL     NO-UNDO.
DEFINE VARIABLE de-vlr-comis-item   AS DECIMAL     NO-UNDO.
DEFINE VARIABLE de-vlr-comis-rep-1  AS DECIMAL     NO-UNDO.
DEFINE VARIABLE de-vlr-comis-rep-2  AS DECIMAL     NO-UNDO.


IF  VALID-HANDLE (wh-pd4000u19-nome-abrev) 
AND VALID-HANDLE (wh-pd4000u19-nr-pedcli) 
AND VALID-HANDLE (wh-pd4000u19-it-codigo)  
AND VALID-HANDLE (wh-pd4000u19-nr-sequencia) THEN DO:

    FIND FIRST ped-venda NO-LOCK
         WHERE ROWID(ped-venda) = pd4000u19-row-ped-venda NO-ERROR.
    IF AVAIL ped-venda THEN DO:    
        CREATE ttItemPedido.
        ASSIGN ttItemPedido.iNrPedido         = ped-venda.nr-pedido
               ttItemPedido.iNrSequencia      = INT(wh-pd4000u19-nr-sequencia:SCREEN-VALUE)
               ttItemPedido.deValorNetFob     = DEC(fi-vlr-net-fob-ef:SCREEN-VALUE IN FRAME {&FRAME-NAME})
               ttItemPedido.deValorFrete      = DEC(fi-vlr-unit-frete:SCREEN-VALUE IN FRAME {&FRAME-NAME})  
               ttItemPedido.dePercComissao    = DEC(PERC_REPRES_1:SCREEN-VALUE IN FRAME {&FRAME-NAME})
               ttItemPedido.dePercComissaoAgt = DEC(PERC_REPRES_2:SCREEN-VALUE IN FRAME {&FRAME-NAME}).

        RUN esp/ApiPoliticaPreco.p PERSISTENT SET h-complem-pedido.
        RUN piCriaComplementoItemPedido IN h-complem-pedido (INPUT TABLE ttItemPedido).
        DELETE PROCEDURE h-complem-pedido.        

        /* Calcula media ponderada dos itens para encontrar o percentual de comissao final do pedido */
        FOR EACH bf-ped-item OF ped-venda NO-LOCK:

            FIND FIRST es_ped_item NO-LOCK
                 WHERE es_ped_item.nome_abrev   = bf-ped-item.nome-abrev  
                   AND es_ped_item.nr_pedcli    = bf-ped-item.nr-pedcli   
                   AND es_ped_item.nr_sequencia = bf-ped-item.nr-sequencia
                   AND es_ped_item.it_codigo    = bf-ped-item.it-codigo    NO-ERROR.
            IF AVAIL es_ped_item THEN DO:
                
                ASSIGN de-vlr-tot-item      = de-vlr-tot-item     + (bf-ped-item.qt-pedida * es_ped_item.vlr_net_fob_ef).
                
                /* representate 1 */
                IF es_ped_item.cod_repres_1 <> 0 THEN DO:

                    FIND FIRST repres NO-LOCK
                      WHERE repres.cod-rep = es_ped_item.cod_repres_1 NO-ERROR.
                    IF AVAIL repres THEN DO:

                        FIND FIRST tt-comis NO-LOCK
                         WHERE tt-comis.cod-rep = es_ped_item.cod_repres_1 NO-ERROR.
                        IF NOT AVAIL tt-comis THEN DO:
    
                            CREATE tt-comis.
                            ASSIGN tt-comis.cod-rep    = es_ped_item.cod_repres_1
                                   tt-comis.nome-abrev = repres.nome-abrev
                                   tt-comis.vlr-comis  = (es_ped_item.vlr_net_fob_ef * bf-ped-item.qt-pedida) * (es_ped_item.perc_repres_1 / 100).
                        END.
                        ELSE DO:
    
                            ASSIGN tt-comis.vlr-comis = tt-comis.vlr-comis + (es_ped_item.vlr_net_fob_ef * bf-ped-item.qt-pedida) * (es_ped_item.perc_repres_1 / 100).
                        END.
                    END.
                END.

                /* representante 2*/
                IF es_ped_item.cod_repres_2 <> 0 THEN DO:

                    FIND FIRST repres NO-LOCK
                      WHERE repres.cod-rep = es_ped_item.cod_repres_2 NO-ERROR.
                    IF AVAIL repres THEN DO:

                        FIND FIRST tt-comis NO-LOCK
                         WHERE tt-comis.cod-rep = es_ped_item.cod_repres_2 NO-ERROR.
                        IF NOT AVAIL tt-comis THEN DO:
    
                            CREATE tt-comis.
                            ASSIGN tt-comis.cod-rep   = es_ped_item.cod_repres_2
                                   tt-comis.nome-abrev = repres.nome-abrev
                                   tt-comis.vlr-comis = (es_ped_item.vlr_net_fob_ef * bf-ped-item.qt-pedida) * (es_ped_item.perc_repres_2 / 100).
                        END.
                        ELSE DO:
    
                            ASSIGN tt-comis.vlr-comis = tt-comis.vlr-comis + (es_ped_item.vlr_net_fob_ef * bf-ped-item.qt-pedida) * (es_ped_item.perc_repres_2 / 100).
                        END.
                    END.
                END.
            END.
        END.

        FOR EACH ped-repre EXCLUSIVE-LOCK
          WHERE ped-repre.nr-pedido   = ped-venda.nr-pedido:

            FIND FIRST tt-comis NO-LOCK
              WHERE tt-comis.nome-abrev = ped-repre.nome-ab-rep NO-ERROR.
            IF AVAIL tt-comis THEN DO:

                IF  ((tt-comis.vlr-comis / de-vlr-tot-item) * 100) > 0
                AND ((tt-comis.vlr-comis / de-vlr-tot-item) * 100) <> ? THEN DO:
    
                    ASSIGN ped-repre.perc-comis = (tt-comis.vlr-comis / de-vlr-tot-item) * 100.
                END.
                ELSE 
                    ASSIGN ped-repre.perc-comis = 0.
            END.
        END.
        RELEASE ped-repre NO-ERROR.
                      
        FIND FIRST ped-item NO-LOCK
             WHERE ROWID(ped-item) = pd4000u19-row-ped-item NO-ERROR.
        IF AVAIL ped-item AND AVAIL ped-venda AND AVAIL es_ped_venda THEN DO: 
            FIND FIRST es_ped_item NO-LOCK
                 WHERE es_ped_item.nome_abrev   = ped-item.nome-abrev  
                   AND es_ped_item.nr_pedcli    = ped-item.nr-pedcli   
                   AND es_ped_item.nr_sequencia = ped-item.nr-sequencia
                   AND es_ped_item.it_codigo    = ped-item.it-codigo    NO-ERROR.
            IF AVAIL es_ped_item THEN DO:
                FIND FIRST repres NO-LOCK 
                     WHERE repres.nome-abrev = ped-venda.no-ab-rep NO-ERROR.

                EMPTY TEMP-TABLE ttParametros2.
                EMPTY TEMP-TABLE ttOutrosItens.

                CREATE ttParametros2.
                ASSIGN ttParametros2.cCodEstabel       = ped-venda.cod-estabel
                       ttParametros2.iCodEmitente      = ped-venda.cod-emitente
                       ttParametros2.cItCodigo         = es_ped_item.it_codigo
                       ttParametros2.DtImplant         = ped-venda.dt-implant
                       ttParametros2.iCodRep           = IF AVAIL repres THEN repres.cod-rep ELSE 0
                       ttParametros2.dePrecoNetFob     = es_ped_item.vlr_net_fob_ef
                       ttParametros2.deQuantidade      = ped-item.qt-pedida
                       ttParametros2.deValorFrete      = es_ped_item.vlr_unit_frete
                       ttParametros2.iTipoVenda        = es_ped_venda.tipo-venda       
                       ttParametros2.iTipoCarregamento = es_ped_venda.cod_tipo_carga
                       ttParametros2.cCidade           = emitente.cidade
                       ttParametros2.cUf               = emitente.estado
                       ttParametros2.cCodCampanha      = es_ped_venda.campanha
                       ttParametros2.cCodCultura       = es_ped_venda.cultura.

                FOR EACH bf-ped-item OF ped-venda WHERE
                         bf-ped-item.nr-sequencia <> ped-item.nr-sequencia NO-LOCK:
                    CREATE ttOutrosItens.
                    ASSIGN ttOutrosItens.cItCodigo    = bf-ped-item.it-codigo
                           ttOutrosItens.deQuantidade = bf-ped-item.qt-pedida.
                END.
            
                RUN esp/ApiPoliticaPreco.p PERSISTENT SET h-complem-pedido.
                RUN piBuscaComplemento IN h-complem-pedido (INPUT TABLE ttParametros2, INPUT TABLE ttOutrosItens, OUTPUT TABLE ttRetorno2, OUTPUT TABLE rowErrors).
                DELETE PROCEDURE h-complem-pedido.
            
                FIND FIRST ttRetorno2 NO-LOCK NO-ERROR.
                IF AVAIL ttRetorno2 THEN DO:
                    IF VALID-HANDLE(wh-pd4000u19-vl-preori) THEN
                        ASSIGN wh-pd4000u19-vl-preori:SCREEN-VALUE = STRING(ttRetorno2.dePrecoNf).
                    IF VALID-HANDLE(wh-pd4000u19-vl-preori-un-fat) THEN
                        ASSIGN wh-pd4000u19-vl-preori-un-fat:SCREEN-VALUE = STRING(ttRetorno2.dePrecoNf).
                END.
            END.
        END.

    END. 
END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "ES_PED_ITEM"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

