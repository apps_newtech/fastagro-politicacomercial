/*
**    Programa: escd0708-u00
**    Objetivo: Chamador de UPC espec�fica
**       Autor: Anderson - Newtech
** Atualiza��o: 17/05/2021
*/

def input param p-ind-event  as char          no-undo.
def input param p-ind-object as char          no-undo.
def input param p-wgh-object as handle        no-undo.
def input param p-wgh-frame  as widget-handle no-undo.
def input param p-cod-table  as char          no-undo.
def input param p-row-table  as rowid         no-undo.

/** Por Anderson - Newtech em 14.02.2018 */
run espupc/escd0708-u01.p (input p-ind-event,
                           input p-ind-object,
                           input p-wgh-object,
                           input p-wgh-frame,
                           input p-cod-table,
                           input p-row-table).
IF RETURN-VALUE = "NOK" THEN 
   RETURN "NOK".
   
RETURN "OK".
