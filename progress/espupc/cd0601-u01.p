/*  
:: ******************************************************************************************************
:: * Produto.........: [Datasul 12]
:: * Programa .......: upc\upc-cd0601.p
:: * Descricao ......: Customiza��o no Cadastro de Depositos | Integra��o Manusis
:: * Procedimento ...: Cadastros Gerais.
:: * Responsavel.....: [Fernando Farah] [10/03/2020]
:: * Desenvolvido Por: [Fernando Farah] [10/03/2020]
:: * Liberado em.....: 
:: * Versao..........: 1.00.000
:: * Cliente.........: [CATTALINI]
:: **************************************************************************************************** */ 

{include/DefManutencaoItem.i}
/****************** DEFINICAO PARAMETROS ********************/
def input param p-ind-event  as char          no-undo.
def input param p-ind-object as char          no-undo.
def input param p-wgh-object as handle        no-undo.
def input param p-wgh-frame  as widget-handle no-undo.
def input param p-cod-table  as char          no-undo.
def input param p-row-table  as rowid         no-undo.

{utp/ut-glob.i}


DEFINE VARIABLE c-objeto           as char          no-undo.

DEFINE NEW GLOBAL SHARED VARIABLE wh-ge-nome      as widget-handle no-undo.
DEFINE NEW GLOBAL SHARED VARIABLE tg-integra-manusis    AS WIDGET-HANDLE NO-UNDO.



assign  c-objeto = entry(num-entries(p-wgh-object:private-data, "~/"), p-wgh-object:private-data, "~/").

/* MESSAGE "Evento..........:" string(p-ind-event)    skip               */
/*         "Objeto..........:" string(p-ind-object)   skip               */
/*         "Handle do Objeto:" string(p-wgh-object)   skip               */
/*         "Handle da Frame.:" string(p-wgh-frame)    skip               */
/*         "Tabela..........:" p-cod-table            skip               */
/*         "Rowid...........:" string(p-row-table)    skip               */
/*         "p-wgh-object....:" p-wgh-object:file-name view-as alert-box. */

if p-ind-event  = "BEFORE-INITIALIZE" and 
   p-ind-object = 'VIEWER'     and 
   c-objeto = 'v01in084.w'     then 
DO:
    RUN utils/findWidget.p (INPUT "nome", 
                            INPUT "FILL-IN", 
                            INPUT p-wgh-frame, 
                            OUTPUT wh-ge-nome).     

    if valid-handle(wh-ge-nome) then do:
       create TOGGLE-BOX tg-integra-manusis
       assign frame        = p-wgh-frame
              format       = "yes/no"
              width        = 15
              LABEL        = "Integra Manusis?"
              row          = wh-ge-nome:ROW
              col          = wh-ge-nome:width + 20
              fgcolor      = wh-ge-nome:FGCOLOR
              visible      = yes
              font         = wh-ge-nome:FONT .
    end.
END.

if p-ind-event  = 'DISPLAY'    and 
   p-ind-object = 'VIEWER'     and 
   c-objeto     = 'v01in084.w' THEN
DO:
    FIND FIRST deposito exclusive-lock WHERE rowid(deposito) = p-row-table NO-ERROR.
    IF AVAIL deposito THEN
    DO:
        FIND FIRST ext-deposito exclusive-lock 
            WHERE ext-deposito.cod-depos = deposito.cod-depos NO-ERROR.
        IF AVAIL ext-deposito THEN
        DO:
            if valid-handle(tg-integra-manusis) then
                ASSIGN tg-integra-manusis:checked = ext-deposito.log-integra-manusis.
        END.
        ELSE
        DO:
            if valid-handle(tg-integra-manusis) then
                ASSIGN tg-integra-manusis:checked = no.
                   
        END.
    END.
END.

if p-ind-event  = 'ENABLE' and 
   p-ind-object = 'VIEWER'     and 
   c-objeto     = 'v01in084.w'     then 
DO: 
    if valid-handle(tg-integra-manusis) then
        ASSIGN tg-integra-manusis:sensitive  = true.
END.

if p-ind-event  = 'ASSIGN' and 
   p-ind-object = 'VIEWER'     and 
   c-objeto = 'v01in084.w'     THEN
DO:
    FIND FIRST deposito exclusive-lock WHERE rowid(deposito) = p-row-table NO-ERROR.
    IF AVAIL deposito THEN
    DO:
        IF tg-integra-manusis:checked THEN // Efetua a comunica��o com Web Services com base nos parametros cadastrados
        DO:
            FIND FIRST param-manusis NO-LOCK
                WHERE param-manusis.nome-metodo = 'ManutencaoAlmox' 
                NO-ERROR.
            IF AVAIL param-manusis THEN
            DO:
                EMPTY TEMP-TABLE tt-deposito.
                FOR EACH estabelec NO-LOCK,
                    EACH ext-estabelec NO-LOCK OF estabelec
                    WHERE ext-estabelec.log-integra-manusis:
                    
                    CREATE tt-deposito.
                    ASSIGN tt-deposito.code                  = deposito.cod-depos
                           tt-deposito.description           = deposito.nome
                           tt-deposito.area_API_id           = string(int(estabelec.cod-estabel),'999'). // Realizar Cast

                END.

                empty temp-table tt-param.
                CREATE tt-param.
                BUFFER-COPY param-manusis TO tt-param.
                RUN VALUE (param-manusis.api)(INPUT-OUTPUT TABLE tt-param,
                                              INPUT-OUTPUT TABLE tt-deposito).

                MESSAGE RETURN-VALUE
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.

                IF RETURN-VALUE = 'NOK':u THEN 
                DO:
                    run utp/ut-msgs.p (input "show":U, input 17006, input 'Problemas na conex�o ~~ Favor verificar CSCD0010 par�metros incorretos ' + cError).
                    RETURN 'NOK':u.
                END.
                ELSE
                DO:
                    FIND FIRST ext-deposito exclusive-lock WHERE ext-deposito.cod-depos = deposito.cod-depos NO-ERROR.
                    IF AVAIL ext-deposito THEN
                    DO:
                        ASSIGN ext-deposito.log-integra-manusis = TRUE.
                    END.
                    ELSE
                    DO:
                        CREATE ext-deposito.
                        ASSIGN ext-deposito.cod-depos = deposito.cod-depos
                               ext-deposito.log-integra-manusis = TRUE.
                        
                    END.
                END.
            END.
        END.

    END.

END.

if p-ind-event  = 'END-UPDATE' and
   p-ind-object = 'VIEWER'     and
   c-objeto = 'v01in084.w'     then 
DO:
    ASSIGN tg-integra-manusis:sensitive  = FALSE
           .
END.

if p-ind-event  = 'CANCEL' or p-ind-event  = 'UNDO' and 
   p-ind-object = 'VIEWER'     and 
   c-objeto = 'v01in084.w'     THEN
DO:
    ASSIGN tg-integra-manusis:sensitive  = FALSE
           .
END.
