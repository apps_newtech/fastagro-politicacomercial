&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME w-window
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS w-window 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i XX9999 9.99.99.999}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> <m�dulo>}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
DEFINE NEW GLOBAL SHARED VARIABLE gr-ped-venda AS ROWID NO-UNDO.
/* Parameters Definitions ---                                           */
DEFINE INPUT PARAMETER pRowID AS ROWID NO-UNDO.
/* Local Variable Definitions ---                                       */
{esp/ApiPoliticaPreco.i}

DEFINE VARIABLE h-complem-pedido AS HANDLE      NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE JanelaDetalhe
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-3 RECT-4 cmb-tp-venda ~
fi-tp-carregamento fi-cod-transp fi-cultura fi-campanha fi-safra ~
edt-detalhe-rota bt-ok bt-cancelar bt-ajuda 
&Scoped-Define DISPLAYED-OBJECTS fi-nr-pedido cmb-tp-venda ~
fi-tp-carregamento fi-desc-tp-carregamento fi-cod-transp fi-desc-transp ~
fi-cod-rep-regional fi-nome-rep-regional fi-cod-rep-comercial ~
fi-nome-rep-comercial fi-cultura fi-desc-cultura fi-campanha fi-safra ~
edt-detalhe-rota 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR w-window AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-ajuda 
     LABEL "Ajuda" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-cancelar AUTO-END-KEY 
     LABEL "Cancelar" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-ok AUTO-GO 
     LABEL "OK" 
     SIZE 10 BY 1.

DEFINE VARIABLE cmb-tp-venda AS INTEGER FORMAT ">>9":U INITIAL 1 
     LABEL "Tipo Venda" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Direta",1,
                     "Distribui��o",2,
                     "Agenciamento",3,
                     "Bonifica��o",4
     DROP-DOWN-LIST
     SIZE 16 BY .88 NO-UNDO.

DEFINE VARIABLE edt-detalhe-rota AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP MAX-CHARS 2000 SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL
     SIZE 97 BY 3.29 NO-UNDO.

DEFINE VARIABLE fi-campanha AS CHARACTER FORMAT "X(100)":U 
     LABEL "Campanha" 
     VIEW-AS FILL-IN 
     SIZE 15.72 BY .88 NO-UNDO.

DEFINE VARIABLE fi-cod-rep-comercial AS INTEGER FORMAT "->>>,>>9":U INITIAL 0 
     LABEL "Rep Comercial" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE fi-cod-rep-regional AS INTEGER FORMAT "->>>,>>9":U INITIAL 0 
     LABEL "Rep Regional" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE fi-cultura AS CHARACTER FORMAT "X(2000)":U 
     LABEL "Cultura" 
     VIEW-AS FILL-IN 
     SIZE 12.72 BY .88 NO-UNDO.

DEFINE VARIABLE fi-desc-cultura AS CHARACTER FORMAT "X(2000)":U 
     VIEW-AS FILL-IN 
     SIZE 35.86 BY .88 NO-UNDO.

DEFINE VARIABLE fi-desc-transp AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 59.72 BY .88 NO-UNDO.

DEFINE VARIABLE fi-desc-tp-carregamento AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 36 BY .88 NO-UNDO.

DEFINE VARIABLE fi-cod-transp AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     LABEL "Transp Padr�o" 
     VIEW-AS FILL-IN 
     SIZE 11.57 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nome-rep-comercial AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 23.86 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nome-rep-regional AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 23.86 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nr-pedido AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     LABEL "Pedido" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE fi-safra AS CHARACTER FORMAT "X(100)":U 
     LABEL "Safra" 
     VIEW-AS FILL-IN 
     SIZE 19.57 BY .88 NO-UNDO.

DEFINE VARIABLE fi-tp-carregamento AS CHARACTER FORMAT "x(20)":U INITIAL "" 
     LABEL "Tp Carregamento" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 100 BY 1.38
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 99.14 BY 1.75.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 99 BY 5.33.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 99.43 BY 4.13.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     fi-nr-pedido AT ROW 1.67 COL 16.57 COLON-ALIGNED WIDGET-ID 4
     cmb-tp-venda AT ROW 3.38 COL 16.43 COLON-ALIGNED WIDGET-ID 44
     fi-tp-carregamento AT ROW 3.38 COL 48.86 COLON-ALIGNED WIDGET-ID 14
     fi-desc-tp-carregamento AT ROW 3.38 COL 56.14 COLON-ALIGNED NO-LABEL WIDGET-ID 58
     fi-cod-transp AT ROW 4.33 COL 16.43 COLON-ALIGNED WIDGET-ID 16
     fi-desc-transp AT ROW 4.33 COL 28.29 COLON-ALIGNED NO-LABEL WIDGET-ID 56
     fi-cod-rep-regional AT ROW 5.29 COL 16.43 COLON-ALIGNED WIDGET-ID 42
     fi-nome-rep-regional AT ROW 5.29 COL 22.72 COLON-ALIGNED NO-LABEL WIDGET-ID 48
     fi-cod-rep-comercial AT ROW 5.29 COL 66 COLON-ALIGNED WIDGET-ID 40
     fi-nome-rep-comercial AT ROW 5.29 COL 72.29 COLON-ALIGNED NO-LABEL WIDGET-ID 46
     fi-cultura AT ROW 6.25 COL 16.43 COLON-ALIGNED WIDGET-ID 52
     fi-desc-cultura AT ROW 6.25 COL 29.43 COLON-ALIGNED NO-LABEL WIDGET-ID 54
     fi-campanha AT ROW 6.25 COL 77 COLON-ALIGNED WIDGET-ID 50
     fi-safra AT ROW 7.21 COL 16.43 COLON-ALIGNED WIDGET-ID 60
     edt-detalhe-rota AT ROW 9.58 COL 3.43 NO-LABEL WIDGET-ID 24
     bt-ok AT ROW 13.58 COL 2
     bt-cancelar AT ROW 13.58 COL 13
     bt-ajuda AT ROW 13.58 COL 90
     "Detalhe Rota" VIEW-AS TEXT
          SIZE 12 BY .67 AT ROW 8.75 COL 3.14 WIDGET-ID 22
     RECT-1 AT ROW 13.38 COL 1
     RECT-2 AT ROW 1.25 COL 1.86 WIDGET-ID 2
     RECT-3 AT ROW 3.17 COL 2 WIDGET-ID 6
     RECT-4 AT ROW 9 COL 2 WIDGET-ID 20
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 100.72 BY 14.04
         FONT 1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: JanelaDetalhe
   Allow: Basic,Browse,DB-Fields,Smart,Window,Query
   Container Links: 
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW w-window ASSIGN
         HIDDEN             = YES
         TITLE              = "Informa��es Adicionais Pedido"
         HEIGHT             = 14.04
         WIDTH              = 100.72
         MAX-HEIGHT         = 21.13
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 21.13
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB w-window 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/w-window.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW w-window
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   FRAME-NAME                                                           */
/* SETTINGS FOR FILL-IN fi-cod-rep-comercial IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-cod-rep-regional IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-desc-cultura IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-desc-transp IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-desc-tp-carregamento IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-nome-rep-comercial IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-nome-rep-regional IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-nr-pedido IN FRAME F-Main
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-window)
THEN w-window:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME w-window
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-window w-window
ON END-ERROR OF w-window /* Informa��es Adicionais Pedido */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-window w-window
ON WINDOW-CLOSE OF w-window /* Informa��es Adicionais Pedido */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda w-window
ON CHOOSE OF bt-ajuda IN FRAME F-Main /* Ajuda */
OR HELP OF FRAME {&FRAME-NAME}
DO:
  {include/ajuda.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancelar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancelar w-window
ON CHOOSE OF bt-cancelar IN FRAME F-Main /* Cancelar */
DO:
  apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok w-window
ON CHOOSE OF bt-ok IN FRAME F-Main /* OK */
DO:
  RUN pi-save-record.
  IF RETURN-VALUE = "NOK" THEN RETURN NO-APPLY.
  apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-campanha
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-campanha w-window
ON F5 OF fi-campanha IN FRAME F-Main /* Campanha */
DO:
  DO WITH FRAME {&FRAME-NAME} :
      {include/zoomvar.i &prog-zoom=esp\espc008-z01.w
                                &campo=fi-campanha
                                &campozoom=cod_campanha
                                &parametros="run pi-seta-inicial in wh-pesquisa
                                   (INPUT ped-venda.cod-estabel, input frame {&FRAME-NAME} fi-cultura)."}  
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-campanha w-window
ON MOUSE-SELECT-DBLCLICK OF fi-campanha IN FRAME F-Main /* Campanha */
DO:
      {include/zoomvar.i &prog-zoom=esp\espc008-z01.w
                                &campo=fi-campanha
                                &campozoom=cod_campanha
                                &parametros="run pi-seta-inicial in wh-pesquisa
                                   (INPUT ped-venda.cod-estabel, input frame {&FRAME-NAME} fi-cultura)."}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-cod-rep-comercial
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cod-rep-comercial w-window
ON F5 OF fi-cod-rep-comercial IN FRAME F-Main /* Rep Comercial */
DO:
    {include/zoomvar.i &prog-zoom=adzoom/z01ad229.w
                       &campo=fi-cod-rep-comercial
                       &campozoom=cod-rep
                       &campo2=fi-nome-rep-comercial
                       &campoZoom2=nome-abrev}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cod-rep-comercial w-window
ON MOUSE-SELECT-DBLCLICK OF fi-cod-rep-comercial IN FRAME F-Main /* Rep Comercial */
DO:
    {include/zoomvar.i &prog-zoom=adzoom/z01ad229.w
                       &campo=fi-cod-rep-comercial
                       &campozoom=cod-rep
                       &campo2=fi-nome-rep-comercial
                       &campoZoom2=nome-abrev} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-cod-rep-regional
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cod-rep-regional w-window
ON F5 OF fi-cod-rep-regional IN FRAME F-Main /* Rep Regional */
DO:
    {include/zoomvar.i &prog-zoom=adzoom/z01ad229.w
                       &campo=fi-cod-rep-comercial
                       &campozoom=cod-rep
                       &campo2=fi-nome-rep-comercial
                       &campoZoom2=nome-abrev}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cod-rep-regional w-window
ON MOUSE-SELECT-DBLCLICK OF fi-cod-rep-regional IN FRAME F-Main /* Rep Regional */
DO:
    {include/zoomvar.i &prog-zoom=adzoom/z01ad229.w
                       &campo=fi-cod-rep-regional
                       &campozoom=cod-rep
                       &campo2=fi-nome-rep-regional
                       &campoZoom2=nome-abrev}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-cultura
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cultura w-window
ON F5 OF fi-cultura IN FRAME F-Main /* Cultura */
DO:
  DO WITH FRAME {&FRAME-NAME} :
      {include/zoomvar.i &prog-zoom=esp\ESPC013-p01.w
                                &campo=fi-cultura
                                &campozoom=cod_cultura
                                &campo2=fi-desc-cultura
                                &campoZoom2=desc_cultura}
  END.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cultura w-window
ON MOUSE-SELECT-DBLCLICK OF fi-cultura IN FRAME F-Main /* Cultura */
DO:
  DO WITH FRAME {&FRAME-NAME} :
      {include/zoomvar.i &prog-zoom=esp\ESPC013-p01.w
                                &campo=fi-cultura
                                &campozoom=cod_cultura
                                &campo2=fi-desc-cultura
                                &campoZoom2=desc_cultura}                                
  END.  
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

                 
&Scoped-define SELF-NAME fi-cod-transp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cod-transp w-window
ON F5 OF fi-cod-transp IN FRAME F-Main /* Fornec Padr�o */
DO:
    {include/zoomvar.i &prog-zoom=adzoom/z01ad268.w 
                       &campo=fi-cod-transp
                       &campozoom=cod-transp
                       &campo2=fi-desc-transp
                       &campozoom2=nome}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cod-transp w-window
ON MOUSE-SELECT-DBLCLICK OF fi-cod-transp IN FRAME F-Main /* Fornec Padr�o */
DO:
    {include/zoomvar.i &prog-zoom=adzoom/z01ad268.w 
                       &campo=fi-cod-transp
                       &campozoom=cod-transp
                       &campo2=fi-desc-transp
                       &campozoom2=nome}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-safra
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-safra w-window
ON F5 OF fi-safra IN FRAME F-Main /* Safra */
DO:
  DO WITH FRAME {&FRAME-NAME} :
      {include/zoomvar.i &prog-zoom=esp\espc008-p01.w
                                &campo=fi-campanha
                                &campozoom=cod_campanha}  
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-safra w-window
ON MOUSE-SELECT-DBLCLICK OF fi-safra IN FRAME F-Main /* Safra */
DO:
      {include/zoomvar.i &prog-zoom=esp\espc008-p01.w
                                &campo=fi-campanha
                                &campozoom=cod_campanha}  
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-tp-carregamento
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-tp-carregamento w-window
ON F5 OF fi-tp-carregamento IN FRAME F-Main /* Tp Carregamento */
DO:
  DO WITH FRAME {&FRAME-NAME} :
      {include/zoomvar.i &prog-zoom=esp\espc009-p01.w
                         &campo=fi-tp-carregamento
                         &campozoom=cod_tipo_carga
                         &campo2=fi-desc-tp-carregamento
                         &campozoom2=desc_tipo_carga}
  END.


END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-tp-carregamento w-window
ON MOUSE-SELECT-DBLCLICK OF fi-tp-carregamento IN FRAME F-Main /* Tp Carregamento */
DO:
  DO WITH FRAME {&FRAME-NAME} :
      {include/zoomvar.i &prog-zoom=esp\espc009-p01.w
                         &campo=fi-tp-carregamento
                         &campozoom=cod_tipo_carga
                         &campo2=fi-desc-tp-carregamento
                         &campozoom2=desc_tipo_carga}
  END.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK w-window 


/* ***************************  Main Block  *************************** */
fi-cod-rep-comercial:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME}.
fi-cod-rep-regional :load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME}.
fi-tp-carregamento:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME}.
fi-campanha:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME}.
fi-cultura:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME}.
fi-cod-transp:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME}.
/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects w-window  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available w-window  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI w-window  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-window)
  THEN DELETE WIDGET w-window.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI w-window  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fi-nr-pedido cmb-tp-venda fi-tp-carregamento fi-desc-tp-carregamento 
          fi-cod-transp fi-desc-transp fi-cod-rep-regional 
          fi-nome-rep-regional fi-cod-rep-comercial fi-nome-rep-comercial 
          fi-cultura fi-desc-cultura fi-campanha fi-safra edt-detalhe-rota 
      WITH FRAME F-Main IN WINDOW w-window.
  ENABLE RECT-1 RECT-2 RECT-3 RECT-4 cmb-tp-venda fi-tp-carregamento 
         fi-cod-transp fi-cultura fi-campanha fi-safra edt-detalhe-rota bt-ok 
         bt-cancelar bt-ajuda 
      WITH FRAME F-Main IN WINDOW w-window.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
  VIEW w-window.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy w-window 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit w-window 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize w-window 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  {include/win-size.i}
  
  {utp/ut9000.i "XX9999" "9.99.99.999"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  RUN pi-get-record.

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-get-record w-window 
PROCEDURE pi-get-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE BUFFER bf-repres_financ FOR repres_financ.
DEFINE BUFFER bf-representante FOR representante.

    FIND FIRST ped-venda NO-LOCK
         WHERE ROWID(ped-venda) = gr-ped-venda NO-ERROR.
    IF AVAIL ped-venda THEN DO:
        ASSIGN fi-nr-pedido      :SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(ped-venda.nr-pedido).

        FIND es_ped_venda EXCLUSIVE-LOCK
             WHERE es_ped_venda.nr_pedido = ped-venda.nr-pedido NO-ERROR.
        IF AVAIL es_ped_venda THEN DO:
            ASSIGN cmb-tp-venda      :SCREEN-VALUE IN FRAME {&FRAME-NAME} = IF (es_ped_venda.tipo-venda <> 0 AND es_ped_venda.tipo-venda <> ?) THEN STRING(es_ped_venda.tipo-venda) ELSE "1".
            ASSIGN fi-tp-carregamento:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(es_ped_venda.cod_tipo_carga).
            ASSIGN fi-cod-transp     :SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(es_ped_venda.cod-fornec-padrao-frete).
            ASSIGN fi-campanha      :SCREEN-VALUE IN FRAME {&FRAME-NAME} = (es_ped_venda.campanha).
            ASSIGN fi-cultura       :SCREEN-VALUE IN FRAME {&FRAME-NAME} = (es_ped_venda.cultura).
            ASSIGN fi-safra       :SCREEN-VALUE IN FRAME {&FRAME-NAME} = (es_ped_venda.safra).
            ASSIGN edt-detalhe-rota  :SCREEN-VALUE IN FRAME {&FRAME-NAME} = (es_ped_venda.detalhe-rota).
            ASSIGN fi-cod-rep-comercial  :SCREEN-VALUE IN FRAME {&FRAME-NAME} = string(es_ped_venda.cod-rep-comercial).
            ASSIGN fi-cod-rep-regional   :SCREEN-VALUE IN FRAME {&FRAME-NAME} = string(es_ped_venda.cod-rep-regional).

            IF es_ped_venda.detalhe-rota = "" THEN DO:
                FOR FIRST es_politica_detalhe_rota WHERE
                          es_politica_detalhe_rota.cod_cliente = ped-venda.cod-emitente AND
                          es_politica_detalhe_rota.cod_entrega = ped-venda.cod-entrega NO-LOCK:
                    ASSIGN edt-detalhe-rota:SCREEN-VALUE IN FRAME {&FRAME-NAME} = es_politica_detalhe_rota.descricao.
                END.
            END.
        END.
        ELSE DO:
            FOR FIRST es_politica_detalhe_rota WHERE
                      es_politica_detalhe_rota.cod_cliente = ped-venda.cod-emitente AND
                      es_politica_detalhe_rota.cod_entrega = ped-venda.cod-entrega NO-LOCK:
                ASSIGN edt-detalhe-rota:SCREEN-VALUE IN FRAME {&FRAME-NAME} = es_politica_detalhe_rota.descricao.
            END.
        END.

        FOR FIRST transporte WHERE
                  transporte.cod-transp = INT(fi-cod-transp:SCREEN-VALUE IN FRAME {&FRAME-NAME}) NO-LOCK:
            ASSIGN fi-desc-transp:SCREEN-VALUE IN FRAME {&FRAME-NAME} = transporte.nome.
        END.

        FOR FIRST es_politica_tipo_cultura WHERE
                  es_politica_tipo_cultura.cod_cultura = fi-cultura:SCREEN-VALUE IN FRAME {&FRAME-NAME} NO-LOCK:
            ASSIGN fi-desc-cultura:SCREEN-VALUE IN FRAME {&FRAME-NAME} = es_politica_tipo_cultura.desc_cultura.
        END.

        FOR FIRST es_politica_tipo_carga WHERE
                  es_politica_tipo_carga.cod_tipo_carga = fi-tp-carregamento:SCREEN-VALUE IN FRAME {&FRAME-NAME} NO-LOCK:
            ASSIGN fi-desc-tp-carregamento:SCREEN-VALUE IN FRAME {&FRAME-NAME} = es_politica_tipo_carga.desc_tipo_carga.
        END.
        
        FIND FIRST repres NO-LOCK 
             WHERE repres.nome-abrev = ped-venda.no-ab-reppri NO-ERROR.
        IF NOT AVAIL repres THEN LEAVE.

        FIND FIRST representante NO-LOCK
             WHERE representante.cdn_repres = repres.cod-rep NO-ERROR.
        IF AVAIL representante THEN DO: //Regional
           FIND FIRST estrut_repres NO-LOCK
                WHERE estrut_repres.cdn_repres_filho = representante.cdn_repres NO-ERROR.
           IF AVAIL estrut_repres THEN DO:
                FIND FIRST bf-representante NO-LOCK 
                    WHERE bf-representante.cdn_repres = estrut_repres.cdn_repres_pai NO-ERROR.
        
                ASSIGN fi-cod-rep-regional   :SCREEN-VALUE IN FRAME {&FRAME-NAME} = string(estrut_repres.cdn_repres_pai)
                       fi-nome-rep-regional  :SCREEN-VALUE IN FRAME {&FRAME-NAME} = string(bf-representante.nom_abrev) WHEN AVAIL bf-representante.
           END.

           FIND FIRST estrut_repres NO-LOCK
                WHERE estrut_repres.cdn_repres_filho = INPUT FRAME {&FRAME-NAME} fi-cod-rep-regional NO-ERROR.
           IF AVAIL estrut_repres THEN DO: //Comercial
                FIND FIRST bf-representante NO-LOCK 
                    WHERE bf-representante.cdn_repres = estrut_repres.cdn_repres_pai NO-ERROR.
        
               ASSIGN fi-cod-rep-comercial  :SCREEN-VALUE IN FRAME {&FRAME-NAME} = string(estrut_repres.cdn_repres_pai) 
                      fi-nome-rep-comercial :SCREEN-VALUE IN FRAME {&FRAME-NAME} = string(bf-representante.nom_abrev) WHEN AVAIL bf-representante.
           END.
        
        END.
    END.

    RETURN 'OK':u.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-save-record w-window 
PROCEDURE pi-save-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

    FIND FIRST ped-venda NO-LOCK
         WHERE ROWID(ped-venda) = gr-ped-venda NO-ERROR.
    IF AVAIL ped-venda THEN DO:        

        IF input frame {&frame-name} fi-tp-carregamento <> "" THEN DO:
            FIND FIRST es_politica_tipo_carga WHERE
                       es_politica_tipo_carga.cod_tipo_carga = input frame {&frame-name} fi-tp-carregamento NO-LOCK NO-ERROR.
            IF NOT AVAIL es_politica_tipo_carga THEN DO:
                RUN utp/ut-msgs.p (input "show":U, input 17006, input "N�o encontrado o Tipo de Carregamento informado!~~N�o encontrado o Tipo de Carregamento informado!").
                RETURN "NOK".
            END.
        END.
    
        IF input frame {&frame-name} fi-cod-transp <> 0 THEN DO:
            FIND FIRST transporte WHERE
                       transporte.cod-transp = input frame {&frame-name} fi-cod-transp NO-LOCK NO-ERROR.
            IF NOT AVAIL transporte THEN DO:
                RUN utp/ut-msgs.p (input "show":U, input 17006, input "N�o encontrado o Transportador Padr�o informado!~~N�o encontrado o Transportador Padr�o informado!").
                RETURN "NOK".
            END.
        END.
            
        IF input frame {&frame-name} fi-campanha <> "" THEN DO:
            FIND FIRST es_politica_campanha WHERE
                       es_politica_campanha.cod_estabel  = ped-venda.cod-estabel AND
                       es_politica_campanha.cod_campanha = input frame {&frame-name} fi-campanha NO-LOCK NO-ERROR.
            IF NOT AVAIL es_politica_campanha THEN DO:
                RUN utp/ut-msgs.p (input "show":U, input 17006, input "N�o encontrada Campanha informada!~~N�o encontrada Campanha informada!").
                RETURN "NOK".
            END.
            ELSE DO:
                FIND FIRST es_politica_campanha WHERE
                           es_politica_campanha.cod_estabel  = ped-venda.cod-estabel AND
                           es_politica_campanha.cod_campanha = input frame {&frame-name} fi-campanha AND 
                           es_politica_campanha.cod_cultura  = input frame {&frame-name} fi-cultura NO-LOCK NO-ERROR.
                IF NOT AVAIL es_politica_campanha THEN DO:
                    RUN utp/ut-msgs.p (input "show":U, input 17006, input "Campanha n�o relacionada a cultura informada!~~Campanha n�o relacionada a cultura informada!").
                    RETURN "NOK".
                END.
            END.
        END.

        CREATE ttPedido.
        ASSIGN ttPedido.iNrPedido             = ped-venda.nr-pedido
               ttPedido.iTipoVenda            = input frame {&frame-name} cmb-tp-venda
               ttPedido.iTipoCarregamento     = input frame {&frame-name} fi-tp-carregamento
               ttPedido.iCodFornecPadraoFrete = input frame {&frame-name} fi-cod-transp
               ttPedido.cCodCampanha          = input frame {&frame-name} fi-campanha
               ttPedido.cCodCultura           = input frame {&frame-name} fi-cultura
               ttPedido.cSafra                = input frame {&frame-name} fi-safra
               ttPedido.cDetalheRota          = input frame {&frame-name} edt-detalhe-rota.
    
        RUN esp/ApiPoliticaPreco.p PERSISTENT SET h-complem-pedido.
        RUN piCriaComplementoPedido IN h-complem-pedido (INPUT TABLE ttPedido).
        DELETE PROCEDURE h-complem-pedido.
    END.

    RETURN 'OK':u.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records w-window  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this JanelaDetalhe, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed w-window 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

