&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mg               ORACLE
          mgnitro          ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESPC001-V01 1.00.00.000}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.
DEF VAR hprogramzoom AS HANDLE NO-UNDO.
DEF VAR i_tipo_venda AS INT NO-UNDO INITIAL 1.
DEF VAR i_tipo_frete AS INT NO-UNDO INITIAL 1.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES es_param_politica_estab
&Scoped-define FIRST-EXTERNAL-TABLE es_param_politica_estab


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR es_param_politica_estab.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS es_param_politica_estab.cod_estabel ~
es_param_politica_estab.juros es_param_politica_estab.qtd_dias ~
es_param_politica_estab.comp_premio es_param_politica_estab.premio_dist_emp ~
es_param_politica_estab.premio_dist es_param_politica_estab.premio_agen_emp ~
es_param_politica_estab.premio_agen_dist 
&Scoped-define ENABLED-TABLES es_param_politica_estab
&Scoped-define FIRST-ENABLED-TABLE es_param_politica_estab
&Scoped-Define ENABLED-OBJECTS rt-key RECT-5 RECT-6 RECT-7 
&Scoped-Define DISPLAYED-FIELDS es_param_politica_estab.cod_estabel ~
es_param_politica_estab.juros es_param_politica_estab.qtd_dias ~
es_param_politica_estab.comp_premio es_param_politica_estab.premio_dist_emp ~
es_param_politica_estab.premio_dist es_param_politica_estab.premio_agen_emp ~
es_param_politica_estab.premio_agen_dist 
&Scoped-define DISPLAYED-TABLES es_param_politica_estab
&Scoped-define FIRST-DISPLAYED-TABLE es_param_politica_estab
&Scoped-Define DISPLAYED-OBJECTS fi-nome ls_tipo_venda ls_tipo_frete 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fi-nome AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 47.43 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88 BY 2.75.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88 BY 3.75.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88 BY 5.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88.57 BY 1.25.

DEFINE VARIABLE ls_tipo_frete AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "CIF","1",
                     "FOB","2" 
     SIZE 25 BY 4.25 NO-UNDO.

DEFINE VARIABLE ls_tipo_venda AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "Direta","1",
                     "Distribui��o","2",
                     "Agenciamento","3" 
     SIZE 25 BY 4.25 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     es_param_politica_estab.cod_estabel AT ROW 1.17 COL 18.14 COLON-ALIGNED WIDGET-ID 2
          LABEL "Estab"
          VIEW-AS FILL-IN 
          SIZE 12 BY .88
     fi-nome AT ROW 1.17 COL 31.57 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     es_param_politica_estab.juros AT ROW 3.13 COL 18.14 COLON-ALIGNED WIDGET-ID 6
          VIEW-AS FILL-IN 
          SIZE 12 BY .88
     es_param_politica_estab.qtd_dias AT ROW 3.5 COL 67 COLON-ALIGNED WIDGET-ID 16
          LABEL "Qtde Dias Perm. Alt. Entrega:"
          VIEW-AS FILL-IN 
          SIZE 12 BY 1
     es_param_politica_estab.comp_premio AT ROW 4.29 COL 18.14 COLON-ALIGNED WIDGET-ID 4
          VIEW-AS FILL-IN 
          SIZE 12 BY .88
     es_param_politica_estab.premio_dist_emp AT ROW 6.58 COL 18.14 COLON-ALIGNED WIDGET-ID 14
          LABEL "Premio Dist"
          VIEW-AS FILL-IN 
          SIZE 12 BY .88
     es_param_politica_estab.premio_dist AT ROW 6.58 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 12
          VIEW-AS FILL-IN 
          SIZE 12 BY .88
     es_param_politica_estab.premio_agen_emp AT ROW 7.67 COL 18.14 COLON-ALIGNED WIDGET-ID 10
          LABEL "Premio Agen"
          VIEW-AS FILL-IN 
          SIZE 12 BY .88
     es_param_politica_estab.premio_agen_dist AT ROW 7.67 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 8
          VIEW-AS FILL-IN 
          SIZE 12 BY .88
     ls_tipo_venda AT ROW 9.5 COL 20 NO-LABEL WIDGET-ID 30
     ls_tipo_frete AT ROW 9.5 COL 61.43 NO-LABEL WIDGET-ID 36
     "Empresa" VIEW-AS TEXT
          SIZE 9 BY .67 AT ROW 5.75 COL 20.29 WIDGET-ID 26
     "Tipo Frete:" VIEW-AS TEXT
          SIZE 10 BY .67 AT ROW 9.58 COL 50.86 WIDGET-ID 34
     "Tipo Venda:" VIEW-AS TEXT
          SIZE 11 BY .67 AT ROW 9.58 COL 8.43 WIDGET-ID 32
     "Distribuidor" VIEW-AS TEXT
          SIZE 12 BY .67 AT ROW 5.75 COL 34 WIDGET-ID 28
     rt-key AT ROW 1 COL 1
     RECT-5 AT ROW 2.75 COL 1 WIDGET-ID 20
     RECT-6 AT ROW 5.5 COL 1 WIDGET-ID 24
     RECT-7 AT ROW 9.29 COL 1 WIDGET-ID 38
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE  WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgnitro.es_param_politica_estab
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 13.5
         WIDTH              = 88.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN es_param_politica_estab.cod_estabel IN FRAME f-main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN fi-nome IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR SELECTION-LIST ls_tipo_frete IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR SELECTION-LIST ls_tipo_venda IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN es_param_politica_estab.premio_agen_emp IN FRAME f-main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN es_param_politica_estab.premio_dist_emp IN FRAME f-main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN es_param_politica_estab.qtd_dias IN FRAME f-main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME es_param_politica_estab.cod_estabel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es_param_politica_estab.cod_estabel V-table-Win
ON F5 OF es_param_politica_estab.cod_estabel IN FRAME f-main /* Estab */
DO:
  {include/zoomvar.i
        &prog-zoom=adzoom/z01ad107.w
        &campo="es_param_politica_estab.cod_estabel"
        &campozoom=cod-estabel
        &frame="f-main"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es_param_politica_estab.cod_estabel V-table-Win
ON LEAVE OF es_param_politica_estab.cod_estabel IN FRAME f-main /* Estab */
DO:
  FIND FIRST estabelec WHERE
             estabelec.cod-estabel = es_param_politica.cod_estabel:SCREEN-VALUE IN FRAME {&FRAME-NAME} NO-LOCK NO-ERROR.

  IF AVAIL estabelec THEN
      ASSIGN fi-nome:SCREEN-VALUE IN FRAME {&FRAME-NAME} = estabelec.nome.
  ELSE 
      ASSIGN fi-nome:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es_param_politica_estab.cod_estabel V-table-Win
ON MOUSE-SELECT-DBLCLICK OF es_param_politica_estab.cod_estabel IN FRAME f-main /* Estab */
DO:
  APPLY "F5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ls_tipo_frete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ls_tipo_frete V-table-Win
ON VALUE-CHANGED OF ls_tipo_frete IN FRAME f-main
DO:
  ASSIGN i_tipo_frete = int(ls_tipo_frete:SCREEN-VALUE IN FRAME {&FRAME-NAME}).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ls_tipo_venda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ls_tipo_venda V-table-Win
ON VALUE-CHANGED OF ls_tipo_venda IN FRAME f-main
DO:
  ASSIGN i_tipo_venda = int(ls_tipo_venda:SCREEN-VALUE IN FRAME {&FRAME-NAME}).
      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF
  DO WITH FRAME f-main:
    
      cod_estabel:load-mouse-pointer ("image\lupa.cur") in frame f-main.

  END.         
  
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win 
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'cod_estabel':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = es_param_politica_estab
           &WHERE = "WHERE es_param_politica_estab.cod_estabel eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "es_param_politica_estab"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "es_param_politica_estab"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}
    
    /*:T Ponha na pi-validate todas as valida��es */
    /*:T N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    RUN pi-validate.
    
    /*:T Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */
    ASSIGN es_param_politica_estab.tipo_frete = i_tipo_frete
           es_param_politica_estab.tipo_venda = i_tipo_venda.
    
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
      
    
    DO WITH FRAME f-main:
    
        DISABLE ls_tipo_venda.
        DISABLE ls_tipo_frete.

    END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ).

    DO WITH FRAME {&FRAME-NAME}:
        IF AVAIL es_param_politica_estab THEN DO:
            ASSIGN ls_tipo_venda:SCREEN-VALUE = string(es_param_politica_estab.tipo_venda)
                   ls_tipo_frete:SCREEN-VALUE = string(es_param_politica_estab.tipo_frete).

        FIND FIRST estabelec WHERE
                 estabelec.cod-estabel = es_param_politica.cod_estabel NO-LOCK NO-ERROR.
        
        IF AVAIL estabelec THEN
          ASSIGN fi-nome:SCREEN-VALUE IN FRAME {&FRAME-NAME} = estabelec.nome.
        ELSE 
          ASSIGN fi-nome:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".


        END.
    END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

    DO WITH FRAME f-main:
    
        ENABLE ls_tipo_venda.
        ENABLE ls_tipo_frete.

    END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize V-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    
/*:T    Segue um exemplo de valida��o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */

    DO WITH FRAME f-main:
    
        FIND FIRST estabelec WHERE estabelec.cod-estabel = es_param_politica_estab.cod_estabel:SCREEN-VALUE NO-LOCK NO-ERROR.
        IF NOT AVAIL estabelec THEN
            RUN utp/ut-msgs.p (INPUT "show":U,
                               INPUT 17242,
                               INPUT "Estabelecimento n�o existe.~~" +
                                     "Deve ser informado um Estabelecimento v�lido.").
            APPLY "entry" TO es_param_politica_estab.cod_estabel.
            RETURN 'ADM-ERROR':U.

        
    END.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win 
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "cod_estabel" "es_param_politica_estab" "cod_estabel"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "es_param_politica_estab"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

