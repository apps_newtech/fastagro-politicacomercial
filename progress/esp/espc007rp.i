/*Obketivo: Calcular e atualizar os pre�os no item da tabela de pre�o */

ASSIGN es_politica_tabela_preco_item.preco_netfob_venda_direta  = (es_politica_estab_item.cust_prod + (es_politica_tabela_preco_item.perc_desconto * es_param_politica_estab.comp_premio / 100)) / (1 - (es_politica_tabela_preco_item.perc_margem / 100))
       es_politica_tabela_preco_item.preco_netfob_venda_distrib = (es_politica_estab_item.cust_prod + (es_politica_tabela_preco_item.perc_premio * es_param_politica_estab.premio_dist_emp / 100)) / (1 - (es_politica_tabela_preco_item.perc_margem_distrib / 100))
       es_politica_tabela_preco_item.perc_margem_bruta_direta   = es_politica_tabela_preco_item.perc_margem - es_politica_tabela_preco_item.perc_comissao
       es_politica_tabela_preco_item.perc_margem_bruta_unit     = es_politica_tabela_preco_item.preco_netfob_venda_direta * (es_politica_tabela_preco_item.perc_margem / 100).
