/*----------------------------------------------------------------------------------------------
**
**    Programa: ESFT051
**    Objetivo: Relat�rio de Confer�ncia de Embarques
**       Autor: Patr�cia Girotto - Datasul SP
** Atualiza��o: 08/12/2003
** Atualiza��o: 21/06/2004 - Maria de F�tima Ferraz - Inclus�o de tr�s novas linhas no relat�rio
**              26/07/2004 - Patr�cia Girotto - Verifica��o dos Dados que estavam saindo
**                           incorreto, qdo selecionado 1 faixa de embarques.              
**              04/04/2005 - Sidnei Barbosa - Datasul SP
**                           Icluido regra para considerar somente os Emnbarques que tiver com
**                           com Situacao e Impressao de Embarque = Liberado.
**              08/11/2019 - Edrian Golob - Newtech
**                           Altera��o de agrupamento de Registros por Endere�amento
** 
----------------------------------------------------------------------------------------------*/

{include/i-prgvrs.i ESFT051 "2.06.00.005"}
{include/tt-edit.i}

/*----- TEMP-TABLE DE PARAMETROS ----------*/
{esp/ESFT051.i}

Define NEW Shared Temp-table ttwm-box No-undo Like wm-box
    Field RowNum As Integer Init 1
    Field r-Rowid AS Rowid. 

/*---- DEINICAO DE PARAMETROS -------------*/
DEF INPUT PARAM raw-param AS RAW NO-UNDO.
DEF INPUT PARAM TABLE FOR tt-raw-digita.

/*----- DEFINICAO DE VARIAVEIS LOCAIS -----*/
DEFINE VARIABLE h-acomp      AS HANDLE                        NO-UNDO. 
DEFINE VARIABLE i            AS INT                           NO-UNDO.
DEFINE VARIABLE d-peso-bruto AS DEC  FORMAT ">>>,>>>,>>9"     NO-UNDO.
DEFINE VARIABLE d-peso-liq   AS DEC  FORMAT ">>>,>>>,>>9"     NO-UNDO.
DEFINE VARIABLE d-volume     AS DEC  FORMAT ">>>,>>9"         NO-UNDO.
DEFINE VARIABLE c-time       AS CHAR FORMAT "x(08)"           NO-UNDO.
DEFINE VARIABLE wgbosc030    AS HANDLE                        NO-UNDO.
DEFINE VARIABLE vNumCont     AS INTEGER                       NO-UNDO.
DEFINE VARIABLE iDiferenca   LIKE wm-box-movto.qtd-item       NO-UNDO.
DEFINE VARIABLE c-des-local  AS CHARACTER FORMAT 'X(19)'      NO-UNDO.
DEFINE VARIABLE c-mes-fabric AS CHARACTER   NO-UNDO.
DEFINE VARIABLE c-cod-depos AS CHARACTER   NO-UNDO.
DEFINE VARIABLE c-cod-localiz AS CHARACTER   NO-UNDO.

DEF VAR d-qtd-total        AS DECIMAL NO-UNDO.
DEF VAR d-num-volumes      AS DECIMAL NO-UNDO.
DEF VAR d-qtd-item-picking AS DECIMAL NO-UNDO.
DEF VAR d-qt-diferenca     AS DECIMAL COLUMN-LABEL "Diferen�a" NO-UNDO.
DEF VAR l-codigo-item      AS LOGICAL NO-UNDO.
 
/*
DEFINE TEMP-TABLE tt-tarefa-embarque NO-UNDO LIKE wm-box-movto
    FIELD val-prioridade AS INTEGER
    FIELD nr-pedcli      LIKE wm-docto-itens-ped.nr-pedcli
    FIELD nome-abrev     LIKE wm-docto-itens-ped.nome-abrev
    INDEX idx-prioridade nr-pedcli nome-abrev val-prioridade DESC.
*/
DEFINE TEMP-TABLE tt-endereco-separacao NO-UNDO LIKE wm-box-movto
    FIELD val-prioridade AS INTEGER
    FIELD nr-pedcli      LIKE wm-docto-itens-ped.nr-pedcli
    FIELD nome-abrev     LIKE wm-docto-itens-ped.nome-abrev
    FIELD des-local   AS CHAR
    FIELD qtd-total   LIKE wm-box-movto.qtd-item
    field mes-fabric  as char
    FIELD num-volumes AS DEC  FORMAT ">>>,>>9"
    INDEX idx-prioridade nr-pedcli nome-abrev val-prioridade DESC.

/*----- DEFINICAO DE STREAM ---------------*/
DEF NEW SHARED STREAM str-rp.

{include/pi-edit.i}

/*----- PREPARACAO DOS PARAMETROS ---------*/
CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

FOR EACH tt-raw-digita.
    CREATE tt-digita.
    RAW-TRANSFER tt-raw-digita.RAW TO tt-digita.
END.

/*----- Defini��o de Temp-Tables ----------*/
DEF TEMP-TABLE tt-embarque NO-UNDO
    FIELD nr-embarque   LIKE res-cli.cdd-embarq
    FIELD nr-resumo     LIKE res-cli.nr-resumo
    FIELD dt-embarque   LIKE embarque.dt-embarque
    FIELD nome-transp   LIKE embarque.nome-transp
    FIELD placa         LIKE embarque.placa
    FIELD cod-estabel   LIKE embarque.cod-estabel
    INDEX in-embarque nr-embarque nr-resumo.

DEF TEMP-TABLE tt-cliente NO-UNDO
    FIELD nr-embarque   LIKE res-cli.cdd-embarq
    FIELD nr-resumo     LIKE res-cli.nr-resumo
    FIELD cod-cliente   LIKE emitente.cod-emitente
    FIELD nr-pedido     LIKE ped-venda.nr-pedido
    FIELD nr-pedcli     LIKE ped-venda.nr-pedcli
    FIELD nome-emit     LIKE emitente.nome-emit
    INDEX in-cliente nr-embarque nr-resumo cod-cliente.

DEF TEMP-TABLE tt-it-pre-fat NO-UNDO
    FIELD nr-embarque   LIKE res-cli.cdd-embarq
    FIELD nr-resumo     LIKE res-cli.nr-resumo
    FIELD cod-cliente   LIKE emitente.cod-emit
    FIELD nome-abrev    LIKE emitente.nome-abrev
    FIELD nr-pedcli     LIKE it-pre-fat.nr-pedcli
    FIELD nr-pedido     LIKE ped-venda.nr-pedido
    FIELD it-codigo     LIKE ITEM.it-codigo
    FIELD cod-refer     LIKE it-pre-fat.cod-refer 
    FIELD nr-entrega    LIKE it-pre-fat.nr-entrega
    FIELD desc-item     AS   CHAR FORMAT "x(50)"  COLUMN-LABEL "Descri��o"
    FIELD peso-liq-tot  LIKE res-cli.peso-liq-tot
    FIELD qt-volumes    LIKE res-emb.qt-volumes
    FIELD qt-alocada    LIKE it-dep-fat.qt-alocada
    FIELD qt-lida       LIKE it-dep-fat.qt-alocada
    FIELD desc-emb      LIKE embalag.descricao
    FIELD nr-seq        LIKE it-pre-fat.nr-seq
    FIELD nr-serlote    LIKE it-dep-fat.nr-serlote
    FIELD qt-diferenca  AS   DEC                  COLUMN-LABEL "Diferen�a"
    INDEX in-it-pre-fat nr-embarque nr-resumo cod-cliente nr-pedcli.

def var de-qtd-item     like wm-box-movto.qtd-item no-undo.
def var l-imprime-item  as   logical               no-undo.

def var c-desc as char no-undo.

def buffer b-tt-it-pre-fat for tt-it-pre-fat.

def var i-cont as int no-undo.
/*----- INCLUDES PARA O RELATORIO ---------*/
{include/i-rpvar.i}


/*----- Defini��o de Frames Locais --------*/
FORM
    SKIP (2)
    "*** CONFER�NCIA DE EMBARQUE  /  COMPROVANTE DE ENTREGA ***" AT 038  
    i-cont at 130  " / 2" SKIP(1)
    "Embarque:"         AT 001  tt-embarque.nr-embarque
    "Resumo:"           AT 025  tt-embarque.nr-resumo
    "Data Embarque:"    AT 046  tt-embarque.dt-embarque         
    c-time              AT 072
    "Transp.:"          AT 002  tt-embarque.nome-transp
    "Placa:"            AT 064  tt-embarque.placa               SKIP(1)
    c-desc              at 002 format "X(150)"
    WITH DOWN WIDTH 300 NO-BOX STREAM-IO FRAME f-embarque NO-LABEL.

FORM
    "C�d Cliente:"      AT 003  tt-cliente.cod-cliente "-" tt-cliente.nome-emit SKIP(1)
    "     Pedido:"      AT 003  tt-cliente.nr-pedido SKIP
    "Ped Cliente:"      AT 003  tt-cliente.nr-pedcli SKIP(1)
    WITH DOWN WIDTH 300 NO-BOX STREAM-IO FRAME f-cliente NO-LABEL.

FORM 
    tt-it-pre-fat.it-codigo     
    tt-it-pre-fat.desc-item   
    c-cod-depos                                  format "x(5)"        column-label "Depos"               
    c-cod-localiz                                format "x(5)"        column-label "Localiz"               
    tt-endereco-separacao.cod-lote
    tt-endereco-separacao.mes-fabric             format "x(10)"        column-label "Fabricacao"
    d-qtd-total                                  FORMAT ">>>,>>>,>>9"  COLUMN-LABEL "Qt Item"
    d-num-volumes                                                      COLUMN-LABEL "Qt Vol"
/*     d-qtd-item-picking                           FORMAT ">>>,>>>,>>9"  COLUMN-LABEL "Qt.Lida"  */
/*     d-qt-diferenca                               FORMAT "->>>,>>>,>>9"                         */
/*     tt-endereco-separacao.des-local                                    COLUMN-LABEL "End. WMS" */
    WITH DOWN WIDTH 320 NO-BOX STREAM-IO FRAME f-it-pre-fat.

DEFINE VARIABLE c-editor AS CHARACTER   NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 
&ANALYZE-RESUME

/* *********************** Procedure Settings ************************ */
&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS
{utp/ut-glob.i}
&ANALYZE-RESUME


/*----- DIRECIONAMENTO DO RELATORIO -------*/
{include/i-rpout.i}

IF NOT VALID-HANDLE(wgbosc030) THEN DO:
    Run scbo/bosc030.p Persistent Set wgbosc030       No-error.
END.

RUN pi-executar.

IF VALID-HANDLE(wgbosc030) THEN DO:
    DELETE PROCEDURE wgbosc030       No-error.
END.


{include/i-rpclo.i}

&ANALYZE-RESUME
/*-----------------------------------------*/


/*********************** Internal Procedures  ************************/
PROCEDURE pi-carrega-cliente :
    FIND FIRST emitente WHERE emitente.nome-abrev = res-cli.nome-abrev NO-LOCK NO-ERROR.
    IF NOT AVAIL emitente THEN RETURN NO-APPLY.

    /* Verifica se j� existe a tt-cliente */
    FIND FIRST tt-cliente
         WHERE tt-cliente.nr-embarque = res-cli.cdd-embarq
           AND tt-cliente.nr-resumo   = res-cli.nr-resumo
           AND tt-cliente.cod-cliente = emitente.cod-emitente NO-ERROR.
    IF NOT AVAIL tt-cliente THEN DO:
       CREATE tt-cliente.
       ASSIGN tt-cliente.nr-embarque  = res-cli.cdd-embarq
              tt-cliente.nr-resumo    = res-cli.nr-resumo
              tt-cliente.cod-cliente  = emitente.cod-emitente
              tt-cliente.nome-emit    = emitente.nome-emit.
        
       /* Localizar o it-pre-fat do resumo */
       FIND FIRST it-pre-fat OF res-cli NO-LOCK NO-ERROR.
       IF AVAIL it-pre-fat THEN DO:
          /* Localizar o pedido do resumo */
          FIND FIRST ped-venda NO-LOCK
               WHERE ped-venda.nome-abrev = it-pre-fat.nome-abrev
                 AND ped-venda.nr-pedcli  = it-pre-fat.nr-pedcli NO-ERROR.
          IF AVAIL ped-venda THEN DO:
             ASSIGN tt-cliente.nr-pedcli  = ped-venda.nr-pedcli
                    tt-cliente.nr-pedido  = ped-venda.nr-pedido.
          END.
       END.

       /* Carregar tt-it-pre-fat */
       RUN pi-carrega-it-pre-fat.
    END.
END PROCEDURE.


PROCEDURE pi-carrega-embarque :
    FOR EACH tt-embarque:   DELETE tt-embarque.     END.
    FOR EACH tt-cliente:    DELETE tt-cliente.      END.
    FOR EACH tt-it-pre-fat: DELETE tt-it-pre-fat.   END.
    
    /* Carregando os embarque */
    FOR EACH res-cli NO-LOCK
        WHERE res-cli.cdd-embarq   >= tt-param.nr-embarque-ini
          AND res-cli.cdd-embarq   <= tt-param.nr-embarque-fim
          AND res-cli.nr-resumo     >= tt-param.nr-resumo-ini
          AND res-cli.nr-resumo     <= tt-param.nr-resumo-fim
          AND res-cli.nome-abrev    >= tt-param.nome-abrev-ini
          AND res-cli.nome-abrev    <= tt-param.nome-abrev-fim,
        FIRST embarque OF res-cli NO-LOCK
            WHERE embarque.cod-estabel >= tt-param.cod-estab-ini
              AND embarque.cod-estabel <= tt-param.cod-estab-fim:

        FIND FIRST es_embarque
             WHERE es_embarque.nr_embarque   = res-cli.cdd-embarq
               AND es_embarque.nr_resumo     = res-cli.nr-resumo
               AND es_embarque.ind_aprov_emb = 1
               AND es_embarque.sit_emb_bloq  = 2 NO-LOCK NO-ERROR.
        IF AVAIL es_embarque AND (es_embarque.ind_aprov_emb <> 1 OR es_embarque.sit_emb_bloq <> 2) THEN NEXT.
        
        FIND es_param_estab NO-LOCK
            WHERE es_param_estab.cod_estabel = embarque.cod-estabel NO-ERROR.
        IF NOT AVAIL es_param_estab THEN NEXT.

        RUN pi-acompanhar IN h-acomp ("Carregando Embarque... " + string(res-cli.cdd-embarq) + "/" + STRING(res-cli.nr-resumo)).

        /*-----> Verifica se Embarque est� Liberado para Impress�o - Sidnei Barbosa - 04/04/05 <-----*/
       /* FIND controla_emb WHERE
             controla_emb.nr_embarque = res-cli.nr-embarque NO-LOCK NO-ERROR.
        IF AVAIL controla_emb AND controla_emb.liberado_imp = 1 THEN NEXT. /*Bloqueado*/*/




        FIND FIRST pre-fatur OF embarque 
             WHERE pre-fatur.nr-pedcli >= tt-param.nr-pedcli-ini 
             AND   pre-fatur.nr-pedcli <= tt-param.nr-pedcli-fim 
             NO-LOCK NO-ERROR.
        IF NOT AVAIL pre-fatur THEN NEXT.

        FIND FIRST it-pre-fat OF pre-fatur
             WHERE it-pre-fat.it-codigo >= tt-param.it-codigo-ini
             AND   it-pre-fat.it-codigo <= tt-param.it-codigo-fim
             NO-LOCK NO-ERROR.
        IF NOT AVAIL it-pre-fat THEN NEXT.

        FIND FIRST it-dep-fat OF pre-fatur
             WHERE it-dep-fat.nr-serlote >= tt-param.lote-ini
             AND   it-dep-fat.nr-serlote <= tt-param.lote-fim
             NO-LOCK NO-ERROR.
        IF NOT AVAIL it-dep-fat THEN NEXT.

        /* Verifica se j� foi criado o tt-embarque */
        FIND FIRST tt-embarque
             WHERE tt-embarque.nr-embarque   = res-cli.cdd-embarq
               AND tt-embarque.nr-resumo     = res-cli.nr-resumo NO-ERROR.
        IF NOT AVAIL tt-embarque THEN DO:
           CREATE tt-embarque.
           ASSIGN tt-embarque.nr-embarque = res-cli.cdd-embarq
                  tt-embarque.nr-resumo   = res-cli.nr-resumo
                  tt-embarque.dt-embarque = embarque.dt-embarque
                  tt-embarque.nome-transp = embarque.nome-transp
                  tt-embarque.placa       = embarque.placa
                  tt-embarque.cod-estabel = embarque.cod-estabel.
        END.

        /* Criar a tt-cliente */
        RUN pi-carrega-cliente.
    END.
END PROCEDURE.


PROCEDURE pi-carrega-it-pre-fat :
    DEF VAR i-cont          AS INT NO-UNDO.
    DEF VAR d-qt-vol        AS DEC NO-UNDO. 
    DEF VAR r-tt-it-pre-fat AS ROWID NO-UNDO.

    FOR EACH  it-pre-fat OF res-cli 
        WHERE it-pre-fat.it-codigo >= tt-param.it-codigo-ini
        AND   it-pre-fat.it-codigo <= tt-param.it-codigo-fim NO-LOCK:

        /* Verif se j� existe a it-pre-fat */
        FIND FIRST tt-it-pre-fat
             WHERE tt-it-pre-fat.nr-embarque = it-pre-fat.cdd-embarq
             AND   tt-it-pre-fat.nr-resumo   = it-pre-fat.nr-resumo
             AND   tt-it-pre-fat.cod-cliente = emitente.cod-emitente
             AND   tt-it-pre-fat.nr-pedcli   = it-pre-fat.nr-pedcli
             AND   tt-it-pre-fat.nr-seq      = it-pre-fat.nr-seq
             AND   tt-it-pre-fat.it-codigo   = it-pre-fat.it-codigo
             AND   tt-it-pre-fat.cod-refer   = it-pre-fat.cod-refer
             NO-ERROR.
        IF NOT AVAIL tt-it-pre-fat THEN DO:
           CREATE tt-it-pre-fat.
           ASSIGN tt-it-pre-fat.nr-embarque = it-pre-fat.cdd-embarq
                  tt-it-pre-fat.nr-resumo   = it-pre-fat.nr-resumo  
                  tt-it-pre-fat.cod-cliente = emitente.cod-emitente
                  tt-it-pre-fat.nome-abrev  = emitente.nome-abrev
                  tt-it-pre-fat.nr-pedcli   = it-pre-fat.nr-pedcli  
                  tt-it-pre-fat.nr-seq      = it-pre-fat.nr-seq
                  tt-it-pre-fat.it-codigo   = it-pre-fat.it-codigo
                  tt-it-pre-fat.cod-refer   = it-pre-fat.cod-refer.

            FIND FIRST ped-venda NO-LOCK
                 WHERE ped-venda.nome-abrev  = it-pre-fat.nome-abrev
                   AND ped-venda.nr-pedcli   = it-pre-fat.nr-pedcli NO-ERROR.
            IF AVAIL ped-venda THEN
                ASSIGN tt-it-pre-fat.nr-pedido = ped-venda.nr-pedido.

            /* Localizar o descricao do item */
            FIND FIRST ITEM WHERE ITEM.it-codigo = it-pre-fat.it-codigo NO-LOCK NO-ERROR.
            IF AVAIL ITEM THEN
               ASSIGN tt-it-pre-fat.peso-liq-tot  = it-pre-fat.qt-alocada * item.peso-liquido
                      tt-it-pre-fat.desc-item     = ITEM.desc-item.

            /* Lozalizar informacoes da embalagem */
            FIND FIRST res-emb OF res-cli NO-LOCK NO-ERROR.
            IF AVAIL res-emb THEN DO:
               IF res-emb.qt-volumes = 1 THEN /* Granel */
                  ASSIGN tt-it-pre-fat.qt-volumes = res-emb.qt-volumes.
               ELSE 
                  ASSIGN tt-it-pre-fat.qt-volumes = tt-it-pre-fat.peso-liq-tot / ITEM.lote-mulven.

               FIND FIRST embalag OF res-emb NO-LOCK NO-ERROR.
               IF AVAIL embalag THEN
                  ASSIGN tt-it-pre-fat.desc-emb = embalag.descricao.
            END.

            ASSIGN i-cont   = 0
                   d-qt-vol = tt-it-pre-fat.qt-volumes.

            /* Itens controlados por lote */
            IF ITEM.tipo-con-est = 3 THEN DO:
               ASSIGN r-tt-it-pre-fat = ROWID(tt-it-pre-fat).

               FOR EACH  it-dep-fat NO-LOCK
                   WHERE it-dep-fat.cdd-embarq    = it-pre-fat.cdd-embarq
                   AND   it-dep-fat.nr-resumo      = it-pre-fat.nr-resumo
                   AND   it-dep-fat.nome-abrev     = it-pre-fat.nome-abrev
                   AND   it-dep-fat.nr-pedcli      = it-pre-fat.nr-pedcli
                   AND   it-dep-fat.nr-sequencia   = it-pre-fat.nr-sequencia
                   AND   it-dep-fat.it-codigo      = it-pre-fat.it-codigo
                   AND   it-dep-fat.cod-refer      = it-pre-fat.cod-refer
                   AND   it-dep-fat.nr-serlote    >= tt-param.lote-ini
                   AND   it-dep-fat.nr-serlote    <= tt-param.lote-fim
                   BREAK BY it-dep-fat.nr-sequencia:

                   find first tt-it-pre-fat 
                        where tt-it-pre-fat.nr-embarque    = it-pre-fat.cdd-embarq
                        AND   tt-it-pre-fat.nr-resumo      = it-pre-fat.nr-resumo
                        AND   tt-it-pre-fat.nome-abrev     = it-pre-fat.nome-abrev
                        AND   tt-it-pre-fat.nr-pedcli      = it-pre-fat.nr-pedcli
                        AND   tt-it-pre-fat.nr-seq         = it-pre-fat.nr-seq
                        AND   tt-it-pre-fat.it-codigo      = it-pre-fat.it-codigo
                        AND   tt-it-pre-fat.cod-refer      = it-pre-fat.cod-refer
                        AND   tt-it-pre-fat.nr-serlote     = it-dep-fat.nr-serlote
                       no-lock no-error.

                   if not avail tt-it-pre-fat then do:
                      FIND FIRST b-tt-it-pre-fat
                           WHERE ROWID(b-tt-it-pre-fat) = r-tt-it-pre-fat NO-LOCK NO-ERROR.
                      CREATE tt-it-pre-fat.
                      BUFFER-COPY b-tt-it-pre-fat TO tt-it-pre-fat NO-ERROR.
                      ASSIGN tt-it-pre-fat.qt-alocada    = it-dep-fat.qt-alocada
                             tt-it-pre-fat.nr-serlote    = it-dep-fat.nr-serlote
                             tt-it-pre-fat.peso-liq-tot  = it-dep-fat.qt-alocada * item.peso-liquido
                             tt-it-pre-fat.qt-volumes    = (d-qt-vol * it-dep-fat.qt-alocada) / it-pre-fat.qt-alocada
                             i-cont                      = i-cont + 1 .
                   end.
                   else do:
                       ASSIGN tt-it-pre-fat.qt-alocada    = tt-it-pre-fat.qt-alocada  + it-dep-fat.qt-alocada
                              tt-it-pre-fat.peso-liq-tot  = tt-it-pre-fat.peso-liq-tot + it-dep-fat.qt-alocada * item.peso-liquido
                              tt-it-pre-fat.qt-volumes    = tt-it-pre-fat.qt-volumes + (d-qt-vol * it-dep-fat.qt-alocada) / it-pre-fat.qt-alocada
                              tt-it-pre-fat.nr-serlote    = it-dep-fat.nr-serlote
                              i-cont                      = i-cont + 1 .
                   end.
               END.
               FIND FIRST b-tt-it-pre-fat
                    WHERE ROWID(b-tt-it-pre-fat) = r-tt-it-pre-fat NO-ERROR.
               delete b-tt-it-pre-fat.

            END.
        END.
    END.

    /*------------ > Carrega Quantidade Lida <-------------------------------------*/
    FOR EACH tt-it-pre-fat:
        FOR EACH  conf_embar NO-LOCK
            WHERE conf_embar.nr_embarque = tt-it-pre-fat.nr-embarque
            AND   conf_embar.nr_resumo   = tt-it-pre-fat.nr-resumo
            AND   conf_embar.it_codigo   = tt-it-pre-fat.it-codigo
            AND   conf_embar.nr_batch    = tt-it-pre-fat.nr-serlote:
            ASSIGN tt-it-pre-fat.qt-lida = tt-it-pre-fat.qt-lida + conf_embar.qt_lida.
       END.
    END.
END PROCEDURE.


PROCEDURE pi-executar :
    
    run utp/ut-acomp.p persistent set h-acomp.
    run pi-inicializar in h-acomp("Gerando dados para relat�rio").

        IF tt-param.id-docto = 0 THEN
        RUN pi-carrega-embarque.
    ELSE
        RUN listaTarefasDocto (INPUT tt-param.id-docto).

    /*-----> Impressao do relat�rio <--------------------------------*/
    FOR EACH tt-embarque NO-LOCK:
        RUN pi-acompanhar IN h-acomp ("Imprimindo Ordem Carreg... " + string(tt-embarque.nr-embarque)).
        c-time = STRING(TIME, "hh:mm:ss").
        do i-cont = 1 to 2:
            /*PAGE.*/

            assign c-desc = "".

            FOR first tt-cliente NO-LOCK
                WHERE tt-cliente.nr-embarque = tt-embarque.nr-embarque
                AND   tt-cliente.nr-resumo   = tt-embarque.nr-resumo:

                FOR first tt-it-pre-fat NO-LOCK
                        WHERE tt-it-pre-fat.nr-embarque = tt-cliente.nr-embarque
                        AND   tt-it-pre-fat.nr-resumo   = tt-cliente.nr-resumo
                        AND   tt-it-pre-fat.cod-cliente = tt-cliente.cod-cliente:

                    find first ped-venda
                         where ped-venda.nome-abrev = tt-it-pre-fat.nome-abrev
                           and ped-venda.nr-pedcli  = tt-it-pre-fat.nr-pedcli 
                         no-lock no-error.
                    if avail ped-venda then do:
                        find first emitente
                             where emitente.nome-abrev = ped-venda.nome-abrev
                             no-lock no-error.
                        find first es_emitente
                             where es_emitente.cod_emitente = emitente.cod-emitente
                             no-lock no-error.
                        FIND es_param_estab NO-LOCK
                            WHERE es_param_estab.cod_estabel = ped-venda.cod-estabel NO-ERROR.

                        FIND FIRST es_ped_venda NO-LOCK 
                         WHERE es_ped_venda.nr_pedido = ped-venda.nr-pedido NO-ERROR.

                        IF NOT AVAIL es_param_estab THEN NEXT.

                        /* 29/04/2016 - FRANK -  busCa o clienTE final de pedido da Trading */
                        DEF BUFFER bf_es_ped_venda_cex FOR es_ped_venda_cex.
                        DEF BUFFER bf_processo_exp FOR processo-exp.
                        DEF BUFFER bf_emitente FOR emitente.
                        DEF BUFFER bf_es_emitente FOR es_emitente.
                        DEF BUFFER bf_tt-it-pre-fat FOR tt-it-pre-fat.

                        FOR FIRST bf_es_ped_venda_cex NO-LOCK
                            WHERE bf_es_ped_venda_cex.nr_pedcli  = ped-venda.nr-pedcli
                              AND bf_es_ped_venda_cex.nome_abrev = ped-venda.nome-abrev
                              AND bf_es_ped_venda_cex.l_trading,
                            FIRST bf_processo_exp NO-LOCK
                            WHERE bf_processo_exp.cod-estabel = es_param_estab.cod_estabel_trd
                              AND bf_processo_exp.nr-proc-exp = ped-venda.nr-pedcli, 
                            FIRST bf_emitente NO-LOCK
                            WHERE bf_emitente.cod-emitente = bf_processo_exp.cod-emitente,
                            FIRST bf_es_emitente NO-LOCK
                            WHERE bf_es_emitente.cod_emitente = bf_emitente.cod-emitente:

                            ASSIGN tt-cliente.cod-cliente    = bf_emitente.cod-emitente
                                   tt-cliente.nome-emit      = bf_emitente.nome-emit.

                            FOR EACH bf_tt-it-pre-fat NO-LOCK
                                WHERE bf_tt-it-pre-fat.nr-embarque = tt-cliente.nr-embarque
                                AND   bf_tt-it-pre-fat.nr-resumo   = tt-cliente.nr-resumo:
                                ASSIGN bf_tt-it-pre-fat.cod-cliente = bf_emitente.cod-emitente.
                            END.

                            if bf_es_emitente.PALLET_EMIT = "1" then do:
                                assign c-desc = "*** PALLETIZADO".
                                if bf_es_emitente.AMARRADO_EMIT = "1" then
                                    assign c-desc = c-desc + " / AMARRADO".
                                if bf_ES_EMITENTE.caixa_pallet  then
                                    assign c-desc = c-desc + " / CAIXA SEM PALLET".
                                if bf_ES_EMITENTE.amarrac_espec then
                                    assign c-desc = c-desc + " / AMARRA��O ESPECIAL".
                                if bf_ES_EMITENTE.log_etiqueta_extra  then
                                    assign c-desc = c-desc + " / ETIQUETA EXTRA".
                            end.
                            else
                            if bf_es_emitente.AMARRADO_EMIT = "1" then
                                assign c-desc = "*** AMARRADO".
                            ELSE
                            if bf_ES_EMITENTE.caixa_pallet then
                                assign c-desc = c-desc + "*** CAIXA SEM PALLET".
                            ELSE
                            if bf_ES_EMITENTE.amarrac_espec then
                                assign c-desc = c-desc + "*** AMARRA��O ESPECIAL".
                            ELSE
                            if bf_ES_EMITENTE.log_etiqueta_extra then
                                assign c-desc = c-desc + "*** ETIQUETA EXTRA".

                        END.

                        if avail es_emitente AND NOT AVAIL bf_es_emitente THEN do:

                            if es_emitente.PALLET_EMIT = "1" then do:
                                assign c-desc = "*** PALLETIZADO".
                                if es_emitente.AMARRADO_EMIT = "1" then
                                    assign c-desc = c-desc + " / AMARRADO".
                                if ES_EMITENTE.caixa_pallet then
                                    assign c-desc = c-desc + " / CAIXA SEM PALLET".
                                if ES_EMITENTE.amarrac_espec then
                                    assign c-desc = c-desc + " / AMARRA��O ESPECIAL".
                                if ES_EMITENTE.log_etiqueta_extra then
                                    assign c-desc = c-desc + " / ETIQUETA EXTRA".
                            end.
                            ELSE
                                if es_emitente.AMARRADO_EMIT = "1" then
                                    assign c-desc = "*** AMARRADO".
                                ELSE
                                if ES_EMITENTE.caixa_pallet then
                                    assign c-desc = c-desc + "*** CAIXA SEM PALLET".
                                ELSE
                                if ES_EMITENTE.amarrac_espec then
                                    assign c-desc = c-desc + "*** AMARRA��O ESPECIAL".
                                ELSE
                                if ES_EMITENTE.log_etiqueta_extra then
                                    assign c-desc = c-desc + "*** ETIQUETA EXTRA".
                        END.
                    end.
                end.
            end.
            DISP i-cont
                 tt-embarque.nr-embarque
                 tt-embarque.nr-resumo  
                 tt-embarque.dt-embarque
                 c-time
                 tt-embarque.nome-transp
                 tt-embarque.placa 
                 c-desc 
                 WITH FRAME f-embarque.
            
            
/*             FOR FIRST wm-local                                                     */
/*                 WHERE wm-local.cod-estabel      = tt-embarque.cod-estabel AND      */
/*                       wm-local.log-local-padrao = YES                     NO-LOCK: */
/*             END.                                                                   */
     
            FIND FIRST wm-equipamento
                WHERE wm-equipamento.cdn-tipo-equipamento = 1 NO-LOCK NO-ERROR.
    
            FOR EACH tt-endereco-separacao:
                DELETE tt-endereco-separacao.
            END.
    
            RUN listaTarefasEmbarque (INPUT tt-embarque.cod-estabel,
/*                                       INPUT wm-local.cod-local, */
                                      INPUT tt-embarque.nr-embarque /*,
                                      OUTPUT TABLE tt-tarefa-embarque*/ ).
    
            FOR EACH  tt-cliente NO-LOCK
                WHERE tt-cliente.nr-embarque = tt-embarque.nr-embarque
                AND   tt-cliente.nr-resumo   = tt-embarque.nr-resumo:
    
                DISP STRING(tt-cliente.cod-cliente) @ tt-cliente.cod-cliente 
                     tt-cliente.nome-emit
                     STRING(tt-cliente.nr-pedido)   @ tt-cliente.nr-pedido
                     tt-cliente.nr-pedcli WITH FRAME f-cliente.
    
                 ASSIGN i = 1
                      d-peso-bruto        = 0
                      d-peso-liq          = 0 
                      d-volume            = 0
                      d-qtd-total         = 0
                      d-num-volumes       = 0
                      d-qtd-item-picking  = 0
                      d-qt-diferenca      = 0.
                
                FOR EACH  tt-it-pre-fat NO-LOCK
                    WHERE tt-it-pre-fat.nr-embarque = tt-cliente.nr-embarque
                    AND   tt-it-pre-fat.nr-resumo   = tt-cliente.nr-resumo
                    AND   tt-it-pre-fat.cod-cliente = tt-cliente.cod-cliente
                    BREAK BY tt-it-pre-fat.nr-pedido
                          BY tt-it-pre-fat.it-codigo
                          BY tt-it-pre-fat.nr-serlote:

                    IF  FIRST-OF(tt-it-pre-fat.it-codigo) THEN
                        ASSIGN l-imprime-item = no.
                    ELSE IF l-imprime-item AND CAN-FIND(FIRST tt-endereco-separacao) THEN NEXT.
                    
                    IF  FIRST-OF(tt-it-pre-fat.nr-serlote) THEN DO:
                    
                        FIND FIRST ITEM
                            WHERE ITEM.it-codigo = tt-it-pre-fat.it-codigo NO-LOCK NO-ERROR.
                            
                        assign de-qtd-item = 0
                               l-imprime-item = no.
                        for each it-dep-fat 
                           where it-dep-fat.cdd-embarq  = tt-cliente.nr-embarque
                           AND   it-dep-fat.nr-resumo   = tt-cliente.nr-resumo
                           AND   it-dep-fat.nome-abrev  = tt-it-pre-fat.nome-abrev
                           and   it-dep-fat.nr-pedcli   = tt-it-pre-fat.nr-pedcli
                           and   it-dep-fat.it-codigo   = tt-it-pre-fat.it-codigo
                           AND   it-dep-fat.nr-serlote  = tt-it-pre-fat.nr-serlote no-lock:
                           assign de-qtd-item = de-qtd-item + it-dep-fat.qt-alocada.
                        end.

                        FOR EACH tt-endereco-separacao
                            WHERE tt-endereco-separacao.cod-item  = tt-it-pre-fat.it-codigo
                            BREAK BY tt-endereco-separacao.cod-item
                                  BY tt-endereco-separacao.cod-lote
                                  BY tt-endereco-separacao.des-local:

                            ASSIGN iDiferenca = iDiferenca + tt-endereco-separacao.qtd-item-picking - (tt-endereco-separacao.qtd-item * tt-endereco-separacao.qti-embalagem).

                            ASSIGN tt-endereco-separacao.qtd-total   = (tt-endereco-separacao.qtd-item * tt-endereco-separacao.qti-embalagem)
                                   tt-endereco-separacao.num-volumes = (tt-endereco-separacao.qtd-total / ITEM.lote-mulven).
    
                            for each saldo-estoq no-lock
                                where saldo-estoq.cod-estabel = tt-embarque.cod-estabel
                                and  saldo-estoq.it-codigo    = tt-it-pre-fat.it-codigo
                                and  saldo-estoq.lote         = tt-endereco-separacao.cod-lote:
                                assign tt-endereco-separacao.mes-fabric = string(month(saldo-estoq.dt-vali-lote),"99") + "/" + string(year(saldo-estoq.dt-vali-lote) - 1,"9999"). 
                                assign c-cod-depos = saldo-estoq.cod-depos.                                 
                                assign c-cod-localiz = saldo-estoq.cod-localiz.                                 

                                leave.
                            end.

                            /*Altera��o Edrian Newtech - 08-11-2019 */
                            ACCUMULATE tt-endereco-separacao.qtd-total        (TOTAL BY tt-endereco-separacao.des-local).
                            ACCUMULATE  tt-endereco-separacao.num-volumes     (TOTAL BY tt-endereco-separacao.des-local).
                            ACCUMULATE tt-endereco-separacao.qtd-item-picking (TOTAL BY tt-endereco-separacao.des-local).
                            ACCUMULATE tt-it-pre-fat.qt-diferenca             (TOTAL BY tt-endereco-separacao.des-local).

                            IF FIRST-OF(tt-endereco-separacao.cod-item) THEN ASSIGN l-codigo-item = YES.
                                                                           
                            IF  LAST-OF(tt-endereco-separacao.des-local)  THEN DO:

                                ASSIGN d-qtd-total         = accum total by tt-endereco-separacao.des-local tt-endereco-separacao.qtd-total         
                                       d-num-volumes       = accum total by tt-endereco-separacao.des-local tt-endereco-separacao.num-volumes       
                                       d-qtd-item-picking  = accum total by tt-endereco-separacao.des-local tt-endereco-separacao.qtd-item-picking  
                                       d-qt-diferenca      = accum total by tt-endereco-separacao.des-local tt-it-pre-fat.qt-diferenca. 

                                DISP tt-it-pre-fat.it-codigo     WHEN l-codigo-item = YES
                                     tt-it-pre-fat.desc-item     WHEN l-codigo-item = YES
                                     c-cod-depos 
                                     c-cod-localiz
                                     tt-endereco-separacao.cod-lote
                                     tt-endereco-separacao.mes-fabric
                                     d-qtd-total        
                                     WITH FRAME f-it-pre-fat.

                                DOWN WITH FRAME f-it-pre-fat.

                                ASSIGN i = i + 1
                                       d-peso-liq   = d-peso-liq   + accum total by tt-endereco-separacao.des-local tt-endereco-separacao.qtd-total  
                                       d-volume     = d-volume     + accum total by tt-endereco-separacao.des-local tt-endereco-separacao.num-volumes
                                       de-qtd-item  = de-qtd-item  - accum total by tt-endereco-separacao.des-local tt-endereco-separacao.qtd-total
                                       l-imprime-item = yes.

                                ASSIGN iDiferenca   = 0.

                                ASSIGN l-codigo-item = NO.
                            END.
                        END.

                        if  de-qtd-item > 0 then do:
                            for FIRST saldo-estoq no-lock
                                where saldo-estoq.cod-estabel  = tt-embarque.cod-estabel
                                  and saldo-estoq.it-codigo    = tt-it-pre-fat.it-codigo
                                  and saldo-estoq.lote         = tt-it-pre-fat.nr-serlote:

                                assign c-mes-fabric = string(month(saldo-estoq.dt-vali-lote),"99") + "/" + string(year(saldo-estoq.dt-vali-lote) - 1,"9999").                                 
                                assign c-cod-depos = saldo-estoq.cod-depos.                                 
                                assign c-cod-localiz = saldo-estoq.cod-localiz.                                 
                            end.

                            DISP tt-it-pre-fat.it-codigo when l-imprime-item = no     
                                 tt-it-pre-fat.desc-item when l-imprime-item = no    
                                 c-cod-depos 
                                 c-cod-localiz
                                 tt-it-pre-fat.nr-serlote @ tt-endereco-separacao.cod-lote
                                 c-mes-fabric @ tt-endereco-separacao.mes-fabric
/*                                  de-qtd-item @ tt-endereco-separacao.qtd-total */
                                tt-it-pre-fat.qt-alocada @ d-qtd-total
/*                                  (de-qtd-item / ITEM.lote-mulven) @ tt-endereco-separacao.num-volumes */
                                 (tt-it-pre-fat.qt-alocada / ITEM.lote-mulven) @ d-num-volumes   
                                 WITH FRAME f-it-pre-fat.
                            DOWN WITH FRAME f-it-pre-fat.
                            ASSIGN i = i + 1
                                   d-peso-liq   = d-peso-liq   + de-qtd-item
                                   d-volume     = d-volume     + (de-qtd-item / ITEM.lote-mulven).
                                   de-qtd-item  = de-qtd-item  - de-qtd-item.
                        end.
                    END.
                END.
    
                PUT FILL("-", 145) FORMAT "x(145)".
                PUT SKIP
                    "TOTAL --->"    AT 078
                    d-qtd-total      AT 137
                    d-num-volumes    AT 148
                 .
                /*PUT CHR(18).*/
    
                EMPTY TEMP-TABLE tt-editor.
                IF ped-venda.cond-redespa <> '' THEN
                    ASSIGN c-editor = ped-venda.cond-redespa.
                ELSE 
                    ASSIGN c-editor = '    '.
                RUN pi-print-editor (INPUT replace(REPLACE(c-editor,CHR(10),' '),CHR(13),' '),
                                                   INPUT 85).

                FOR FIRST tt-editor .
                END.

                FIND FIRST reg-export WHERE
                           reg-export.nr-proc-exp = ped-venda.nr-pedcli NO-LOCK no-error.

                /* --->  21/06/2004 - Maria de F�tima Ferraz - Inclus�o campos abaixo <----- */
                PUT UNFORMATTED SKIP(1)
                    "N�mero LPCO                          : " (IF AVAIL reg-export THEN reg-export.nr-re-siscomex ELSE "") SKIP(1)
                    "N�mero Dossie                        : " (IF AVAIL reg-export THEN reg-export.cod-reg-vda ELSE "") SKIP(1)
                    "Quantidade de Pallets                : _______________________________________" + "          ***** OBSERVA��O *****   " SKIP(1)
                    "Quantidade de Madeirite (compensado) : _______________________________________" + "          " + IF AVAIL tt-editor THEN tt-editor.conteudo ELSE ''   SKIP(1).

                IF AVAIL tt-editor THEN
                    DELETE tt-editor.

                FOR EACH tt-editor.
                    PUT UNFORMATTED tt-editor.conteudo AT 89 SKIP.
                END.

                PUT SKIP.

                PUT UNFORMATTED
                    "Quantidade de Barrica ou Caixas      : _______________________________________" SKIP(1)
                    "Nome Leg�vel do Motorista / Data     : _______________________________________" SKIP(1)
                    "Assinatura do Motorista              : _______________________________________" SKIP(1)
                    "Responsavel Separacao                : _______________________________________" SKIP(1)
                    "Responsavel Conferencia              : _______________________________________" SKIP(1)
     	              "Responsavel Carregamento             : _______________________________________" SKIP(1).

            END.

            //Informa��es Espec�ficas adicionadas 
            IF AVAIL es_ped_venda THEN
            DO:
             PUT SKIP.

             PUT UNFORMATTED
               '                                                                                       ***** DETALHE ROTA *****' SKIP.
             PUT UNFORMATTED
               '               ' es_ped_venda.detalhe-rota SKIP(2).
            END.
             




            PAGE.
        end.
    END. /*tt-embarque*/

    RUN pi-finalizar IN h-acomp.
END PROCEDURE.

PROCEDURE listaTarefasEmbarque:
    DEF INPUT PARAM p-cod-estabel    LIKE wm-pedidos.cod-estabel              NO-UNDO.
/*     DEF INPUT PARAM p-cod-local      LIKE wm-pedidos.cod-local                NO-UNDO. */
    DEF INPUT PARAM p-nr-embarque    LIKE wm-pedidos.cdd-embarq              NO-UNDO.

/*    DEF OUTPUT PARAM TABLE FOR tt-tarefa-embarque.*/
    
    /* Localiza o documento relacionado ao embarque */
    /*
/*     FIND FIRST wm-docto NO-LOCK                                             */
/*         WHERE wm-docto.cod-estabel      = p-cod-estabel                     */
/*           AND wm-docto.cod-local        = p-cod-local                       */
/*           AND wm-docto.num-docto        = STRING(p-nr-embarque)             */
/*           AND wm-docto.ind-origem-docto = 4 NO-ERROR. /* Pr�-Faturamento */ */
/*                                                                             */
/*     IF  AVAIL wm-docto THEN DO:                                             */
    */
    FOR EACH wm-docto NO-LOCK
            WHERE wm-docto.cod-estabel      = p-cod-estabel
             /* AND wm-docto.cod-local        = p-cod-local*/
              AND wm-docto.num-docto        = STRING(p-nr-embarque)
              AND wm-docto.ind-origem-docto = 4:

        /* Localiza o movimento relacionado � tarefa */
        FOR EACH wm-box-movto NO-LOCK
            WHERE wm-box-movto.cod-estabel    = wm-docto.cod-estabel
              AND wm-box-movto.cod-local      = wm-docto.cod-local
              AND wm-box-movto.id-docto       = wm-docto.id-docto
              AND wm-box-movto.ind-tipo-movto = 2:

            /* Localiza o endere�o relacionado ao movimento */
            FIND FIRST wm-box NO-LOCK
                 WHERE wm-box.cod-estabel = wm-box-movto.cod-estabel
                   AND wm-box.cod-local   = wm-box-movto.cod-local
                   AND wm-box.id-box      = wm-box-movto.id-box NO-ERROR.

            IF AVAIL wm-box THEN DO:

               /* Pegar Informacoes Box */

               Run SetConstraintBoxes  In wgbosc030 (Input wm-box.cod-estabel,
                                                     Input wm-box.cod-local,
                                                     INPUT wm-box.id-box,
                                                     Input wm-box.id-box).

               Run openQueryStatic In wgbosc030 (Input 'Boxes':U).

               Run getBatchRecords IN wgbosc030 (Input   ?,
                                                 Input  NO,
                                                 Input  ?,
                                                 Output vNumCont,
                                                 Output Table ttwm-box).

               Find First ttwm-box No-error.

               If  Avail ttwm-box THEN DO:
                   Assign c-des-local         = ttwm-box.cod-bloco            + '/':U +
                                                ttwm-box.cod-rua              + '/':U +
                                                ttwm-box.cod-nivel            + '/':U +
                                                ttwm-box.cod-coluna           + '/':U +
                                                If ttwm-box.ind-posicao-box = 1 Then 'E' Else 'D'.
               END.


               CREATE tt-endereco-separacao.

               BUFFER-COPY wm-box-movto TO tt-endereco-separacao NO-ERROR.
               ASSIGN tt-endereco-separacao.des-local = c-des-local.

            END.
        END.
    END.
END PROCEDURE.

PROCEDURE listaTarefasDocto:
    
    DEF INPUT PARAM p-id-docto LIKE wm-docto.id-docto NO-UNDO.

    FOR EACH wm-docto NO-LOCK
       WHERE wm-docto.id-docto = p-id-docto:

        PUT UNFORMATTED SKIP (2)
        "*** CONFER�NCIA DE EMBARQUE  /  COMPROVANTE DE ENTREGA ***" AT 038  
        1 at 130  " / 1" SKIP(1)
        "Docto: "         AT 001  wm-docto.num-docto
        "Data Docto: "    AT 025  STRING(TODAY, "99/99/9999") SPACE STRING(TIME, "HH:MM:SS") SKIP (2).

        /* Localiza o movimento relacionado � tarefa */
        FOR EACH wm-box-movto NO-LOCK
            WHERE wm-box-movto.cod-estabel    = wm-docto.cod-estabel
              AND wm-box-movto.cod-local      = wm-docto.cod-local
              AND wm-box-movto.id-docto       = wm-docto.id-docto
              AND wm-box-movto.ind-tipo-movto = 2:

            CREATE tt-it-pre-fat.
            ASSIGN tt-it-pre-fat.nr-embarque = 0
                   tt-it-pre-fat.nr-resumo   = 0 
                   tt-it-pre-fat.cod-cliente = 0
                   tt-it-pre-fat.nome-abrev  = ""
                   tt-it-pre-fat.nr-pedcli   = ""  
                   tt-it-pre-fat.nr-seq      = wm-box-movto.num-seq-item
                   tt-it-pre-fat.it-codigo   = wm-box-movto.cod-item
                   tt-it-pre-fat.cod-refer   = wm-box-movto.cod-refer
                   tt-it-pre-fat.nr-serlote  = wm-box-movto.cod-lote
                   tt-it-pre-fat.qt-alocada  = wm-box-movto.qtd-item.

            /* Localizar o descricao do item */
            FIND FIRST ITEM WHERE ITEM.it-codigo = tt-it-pre-fat.it-codigo NO-LOCK NO-ERROR.
            IF AVAIL ITEM THEN
               ASSIGN tt-it-pre-fat.peso-liq-tot  = wm-box-movto.qtd-item * item.peso-liquido
                      tt-it-pre-fat.desc-item     = ITEM.desc-item.

            /* Lozalizar informacoes da embalagem */
            ASSIGN tt-it-pre-fat.qt-volumes = tt-it-pre-fat.peso-liq-tot / ITEM.lote-mulven.

            /* Localiza o endere�o relacionado ao movimento */
            FIND FIRST wm-box NO-LOCK
                 WHERE wm-box.cod-estabel = wm-box-movto.cod-estabel
                   AND wm-box.cod-local   = wm-box-movto.cod-local
                   AND wm-box.id-box      = wm-box-movto.id-box NO-ERROR.

            IF AVAIL wm-box THEN DO:

               /* Pegar Informacoes Box */

               Run SetConstraintBoxes  In wgbosc030 (Input wm-box.cod-estabel,
                                                     Input wm-box.cod-local,
                                                     INPUT wm-box.id-box,
                                                     Input wm-box.id-box).

               Run openQueryStatic In wgbosc030 (Input 'Boxes':U).

               Run getBatchRecords IN wgbosc030 (Input   ?,
                                                 Input  NO,
                                                 Input  ?,
                                                 Output vNumCont,
                                                 Output Table ttwm-box).

               Find First ttwm-box No-error.

               If  Avail ttwm-box THEN DO:
                   Assign c-des-local = ttwm-box.cod-bloco            + '/':U +
                                        ttwm-box.cod-rua              + '/':U +
                                        ttwm-box.cod-nivel            + '/':U +
                                        ttwm-box.cod-coluna           + '/':U +
                                     If ttwm-box.ind-posicao-box = 1 Then 'E' Else 'D'.
               END.


               CREATE tt-endereco-separacao.

               BUFFER-COPY wm-box-movto TO tt-endereco-separacao NO-ERROR.
               ASSIGN tt-endereco-separacao.des-local = c-des-local.

            END.
        END.
    END.

    /*Relatorio Sem Embarque*/
    ASSIGN i = 1
           d-peso-bruto        = 0
           d-peso-liq          = 0 
           d-volume            = 0
           d-qtd-total         = 0
           d-num-volumes       = 0
           d-qtd-item-picking  = 0
           d-qt-diferenca      = 0.


    FOR EACH  tt-it-pre-fat NO-LOCK
     BREAK BY tt-it-pre-fat.it-codigo:
              
        IF  FIRST-OF(tt-it-pre-fat.it-codigo) THEN DO:
        
            FIND FIRST ITEM
                WHERE ITEM.it-codigo = tt-it-pre-fat.it-codigo NO-LOCK NO-ERROR.

            assign de-qtd-item = 0
                   l-imprime-item = no.
            for each b-tt-it-pre-fat 
               where b-tt-it-pre-fat.it-codigo   = tt-it-pre-fat.it-codigo no-lock:
               assign de-qtd-item = de-qtd-item + b-tt-it-pre-fat.qt-alocada.
            end.                       

            FOR EACH tt-endereco-separacao
                WHERE tt-endereco-separacao.cod-item  = tt-it-pre-fat.it-codigo
                BREAK BY tt-endereco-separacao.cod-item
                      BY tt-endereco-separacao.cod-lote
                      BY tt-endereco-separacao.des-local:
                ASSIGN iDiferenca = (tt-endereco-separacao.qtd-item * tt-endereco-separacao.qti-embalagem)
                       iDiferenca = tt-endereco-separacao.qtd-item-picking - iDiferenca
                       tt-endereco-separacao.qtd-total   = (tt-endereco-separacao.qtd-item * tt-endereco-separacao.qti-embalagem)
                       tt-endereco-separacao.num-volumes = (tt-endereco-separacao.qtd-total / ITEM.lote-mulven).

                IF tt-endereco-separacao.cod-estabel = "001" OR tt-endereco-separacao.cod-estabel = "002" THEN
                    for each saldo-estoq no-lock
                        where saldo-estoq.cod-estabel = "001"
                        and  saldo-estoq.it-codigo    = tt-it-pre-fat.it-codigo
                        and  saldo-estoq.lote         = tt-endereco-separacao.cod-lote:
                        assign tt-endereco-separacao.mes-fabric = string(month(saldo-estoq.dt-vali-lote),"99") + "/" + string(year(saldo-estoq.dt-vali-lote) - 1,"9999"). 
                        leave.
                    end.
                ELSE
                    for each saldo-estoq no-lock
                        where saldo-estoq.cod-estabel = tt-endereco-separacao.cod-estabel
                        and  saldo-estoq.it-codigo    = tt-it-pre-fat.it-codigo
                        and  saldo-estoq.lote         = tt-endereco-separacao.cod-lote:
                        assign tt-endereco-separacao.mes-fabric = string(month(saldo-estoq.dt-vali-lote),"99") + "/" + string(year(saldo-estoq.dt-vali-lote) - 1,"9999"). 
                        assign c-cod-depos = saldo-estoq.cod-depos.                                 
                        assign c-cod-localiz = saldo-estoq.cod-localiz.                                 

                        leave.
                    end.
		    
                /*Altera��o Edrian Newtech - 08-11-2019 */
                            /*Altera��o Edrian Newtech - 08-11-2019 */
                            ACCUMULATE tt-endereco-separacao.qtd-total        (TOTAL BY tt-endereco-separacao.des-local).
                            ACCUMULATE  tt-endereco-separacao.num-volumes     (TOTAL BY tt-endereco-separacao.des-local).
                            ACCUMULATE tt-endereco-separacao.qtd-item-picking (TOTAL BY tt-endereco-separacao.des-local).
                            ACCUMULATE tt-it-pre-fat.qt-diferenca             (TOTAL BY tt-endereco-separacao.des-local).

                            IF FIRST-OF(tt-endereco-separacao.cod-item) THEN ASSIGN l-codigo-item = YES.
                                                                           
                            IF  LAST-OF(tt-endereco-separacao.des-local)  THEN DO:
                                ASSIGN d-qtd-total         = accum total by tt-endereco-separacao.des-local tt-endereco-separacao.qtd-total         
                                       d-num-volumes       = accum total by tt-endereco-separacao.des-local tt-endereco-separacao.num-volumes       
                                       d-qtd-item-picking  = accum total by tt-endereco-separacao.des-local tt-endereco-separacao.qtd-item-picking  
                                       d-qt-diferenca      = accum total by tt-endereco-separacao.des-local tt-it-pre-fat.qt-diferenca. 

                            DISP tt-it-pre-fat.it-codigo     WHEN l-codigo-item
                                 tt-it-pre-fat.desc-item     WHEN l-codigo-item
                                 c-cod-depos 
                                 c-cod-localiz
                                 tt-endereco-separacao.cod-lote
                                 tt-endereco-separacao.mes-fabric
                                 d-qtd-total        
                                 
                                   WITH FRAME f-it-pre-fat.

                              DOWN WITH FRAME f-it-pre-fat.
                              ASSIGN i = i + 1
                                     d-peso-liq   = d-peso-liq   + accum total by tt-endereco-separacao.des-local tt-endereco-separacao.qtd-total   
                                     d-volume     = d-volume     + accum total by tt-endereco-separacao.des-local tt-endereco-separacao.num-volumes 
                                     de-qtd-item  = de-qtd-item  - accum total by tt-endereco-separacao.des-local tt-endereco-separacao.qtd-total   
                                     l-imprime-item = yes.

                              ASSIGN l-codigo-item = NO.
                            END.
                       
            END.

            if  de-qtd-item > 0 then do:
                for FIRST saldo-estoq no-lock
                    where saldo-estoq.cod-estabel  = "001"
                      and saldo-estoq.it-codigo    = tt-it-pre-fat.it-codigo
                      and saldo-estoq.lote         = tt-it-pre-fat.nr-serlote:

                    assign c-mes-fabric = string(month(saldo-estoq.dt-vali-lote),"99") + "/" + string(year(saldo-estoq.dt-vali-lote) - 1,"9999").                                 
                    assign c-cod-depos = saldo-estoq.cod-depos.                                 
                    assign c-cod-localiz = saldo-estoq.cod-localiz.                                 

                end.
                
                DISP tt-it-pre-fat.it-codigo when l-imprime-item = no     
                     tt-it-pre-fat.desc-item when l-imprime-item = no    
                     c-cod-depos 
                     c-cod-localiz
                     tt-it-pre-fat.nr-serlote @ tt-endereco-separacao.cod-lote
                     c-mes-fabric @ tt-endereco-separacao.mes-fabric
                     de-qtd-item @ d-qtd-total
                     
                     WITH FRAME f-it-pre-fat.

                DOWN WITH FRAME f-it-pre-fat.
                ASSIGN i = i + 1
                       d-peso-liq   = d-peso-liq   + de-qtd-item
                       d-volume     = d-volume     + (de-qtd-item / ITEM.lote-mulven).
                       de-qtd-item  = de-qtd-item  - de-qtd-item.
            end.
        END.
    END.


    PUT FILL("-", 145) FORMAT "x(145)".
    PUT SKIP
        "TOTAL --->"    AT 078
        d-qtd-total      AT 137
        d-num-volumes    AT 148
     .
    /*PUT CHR(18).*/

    /* --->  21/06/2004 - Maria de F�tima Ferraz - Inclus�o campos abaixo <----- */
    PUT UNFORMATTED SKIP(1)
        "Quantidade de Pallets                : _______________________________________" + "          ***** OBSERVA��O *****   " SKIP(1)
        "Quantidade de Madeirite (compensado) : _______________________________________" + "          " + IF AVAIL tt-editor THEN tt-editor.conteudo ELSE ''   SKIP.

    PUT SKIP.

    PUT UNFORMATTED
        "Quantidade de Barrica ou Caixas      : _______________________________________" SKIP(1)
        "Nome Leg�vel do Motorista / Data     : _______________________________________" SKIP(1)
        "Assinatura do Motorista              : _______________________________________" SKIP(1)
        "Responsavel Separacao                : _______________________________________" SKIP(1)
        "Responsavel Conferencia              : _______________________________________" SKIP(1)
        "Responsavel Carregamento             : _______________________________________" SKIP(1).

END PROCEDURE.
