/********************************************************************************
*** 28/04/2021 Anderson Luchini
**             Defini��es para API da pol�tica comercial da Fast Agro.
*******************************************************************************/

DEF TEMP-TABLE ttParametros
    FIELD cCodEstabel           AS CHAR
    FIELD iCodEmitente          AS INT
    FIELD cRegiao               AS CHAR
    FIELD cItCodigo             AS CHAR
    FIELD DtImplant             AS DATE
    FIELD DtVencto              AS DATE
    FIELD DtEntrega             AS DATE
    FIELD cUsuario              AS CHAR
    FIELD iCodAgente            AS INT
    FIELD dePrecoNetFob         AS DECIMAL
    FIELD dePrecoNf             AS DECIMAL
    FIELD deQuantidade          AS DECIMAL
    FIELD dePercComisAgent      AS DECIMAL
    FIELD deValorFrete          AS DECIMAL
    FIELD iTipoVenda            AS INTEGER
    FIELD iTipoFrete            AS INTEGER
    FIELD iTipoCarregamento     AS CHARACTER
    FIELD cCidade               AS CHARACTER 
    FIELD cUf                   AS CHARACTER 
    FIELD cCodCampanha          AS CHARACTER
    FIELD cCodCultura           AS CHARACTER
    FIELD iTipoOperacao         AS INTEGER
    FIELD cLocalEntrega         AS CHARACTER.

DEF TEMP-TABLE ttOutrosItens
    FIELD cItCodigo             AS CHAR
    FIELD deQuantidade          AS DECIMAL.

DEF TEMP-TABLE ttRetorno
    FIELD cCodTabPreco          AS CHARACTER
    FIELD cCorFaixa             AS CHARACTER
    FIELD dePrecoTabela         AS DECIMAL
    FIELD dePrecoNetFobTarget   AS DECIMAL
    FIELD dePrecoNetFob         AS DECIMAL
    FIELD dePrecoNetFobEF       AS DECIMAL
    FIELD dePrecoNetFobEFReal   AS DECIMAL
    FIELD dePrecoEF             AS DECIMAL
    FIELD dePrecoNf             AS DECIMAL
    FIELD dePrecoNfReal         AS DECIMAL
    FIELD dePrecoOrig           AS DECIMAL
    FIELD deValorTotal          AS DECIMAL
    FIELD cCodTabFrete          AS CHARACTER
    FIELD deValorFrete          AS DECIMAL  
    FIELD deValorFreteReal      AS DECIMAL  
    FIELD deTaxaFinanceira      AS DECIMAL  
    FIELD cOrigem               AS CHARACTER
    //Comiss�o
    FIELD deComissaoTotal       AS DECIMAL
    FIELD dePercComissao        AS DECIMAL
    FIELD dePercComissaoReal    AS DECIMAL
    FIELD deComissaoMaxFaixa    AS DECIMAL
    FIELD dePercComissaoAgt     AS DECIMAL
    FIELD deComissaoAgt         AS DECIMAL
    FIELD deComissaoAgtTotal    AS DECIMAL
    //Gerencial
    FIELD dePercComisAjust      AS DECIMAL  
    FIELD dePremioDistrib       AS DECIMAL 
    FIELD deComissaoUnit        AS DECIMAL 
    FIELD deCusto               AS DECIMAL 
    FIELD dePercMbVista         AS DECIMAL 
    FIELD dePercMbPrazo         AS DECIMAL 
    FIELD deMbVistaUnit         AS DECIMAL 
    FIELD deMbVistaTotal        AS DECIMAL
    FIELD deMbPrazoUnit         AS DECIMAL 
    FIELD deMbPrazoTotal        AS DECIMAL
    FIELD dePercMargemVista     AS DECIMAL 
    FIELD deMargemVista         AS DECIMAL 
    FIELD deFinanceiro          AS DECIMAL 
    FIELD deValorICMS           AS DECIMAL 
    FIELD deValorPisCofins      AS DECIMAL 
    FIELD deQtdeMidas           AS DECIMAL 
    FIELD deFoliar              AS DECIMAL 
    FIELD deQtdeFoliar          AS DECIMAL 
    FIELD cCheckFoliares        AS CHARACTER
    FIELD deQtdeUlexita         AS DECIMAL 
    FIELD cCheckUlexita         AS CHARACTER
    FIELD deFaixaPreto          AS DECIMAL //
    FIELD deFaixaVermelho       AS DECIMAL //
    FIELD deFaixaAmarelo        AS DECIMAL //
    FIELD deFaixaVerde          AS DECIMAL //
    FIELD deMinimo              AS DECIMAL 
    FIELD cFaixa                AS CHARACTER // 
    FIELD cPercFaixaAcima       AS CHARACTER // 
    FIELD dePercComisBaseFaixa  AS DECIMAL 
    FIELD deDeltaPreco          AS DECIMAL 
    FIELD dePercVarPrecoFaixa   AS DECIMAL 
    FIELD deDeltaComisMaxFaixa  AS DECIMAL 
    FIELD deVarComisFaixa       AS DECIMAL 
    FIELD deFormulaBaseFrete    AS DECIMAL
    FIELD cNatOperacao          AS CHARACTER. //

DEF TEMP-TABLE ttFaixaPreco
    FIELD cCorFaixa             AS CHARACTER
    FIELD dePrecoTabela         AS DECIMAL.

DEF TEMP-TABLE ttParametros2
    FIELD iCodEmitente          AS INT
    FIELD cCodEstabel           AS CHAR
    FIELD cItCodigo             AS CHAR
    FIELD DtImplant             AS DATE
    FIELD iCodRep               AS INT
    FIELD dePrecoNetFob         AS DECIMAL
    FIELD deQuantidade          AS DECIMAL
    FIELD deValorFrete          AS DECIMAL
    FIELD iTipoVenda            AS INTEGER
    FIELD iTipoCarregamento     AS CHARACTER
    FIELD cCidade               AS CHARACTER 
    FIELD cUf                   AS CHARACTER 
    FIELD cCodCampanha          AS CHARACTER
    FIELD cCodCultura           AS CHARACTER.

DEF TEMP-TABLE ttRetorno2
    FIELD cCodTabPreco      AS CHARACTER
    FIELD cCorFaixa         AS CHARACTER
    FIELD deFaixaPreco      AS DECIMAL
    FIELD dePercComissao    AS DECIMAL  
    FIELD cCodTabFrete      AS CHARACTER
    FIELD deValorFrete      AS DECIMAL  
    FIELD deTaxaFinanceira  AS DECIMAL  
    FIELD cOrigem           AS CHARACTER
    FIELD dePrecoTarget     AS DECIMAL
    FIELD dePrecoNf         AS DECIMAL
    FIELD dePrecoNetFobEF   AS DECIMAL
    FIELD deComissaoUnit    AS DECIMAL.  

DEF TEMP-TABLE ttPedido
    FIELD iNrPedido             AS INTEGER
    FIELD cCodEstabel           AS CHARACTER
    FIELD iTipoVenda            AS INTEGER
    FIELD iTipoCarregamento     AS CHARACTER
    FIELD iCodFornecPadraoFrete AS INTEGER
    FIELD cCodCampanha          AS CHARACTER
    FIELD cCodCultura           AS CHARACTER
    FIELD cDetalheRota          AS CHARACTER
    FIELD cSafra                AS CHARACTER.

DEF TEMP-TABLE ttItemPedido
    FIELD iNrPedido             AS INTEGER
    FIELD iNrSequencia          AS INTEGER
    FIELD iNrSequenciaPedWeb    AS INTEGER
    FIELD deValorNetFob         AS DECIMAL
    FIELD deValorNetFobEF       AS DECIMAL
    FIELD deValorFrete          AS DECIMAL  
    FIELD dePercComissao        AS DECIMAL
    FIELD dePercComissaoAgt     AS DECIMAL.

DEF TEMP-TABLE ttSituacaoPedido
    FIELD lCompleto             AS LOGICAL
    FIELD iSituacaoPed          AS INTEGER
    FIELD iSituacaoCred         AS INTEGER
    FIELD cAprovadorCred        AS CHARACTER
    FIELD iSituacaoMLA          AS INTEGER
    FIELD cAprovadorMLA         AS CHARACTER.

DEF TEMP-TABLE ttRepres NO-UNDO
    FIELD iCodRep  AS INT
    FIELD cUsuario AS CHAR.

DEF NEW GLOBAL SHARED VAR c-seg-usuario AS CHARACTER FORMAT "x(12)" NO-UNDO.
DEF BUFFER b-es_politica_tabela_preco_item FOR es_politica_tabela_preco_item.
DEF BUFFER b-ped-item FOR ped-item.
