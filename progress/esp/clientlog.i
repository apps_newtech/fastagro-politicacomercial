/** Por Matheus Antonelli em 28.03.2019 */

PROCEDURE pi-clientlog :

    DEFINE INPUT  PARAMETER p-mensagem AS CHARACTER NO-UNDO.
    DEFINE INPUT  PARAMETER p-subsys   AS CHARACTER NO-UNDO.

    IF NOT (LOG-MANAGER:LOGFILE-NAME <> ? AND LOG-MANAGER:LOGFILE-NAME > "") OR 
           (LOG-MANAGER:LOGGING-LEVEL < 1  OR LOG-MANAGER:LOGGING-LEVEL > 4) THEN RETURN.

    IF LOG-MANAGER:LOGGING-LEVEL >= 2 THEN DO:
        IF p-subsys <> "" THEN DO:
            LOG-MANAGER:WRITE-MESSAGE(p-mensagem, p-subsys).
        END.
        ELSE DO:
            LOG-MANAGER:WRITE-MESSAGE(p-mensagem).
        END.
    END.

END PROCEDURE.
