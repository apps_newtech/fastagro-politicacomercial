/******************************************************************************
**  Objetivo:   Include utilizada para os programas ESFT026.w e ESFT026rp.p
**  Autor...:   Patr�cia Girotto - Datasul SP
**  Dt Atual:   08/12/2003
**              - Include para ser utilizada nos programas acima
******************************************************************************/

define temp-table tt-param no-undo
    field destino           as integer
    field arquivo           as char format "x(35)"
    field usuario           as char format "x(12)"
    field data-exec         as date
    field hora-exec         as integer
    field classifica        as integer
    field desc-classifica   as char format "x(40)"
    FIELD cod-estab-ini     LIKE it-dep-fat.cod-estabel
    FIELD cod-estab-fim     LIKE it-dep-fat.cod-estabel
    FIELD nr-embarque-ini   LIKE it-pre-fat.nr-embarque
    FIELD nr-embarque-fim   LIKE it-pre-fat.nr-embarque
    FIELD nr-resumo-ini     LIKE it-pre-fat.nr-resumo
    FIELD nr-resumo-fim     LIKE it-pre-fat.nr-resumo
    FIELD nr-pedcli-ini     LIKE pre-fatur.nr-pedcli
    FIELD nr-pedcli-fim     LIKE pre-fatur.nr-pedcli
    FIELD nome-abrev-ini    LIKE pre-fatur.nome-abrev
    FIELD nome-abrev-fim    LIKE pre-fatur.nome-abrev
    FIELD it-codigo-ini     LIKE it-pre-fat.it-codigo
    FIELD it-codigo-fim     LIKE it-pre-fat.it-codigo
    FIELD lote-ini          LIKE it-dep-fat.nr-serlote
    FIELD lote-fim          LIKE it-dep-fat.nr-serlote
    FIELD id-docto          LIKE wm-docto.id-docto
    .

define temp-table tt-digita no-undo
    field ordem            as integer   format ">>>>9"
    field exemplo          as character format "x(30)"
    index id ordem.

define buffer b-tt-digita for tt-digita.

def temp-table tt-raw-digita
   field raw-digita      as raw.
