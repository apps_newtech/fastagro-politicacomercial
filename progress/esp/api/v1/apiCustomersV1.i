DEFINE TEMP-TABLE {1} NO-UNDO SERIALIZE-NAME 'customers':U
	FIELD atividade       AS CHARACTER INITIAL ? SERIALIZE-NAME 'activityBranch':U
	FIELD cgc             AS CHARACTER INITIAL ? SERIALIZE-NAME 'personalId':U
	FIELD cidade          AS CHARACTER INITIAL ? SERIALIZE-NAME 'city':U
	FIELD cod-canal-venda AS INTEGER   INITIAL ? SERIALIZE-NAME 'salesChannel':U
	FIELD cod-cond-pag    AS INTEGER   INITIAL ? SERIALIZE-NAME 'paymentTerms':U
	FIELD cod-emitente    AS INTEGER   INITIAL ? SERIALIZE-NAME 'customerCode':U
	FIELD cod-gr-cli      AS INTEGER   INITIAL ? SERIALIZE-NAME 'customerGroup':U
	FIELD cod-inscr-inss  AS CHARACTER INITIAL ? SERIALIZE-NAME 'registrationInss':U
	FIELD cod-rep         AS INTEGER   INITIAL ? SERIALIZE-NAME 'representativeCode':U
	FIELD cod-transp      AS INTEGER   INITIAL ? SERIALIZE-NAME 'standardCarrierCode':U
	FIELD dt-ult-venda    AS DATE      INITIAL ? SERIALIZE-NAME 'lastSale':U
	FIELD e-mail          AS CHARACTER INITIAL ? SERIALIZE-NAME 'email':U
	FIELD endereco_text   AS CHARACTER INITIAL ? SERIALIZE-NAME 'completeAddress':U
	FIELD estado          AS CHARACTER INITIAL ? SERIALIZE-NAME 'state':U
	FIELD home-page       AS CHARACTER INITIAL ? SERIALIZE-NAME 'homePage':U
	FIELD ins-estadual    AS CHARACTER INITIAL ? SERIALIZE-NAME 'stateRegistration':U
	FIELD ins-municipal   AS CHARACTER INITIAL ? SERIALIZE-NAME 'municipalRegistration':U
	FIELD linha-produt    AS CHARACTER INITIAL ? SERIALIZE-NAME 'productLine':U
	FIELD nat-ope-ext     AS CHARACTER INITIAL ? SERIALIZE-NAME 'interstateTransactionType':U
	FIELD nat-operacao    AS CHARACTER INITIAL ? SERIALIZE-NAME 'transactionType':U
	FIELD natureza        AS INTEGER   INITIAL ? SERIALIZE-NAME 'operation':U
	FIELD nome-abrev      AS CHARACTER INITIAL ? SERIALIZE-NAME 'shortName':U
	FIELD nome-emit       AS CHARACTER INITIAL ? SERIALIZE-NAME 'customerName':U
	FIELD nome-matriz     AS CHARACTER INITIAL ? SERIALIZE-NAME 'matrixName':U
	FIELD nome-mic-reg    AS CHARACTER INITIAL ? SERIALIZE-NAME 'shortRegion':U
	FIELD nr-mesina       AS INTEGER   INITIAL ? SERIALIZE-NAME 'monthsInactive':U
	FIELD nr-tabpre       AS CHARACTER INITIAL ? SERIALIZE-NAME 'priceTable':U
	FIELD observacoes     AS CHARACTER INITIAL ? SERIALIZE-NAME 'comments':U
    FIELD ind-cre-cli     AS INTEGER   INITIAL ? SERIALIZE-NAME 'creditIndicator':U
    FIELD desc-cre-cli    AS CHARACTER INITIAL ? SERIALIZE-NAME 'descCreditIndicator':U
    FIELD lim-credito     AS DECIMAL   INITIAL ? SERIALIZE-NAME 'creditLimit':U
    FIELD lim-adicional   AS DECIMAL   INITIAL ? SERIALIZE-NAME 'additionalCreditLimit':U
    FIELD bonificacao     AS DECIMAL   INITIAL ? SERIALIZE-NAME 'bonus':U
	FIELD ramal1          AS CHARACTER
	FIELD phone1          AS CHARACTER 
    FIELD phone2          AS CHARACTER
    FIELD location        AS CHARACTER /* Campo concatenado: cidade + estado */
    FIELD deleted         AS LOGICAL
    .

DEFINE TEMP-TABLE tt-canal-cliente NO-UNDO SERIALIZE-NAME 'clientChannel':U
	FIELD cod-canal-venda   AS INTEGER   INITIAL ? SERIALIZE-NAME 'salesChannel':U
	FIELD cod-emitente      AS INTEGER   INITIAL ? SERIALIZE-NAME 'customerCode':U
	FIELD dat-inic-validade AS DATE      INITIAL ? SERIALIZE-NAME 'validityStartDate':U
	FIELD dat-fim-validade  AS DATE      INITIAL ? SERIALIZE-NAME 'expiryDate':U
	FIELD bonificacao       AS DECIMAL   INITIAL ? SERIALIZE-NAME 'discountPercentage':U
	FIELD cod-cond-pag      AS INTEGER   INITIAL ? SERIALIZE-NAME 'paymentTerms':U
	FIELD nr-tabpre         AS CHARACTER INITIAL ? SERIALIZE-NAME 'priceTable':U
	FIELD cod-transp        AS INTEGER   INITIAL ? SERIALIZE-NAME 'standardCarrierCode':U.

DEFINE TEMP-TABLE tt-canal-clien-estab NO-UNDO SERIALIZE-NAME 'clientChannelEstablishment'
    FIELD cod-canal-venda   AS INTEGER   INITIAL ? SERIALIZE-NAME 'salesChannel':U
    FIELD cod-emitente      AS INTEGER   INITIAL ? SERIALIZE-NAME 'customerCode':U
    FIELD cod-estabel       AS CHARACTER INITIAL ? SERIALIZE-NAME 'establishment'
    FIELD dat-inic-validade AS DATE      INITIAL ? SERIALIZE-NAME 'validityStartDate':U
	FIELD dat-fim-validade  AS DATE      INITIAL ? SERIALIZE-NAME 'expiryDate':U
    FIELD bonificacao       AS DECIMAL   INITIAL ? SERIALIZE-NAME 'discountPercentage':U
    FIELD cod-cond-pag      AS INTEGER   INITIAL ? SERIALIZE-NAME 'paymentTerms':U
	FIELD nr-tabpre         AS CHARACTER INITIAL ? SERIALIZE-NAME 'priceTable':U
	FIELD cod-transp        AS INTEGER   INITIAL ? SERIALIZE-NAME 'standardCarrierCode':U.

DEFINE TEMP-TABLE tt-distrib-emit-estab NO-UNDO SERIALIZE-NAME 'clientEstablishment'
    FIELD cod-emitente        AS INTEGER   INITIAL ? SERIALIZE-NAME 'customerCode':U
    FIELD cod-estabel         AS CHARACTER INITIAL ? SERIALIZE-NAME 'establishment'
    FIELD cod-canal-venda     AS INTEGER   INITIAL ? SERIALIZE-NAME 'salesChannel':U
    FIELD cod-cond-pag        AS INTEGER   INITIAL ? SERIALIZE-NAME 'paymentTerms':U
	FIELD nr-tabpre           AS CHARACTER INITIAL ? SERIALIZE-NAME 'priceTable':U
    FIELD val-perc-desc-clien AS DECIMAL   INITIAL ? SERIALIZE-NAME 'discountPercentage':U.
	
DEFINE DATASET returnCustomer
    FOR {1}, tt-canal-cliente, tt-canal-clien-estab, tt-distrib-emit-estab
    DATA-RELATION FOR {1}, tt-canal-cliente      RELATION-FIELDS (cod-emitente, cod-emitente) NESTED
    DATA-RELATION FOR {1}, tt-canal-clien-estab  RELATION-FIELDS (cod-emitente, cod-emitente) NESTED
    DATA-RELATION FOR {1}, tt-distrib-emit-estab RELATION-FIELDS (cod-emitente, cod-emitente) NESTED.
