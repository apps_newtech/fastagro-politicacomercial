DEFINE TEMP-TABLE Product NO-UNDO SERIALIZE-NAME 'product':U
	FIELD it-codigo            AS CHARACTER    INITIAL ? SERIALIZE-NAME 'product':U
    FIELD desc-item            AS CHARACTER    INITIAL ? SERIALIZE-NAME 'productDescription':U
    FIELD ge-codigo            AS INTEGER      INITIAL ? SERIALIZE-NAME 'stockGroup':U
    FIELD class-fiscal         AS CHARACTER    INITIAL ? SERIALIZE-NAME 'fiscalClassification':U
    FIELD fm-codigo            AS CHARACTER    INITIAL ? SERIALIZE-NAME 'family':U
    FIELD fm-cod-com           AS CHARACTER    INITIAL ? SERIALIZE-NAME 'commercialFamily':U.