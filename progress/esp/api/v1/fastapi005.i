/********************************************************************************
*** 20/05/2021 Douglas Souza
**             Defini��es para API da pol�tica comercial da Fast Agro.
*******************************************************************************/
DEF TEMP-TABLE ttes_pedido_web_item
    FIELD it_codigo         as char  // es_pedido_web_item.it_codigo        
    FIELD nr_pedido_web     as INT   // es_pedido_web_item.nr_pedido_web    
    FIELD seq_item          as INT // es_pedido_web_item.seq_item         
    FIELD quantidade        as DEC   // es_pedido_web_item.quantidade       
    FIELD un                as char  // es_pedido_web_item.un               
    FIELD cod_emitente      as INT   // es_pedido_web.cod_emitente          
    FIELD dt_entrega        as DATE  // es_pedido_web.dt_entrega            
    FIELD dt_vencimento     as DATE  // es_pedido_web.dt_vencimento         
    FIELD nr_pedcli         as char  // es_pedido_web.nr_pedcli             
    FIELD nr_pedido         as INT  // es_pedido_web.nr_pedido             
    FIELD nome_abrev        as char  // es_pedido_web.nome_abrev            
    FIELD tipo_frete        as char  // es_pedido_web.tipo_frete            
    FIELD tipo_carregamento as char  // es_pedido_web.tipo_carregamento .   
    FIELD bairro            as char  // emitente.bairro                     
    FIELD cidade            as char  // emitente.cidade                     
    FIELD e-mail            as char  // emitente.e-mail                     
    FIELD endereco          as char  // emitente.endereco                   
    FIELD estado            as char  // emitente.estado                     
    FIELD telefone1         as char  // emitente.telefone[1]                
    FIELD telefone2         as char  // emitente.telefone[2]                
    FIELD endereco-cob      as char  // emitente.endereco-cob               
    FIELD cgc               as char  // emitente.cgc                        
    FIELD ins-estadual      as char  // emitente.ins-estadual               
    FIELD contato           as char  // emitente.contato                    
    FIELD cep               as char  // emitente.cep                        
    FIELD item-descricao    as char.  // item.descricao-1.                   
