BLOCK-LEVEL ON ERROR UNDO, THROW.

USING PROGRESS.json.*.
USING PROGRESS.json.ObjectModel.*.
USING com.totvs.framework.api.*.

{include/i-prgvrs.i apiRepresentativesPublic 2.00.00.002 } /*** "010002" ***/

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i apiRepresentativesPublic MCD}
&ENDIF

{method/dbotterr.i}
{cdp/utils.i}

{esp/api/v1/apiRepresentativesPublicV1.i RepresentativesPublic}

DEFINE VARIABLE boHandler AS HANDLE NO-UNDO.

/*:T--- FUNCTIONS ---*/

FUNCTION fn-get-id-from-path RETURNS CHARACTER (
    INPUT oRequest AS JsonAPIRequestParser
):
    RETURN oRequest:getPathParams():GetCharacter(1).
END FUNCTION.  

FUNCTION fn-has-row-errors RETURNS LOGICAL ():

    FOR EACH RowErrors 
        WHERE UPPER(RowErrors.ErrorType) = 'INTERNAL':U:
        DELETE RowErrors. 
    END.

    RETURN CAN-FIND(FIRST RowErrors 
        WHERE UPPER(RowErrors.ErrorSubType) = 'ERROR':U).
    
END FUNCTION.

/*:T--- QUERY PROCEDURES V1 ---*/

PROCEDURE pi-get-v1:

    DEFINE INPUT  PARAM oInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM oOutput AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM TABLE FOR RowErrors.

    DEFINE VARIABLE oRequest AS JsonAPIRequestParser NO-UNDO.
    DEFINE VARIABLE cExcept  AS CHARACTER            NO-UNDO.
    DEFINE VARIABLE tableKey AS integer      NO-UNDO.

    ASSIGN oRequest = NEW JsonAPIRequestParser(oInput).

    ASSIGN tableKey = INTEGER(fn-get-id-from-path(oRequest)).


    ASSIGN cExcept = JsonAPIUtils:getTableExceptFieldsBySerializedFields(
        TEMP-TABLE RepresentativesPublic:HANDLE, oRequest:getFields()
    ).

    FOR FIRST repres FIELDS (
        bairro cep cgc cidade cod-rep Complemento dt-deslig e-mail endereco endereco_text estado ind-situacao ins-municipal inscr-est natureza nome nome-ab-reg nome-abrev pais telefax telefone telex tipo-repres
    comis-max) NO-LOCK 
        WHERE repres.cod-rep EQ tableKey:
        
        CREATE RepresentativesPublic.
        TEMP-TABLE RepresentativesPublic:HANDLE:DEFAULT-BUFFER-HANDLE:BUFFER-COPY(
            BUFFER repres:HANDLE, cExcept
        ).

        FOR FIRST mg.cidade NO-LOCK
            WHERE cidade.cidade = repres.cidade: END.

        ASSIGN RepresentativesPublic.cod-ibge         = IF AVAIL cidade THEN STRING(cidade.cdn-munpio-ibge) ELSE ""
               RepresentativesPublic.desc-natureza    = {adinc/i03ad098.i 04 repres.natureza}
               RepresentativesPublic.desc-tipo-repres = {adinc/i01ad229.i 04 repres.tipo-repres}
               RepresentativesPublic.telefone1        = repres.telefone[1]
               RepresentativesPublic.telefone2        = repres.telefone[2]
               RepresentativesPublic.dComissaoMax     = (repres.comis-max).

        ASSIGN oOutput = JsonAPIUtils:convertTempTableFirstItemToJsonObject(
            TEMP-TABLE RepresentativesPublic:HANDLE, (LENGTH(TRIM(cExcept)) > 0)
        ).
    END.
    
    CATCH eSysError AS Progress.Lang.SysError:
        CREATE RowErrors.
        ASSIGN RowErrors.ErrorNumber = 17006
               RowErrors.ErrorDescription = eSysError:getMessage(1)
               RowErrors.ErrorSubType = "ERROR".
    END.
    FINALLY: 
        IF fn-has-row-errors() THEN DO:
            UNDO, RETURN 'NOK':U.
        END.
    END FINALLY.

END PROCEDURE.


PROCEDURE pi-query-v1:

    DEFINE INPUT  PARAM oInput      AS JsonObject   NO-UNDO.
    DEFINE OUTPUT PARAM aOutput     AS JsonArray    NO-UNDO.
    DEFINE OUTPUT PARAM lHasNext    AS LOGICAL      NO-UNDO INITIAL FALSE.
    DEFINE OUTPUT PARAM iTotalHits  AS INTEGER      NO-UNDO.
    DEFINE OUTPUT PARAM TABLE FOR RowErrors.

    EMPTY TEMP-TABLE RowErrors.
    EMPTY TEMP-TABLE RepresentativesPublic.

    DEFINE VARIABLE oRequest   AS JsonAPIRequestParser  NO-UNDO.
    DEFINE VARIABLE iCount     AS INTEGER INITIAL 0     NO-UNDO.

    DEFINE VARIABLE cExcept     AS CHARACTER             NO-UNDO.
    DEFINE VARIABLE cQueryQuickSearch AS CHARACTER      NO-UNDO.
    DEFINE VARIABLE cQuery      AS CHARACTER             NO-UNDO.
    DEFINE VARIABLE cQueryName  AS CHARACTER             NO-UNDO.
	DEFINE VARIABLE cBy         AS CHARACTER             NO-UNDO.
    DEFINE VARIABLE lTotalCount AS LOGICAL               NO-UNDO.

    ASSIGN oRequest = NEW JsonAPIRequestParser(oInput).    

    ASSIGN cExcept = JsonAPIUtils:getTableExceptFieldsBySerializedFields(
        TEMP-TABLE RepresentativesPublic:HANDLE, oRequest:getFields()
    ).

    IF oRequest:getQueryParams():has("totalCount") THEN DO:
    	ASSIGN lTotalCount = LOGICAL(oRequest:getQueryParams():getJsonArray("totalCount"):getCharacter(1)).
    END.

    ASSIGN cQuery = 'FOR EACH repres NO-LOCK':U.
    ASSIGN cQueryQuickSearch = JsonAPIUtils:getPropertyJsonObject(oRequest:getQueryParams(), 'quickSearch':U).
	IF cQueryQuickSearch NE ? AND LENGTH(TRIM(cQueryQuickSearch)) > 0 THEN DO:
		ASSIGN cQuery = cQuery + ' WHERE (repres.nome-abrev MATCHES "*':U + cQueryQuickSearch + '*"':U
               cQuery = cQuery + ' OR repres.nome MATCHES "*':U + cQueryQuickSearch + '*") ':U
               cQuery = cQuery + ' OR string(repres.cod-rep) = "':U + cQueryQuickSearch + '" ':U.
              

        /*ASSIGN iCode = INT(cQueryQuickSearch) NO-ERROR.
        IF iCode NE ? AND iCode > 0 THEN DO:
            ASSIGN cQuery = cQuery + ' OR emitente.cod-emitente = ':U + STRING(iCode) + ' ':U.
            ASSIGN cQuery = cQuery + ' OR emitente.cgc          BEGINS ':U + STRING(iCode) + ' ':U.
        END.     */   
	END.
    ELSE DO:
       ASSIGN cQuery = buildWhere(TEMP-TABLE RepresentativesPublic:HANDLE, oRequest:getQueryParams(), "", cQuery). // FUNCTION buildWhere NA INCLUDE CDP/UTILS.I
    END.
            cBy    = buildBy(TEMP-TABLE RepresentativesPublic:HANDLE, oRequest:getOrder()).                      // FUNCTION buildBy    NA INCLUDE CDP/UTILS.I
            cQuery = cQuery + cBy.           

    DEFINE QUERY findQuery FOR repres 
        FIELDS(comis-max bairro cep cgc cidade cod-rep Complemento dt-deslig e-mail endereco endereco_text estado ind-situacao ins-municipal inscr-est natureza nome nome-ab-reg nome-abrev pais telefax telefone telex tipo-repres)
    SCROLLING.

    QUERY findQuery:QUERY-PREPARE(cQuery).
    QUERY findQuery:QUERY-OPEN().
    QUERY findQuery:REPOSITION-TO-ROW(oRequest:getStartRow()).

    REPEAT:

        GET NEXT findQuery.
        IF QUERY findQuery:QUERY-OFF-END THEN LEAVE.

        IF oRequest:getPageSize() EQ iCount THEN DO:
            ASSIGN lHasNext = TRUE.
            LEAVE.
        END.

        CREATE RepresentativesPublic.
        TEMP-TABLE RepresentativesPublic:HANDLE:DEFAULT-BUFFER-HANDLE:BUFFER-COPY(
            BUFFER repres:HANDLE, cExcept
        ).

        FOR FIRST mg.cidade NO-LOCK
            WHERE cidade.cidade = repres.cidade: END.

        ASSIGN RepresentativesPublic.cod-ibge         = IF AVAIL cidade THEN STRING(cidade.cdn-munpio-ibge) ELSE ""
               RepresentativesPublic.desc-natureza    = {adinc/i03ad098.i 04 repres.natureza}
               RepresentativesPublic.desc-tipo-repres = {adinc/i01ad229.i 04 repres.tipo-repres}
               RepresentativesPublic.telefone1        = repres.telefone[1]
               RepresentativesPublic.telefone2        = repres.telefone[2]
               RepresentativesPublic.dComissaoMax     = (repres.comis-max).
        
        ASSIGN iCount = iCount + 1.
    END.

    IF lTotalCount EQ YES THEN DO:
        GET FIRST findQuery.

    	DO WHILE NOT QUERY findQuery:QUERY-OFF-END:
    		iTotalHits = iTotalHits + 1.
    		GET NEXT findQuery.
    	END.
    END.
    ELSE iTotalHits = iCount.

    ASSIGN aOutput = JsonAPIUtils:convertTempTableToJsonArray(
        TEMP-TABLE RepresentativesPublic:HANDLE, (LENGTH(TRIM(cExcept)) > 0)
    ).

    CATCH eSysError AS Progress.Lang.SysError:
        CREATE RowErrors.
        ASSIGN RowErrors.ErrorNumber = 17006
               RowErrors.ErrorDescription = eSysError:getMessage(1)
               RowErrors.ErrorSubType = "ERROR".
    END.
    FINALLY: 
        IF fn-has-row-errors() THEN DO:
            UNDO, RETURN 'NOK':U.
        END.
    END FINALLY.

END PROCEDURE.


