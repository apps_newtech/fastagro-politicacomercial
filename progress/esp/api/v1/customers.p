USING Progress.Lang.Error.
USING com.totvs.framework.api.JsonApiResponseBuilder.

{utp/ut-api.i}
{utp/ut-api-utils.i}

{include/i-prgvrs.i customers 2.00.00.000 } /*** "010000" ***/

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i customers MPD}
&ENDIF

{utp/ut-api-action.i pi-diff  GET /app/diff~*} /* rota utilizada pelo app de pedidos offline */
{utp/ut-api-action.i pi-diff  GET /app/~*} /* rota utilizada pelo app de pedidos offline */
{utp/ut-api-action.i pi-get   GET /~*/}
{utp/ut-api-action.i pi-query GET /~*}

/************** AS ROTAS PUT | POST | PATCH | DELETE N�O SER�O UTILIZADAS NO APP Pedido OFFLine ************/

// {utp/ut-api-action.i pi-create POST /~*}
// {utp/ut-api-action.i pi-update PUT /~*}
// {utp/ut-api-action.i pi-upatch PATCH /~*}
// {utp/ut-api-action.i pi-delete DELETE /~*}

/************** AS ROTAS PUT | POST | PATCH | DELETE N�O SER�O UTILIZADAS NO APP Pedido OFFLine ************/

{utp/ut-api-notfound.i}

DEFINE VARIABLE apiHandler AS HANDLE NO-UNDO.

{utils/GetNowReturnIsoDate.i}
{utils/ParseJsonResponseAddTotvsSyncDate.i}

/*:T--- PROCEDURES V1 ---*/

PROCEDURE pi-get:
    
    DEFINE INPUT  PARAM oInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM oOutput AS JsonObject NO-UNDO.
    
    IF NOT VALID-HANDLE(apiHandler) THEN DO:
        RUN esp/api/v1/apiCustomers.p PERSISTENT SET apiHandler.
    END.

    RUN pi-get-v1 IN apiHandler (
        INPUT oInput,
        OUTPUT oOutput,
        OUTPUT TABLE RowErrors
    ).

    IF CAN-FIND(FIRST RowErrors WHERE UPPER(RowErrors.ErrorSubType) = 'ERROR':U) THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(TEMP-TABLE RowErrors:HANDLE).
    END.
    ELSE DO:
        IF oOutput EQ ? THEN DO:
            ASSIGN oOutput = JsonApiResponseBuilder:empty(404).
        END.
        ELSE DO:
            ASSIGN oOutput = JsonApiResponseBuilder:ok(oOutput).
        END.
    END.

    CATCH oE AS Error:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(oE).
    END CATCH.

    FINALLY: DELETE PROCEDURE apiHandler NO-ERROR. END FINALLY.

END PROCEDURE.

PROCEDURE pi-diff:
    
    DEFINE INPUT  PARAM oInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM oOutput AS JsonObject NO-UNDO.

    DEFINE VARIABLE lHasNext AS LOGICAL   NO-UNDO.
    DEFINE VARIABLE aResult  AS JsonArray NO-UNDO.
    DEFINE VARIABLE cNow     AS CHARACTER NO-UNDO.

    IF NOT VALID-HANDLE(apiHandler) THEN DO:
        RUN esp/api/v1/apiCustomers.p PERSISTENT SET apiHandler.
    END.

    ASSIGN cNow = GetNowReturnIsoDate().

    RUN pi-diff-v1 IN apiHandler  (
        INPUT oInput,
        OUTPUT aResult,
        OUTPUT lHasNext,
        OUTPUT TABLE RowErrors
    ).

    IF CAN-FIND(FIRST RowErrors WHERE UPPER(RowErrors.ErrorSubType) = 'ERROR':U) THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(TEMP-TABLE RowErrors:HANDLE).
    END.

    ELSE DO:
        ASSIGN oOutput = JsonApiResponseBuilder:ok(aResult, lHasNext)
               oOutput = ParseJsonResponseAddTotvsSyncDate(INPUT oOutput, INPUT cNow). /* devolve data da ultima sincronizacao */
    END.
    
    CATCH oE AS ERROR:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(oE).
    END CATCH.
    
    FINALLY: DELETE PROCEDURE apiHandler NO-ERROR. END FINALLY.

END PROCEDURE.

PROCEDURE pi-query:
    
    DEFINE INPUT  PARAM oInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM oOutput AS JsonObject NO-UNDO.

    DEFINE VARIABLE lHasNext AS LOGICAL   NO-UNDO.
    DEFINE VARIABLE aResult  AS JsonArray NO-UNDO.

    IF NOT VALID-HANDLE(apiHandler) THEN DO:
        RUN esp/api/v1/apiCustomers.p PERSISTENT SET apiHandler.
    END.

        RUN pi-query-v1 IN apiHandler  (
        INPUT oInput,
        OUTPUT aResult,
        OUTPUT lHasNext,
        OUTPUT TABLE RowErrors
    ).

    IF CAN-FIND(FIRST RowErrors WHERE UPPER(RowErrors.ErrorSubType) = 'ERROR':U) THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(TEMP-TABLE RowErrors:HANDLE).
    END.

    ELSE DO:
        ASSIGN oOutput = JsonApiResponseBuilder:ok(aResult, lHasNext).
    END.
    
    CATCH oE AS ERROR:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(oE).
    END CATCH.
    
    FINALLY: DELETE PROCEDURE apiHandler NO-ERROR. END FINALLY.

END PROCEDURE.

/*

PROCEDURE pi-create:

    DEFINE INPUT  PARAM oInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM oOutput AS JsonObject NO-UNDO.

    IF NOT VALID-HANDLE(apiHandler) THEN DO:
        RUN esp/api/v1/apiCustomers.p PERSISTENT SET apiHandler.
    END.

    RUN pi-create-v1 IN apiHandler (
        INPUT oInput,
        OUTPUT oOutput,
        OUTPUT TABLE RowErrors
    ).

    IF CAN-FIND(FIRST RowErrors WHERE UPPER(RowErrors.ErrorSubType) = 'ERROR':U) THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(TEMP-TABLE RowErrors:HANDLE).
    END.
    ELSE DO:
        ASSIGN oOutput = JsonApiResponseBuilder:ok(oOutput, 201).
    END.

    CATCH oE AS Error:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(oE).
    END CATCH.
    
    FINALLY: DELETE PROCEDURE apiHandler NO-ERROR. END FINALLY.

END PROCEDURE.

PROCEDURE pi-update:

    DEFINE INPUT  PARAM oInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM oOutput AS JsonObject NO-UNDO.

    IF NOT VALID-HANDLE(apiHandler) THEN DO:
        RUN esp/api/v1/apiCustomers.p PERSISTENT SET apiHandler.
    END.

    RUN pi-update-v1 IN apiHandler (
        INPUT oInput,
        OUTPUT oOutput,
        OUTPUT TABLE RowErrors
    ).

    IF CAN-FIND(FIRST RowErrors WHERE UPPER(RowErrors.ErrorSubType) = 'ERROR':U) THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(TEMP-TABLE RowErrors:HANDLE).
    END.
    ELSE IF oOutput EQ ? THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:empty(404).
    END.
    ELSE DO:
        ASSIGN oOutput = JsonApiResponseBuilder:ok(oOutput).
    END.

    CATCH oE AS Error:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(oE).
    END CATCH.
    
    FINALLY: DELETE PROCEDURE apiHandler NO-ERROR. END FINALLY.

END PROCEDURE.

PROCEDURE pi-upatch:
    
    DEFINE INPUT  PARAM oInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM oOutput AS JsonObject NO-UNDO.

    IF NOT VALID-HANDLE(apiHandler) THEN DO:
        RUN esp/api/v1/apiCustomers.p PERSISTENT SET apiHandler.
    END.

    RUN pi-upatch-v1 IN apiHandler (
        INPUT oInput,
        OUTPUT oOutput,
        OUTPUT TABLE RowErrors
    ).

    IF CAN-FIND(FIRST RowErrors WHERE UPPER(RowErrors.ErrorSubType) = 'ERROR':U) THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(TEMP-TABLE RowErrors:HANDLE).
    END.
    ELSE IF oOutput EQ ? THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:empty(404).
    END.
    ELSE DO:
        ASSIGN oOutput = JsonApiResponseBuilder:ok(oOutput).
    END.

    CATCH oE AS Error:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(oE).
    END CATCH.
    
    FINALLY: DELETE PROCEDURE apiHandler NO-ERROR. END FINALLY.

END PROCEDURE.

PROCEDURE pi-delete:
    
    DEFINE INPUT  PARAM oInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAM oOutput AS JsonObject NO-UNDO.
   
    IF NOT VALID-HANDLE(apiHandler) THEN DO:
        RUN esp/api/v1/apiCustomers.p PERSISTENT SET apiHandler.
    END.

    RUN pi-delete-v1 IN apiHandler (
        INPUT oInput,
        OUTPUT TABLE RowErrors
    ).

    IF CAN-FIND(FIRST RowErrors WHERE UPPER(RowErrors.ErrorSubType) = 'ERROR':U) THEN DO:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(TEMP-TABLE RowErrors:HANDLE).
    END.
    ELSE DO:
        ASSIGN oOutput = JsonApiResponseBuilder:empty().
    END.

    CATCH oE AS Error:
        ASSIGN oOutput = JsonApiResponseBuilder:asError(oE).
    END CATCH.
    
    FINALLY: DELETE PROCEDURE apiHandler NO-ERROR. END FINALLY.

END PROCEDURE.
*/
