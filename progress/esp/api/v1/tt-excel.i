DEFINE VARIABLE c-headers AS CHARACTER   NO-UNDO.
ASSIGN c-headers = "nr_pedido_web; +
    nr_pedcli; +
    cod_emitente; +
    cgc; +
    ins_estadual; +
    endereco; +
    bairro; +
    cidade; +
    estado; +
    cep; +
    email; +
    telefone; +
    status_pedido_web; +
    status_nr_pedcli; +
    status_aprovacao; +
    aprovador; +
    status_financeiro; +
    cultura; +
    campanha; +
    vendedor; +
    tipo_venda; +
    tipo_frete; +
    tipo_carregamento; +
    data_entrega; +
    data_vencimento; +
    item - Sequ�ncia; +
    item - Descri��o; +
    item - Unidade; +
    item - Quantidade; +
    item - Origem; +
    item - Qtd Embalagem; +
    item - Cor Faixa; +
    item - Valor Total; +
    item - Pre�o NET/FOB com EF; +
    item - Pre�o NET/FOB; +
    item - Pre�o NF; +
    item - Pre�o NET Target;"
    .

DEFINE TEMP-TABLE ttExportaExcel NO-UNDO
    FIELD nr_pedido_web     AS CHARACTER
    FIELD nr_pedcli         AS CHARACTER
    FIELD cod_emitente      AS CHARACTER
    FIELD nome              AS CHARACTER
    FIELD cgc               AS CHARACTER 
    FIELD ins_estadual      AS CHARACTER
    FIELD endereco          AS CHARACTER
    FIELD bairro            AS CHARACTER
    FIELD cidade            AS CHARACTER
    FIELD estado            AS CHARACTER
    FIELD cep               AS CHARACTER
    FIELD email             AS CHARACTER
    FIELD telefone          AS CHARACTER
    FIELD status_pedido_web AS CHARACTER
    FIELD status_nr_pedcli  AS CHARACTER 
    FIELD status_aprovacao  AS CHARACTER
    FIELD aprovador         AS CHARACTER
    FIELD status_financeiro AS CHARACTER 
    FIELD cultura           AS CHARACTER
    FIELD campanha          AS CHARACTER
    FIELD vendedor          AS CHARACTER
    FIELD tipo_venda        AS CHARACTER
    FIELD tipo_frete        AS CHARACTER
    FIELD tipo_carregamento AS CHARACTER
    FIELD data_entrega      AS CHARACTER
    FIELD data_vencimento   AS CHARACTER
    FIELD seq_item          AS CHARACTER
    FIELD desc_item         AS CHARACTER
    FIELD un                AS CHARACTER
    FIELD quantidade        AS CHARACTER
    FIELD origem            AS CHARACTER
    FIELD qtd_embalagem     AS CHARACTER
    FIELD cor_faixa         AS CHARACTER
    FIELD valor_total       AS CHARACTER
    FIELD preco_net_fob_ef  AS CHARACTER
    FIELD preco_net_fob     AS CHARACTER
    FIELD preco_nf          AS CHARACTER
    FIELD preco_net_target  AS CHARACTER
    .
//ITEM - Qtde Embalagem
//ITEM - Frete R$/Kg
//ITEM - Valor Total
//GRID  - Comiss�o Total (R$)
//GRID  - Comiss�o %
//GRID  - Pre�o NET/FOB com EF
//GRID  - Comiss�o Max Faixa 
//GRID  - %Agente
//GRID  - Comiss�o Agente R$/KG

