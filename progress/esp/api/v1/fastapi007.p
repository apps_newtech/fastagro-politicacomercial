// API especifica para trazer cidade, api padr�o n�o atende requisitos de Query Param           

{utp/ut-api.i}
{utp/ut-api-utils.i}

{utp/ut-api-action.i GetCidades          GET         /cidades/~*/ }
{utp/ut-api-action.i GetCidadesQuery     GET         /cidades/~* }

{utp/ut-api-action.i GetRegiao          GET         /regiao/~*/ }
{utp/ut-api-action.i GetRegiaoQuery     GET         /regiao/~* }

{utp/ut-api-action.i GetEntrega          GET         /entrega/~*/~*/ }
{utp/ut-api-action.i GetEntregaQuery     GET         /entrega/~*/~* }

{utp/ut-api-action.i GetTipoOperacao          GET         /tipooperacao/~*/ }
{utp/ut-api-action.i GetTipoOperacaoQuery     GET         /tipooperacao/~* }

{utp/ut-api-action.i GetRepresentante         GET         /representante/~*/ }
{utp/ut-api-action.i GetRepresentanteQuery    GET         /representante/~* }

//Chamada de um metodo invalido
{utp/ut-api-action.i NoAction        POST        /~* }
{utp/ut-api-action.i NoAction        PUT         /~* }
{utp/ut-api-action.i NoAction        DELETE      /~* }

{utp/ut-api-notfound.i}

// Include faz a requisicao dos dados atraves do objeto JSON passado como parametro (jsonInput)
//{esp/api/v1/parseRequest.i} 

{esp/clientlog.i}
{esp/ApiPoliticaPreco.i}

DEFINE TEMP-TABLE ttCidade NO-UNDO LIKE mgcad.cidade.
DEFINE TEMP-TABLE ttEntrega NO-UNDO LIKE mgcad.loc-entr
FIELD localEntrega AS CHAR.


//DEFINE TEMP-TABLE ttRegiao NO-UNDO LIKE regiao. -- Tabela Regiao com carga de dados sem respeitar o Indice.
DEFINE TEMP-TABLE ttRegiao NO-UNDO
    FIELD nome-ab-reg LIKE regiao.nome-ab-reg
    FIELD nome-regiao LIKE regiao.nome-regiao.

DEFINE TEMP-TABLE ttRepresentante NO-UNDO
    FIELD cod_rep AS CHAR
    FIELD nome AS CHAR
    FIELD cod_usuario AS CHAR
    FIELD nome_ab_reg AS CHAR.

DEFINE TEMP-TABLE ttTipoOperacao NO-UNDO LIKE tip-ped-cia
    FIELD codTipoOperacao AS INT .

DEFINE VARIABLE headers     AS JsonObject NO-UNDO.
DEFINE VARIABLE pathParams  AS JsonArray  NO-UNDO.
DEFINE VARIABLE queryParams AS JsonObject NO-UNDO.
DEFINE VARIABLE startRow    AS INTEGER    NO-UNDO.
DEFINE VARIABLE pageSize    AS INTEGER    NO-UNDO.
DEFINE VARIABLE fieldList   AS CHARACTER  NO-UNDO.
DEFINE VARIABLE expandables AS CHARACTER  NO-UNDO.
DEFINE VARIABLE payload     AS LONGCHAR   NO-UNDO.
DEFINE VARIABLE totalCount  AS INTEGER    NO-UNDO.
DEFINE VARIABLE hasNext     AS LOGICAL    NO-UNDO.

DEFINE VARIABLE oJsonPrincipalArray   AS JsonArray  NO-UNDO.

DEFINE NEW GLOBAL SHARED VARIABLE c-seg-usuario AS CHARACTER NO-UNDO.

DEFINE VARIABLE h-politica AS HANDLE      NO-UNDO.
DEFINE VARIABLE piCodRep       AS INTEGER   NO-UNDO.
DEFINE VARIABLE pcPerfilAcesso AS CHARACTER   NO-UNDO.

// Busca o cadastro de campos MDM-CAMPOS ------------------------------------------------------
PROCEDURE GetCidadesQuery:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.
    
    IF  VALID-OBJECT(queryParams) AND queryParams:Has("filter") THEN
    DO:
        ASSIGN 
            filterText = queryParams:GetJsonArray("filter"):GetCharacter(1).
            
        IF  filterText <> ? AND filterText <> "" THEN 
            ASSIGN 
                whereClause = " WHERE cidade BEGINS '" + filterText + "'".
    END.
    
    RUN executeSearch IN THIS-PROCEDURE(
        INPUT startRow,
        INPUT "mgcad.cidade",
        INPUT TEMP-TABLE ttCidade:DEFAULT-BUFFER-HANDLE,
        INPUT pageSize,
        INPUT whereClause,
        OUTPUT totalCount,
        OUTPUT hasNext).
        
    /* Gera um JsonArray a partir da TEMP-TABLE */
    oJsonPrincipalArray:Read(TEMP-TABLE ttCidade:HANDLE).
    
    /* Ajusta o JSON para conter apenas os campos do atributo "fields" */
    RUN applyFieldsAndExpandables IN THIS-PROCEDURE(
        INPUT oJsonPrincipalArray,
        INPUT fieldList,
        INPUT "",
        INPUT "",
        OUTPUT oJsonPrincipalArray).
    
    /* Padroniza o retorno do JSON conforme os padr�es de API TOTVS */
    RUN createJsonResponse IN THIS-PROCEDURE(INPUT oJsonPrincipalArray, INPUT TABLE RowErrors, INPUT hasNext, OUTPUT jsonOutput).
     
END.

PROCEDURE GetCidades:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.

    DEF VAR oJsonPrincipalObject AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.

     IF (pathParams <> ? AND pathParams:LENGTH > 0) AND pathParams:GetCharacter(2) <> ? THEN DO:
       filterText = pathParams:GetCharacter(2).
     END.
    MESSAGE "filterText" filterText.
    oJsonPrincipalObject = NEW JsonObject().

     FIND LAST mgcad.cidade WHERE cidade.cidade = (filterText) NO-LOCK NO-ERROR.
     IF AVAIL cidade THEN DO:
        oJsonPrincipalObject:Add("cidade", cidade.cidade).
        oJsonPrincipalObject:Add("estado", cidade.estado).
     END.
     
    
    
    jsonOutput = oJsonPrincipalObject.
  
END.

// Busca o cadastro de campos MDM-CAMPOS ------------------------------------------------------
PROCEDURE GetRegiaoQuery:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.
    
    IF  VALID-OBJECT(queryParams) AND queryParams:Has("filter") THEN
    DO:
        ASSIGN 
            filterText = queryParams:GetJsonArray("filter"):GetCharacter(1).
            
        IF  filterText <> ? AND filterText <> "" THEN 
            ASSIGN 
                whereClause = " WHERE regiao.nome-regiao BEGINS '" + filterText + "'".
    END.
    
    RUN executeSearch IN THIS-PROCEDURE(
        INPUT startRow,
        INPUT "regiao",
        INPUT TEMP-TABLE ttRegiao:DEFAULT-BUFFER-HANDLE,
        INPUT pageSize,
        INPUT whereClause,
        OUTPUT totalCount,
        OUTPUT hasNext).

    /* Gera um JsonArray a partir da TEMP-TABLE */
    oJsonPrincipalArray:Read(TEMP-TABLE ttRegiao:HANDLE).
    
    /* Ajusta o JSON para conter apenas os campos do atributo "fields" */
    RUN applyFieldsAndExpandables IN THIS-PROCEDURE(
        INPUT oJsonPrincipalArray,
        INPUT fieldList,
        INPUT "",
        INPUT "",
        OUTPUT oJsonPrincipalArray).
    
    /* Padroniza o retorno do JSON conforme os padr�es de API TOTVS */
    RUN createJsonResponse IN THIS-PROCEDURE(INPUT oJsonPrincipalArray, INPUT TABLE RowErrors, INPUT hasNext, OUTPUT jsonOutput).
     
END.

PROCEDURE GetRegiao:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.

    DEF VAR oJsonPrincipalObject AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.

     IF (pathParams <> ? AND pathParams:LENGTH > 0) AND pathParams:GetCharacter(2) <> ? THEN DO:
       filterText = pathParams:GetCharacter(2).
     END.
    MESSAGE "filterText" filterText.
    oJsonPrincipalObject = NEW JsonObject().

     FIND LAST regiao WHERE regiao.nome-ab-reg = (filterText) NO-LOCK NO-ERROR.
     IF AVAIL regiao THEN DO:
        oJsonPrincipalObject:Add("nome-ab-reg", regiao.nome-ab-reg).
        oJsonPrincipalObject:Add("nome-regiao", regiao.nome-regiao).
     END.
     
    
    
    jsonOutput = oJsonPrincipalObject.
  
END.

PROCEDURE GetRepresentanteQuery:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).
    
    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.
    DEFINE VARIABLE cSubRegiao AS CHARACTER   NO-UNDO.


    RUN esp/ApiPoliticaPreco.p PERSISTENT SET h-politica.
    RUN piRetornaPerfil IN h-politica (INPUT c-seg-usuario,
                                      OUTPUT piCodRep,
                                      OUTPUT pcPerfilAcesso,
                                      OUTPUT TABLE ttRepres,
                                      OUTPUT TABLE RowErrors).
    DELETE PROCEDURE h-politica.

    IF  VALID-OBJECT(queryParams) AND queryParams:Has("filter") THEN
    DO:
        ASSIGN 
            filterText = queryParams:GetJsonArray("filter"):GetCharacter(1).
    END.
    
    FOR EACH ttRepres  NO-LOCK:
        FIND FIRST repres WHERE repres.cod-rep = ttRepres.iCodRep NO-LOCK NO-ERROR.

         IF  filterText <> ? AND filterText <> "" THEN DO:
             IF AVAIL repres THEN DO: 
                 IF NOT repres.nome  BEGINS string(filterText) AND NOT string(repres.cod-rep) BEGINS STRING(filterText) THEN NEXT.
             END.
         END.

          

        FIND FIRST repres WHERE repres.cod-rep = ttRepres.iCodRep NO-LOCK NO-ERROR.

                FIND FIRST representante WHERE
                           representante.cdn_rep = ttRepres.iCodRep NO-LOCK NO-ERROR.
                IF AVAIL representante THEN DO:
                    FIND FIRST pessoa_fisic WHERE
                               pessoa_fisic.num_pessoa_fisic = representante.num_pessoa NO-LOCK NO-ERROR.
                    IF AVAIL pessoa_fisic THEN
                             ASSIGN cSubRegiao = pessoa_fisic.cod_sub_regiao_vendas.

                         IF cSubRegiao = "" THEN DO:
                             FIND FIRST pessoa_jurid WHERE
                                    pessoa_jurid.num_pessoa_jurid = representante.num_pessoa NO-LOCK NO-ERROR.
                         IF AVAIL pessoa_jurid THEN
                             ASSIGN cSubRegiao = pessoa_jurid.cod_sub_regiao_vendas.

                         END.
                END.
             
        CREATE ttRepresentante.
        ASSIGN ttRepresentante.cod_rep = string(ttRepres.iCodRep)
               ttRepresentante.nome    = IF AVAIL repres THEN repres.nome ELSE ""
               ttrepresentante.cod_usuario = ttRepres.cUsuario
               ttRepresentante.nome_ab_reg = cSubRegiao.
    END.


    /* Gera um JsonArray a partir da TEMP-TABLE */
    oJsonPrincipalArray:Read(TEMP-TABLE ttrepresentante:HANDLE).
    
    /* Ajusta o JSON para conter apenas os campos do atributo "fields" */
    RUN applyFieldsAndExpandables IN THIS-PROCEDURE(
        INPUT oJsonPrincipalArray,
        INPUT fieldList,
        INPUT "",
        INPUT "",
        OUTPUT oJsonPrincipalArray).
    
    /* Padroniza o retorno do JSON conforme os padr�es de API TOTVS */
    RUN createJsonResponse IN THIS-PROCEDURE(INPUT oJsonPrincipalArray, INPUT TABLE RowErrors, INPUT hasNext, OUTPUT jsonOutput).
     
END.

PROCEDURE GetRepresentante:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.

    DEFINE VARIABLE cSubRegiao AS CHARACTER   NO-UNDO.

    DEF VAR oJsonPrincipalObject AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.

     IF (pathParams <> ? AND pathParams:LENGTH > 0) AND pathParams:GetCharacter(2) <> ? THEN DO:
       filterText = pathParams:GetCharacter(2).
     END.
    MESSAGE "filterText" filterText.
    oJsonPrincipalObject = NEW JsonObject().

    RUN esp/ApiPoliticaPreco.p PERSISTENT SET h-politica.
    RUN piRetornaPerfil IN h-politica (INPUT c-seg-usuario,
                                      OUTPUT piCodRep,
                                      OUTPUT pcPerfilAcesso,
                                      OUTPUT TABLE ttRepres,
                                      OUTPUT TABLE RowErrors).
    DELETE PROCEDURE h-politica.

    FOR EACH ttRepres WHERE string(ttRepres.iCodRep) = (filterText) NO-LOCK:
        FIND FIRST repres WHERE repres.cod-rep = ttRepres.iCodRep NO-LOCK NO-ERROR.

                FIND FIRST representante WHERE
                           representante.cdn_rep = ttRepres.iCodRep NO-LOCK NO-ERROR.
                IF AVAIL representante THEN DO:
                    FIND FIRST pessoa_fisic WHERE
                               pessoa_fisic.num_pessoa_fisic = representante.num_pessoa NO-LOCK NO-ERROR.
                    IF AVAIL pessoa_fisic THEN
                             ASSIGN cSubRegiao = pessoa_fisic.cod_sub_regiao_vendas.

                         IF cSubRegiao = "" THEN DO:
                             FIND FIRST pessoa_jurid WHERE
                                    pessoa_jurid.num_pessoa_jurid = representante.num_pessoa NO-LOCK NO-ERROR.
                         IF AVAIL pessoa_jurid THEN
                             ASSIGN cSubRegiao = pessoa_jurid.cod_sub_regiao_vendas.

                         END.
                END.

        oJsonPrincipalObject:Add("cod_rep"    , string(ttRepres.iCodRep)).
        oJsonPrincipalObject:Add("nome"       , IF AVAIL repres THEN repres.nome ELSE "").
        oJsonPrincipalObject:Add("cod_usuario", ttRepres.cUsuario).
        oJsonPrincipalObject:Add("nome_ab_reg", cSubRegiao).
        
    END.

  
    jsonOutput = oJsonPrincipalObject.
  
END.


// Busca o cadastro de campos MDM-CAMPOS ------------------------------------------------------
PROCEDURE GetEntregaQuery:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.

    IF (pathParams <> ? AND pathParams:LENGTH > 0) AND pathParams:GetCharacter(2) <> ? THEN DO:
       filterText = pathParams:GetCharacter(2).
     END.
    MESSAGE "filterText" filterText.
    

            
        IF  filterText <> ? AND filterText <> "" THEN 
            ASSIGN 
                whereClause = " WHERE string(loc-entr.nome-abrev) = '" +  string(filterText)+ "'".

    RUN executeSearch IN THIS-PROCEDURE(
        INPUT startRow,
        INPUT "loc-entr",
        INPUT TEMP-TABLE ttEntrega:DEFAULT-BUFFER-HANDLE,
        INPUT pageSize,
        INPUT whereClause,
        OUTPUT totalCount,
        OUTPUT hasNext).

    FOR EACH ttEntrega:
        ASSIGN ttEntrega.localEntrega = ttEntrega.cod-entrega.
    END.
        
    /* Gera um JsonArray a partir da TEMP-TABLE */
    oJsonPrincipalArray:Read(TEMP-TABLE ttEntrega:HANDLE).
    
    /* Ajusta o JSON para conter apenas os campos do atributo "fields" */
    RUN applyFieldsAndExpandables IN THIS-PROCEDURE(
        INPUT oJsonPrincipalArray,
        INPUT fieldList,
        INPUT "",
        INPUT "",
        OUTPUT oJsonPrincipalArray).
    
    /* Padroniza o retorno do JSON conforme os padr�es de API TOTVS */
    RUN createJsonResponse IN THIS-PROCEDURE(INPUT oJsonPrincipalArray, INPUT TABLE RowErrors, INPUT hasNext, OUTPUT jsonOutput).
     
END.

PROCEDURE GetEntrega:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.

    DEF VAR oJsonPrincipalObject AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE filterText2 AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.

     IF (pathParams <> ? AND pathParams:LENGTH > 0) AND pathParams:GetCharacter(2) <> ? THEN DO:
       filterText = pathParams:GetCharacter(2).
     END.
      MESSAGE "filterText" filterText.

     IF (pathParams <> ? AND pathParams:LENGTH > 0) AND pathParams:GetCharacter(3) <> ? THEN DO:
       filterText2 = pathParams:GetCharacter(3).
     END.
    MESSAGE "filterText2" filterText2.
    oJsonPrincipalObject = NEW JsonObject().

     FIND LAST loc-entr WHERE string(loc-entr.nome-abrev) = (filterText) 
                          AND string(loc-entr.cod-entrega) = (filterText2) NO-LOCK NO-ERROR.
     IF AVAIL loc-entr THEN DO:
        oJsonPrincipalObject:Add("localEntrega", loc-entr.cod-entrega).
        oJsonPrincipalObject:Add("endereco", loc-entr.endereco).
        oJsonPrincipalObject:Add("bairro", loc-entr.bairro).
        oJsonPrincipalObject:Add("cep", loc-entr.cep).
        oJsonPrincipalObject:Add("cidade", loc-entr.cidade).
        oJsonPrincipalObject:Add("estado", loc-entr.estado).
        oJsonPrincipalObject:Add("ins-estadual", loc-entr.ins-estadual).
     END.
     
    
    
    jsonOutput = oJsonPrincipalObject.
  
END.

// Busca o cadastro de campos MDM-CAMPOS ------------------------------------------------------
PROCEDURE GetTipoOperacaoQuery:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.
    
    IF  VALID-OBJECT(queryParams) AND queryParams:Has("filter") THEN
    DO:
        ASSIGN 
            filterText = queryParams:GetJsonArray("filter"):GetCharacter(1).
            
        IF  filterText <> ? AND filterText <> "" THEN 
            ASSIGN 
                whereClause = " WHERE tip-ped-cia.descricao BEGINS '" + filterText + "'".
    END.
    
    RUN executeSearch IN THIS-PROCEDURE(
        INPUT startRow,
        INPUT "tip-ped-cia",
        INPUT TEMP-TABLE ttTipoOperacao:DEFAULT-BUFFER-HANDLE,
        INPUT pageSize,
        INPUT whereClause,
        OUTPUT totalCount,
        OUTPUT hasNext).

    FOR EACH ttTipoOperacao:
        ASSIGN ttTipoOperacao.codTipoOperacao = ttTipoOperacao.cdn-tip-ped.
    END.
        
    /* Gera um JsonArray a partir da TEMP-TABLE */
    oJsonPrincipalArray:Read(TEMP-TABLE ttTipoOperacao:HANDLE).
    
    /* Ajusta o JSON para conter apenas os campos do atributo "fields" */
    RUN applyFieldsAndExpandables IN THIS-PROCEDURE(
        INPUT oJsonPrincipalArray,
        INPUT fieldList,
        INPUT "",
        INPUT "",
        OUTPUT oJsonPrincipalArray).
    
    /* Padroniza o retorno do JSON conforme os padr�es de API TOTVS */
    RUN createJsonResponse IN THIS-PROCEDURE(INPUT oJsonPrincipalArray, INPUT TABLE RowErrors, INPUT hasNext, OUTPUT jsonOutput).
     
END.

PROCEDURE GetTipoOperacao:

    DEFINE INPUT  PARAMETER jsonInput  AS JsonObject NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput AS JsonObject NO-UNDO.

    DEF VAR oJsonPrincipalObject AS JsonObject NO-UNDO.
    
    RUN parseInputParameters(INPUT jsonInput, OUTPUT headers, OUTPUT pathParams, OUTPUT queryParams, OUTPUT startRow, OUTPUT pageSize, OUTPUT fieldList, OUTPUT expandables, OUTPUT payload).

    oJsonPrincipalArray = NEW JsonArray().

    DEFINE VARIABLE filterText AS CHARACTER NO-UNDO.
    DEFINE VARIABLE whereClause AS CHARACTER NO-UNDO.

     IF (pathParams <> ? AND pathParams:LENGTH > 0) AND pathParams:GetCharacter(2) <> ? THEN DO:
       filterText = pathParams:GetCharacter(2).
     END.
    MESSAGE "filterText" filterText.
    oJsonPrincipalObject = NEW JsonObject().

     FIND LAST tip-ped-cia WHERE string(tip-ped-cia.cdn) = (filterText) NO-LOCK NO-ERROR.
     IF AVAIL tip-ped-cia THEN DO:
        oJsonPrincipalObject:Add("codTipoOperacao", tip-ped-cia.cdn-tip-ped).
        oJsonPrincipalObject:Add("descricao", tip-ped-cia.descricao).
     END.
     
    
    
    jsonOutput = oJsonPrincipalObject.
  
END.

// Essa procedure ser� chamada toda vez que um m�todo inv�lido da API for invocado
PROCEDURE NoAction:
    
    DEF INPUT PARAM jsonInput AS JsonObject NO-UNDO.
    DEF OUTPUT PARAM jsonOutput AS JsonObject NO-UNDO.

END.

{fwk/utils/fndApiServices.i}
