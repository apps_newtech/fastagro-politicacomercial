/******************************************************************************
**  Objetivo:   Include utilizada para os programas ESPD004.W e ESPD004RP.P
**  Autor...:   Edgar Bispo - Datasul SP
**  Dt Atual:   20/01/2003
**              - Include para ser utilizada nos programas acima
******************************************************************************/

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD it-codigo-ini     LIKE ped-ent.it-codigo
    FIELD it-codigo-fim     LIKE ped-ent.it-codigo
    FIELD nome-abrev-ini    LIKE ped-ent.nome-abrev
    FIELD nome-abrev-fim    LIKE ped-ent.nome-abrev
    FIELD fm-codigo-ini     LIKE ITEM.fm-codigo
    FIELD fm-codigo-fim     LIKE ITEM.fm-codigo
    FIELD dt-entrega-ini    LIKE ped-ent.dt-entrega
    FIELD dt-entrega-fim    LIKE ped-ent.dt-entrega
    FIELD estab-ini         LIKE ped-venda.cod-estabel
    FIELD estab-fim         LIKE ped-venda.cod-estabel
    FIELD l-liberado        AS LOG
    FIELD l-bloqueado       AS LOG
    FIELD l-somente-credito AS LOG
    FIELD l-origem          AS LOG
    FIELD l-interno         AS LOG
    FIELD l-externo         AS LOG
    FIELD l-venda           AS LOG
    FIELD l-outro           AS LOG
    FIELD l-remessa         AS LOG
    field l-parcial         as log
    field l-parc-fc         as log
    /** Por Matheus Antonelli em 17.01.2019 */
    FIELD segmento          AS INTEGER EXTENT 2
    FIELD qtd-liquida       AS LOGICAL 
    FIELD l-devolucoes      AS LOG.

define temp-table tt-digita no-undo
    field ordem            as integer   format ">>>>9"
    field exemplo          as character format "x(30)"
    index id ordem.

define buffer b-tt-digita for tt-digita.

def temp-table tt-raw-digita
   field raw-digita      as raw.
