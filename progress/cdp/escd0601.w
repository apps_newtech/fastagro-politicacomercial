&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWindow
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWindow 
/*:T*******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i XX9999 9.99.99.999}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> <m�dulo>}
&ENDIF

CREATE WIDGET-POOL.

/* Preprocessors Definitions ---                                      */

&GLOBAL-DEFINE Program        ESCD0601
&GLOBAL-DEFINE Version        1.00.00.001

&GLOBAL-DEFINE WindowType     Detail

&GLOBAL-DEFINE Folder         NO
&GLOBAL-DEFINE InitialPage    1
&GLOBAL-DEFINE FolderLabels   
&GLOBAL-DEFINE page0Widgets   btOK btCancel btHelp2
&GLOBAL-DEFINE page1Widgets   l-bloq-localiz l-emite-notif c-email
&GLOBAL-DEFINE page2Widgets   

/* Parameters Definitions ---                                           */

DEFINE INPUT PARAMETER c-cod-depos  AS CHAR NO-UNDO.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fpage0

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btOK btCancel btHelp2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWindow AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU smFile 
       MENU-ITEM miQueryJoins   LABEL "&Consultas"    
       MENU-ITEM miReportsJoins LABEL "&Relat�rios"   
       RULE
       MENU-ITEM miExit         LABEL "&Sair"          ACCELERATOR "CTRL-X".

DEFINE SUB-MENU smHelp 
       MENU-ITEM miContents     LABEL "&Conte�do"     
       MENU-ITEM miAbout        LABEL "&Sobre..."     .

DEFINE MENU mbMain MENUBAR
       SUB-MENU  smFile         LABEL "&Arquivo"      
       SUB-MENU  smHelp         LABEL "&Ajuda"        .


/* Definitions of the field level widgets                               */
DEFINE BUTTON btCancel 
     LABEL "Cancelar" 
     SIZE 10 BY 1.

DEFINE BUTTON btHelp2 
     LABEL "Ajuda" 
     SIZE 10 BY 1.

DEFINE BUTTON btOK 
     LABEL "OK" 
     SIZE 10 BY 1.

DEFINE VARIABLE c-email AS CHARACTER FORMAT "X(256)":U 
     LABEL "E-mail" 
     VIEW-AS FILL-IN 
     SIZE 69.57 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-15
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84.14 BY 1.92.

DEFINE RECTANGLE RECT-16
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84.14 BY 3.25.

DEFINE VARIABLE l-bloq-localiz AS LOGICAL INITIAL no 
     LABEL "Bloquear localiza��es" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .83 NO-UNDO.

DEFINE VARIABLE l-bloq-localiz-2 AS LOGICAL INITIAL no 
     LABEL "Bloquear localiza��es" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .83 NO-UNDO.

DEFINE VARIABLE l-bloq-localiz-3 AS LOGICAL INITIAL no 
     LABEL "Bloquear localiza��es" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .83 NO-UNDO.

DEFINE VARIABLE l-emite-notif AS LOGICAL INITIAL no 
     LABEL "Emite notifica��es" 
     VIEW-AS TOGGLE-BOX
     SIZE 21 BY .83 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fpage0
     btOK AT ROW 8.13 COL 2
     btCancel AT ROW 8.13 COL 13
     btHelp2 AT ROW 8.13 COL 80
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 90 BY 8.75
         FONT 1 WIDGET-ID 100.

DEFINE FRAME fPage1
     l-bloq-localiz AT ROW 2.08 COL 14.43 WIDGET-ID 6
     l-bloq-localiz-2 AT ROW 2.08 COL 36.86 WIDGET-ID 16
     l-bloq-localiz-3 AT ROW 2.08 COL 62 WIDGET-ID 18
     l-emite-notif AT ROW 4.29 COL 14.43 WIDGET-ID 12
     c-email AT ROW 5.33 COL 12.43 COLON-ALIGNED WIDGET-ID 14
     " Transfer�ncias" VIEW-AS TEXT
          SIZE 12 BY .54 AT ROW 3.54 COL 5.86 WIDGET-ID 10
     " Invent�rio" VIEW-AS TEXT
          SIZE 8 BY .54 AT ROW 1.29 COL 5.86 WIDGET-ID 4
     RECT-15 AT ROW 1.58 COL 2.86 WIDGET-ID 2
     RECT-16 AT ROW 3.75 COL 2.86 WIDGET-ID 8
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 1.25
         SIZE 88 BY 6.5
         FONT 1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWindow ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 8.29
         WIDTH              = 90.14
         MAX-HEIGHT         = 17
         MAX-WIDTH          = 90.14
         VIRTUAL-HEIGHT     = 17
         VIRTUAL-WIDTH      = 90.14
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU mbMain:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWindow 
/* ************************* Included-Libraries *********************** */

{window/window.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWindow
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* REPARENT FRAME */
ASSIGN FRAME fPage1:FRAME = FRAME fpage0:HANDLE.

/* SETTINGS FOR FRAME fpage0
   FRAME-NAME                                                           */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fPage1:MOVE-BEFORE-TAB-ITEM (btOK:HANDLE IN FRAME fpage0)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME fPage1
                                                                        */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWindow)
THEN wWindow:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fpage0
/* Query rebuild information for FRAME fpage0
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME fpage0 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fPage1
/* Query rebuild information for FRAME fPage1
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME fPage1 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWindow
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWindow wWindow
ON END-ERROR OF wWindow
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWindow wWindow
ON WINDOW-CLOSE OF wWindow
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCancel wWindow
ON CHOOSE OF btCancel IN FRAME fpage0 /* Cancelar */
DO:
    APPLY "CLOSE":U TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btHelp2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btHelp2 wWindow
ON CHOOSE OF btHelp2 IN FRAME fpage0 /* Ajuda */
DO:
    {include/ajuda.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOK wWindow
ON CHOOSE OF btOK IN FRAME fpage0 /* OK */
DO:
    IF  c-email:SENSITIVE IN FRAME fpage1 THEN DO:
        IF  c-email:SCREEN-VALUE IN FRAME fpage1 = "" THEN DO:
            RUN utp/ut-msgs.p (INPUT "show",
                               INPUT 17242,
                               INPUT "E-mail para notificar transfer�ncias deve ser informado.").
            APPLY "entry" TO c-email IN FRAME fpage1.
            RETURN NO-APPLY.
        END.
        ELSE DO:
            IF  NUM-ENTRIES(c-email:SCREEN-VALUE IN FRAME fpage1, "@") <> 2 THEN DO:
                RUN utp/ut-msgs.p (INPUT "show",
                                   INPUT 17242,
                                  INPUT "Informe um e-mail v�lido.").
                APPLY "entry" TO c-email IN FRAME fpage1.
                RETURN NO-APPLY.
            END.
        END.
    END.
    FIND es_deposito WHERE es_deposito.cod-depos = c-cod-depos EXCLUSIVE-LOCK NO-ERROR.
    IF  AVAIL es_deposito THEN DO:
        IF   l-bloq-localiz:CHECKED IN FRAME fpage1 = YES 
        THEN ASSIGN es_deposito.log-bloq-localiz = YES.
        ELSE ASSIGN es_deposito.log-bloq-localiz = NO.
        IF   l-emite-notif:CHECKED IN FRAME fpage1 = YES 
        THEN ASSIGN es_deposito.log-emite-notif = YES.
        ELSE ASSIGN es_deposito.log-emite-notif = NO.
        ASSIGN es_deposito.email = c-email:SCREEN-VALUE IN FRAME fpage1.
    END.
    ELSE DO:
        CREATE es_deposito.
        ASSIGN es_deposito.cod-depos = deposito.cod-depos.
        IF   l-bloq-localiz:CHECKED IN FRAME fpage1 = YES 
        THEN ASSIGN es_deposito.log-bloq-localiz = YES.
        ELSE ASSIGN es_deposito.log-bloq-localiz = NO.
        IF   l-emite-notif:CHECKED IN FRAME fpage1 = YES 
        THEN ASSIGN es_deposito.log-emite-notif = YES.
        ELSE ASSIGN es_deposito.log-emite-notif = NO.
        ASSIGN es_deposito.email = c-email:SCREEN-VALUE IN FRAME fpage1.
    END.
    APPLY "CLOSE":U TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage1
&Scoped-define SELF-NAME l-emite-notif
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL l-emite-notif wWindow
ON VALUE-CHANGED OF l-emite-notif IN FRAME fPage1 /* Emite notifica��es */
DO:
  IF  l-emite-notif:CHECKED IN FRAME fPage1 = YES THEN 
      ASSIGN c-email:SENSITIVE IN FRAME fPage1 = YES.
  ELSE
      ASSIGN c-email:SENSITIVE IN FRAME fPage1 = NO.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME miAbout
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL miAbout wWindow
ON CHOOSE OF MENU-ITEM miAbout /* Sobre... */
DO:
  {include/sobre.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fpage0
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWindow 


/*:T--- L�gica para inicializa��o do programam ---*/
{window/mainblock.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE afterInitializeInterface wWindow 
PROCEDURE afterInitializeInterface :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

FIND deposito WHERE cod-depos = c-cod-depos NO-LOCK NO-ERROR.

ASSIGN c-email:SENSITIVE IN FRAME fpage1 = NO.

FIND FIRST es_deposito WHERE es_deposito.cod-depos = deposito.cod-depos NO-LOCK NO-ERROR.
IF  AVAIL es_deposito THEN DO:
    IF  es_deposito.log-bloq-localiz THEN ASSIGN l-bloq-localiz:CHECKED IN FRAME fpage1 = YES.
    IF  es_deposito.log-emite-notif THEN DO:
        ASSIGN l-emite-notif:CHECKED IN FRAME fpage1 = YES.
        ASSIGN c-email:SENSITIVE IN FRAME fPage1 = YES.
    END.
    ASSIGN c-email:SCREEN-VALUE IN FRAME fpage1 = es_deposito.email.
END.
ELSE ASSIGN l-bloq-localiz:CHECKED IN FRAME fpage1 = NO
            l-emite-notif:CHECKED IN FRAME fpage1 = NO
            c-email:SCREEN-VALUE IN FRAME fpage1 = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

