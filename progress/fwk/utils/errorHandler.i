PROCEDURE piCreateError PRIVATE:
    DEFINE INPUT PARAMETER iErr AS INTEGER   NO-UNDO.
    DEFINE INPUT PARAMETER cPar AS CHARACTER NO-UNDO.

    DEFINE VARIABLE iCont AS INTEGER   NO-UNDO INITIAL 1.
    DEFINE VARIABLE cMsg  AS CHARACTER NO-UNDO.
    DEFINE VARIABLE cHelp AS CHARACTER NO-UNDO.

    FIND LAST RowErrors NO-LOCK NO-ERROR.
    IF   AVAILABLE RowErrors THEN
         ASSIGN iCont = RowErrors.ErrorSequence + 1.

    ASSIGN cMsg = cPar.
    IF  iErr > 0 THEN DO:
        RUN utp/ut-msgs.p (INPUT "msg", INPUT iErr, INPUT cPar).
        ASSIGN cMsg = RETURN-VALUE.
        RUN utp/ut-msgs.p (INPUT "help", INPUT iErr, INPUT cPar).
        ASSIGN cHelp = RETURN-VALUE.
    END.

    CREATE RowErrors.
    ASSIGN RowErrors.ErrorSequence    = iCont
           RowErrors.ErrorNumber      = iErr
           RowErrors.ErrorDescription = cMsg
           RowErrors.ErrorParameters  = cPar
           RowErrors.ErrorHelp        = cHelp
           RowErrors.ErrorType        = "EMS2"
           RowErrors.ErrorSubType     = "ERROR".
END PROCEDURE.
