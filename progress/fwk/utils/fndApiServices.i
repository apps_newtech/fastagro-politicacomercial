&GLOBAL-DEFINE MSG_TYPE_SUCCESS 'success'
&GLOBAL-DEFINE MSG_TYPE_INFO    'information'
&GLOBAL-DEFINE MSG_TYPE_WARNING 'warning'
&GLOBAL-DEFINE MSG_TYPE_ERROR   'error'

{btb/btblslog.i}

/* ************************  Function Implementations ***************** */
FUNCTION stringToExtent RETURNS CHARACTER  EXTENT (INPUT queryFilters AS CHARACTER , INPUT separator AS CHARACTER ):
    DEFINE VARIABLE stringExtent  AS CHARACTER EXTENT NO-UNDO.
    DEFINE VARIABLE totalElements AS INTEGER          NO-UNDO.
    DEFINE VARIABLE countRegs     AS INTEGER          NO-UNDO.
    queryFilters = TRIM(queryFilters).
    IF queryFilters = '' THEN
    DO:
        EXTENT(stringExtent) = 1.
        stringExtent[1] = ''.
        RETURN stringExtent.
    END.
    totalElements = NUM-ENTRIES(queryFilters, separator).
    EXTENT(stringExtent) = totalElements.

    DO countRegs = 1 TO totalElements:
        stringExtent[countRegs] = ENTRY(countRegs, queryFilters, separator).
    END.
    RETURN stringExtent.
END FUNCTION.

FUNCTION getFieldNameByAlias RETURNS CHARACTER  (INPUT aliasName AS CHARACTER, INPUT keyValueFields AS JsonArray):
    DEFINE VARIABLE countRegs AS INTEGER  NO-UNDO.
    DO countRegs = 1 TO keyValueFields:LENGTH:
        IF(VALID-OBJECT(keyValueFields:getJsonObject(countRegs)) AND keyValueFields:getJsonObject(countRegs):has(aliasName)) THEN
        DO:
            RETURN keyValueFields:getJsonObject(countRegs):GetCharacter(aliasName).
        END.
    END.
    RETURN "".
END FUNCTION.

FUNCTION longcharToJsonObject RETURNS JsonObject(INPUT jsonOutput AS LONGCHAR):
    DEFINE VARIABLE jsonObjOutput AS JsonObject NO-UNDO.
    DEFINE VARIABLE objParse AS ObjectModelParser NO-UNDO.

    jsonObjOutput = NEW JsonObject().
    objParse = NEW ObjectModelParser().
    jsonObjOutput = CAST(objParse:Parse(jsonOutput), JsonObject).
    DELETE OBJECT objParse.
    RETURN jsonObjOutput.
END FUNCTION.

/*------------------------------------------------------------------------------
 Purpose: Mascara a senha a ser enviada para o front-end com um caracter inacessivel pelo teclado
 Notes:
------------------------------------------------------------------------------*/
FUNCTION passwordMask RETURNS CHARACTER ():
    DEFINE VARIABLE passwordMask    AS CHARACTER    NO-UNDO.
    DEFINE VARIABLE i               AS INTEGER      NO-UNDO.
    
    ASSIGN passwordMask = FILL(CHR(254), 15).
    
    RETURN passwordMask.
END FUNCTION.

/* **********************  Internal Procedures  *********************** */

PROCEDURE applyFieldsAndExpandables:
    DEFINE INPUT  PARAMETER arrayRecords     AS JsonArray   NO-UNDO.
    DEFINE INPUT  PARAMETER fieldList        AS CHARACTER   NO-UNDO.
    DEFINE INPUT  PARAMETER expandables      AS CHARACTER   NO-UNDO.
    DEFINE INPUT  PARAMETER cExpanOptions    AS CHARACTER   NO-UNDO.
    DEFINE OUTPUT PARAMETER jsonOutput       AS JsonArray  NO-UNDO.

    DEFINE VARIABLE countRegs            AS INTEGER     NO-UNDO.
    DEFINE VARIABLE fieldCount           AS INTEGER     NO-UNDO.
    DEFINE VARIABLE fieldRead            AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE valueRead            AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE arrayWithExpandables AS JsonArray   NO-UNDO.
    DEFINE VARIABLE showExpandables      AS LOGICAL     NO-UNDO INITIAL NO.
    DEFINE VARIABLE iEOCount             AS INTEGER     NO-UNDO.
    DEFINE VARIABLE oArray               AS JsonArray   NO-UNDO.
    DEFINE VARIABLE cNames               AS CHARACTER   NO-UNDO EXTENT.
    DEFINE VARIABLE iCountNames          AS INTEGER     NO-UNDO.

    jsonOutput = NEW JsonArray().
    IF NOT VALID-OBJECT(arrayRecords) THEN RETURN.
    IF fieldList <> '' THEN
    DO:
        IF arrayRecords:LENGTH > 0 THEN
            ASSIGN cNames = arrayRecords:getJsonObject(1):getNames().

        DO countRegs = 1 TO arrayRecords:LENGTH:
            DO iCountNames = 1 TO EXTENT(cNames):
                IF NOT LOOKUP(cNames[iCountNames],fieldList) > 0 THEN
                    arrayRecords:getJsonObject(countRegs):remove(cNames[iCountNames]).
                ASSIGN showExpandables = showExpandables OR CAN-DO(expandables, cNames[iCountNames]).
            END.
        END.
    END.
    ELSE
        ASSIGN showExpandables = YES. /* EXIBE O EXPANDABLES POR PADRAO */

    IF showExpandables THEN DO:
        ASSIGN arrayWithExpandables = NEW JsonArray()
               oArray               = NEW JsonArray().

        DO iEOCount = 1 TO NUM-ENTRIES(cExpanOptions):
            IF LOOKUP(ENTRY(iEOCount,cExpanOptions),expandables) <= 0 THEN
                oArray:ADD(ENTRY(iEOCount,cExpanOptions)).
        END.

        DO countRegs = 1 TO arrayRecords:LENGTH:
            arrayWithExpandables:ADD(arrayRecords:getJsonObject(countRegs)).

            /* Adiciona o atributo _expandables ao objeto */
            IF NOT arrayRecords:getJsonObject(countRegs):has('_expandables') AND ((fieldList <> '' AND LOOKUP('_expandable',fieldList) > 0) OR fieldList = '') THEN
                arrayWithExpandables:getJsonObject(countRegs):add('_expandables', oArray).

            /* Caso um dos itens de expandable nao possua dados, inclui um array vazio para o item*/
            IF TRIM(expandables) <> "" THEN DO:
                DO fieldCount = 1 TO NUM-ENTRIES(expandables):
                    ASSIGN fieldRead = ENTRY(fieldCount, expandables).
                    IF NOT arrayRecords:getJsonObject(countRegs):has(fieldRead) THEN
                        arrayWithExpandables:getJsonObject(countRegs):add(fieldRead, NEW JsonArray()).
                END.
            END.
        END.

        IF VALID-OBJECT(arrayWithExpandables) THEN DO:
            ASSIGN arrayRecords = arrayWithExpandables.
        END.
    END.

    ASSIGN jsonOutput = arrayRecords.
END PROCEDURE.

PROCEDURE fillAliasAndFieldNames PRIVATE:
    DEFINE INPUT  PARAMETER objTable       AS HANDLE    NO-UNDO.
    DEFINE OUTPUT PARAMETER keyValueFields AS JsonArray NO-UNDO.

    DEFINE VARIABLE countRegs AS INTEGER NO-UNDO.
    DEFINE VARIABLE keyValue AS JsonObject NO-UNDO.
    keyValueFields = NEW JsonArray().
    DO countRegs = 1 TO objTable:NUM-FIELDS:
        keyValue = NEW JsonObject().
        keyValue:ADD(objTable:BUFFER-FIELD(countRegs):SERIALIZE-NAME, objTable:BUFFER-FIELD(countRegs):NAME).
        keyValueFields:ADD(keyValue).
    END.
END PROCEDURE.

PROCEDURE executeSearch:
    DEFINE INPUT  PARAMETER startRow         AS INTEGER   NO-UNDO.
    DEFINE INPUT  PARAMETER tableName        AS CHARACTER NO-UNDO.
    DEFINE INPUT  PARAMETER targetHandle     AS HANDLE    NO-UNDO.
    DEFINE INPUT  PARAMETER pageSize         AS INTEGER   NO-UNDO.
    DEFINE INPUT  PARAMETER whereClause      AS CHARACTER NO-UNDO.
    DEFINE OUTPUT PARAMETER totalSearchCount AS INTEGER   NO-UNDO.
    DEFINE OUTPUT PARAMETER hasNext          AS LOGICAL   NO-UNDO.

    DEFINE VARIABLE genericQuery AS HANDLE  NO-UNDO.
    DEFINE VARIABLE countRegs    AS INTEGER NO-UNDO.
    DEFINE VARIABLE bufferTable  AS HANDLE  NO-UNDO.
    DEFINE VARIABLE parsed       AS LOGICAL NO-UNDO.

    RUN buildQuery(INPUT tableName, INPUT whereClause, OUTPUT genericQuery, OUTPUT bufferTable, OUTPUT parsed).
    IF parsed THEN DO:
        genericQuery:QUERY-OPEN.
        totalSearchCount = genericQuery:NUM-RESULTS.
        genericQuery:REPOSITION-TO-ROW(startRow).

        DO countRegs = 1 TO pageSize:
            genericQuery:GET-NEXT().
            IF genericQuery:QUERY-OFF-END THEN LEAVE.
            targetHandle:BUFFER-CREATE ().
            targetHandle:BUFFER-COPY (bufferTable:HANDLE).
        END.

        ASSIGN hasNext = genericQuery:GET-NEXT().
    END.

    IF VALID-HANDLE(genericQuery) THEN
        DELETE OBJECT genericQuery.

    IF VALID-HANDLE(bufferTable) THEN
        DELETE OBJECT bufferTable.
END PROCEDURE.

PROCEDURE buildQuery PRIVATE:
    DEFINE INPUT  PARAMETER tableName     AS CHARACTER  NO-UNDO.
    DEFINE INPUT  PARAMETER whereClause   AS CHARACTER  NO-UNDO.
    DEFINE OUTPUT PARAMETER genericQuery  AS HANDLE     NO-UNDO.
    DEFINE OUTPUT PARAMETER genericBuffer AS HANDLE     NO-UNDO.
    DEFINE OUTPUT PARAMETER parsed        AS LOGICAL    NO-UNDO.

    DEFINE VARIABLE c-clause AS CHARACTER NO-UNDO.

    CREATE QUERY genericQuery.
    CREATE BUFFER genericBuffer FOR TABLE tableName.
    genericQuery:ADD-BUFFER(genericBuffer:HANDLE).
    c-clause = "preselect each " + tableName + " NO-LOCK " + whereClause.
    parsed = genericQuery:QUERY-PREPARE(c-clause).
END PROCEDURE.

PROCEDURE datasetToJSONArray:
    DEFINE INPUT  PARAMETER datasetHandle   AS HANDLE    NO-UNDO.
    DEFINE INPUT  PARAMETER mainDatasetName AS CHARACTER NO-UNDO.
    DEFINE OUTPUT PARAMETER arrayRecords    AS JsonArray NO-UNDO.

    DEFINE VARIABLE objDataset         AS JsonObject        NO-UNDO.
    DEFINE VARIABLE jsonDataset        AS JsonObject        NO-UNDO.
    DEFINE VARIABLE objDatasetLongchar AS LONGCHAR          NO-UNDO.
    DEFINE VARIABLE objParse           AS ObjectModelParser NO-UNDO.
    DEFINE VARIABLE bufferCount        AS INTEGER           NO-UNDO.

    ASSIGN objDataset   = NEW JsonObject()
           arrayRecords = NEW JsonArray().

    objDataset:READ(datasetHandle).

    ASSIGN objDatasetLongchar = objDataset:getJsonText(datasetHandle:NAME)
           objParse           = NEW ObjectModelParser()
           jsonDataset        = CAST(objParse:Parse(objDatasetLongchar), JsonObject).

    IF jsonDataset:has(mainDatasetName) THEN
        ASSIGN arrayRecords = jsonDataset:getJsonArray(mainDatasetName).
    ELSE
        ASSIGN arrayRecords = NEW JsonArray().

    DELETE OBJECT objDataset.
    DELETE OBJECT jsonDataset.
    DELETE OBJECT objParse.
END PROCEDURE.

/*------------------------------------------------------------------------------
 Purpose: Cria um erro na TEMP-TABLE RowErrors
------------------------------------------------------------------------------*/
PROCEDURE createMessage PRIVATE:
    DEFINE INPUT PARAMETER msgCode AS INTEGER   NO-UNDO.
    DEFINE INPUT PARAMETER msgText AS CHARACTER NO-UNDO.
    DEFINE INPUT PARAMETER msgHelp AS CHARACTER NO-UNDO.
    DEFINE INPUT PARAMETER msgType AS CHARACTER NO-UNDO.
    
    CREATE RowErrors.
    
    ASSIGN 
        RowErrors.ErrorNumber  = msgCode
        RowErrors.ErrorSubType = msgType.
    
    RUN utp/ut-msgs.p (INPUT "msg",  INPUT msgCode, INPUT msgText).        
    ASSIGN 
        RowErrors.ErrorDescription = RETURN-VALUE.

    RUN utp/ut-msgs.p (INPUT "help", INPUT msgCode, INPUT msgHelp).        
    ASSIGN 
        RowErrors.ErrorHelp = RETURN-VALUE.
END PROCEDURE.

FUNCTION logicalToString RETURNS CHARACTER (INPUT lValue AS LOGICAL):
    /*------------------------------------------------------------------------------
     Purpose: Converte um logical em string
    ------------------------------------------------------------------------------*/
    RETURN (IF lValue = TRUE THEN "true" ELSE "false").
END FUNCTION.

FUNCTION stringToLogical RETURNS LOGICAL (INPUT cValue AS CHARACTER):
    /*------------------------------------------------------------------------------
     Purpose: Converte uma string "true/yes" em logical
    ------------------------------------------------------------------------------*/
    RETURN (IF cValue = "true" OR cValue = "yes" THEN TRUE ELSE FALSE).
END FUNCTION.

FUNCTION convertToCamelCase RETURNS CHARACTER (INPUT cKey AS CHARACTER):
    /*------------------------------------------------------------------------------
     Purpose: Converte uma string "aaa.BBB.ccc" em "aaaBbbCcc"
    ------------------------------------------------------------------------------*/
    DEFINE VARIABLE cNKey       AS CHARACTER NO-UNDO.
    DEFINE VARIABLE cTmp        AS CHARACTER NO-UNDO.
    DEFINE VARIABLE ix          AS INTEGER   NO-UNDO.

    /* se vier separador "-" ou "_", substitui para "." para tornar a rotina generica */
    ASSIGN cKey = REPLACE(cKey, "_", ".")
           cKey = REPLACE(cKey, "-", ".").

    DO  ix = 1 TO NUM-ENTRIES(cKey, "."):
        ASSIGN cTmp = ENTRY(ix, cKey, ".").
        IF  ix > 1 THEN
            ASSIGN cTmp = upper(substr(cTmp, 1, 1)) + lower(substr(cTmp, 2, LENGTH(cTmp))).
        ASSIGN cNKey = cNKey + cTmp.
    END.

    RETURN cNKey.
END FUNCTION.

/*------------------------------------------------------------------------------
 Purpose: Converte um CHARACTER delimitado para um JsonArray.
 Notes:
------------------------------------------------------------------------------*/    
FUNCTION convertDelimitedCharToJsonArray RETURNS Progress.Json.ObjectModel.JsonArray
    (INPUT delimitedChar AS CHARACTER, INPUT delimit AS CHARACTER, INPUT jsonTyp AS INTEGER):
    DEFINE VARIABLE jsonArr   AS Progress.Json.ObjectModel.JsonArray NO-UNDO.
    DEFINE VARIABLE i         AS INTEGER                             NO-UNDO.
    DEFINE VARIABLE currEntry AS CHARACTER                           NO-UNDO.
    
    ASSIGN
        jsonArr = NEW Progress.Json.ObjectModel.JsonArray()
        delimit = IF delimit = ? OR TRIM(delimit) = "" THEN "," ELSE delimit
        jsonTyp = IF jsonTyp = ? THEN Progress.Json.ObjectModel.JsonDataType:STRING ELSE jsonTyp.
    
    DO  i = 1 TO NUM-ENTRIES(delimitedChar, delimit):
        ASSIGN
            currEntry = ENTRY(i, delimitedChar, delimit).
            
        CASE jsonTyp:
            WHEN Progress.Json.ObjectModel.JsonDataType:BOOLEAN THEN jsonArr:Add(LOGICAL(currEntry)).
            WHEN Progress.Json.ObjectModel.JsonDataType:NUMBER  THEN jsonArr:Add(INTEGER(currEntry)).
            WHEN Progress.Json.ObjectModel.JsonDataType:STRING  THEN jsonArr:Add(currEntry).
        END CASE.
    END.
    
    RETURN jsonArr.
END FUNCTION.

/*------------------------------------------------------------------------------
 Purpose: Converte um JsonArray para um CHARACTER delimitado.
 Notes:
------------------------------------------------------------------------------*/    
FUNCTION convertJsonArrayToDelimitedChar RETURNS CHARACTER
    (INPUT jsonArr AS Progress.Json.ObjectModel.JsonArray, INPUT delimit AS CHARACTER, INPUT jsonTyp AS INTEGER):
    DEFINE VARIABLE delimitedChar AS CHARACTER NO-UNDO INITIAL "".
    DEFINE VARIABLE i             AS INTEGER   NO-UNDO.
    
    ASSIGN
        delimit = IF delimit = ? OR TRIM(delimit) = "" THEN "," ELSE delimit
        jsonTyp = IF jsonTyp = ? THEN Progress.Json.ObjectModel.JsonDataType:STRING ELSE jsonTyp.
    
    DO  i = 1 TO jsonArr:Length:
        IF  TRIM(delimitedChar) <> "" THEN
            ASSIGN
                delimitedChar = delimitedChar + ",".
            
        CASE jsonTyp:
            WHEN Progress.Json.ObjectModel.JsonDataType:BOOLEAN THEN
                ASSIGN 
                    delimitedChar = delimitedChar + STRING(jsonArr:GetLogical(i)).
            WHEN Progress.Json.ObjectModel.JsonDataType:NUMBER  THEN
                ASSIGN 
                    delimitedChar = delimitedChar + STRING(jsonArr:GetInteger(i)).
            WHEN Progress.Json.ObjectModel.JsonDataType:STRING  THEN
                ASSIGN 
                    delimitedChar = delimitedChar + jsonArr:GetCharacter(i).
        END CASE.
    END.
    
    RETURN delimitedChar.
END FUNCTION.

/*------------------------------------------------------------------------------
 Purpose: 
 Notes:
------------------------------------------------------------------------------*/    
FUNCTION isValidString RETURNS LOGICAL(INPUT valor AS CHAR):
    IF valor <> ? AND valor <> "" THEN 
        RETURN TRUE.
    ELSE RETURN FALSE.
END FUNCTION.

/*------------------------------------------------------------------------------
 Purpose: 
 Notes:
------------------------------------------------------------------------------*/    
FUNCTION addWhereClause RETURNS CHAR(INPUT currentWhereClause AS CHAR, INPUT fieldConstraint AS CHAR):
    IF(trim(currentWhereClause) <> '') THEN currentWhereClause = currentWhereClause + ' and '.
    currentWhereClause = currentWhereClause + ' ' + fieldConstraint.
	return currentWhereClause.
END FUNCTION.
