
interface fwk.utils.IPersist :

    METHOD PUBLIC void validate().
    METHOD PUBLIC void save().    
    METHOD PUBLIC void update().        

END interface.
