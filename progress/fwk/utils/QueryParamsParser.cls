USING Progress.Json.ObjectModel.* FROM PROPATH. 
USING com.totvs.framework.api.* FROM PROPATH. 
USING fwk.utils.* FROM PROPATH. 
USING System.Text.RegularExpressions.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS fwk.utils.QueryParamsParser:

	method public STATIC QueryParamData splitPropertyAndValue(input currentClause as longchar):
		DEFINE VARIABLE regxp AS Regex NO-UNDO.
		DEF VAR mc AS Match.
		DEF VAR grupos AS INT EXTENT.
        DEFINE VARIABLE contador AS INT NO-UNDO.
        DEFINE VARIABLE retorno AS QueryParamData NO-UNDO.
		
        regxp = NEW Regex("~{(.*):(.*)~}").
        mc = regxp:MATCH(currentClause).
        grupos = regxp:GetGroupNumbers().
        retorno = new QueryParamData().
        retorno:property =  mc:groups[1]:value.
        retorno:value =  mc:groups[2]:value.        
        return retorno.
		finally:
			delete object regxp.
		end finally.
	end method.
	method public STATIC ClauseData getClauseDataWithWhitespaces(input currentClause as longchar):
		DEFINE VARIABLE regxp AS Regex NO-UNDO.
		DEF VAR mc AS Match.
		DEF VAR grupos AS INT EXTENT.
        DEFINE VARIABLE contador AS INT NO-UNDO.
        DEFINE VARIABLE retorno AS ClauseData NO-UNDO.
		
        regxp = NEW Regex("([\w]+)([\s]+)([\w]+)([\s]+)([\w]+)").
        mc = regxp:MATCH(currentClause).
        grupos = regxp:GetGroupNumbers().
        retorno = new ClauseData().
        retorno:propertyName = mc:groups[1]:value.
        retorno:condition = mc:groups[2]:value.
        retorno:searchValue = mc:groups[3]:value.
        return retorno.
		finally:
			delete object regxp.
		end finally.
	end method.
	method public STATIC ClauseData getClauseDataFromSimpleEqualityMatches(input currentClause as JsonObject):
        DEFINE VARIABLE names AS char extent NO-UNDO.
        def var contador as int no-undo.
        DEFINE VARIABLE retorno AS ClauseData NO-UNDO.        


        retorno = new ClauseData().
        names = currentClause:getNames().
        retorno:propertyName = names[1].
        retorno:searchValue = JsonApiUtils:getPropertyJsonObject(currentClause, names[1]).
        return retorno.
	end method.
	
END CLASS.


