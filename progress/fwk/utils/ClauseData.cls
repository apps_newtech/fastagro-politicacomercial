 
CLASS fwk.utils.ClauseData SERIALIZABLE:
    
    DEFINE SERIALIZABLE PUBLIC PROPERTY propertyName AS char NO-UNDO 
        GET.
        SET.
    
    DEFINE SERIALIZABLE PUBLIC PROPERTY condition AS char NO-UNDO
        GET.
        SET.
        
    DEFINE SERIALIZABLE PUBLIC PROPERTY searchValue AS char NO-UNDO
        GET.
        SET.

END CLASS.
