USING Progress.Json.ObjectModel.* FROM PROPATH.  
BLOCK-LEVEL ON ERROR UNDO, THROW.


class fwk.utils.ServiceFilter SERIALIZABLE abstract :

    METHOD PUBLIC abstract JsonArray getCorrelationFields().
    METHOD PUBLIC abstract void feedCorrelationFields().    
    METHOD PUBLIC char getFieldsAllowedToSearch():
        return ?.
    end METHOD.
    METHOD PUBLIC char getFieldsAllowedToSort():
        return ?.
    end METHOD.

END class.
