FUNCTION stringToExtent RETURNS CHARACTER EXTENT (INPUT strOrig AS CHARACTER , INPUT separator AS CHARACTER ):
    DEFINE VARIABLE stringExtent  AS CHARACTER EXTENT NO-UNDO.
    DEFINE VARIABLE totalElements AS INTEGER          NO-UNDO.
    DEFINE VARIABLE countRegs     AS INTEGER          NO-UNDO.
    strOrig = TRIM(strOrig).
    IF strOrig = '' THEN
    DO:
        EXTENT(stringExtent) = 1.
        stringExtent[1] = ''.
        RETURN stringExtent.
    END.
    totalElements = NUM-ENTRIES(strOrig, separator).
    EXTENT(stringExtent) = totalElements.

    DO countRegs = 1 TO totalElements:
        stringExtent[countRegs] = ENTRY(countRegs, strOrig, separator).
    END.
    RETURN stringExtent.
END FUNCTION.

function addChar returns char(input strOrig as char, input strToAdd as char, input delimtter as char):
    def var retorno as char no-undo.
    if strOrig <> '' then
        retorno = strOrig + delimtter.
    retorno = retorno + strToAdd.
    return retorno.
end function.

function extentToDelimittedChar returns char(input strOrig as char extent, input delimtter as char):
    def var contador as int no-undo.
    def var retorno as char no-undo.
    do contador = 1 to extent(strOrig):
        retorno = addChar(strOrig[contador], retorno, delimtter).
    end.
end function.

function mountWhereClause returns char(input whereClause as char):
    if trim(whereClause) <> '' then
        return ' where ' + whereClause.
    else 
        return ''.
end function.
//TODO:replicado em fndApiServices.i
function addWhereClause returns char(input originalWhereClause as char, input clauseToAdd as char):
    if trim(originalWhereClause) <> '' then
        return originalWhereClause + " and " + clauseToAdd.
    else
        return clauseToAdd.   
end function.

function addSortClause returns char(input originalSortClause as char, input clauseToAdd as char, input sortOrder as char):
    def var resultClause as char no-undo.
    if clauseToAdd <> '' then
        resultClause = ' by ' + clauseToAdd + ' ' + sortOrder.
    if trim(originalSortClause) <> '' then
        return originalSortClause + resultClause.
    else
        return resultClause.   
end function.


PROCEDURE transformDataFromTTHandle:
    DEFINE INPUT  PARAMETER pageNumber AS INTEGER NO-UNDO.
    DEFINE INPUT  PARAMETER pageSize AS INTEGER NO-UNDO.    
    DEFINE INPUT  PARAMETER bufferSourceHandle AS HANDLE NO-UNDO.
    DEFINE INPUT  PARAMETER whereClause AS char NO-UNDO.
    DEFINE INPUT  PARAMETER sortClause AS char NO-UNDO.        
    DEFINE output PARAMETER targetTTHandle AS HANDLE NO-UNDO.    
    DEFINE OUTPUT PARAMETER totalSearchCount AS INTEGER NO-UNDO.
    DEFINE OUTPUT PARAMETER hasNext AS LOGICAL NO-UNDO.

    DEFINE VARIABLE genericQuery AS HANDLE  NO-UNDO.
    DEFINE VARIABLE countRegs    AS INTEGER NO-UNDO.
    DEFINE VARIABLE parsed       AS LOGICAL NO-UNDO.
    DEFINE VARIABLE bufferTTTargetHandle AS HANDLE NO-UNDO.

    RUN buildQueryFromTT(INPUT bufferSourceHandle, 
                         input whereClause,
                         input sortClause,
                         OUTPUT genericQuery, 
                         OUTPUT parsed).
    IF parsed THEN DO:
        create temp-table targetTTHandle.
        targetTTHandle:create-like(bufferSourceHandle:table-handle).
        targetTTHandle:temp-table-prepare(bufferSourceHandle:name).
        bufferTTTargetHandle = targetTTHandle:default-buffer-handle.

        genericQuery:QUERY-OPEN.
        totalSearchCount = genericQuery:NUM-RESULTS.
        genericQuery:REPOSITION-TO-ROW((pageNumber - 1) * pageSize).

        DO countRegs = 1 TO pageSize:
            genericQuery:GET-NEXT().
            IF genericQuery:QUERY-OFF-END THEN LEAVE.
            bufferTTTargetHandle:BUFFER-CREATE().
            bufferTTTargetHandle:BUFFER-COPY(genericQuery:get-buffer-handle(bufferSourceHandle:name)).
        END.

        ASSIGN hasNext = genericQuery:GET-NEXT().
    END.
    finally:
        IF VALID-HANDLE(genericQuery) THEN
            DELETE OBJECT genericQuery.
    end finally.            
END PROCEDURE.

PROCEDURE buildQueryFromTT:
    DEFINE INPUT PARAMETER defaultBufferTTHandle AS handle NO-UNDO.
    DEFINE INPUT PARAMETER whereClause as char no-undo initial ''.
    DEFINE INPUT PARAMETER sortClause as char no-undo initial ''.    
    DEFINE OUTPUT PARAMETER genericQuery  AS HANDLE     NO-UNDO.
    DEFINE OUTPUT PARAMETER parsed        AS LOGICAL    NO-UNDO.

    DEFINE VARIABLE queryStr AS CHARACTER NO-UNDO.
    CREATE QUERY genericQuery.
    genericQuery:ADD-BUFFER(defaultBufferTTHandle).
    queryStr = "preselect each " + defaultBufferTTHandle:name  + mountWhereClause(whereClause) + sortClause.
    parsed = genericQuery:QUERY-PREPARE(queryStr).
END PROCEDURE.
//TODO:replicado em fndApiServices.i
PROCEDURE executeSearch:
    DEFINE INPUT  PARAMETER startRow         AS INTEGER   NO-UNDO.
    DEFINE INPUT  PARAMETER tableName        AS CHARACTER NO-UNDO.
    DEFINE INPUT  PARAMETER targetHandle     AS HANDLE    NO-UNDO.
    DEFINE INPUT  PARAMETER pageSize         AS INTEGER   NO-UNDO.
    DEFINE INPUT  PARAMETER whereClause      AS CHARACTER NO-UNDO.
    DEFINE INPUT  PARAMETER sortClause       AS CHARACTER NO-UNDO.    
    DEFINE OUTPUT PARAMETER totalSearchCount AS INTEGER   NO-UNDO.
    DEFINE OUTPUT PARAMETER hasNext          AS LOGICAL   NO-UNDO.

    DEFINE VARIABLE genericQuery AS HANDLE  NO-UNDO.
    DEFINE VARIABLE countRegs    AS INTEGER NO-UNDO.
    DEFINE VARIABLE bufferTable  AS HANDLE  NO-UNDO.
    DEFINE VARIABLE parsed       AS LOGICAL NO-UNDO.

    RUN buildQueryFromTable(INPUT tableName, 
                            INPUT whereClause, 
                            input sortClause,
                            OUTPUT genericQuery, 
                            OUTPUT bufferTable, 
                            OUTPUT parsed).
    IF parsed THEN DO:
        genericQuery:QUERY-OPEN.
        totalSearchCount = genericQuery:NUM-RESULTS.
        genericQuery:REPOSITION-TO-ROW(startRow).

        DO countRegs = 1 TO pageSize:
            genericQuery:GET-NEXT().
            IF genericQuery:QUERY-OFF-END THEN LEAVE.
            targetHandle:BUFFER-CREATE ().
            targetHandle:BUFFER-COPY (bufferTable:HANDLE).
        END.

        ASSIGN hasNext = genericQuery:GET-NEXT().
    END.

    IF VALID-HANDLE(genericQuery) THEN
        DELETE OBJECT genericQuery.

    IF VALID-HANDLE(bufferTable) THEN
        DELETE OBJECT bufferTable.
END PROCEDURE.

PROCEDURE buildQueryFromTable PRIVATE:
    DEFINE INPUT  PARAMETER tableName     AS CHARACTER  NO-UNDO.
    DEFINE INPUT  PARAMETER whereClause   AS CHARACTER  NO-UNDO.
    DEFINE INPUT  PARAMETER sortClause    AS CHARACTER  NO-UNDO.    
    DEFINE OUTPUT PARAMETER genericQuery  AS HANDLE     NO-UNDO.
    DEFINE OUTPUT PARAMETER genericBuffer AS HANDLE     NO-UNDO.
    DEFINE OUTPUT PARAMETER parsed        AS LOGICAL    NO-UNDO.

    DEFINE VARIABLE c-clause AS CHARACTER NO-UNDO.

    CREATE QUERY genericQuery.
    CREATE BUFFER genericBuffer FOR TABLE tableName.
    genericQuery:ADD-BUFFER(genericBuffer:HANDLE).
    c-clause = "preselect each " + tableName + " NO-LOCK " + whereClause + sortClause.
    parsed = genericQuery:QUERY-PREPARE(c-clause).
END PROCEDURE.



