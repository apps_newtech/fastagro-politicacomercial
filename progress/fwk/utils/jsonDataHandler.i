
function isEmptyJsonObject returns logical (input obj as JsonObject):
    DEF VAR properties AS CHAR EXTENT.
    properties = obj:getNames().
    return (extent(properties) = ?).
end function.

function getValueByPropertyAtArray returns char (input parameterName as char, input array as JsonArray):
    def var contador as int no-undo.
    def var valueFound as char no-undo.

    do contador = 1 to array:LENGTH:
        if(array:getJsonObject(contador):has(parameterName)) then do:
            valueFound = array:getJsonObject(contador):getCharacter(parameterName).
        end.
        else next.
    end.

    return valueFound.
end function.

function getJsonObjectByPropertyAndValue returns JsonObject (input parameterName as char, input searchValue as char, input array as JsonArray):
    def var contador as int no-undo.
    def var objFound as JsonObject no-undo.

    do contador = 1 to array:LENGTH:
        if(array:getJsonObject(contador):has(parameterName)) then do:
            if array:getJsonObject(contador):getCharacter(parameterName) = searchValue then
            do:
                objFound = array:getJsonObject(contador).
                leave.
            end.                
        end.
    end.

    return objFound.
end function.

function applyFieldsListToJsonArray returns JsonArray (INPUT arrayRecords AS JsonArray, INPUT fieldList AS char):
    DEFINE VARIABLE jsonOutput AS JsonArray  NO-UNDO.
    DEFINE VARIABLE countRegs AS INTEGER  NO-UNDO.
    DEFINE VARIABLE objFields AS CHARACTER extent NO-UNDO.
    DEFINE VARIABLE iCountNames AS INT NO-UNDO.

    IF NOT VALID-OBJECT(arrayRecords) THEN RETURN ?.
    jsonOutput = cast(arrayRecords:Clone(), JsonArray).
    IF jsonOutput:LENGTH > 0 THEN
        ASSIGN objFields = jsonOutput:getJsonObject(1):getNames().
    IF trim(fieldList) <> '' THEN
    DO:
        DO countRegs = 1 TO jsonOutput:LENGTH:
            DO iCountNames = 1 TO EXTENT(objFields):
                IF NOT LOOKUP(objFields[iCountNames],fieldList) > 0 THEN
                do:
                    jsonOutput:getJsonObject(countRegs):remove(objFields[iCountNames]).
                end.
            END.
        END.
    END.
    return jsonOutput.
end function.

PROCEDURE transferDataFromHandleToTempTable:
    def input param sourceHandle as handle no-undo.
    def input param targetHandle as handle no-undo.

    def var transition as longchar no-undo.
    def var exportedObj as JsonObject no-undo.
    def var objToImport as JsonObject no-undo.    

    sourceHandle:WRITE-Json("longchar", transition).
    exportedObj = JsonApiUtils:convertLongcharToJsonObject(transition).
    objToImport = new JsonObject().
    objToImport:add(targetHandle:name, exportedObj:getJsonArray(sourceHandle:name)).
    targetHandle:READ-json("longchar", objToImport:getJsonText(), "EMPTY").
end PROCEDURE.

PROCEDURE longcharToJsonArray:
    DEF INPUT  PARAM longcharObj  AS LONGCHAR  NO-UNDO.
    DEF OUTPUT PARAM arrayRecords AS JsonArray NO-UNDO.

    DEFINE VARIABLE objDataset AS JsonObject        NO-UNDO.
    DEFINE VARIABLE objParse   AS ObjectModelParser NO-UNDO.

    ASSIGN objParse     = NEW ObjectModelParser()
           arrayRecords = CAST(objParse:Parse(longcharObj), JsonArray).
    catch errorObj as Progress.Lang.Error:
        arrayRecords = new JsonArray().
    end catch.
    finally:
        DELETE OBJECT objParse no-error.
    end finally.
END PROCEDURE.
