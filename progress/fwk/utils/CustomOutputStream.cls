USING com.totvs.framework.api.*.
USING Progress.Json.ObjectModel.*.
USING Progress.Lang.*.

CLASS fwk.utils.CustomOutputStream INHERITS Progress.IO.OutputStream:
    
    DEFINE PUBLIC PROPERTY lcVar AS LONGCHAR GET.
    PRIVATE SET.

    DEFINE PUBLIC PROPERTY decoratedObj AS JsonObject GET.
    PRIVATE SET.

    METHOD OVERRIDE INT64 Write(INPUT pmData AS MEMPTR, INPUT offset AS INT64, INPUT len AS INT64):
        DEFINE VARIABLE tempObj         AS JsonObject.
        DEFINE VARIABLE lhasChildren    AS LOGICAL.
        
        FIX-CODEPAGE(lcVar) = 'UTF-8'.
        THIS-OBJECT:lcVar = GET-STRING(pmData, 1).

        tempObj = JsonAPIUtils:convertLongcharToJsonObject(lcVar).
        decoratedObj = stripUndesiredAttributes(tempObj, lhasChildren, NEW Jsonobject()).
        
        RETURN GET-SIZE(pmData).
    END METHOD.
   
    METHOD OVERRIDE INT64 Write(INPUT lc AS LONGCHAR):
    END METHOD.    

    METHOD OVERRIDE INT64 Write(INPUT strPart AS CHARACTER):
    END METHOD.

    METHOD JsonObject stripUndesiredAttributes(tempObj AS JsonObject, lhasChildren AS LOGICAL, root AS JsonObject):
        DEFINE VARIABLE properties      AS CHARACTER EXTENT.
        DEFINE VARIABLE jsonAux         AS JsonObject.
        DEFINE VARIABLE cont            AS INTEGER.
        
        properties = tempObj:getNames().
        IF EXTENT(properties) = ? THEN RETURN tempObj.
        DO cont = 1 TO EXTENT(properties):
            IF properties[cont] BEGINS 'prods' AND tempObj:getType(properties[cont]) <> JsonDataType:OBJECT THEN DO:
                tempObj:remove(properties[cont]).
            END.
            ELSE IF tempObj:getType(properties[cont]) = JsonDataType:OBJECT THEN DO:
                lhasChildren = hasChildren(tempObj:getJsonObject(properties[cont])).
                IF lhasChildren THEN DO:
                    root = tempObj:getJsonObject(properties[cont]).
                END.
                
                stripUndesiredAttributes(tempObj:getJsonObject(properties[cont]), lhasChildren, root).
                
                IF NOT lhasChildren THEN DO:
                    addValues(root, tempObj:getJsonObject(properties[cont])).
                    tempObj:Remove(properties[cont]) NO-ERROR.
                END.
            END.
        END.
        
        RETURN tempObj.
    END METHOD.
    
    /*
    *   Verifica se o item possui filhos
    */
    METHOD LOGICAL hasChildren(tempObj AS JsonObject):
        DEFINE VARIABLE properties      AS CHARACTER EXTENT.
        DEFINE VARIABLE cont            AS INTEGER.
        
        properties = tempObj:getNames().
        IF EXTENT(properties) = ? THEN RETURN FALSE.
        
        DO cont = 1 TO EXTENT(properties):
            IF tempObj:getType(properties[cont]) = JsonDataType:OBJECT THEN DO:
                RETURN TRUE.
            END.
        END.
        
        RETURN FALSE.
    END METHOD.
    
    /**
    *   Adiciona os valores de um objeto
    */
    METHOD VOID addValues(root AS JsonObject, tempObj AS JsonObject):
        DEFINE VARIABLE properties      AS CHARACTER EXTENT.
        DEFINE VARIABLE cont            AS INTEGER.
        
        properties = tempObj:getNames().
        DO cont = 1 TO EXTENT(properties):
            root:ADD(properties[cont], JsonAPIUtils:getPropertyJsonObject(tempObj, properties[cont])).
        END.
        
    END METHOD.

END CLASS.
