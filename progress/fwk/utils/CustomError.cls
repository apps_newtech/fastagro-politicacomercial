USING Progress.Json.ObjectModel.*.
using Progress.Lang.*.
using com.totvs.framework.api.*.
BLOCK-LEVEL ON ERROR UNDO, THROW.
CLASS fwk.utils.CustomError INHERITS Progress.Lang.AppError:
    {method/dbotterr.i}    
    DEFINE PUBLIC PROPERTY statusCode AS int 
    get.
    set.
    DEFINE PUBLIC PROPERTY arryaOfErrors AS JsonArray
    GET():
        return JsonApiUtils:convertTempTableToJsonArray(temp-table RowErrors:handle).
    end get.
    SET.
    
    CONSTRUCTOR PUBLIC CustomError():
        super().
    end CONSTRUCTOR.
    
    CONSTRUCTOR PUBLIC CustomError(newStatus as int, messageNumber as int, messageDesc as char):
        this-object:statusCode = newStatus.
        this-object:addMessage(messageDesc, messageNumber).
    end CONSTRUCTOR.
    //TODO:replicado em errorHandler.i
    method public void addErrorByCode(INPUT code AS INTEGER, input msgParameters as char):
        DEFINE VARIABLE iCont AS INTEGER   NO-UNDO INITIAL 1.
        DEFINE VARIABLE cMsg  AS CHARACTER NO-UNDO.
        DEFINE VARIABLE cHelp AS CHARACTER NO-UNDO.

        FIND LAST RowErrors NO-LOCK NO-ERROR.
        IF AVAILABLE RowErrors THEN
            ASSIGN iCont = RowErrors.ErrorSequence + 1.

        ASSIGN cMsg = msgParameters.
        IF  code > 0 THEN DO:
            RUN utp/ut-msgs.p (INPUT "msg", INPUT code, INPUT msgParameters).
            ASSIGN cMsg = RETURN-VALUE.
            RUN utp/ut-msgs.p (INPUT "help", INPUT code, INPUT msgParameters).
            ASSIGN cHelp = RETURN-VALUE.
        END.

        CREATE RowErrors.
        ASSIGN RowErrors.ErrorSequence = iCont
               RowErrors.ErrorNumber      = code
               RowErrors.ErrorDescription = cMsg
               RowErrors.ErrorParameters  = msgParameters
               RowErrors.ErrorHelp        = cHelp
               RowErrors.ErrorType        = "ERROR"
               RowErrors.ErrorSubType     = "ERROR".
    END method.
    method public logical hasErrors():
        return temp-table RowErrors:has-records.
    end method.
END CLASS.
