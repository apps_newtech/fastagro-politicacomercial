USING Progress.Json.ObjectModel.*.
USING openedge.core.*.
USING fwk.utils.*.
using com.totvs.framework.btb.filter.FilterObject.

CLASS fwk.utils.DataManager FINAL:
    METHOD PUBLIC STATIC char logicalToString (INPUT lValue AS LOGICAL):
        RETURN (IF lValue = TRUE THEN "true" ELSE "false").
    END method.

    METHOD PUBLIC STATIC logical stringToLogical (INPUT cValue AS CHARACTER):
        RETURN (IF cValue = "true" OR cValue = "yes" THEN TRUE ELSE FALSE).
    END method.

    method public static JsonArray convertArrayWithSerializeNames(input origem as JsonArray, input targetInstance as char):
        def var cont as int no-undo.
        def var currentObj as JsonObject no-undo.
        def var convertedObj as Persistable no-undo.
        def var arrayReturned as JsonArray no-undo.

        arrayReturned = new JsonArray().
        do cont = 1 to origem:length:
            convertedObj = dynamic-new targetInstance().
            currentObj = origem:getJsonObject(cont).
            convertedObj:fromJson(currentObj).
            arrayReturned:add(convertedObj:serialize()).
        end.
        return arrayReturned.
    end method.

    method public static JsonObject jsonObjectToLowerCase (input obj as JsonObject):

        DEFINE VARIABLE lcQueryParams       AS LONGCHAR          NO-UNDO.

        ASSIGN lcQueryParams = obj:getJsonText()
               lcQueryParams = LC(lcQueryParams).
        return DataManager:longcharToJsonObject(lcQueryParams).
    end method.

    method public static handle getRecordsWithClauses (input filterObj as FilterObject, 
                                                       input tableName as char, 
                                                       input filter as char, 
                                                       input targetSortClause as char):
        def var tableHandle as handle no-undo.
        ASSIGN targetSortClause = REPLACE(targetSortClause, "-", "") /* Remove o sinal '-' para que seja possivel localizar o seu respectivo campo */
               tableHandle = filterObj:getResults(INPUT filter, INPUT tableName, INPUT targetSortClause).
        return tableHandle.               
    end method.

    method public static char processSortByString (input filterObj as FilterObject, input originalSortClause as char):
        def var targetSortClause as char no-undo.
        def var sortReturn as char no-undo.        
        DEFINE VARIABLE cFieldOrder          AS CHARACTER     NO-UNDO.
        DEFINE VARIABLE cIdxOrderDesc        AS CHARACTER     NO-UNDO.
        DEFINE VARIABLE iCount               AS INTEGER       NO-UNDO.        

        targetSortClause = filterObj:getDatabaseField(originalSortClause).
        /* Armazena os campos com ordenacao decrescente */
        DO iCount = 1 TO NUM-ENTRIES(targetSortClause):
            ASSIGN cFieldOrder = ENTRY(iCount, targetSortClause).
            IF  INDEX(cFieldOrder, "-") = 1 THEN DO:
                ASSIGN cIdxOrderDesc = cIdxOrderDesc + "," + STRING(iCount). 
            END.
        END.

        DO iCount = 1 TO NUM-ENTRIES(targetSortClause):
            IF NUM-ENTRIES(ENTRY(iCount, targetSortClause),'.') > 1 THEN
                ASSIGN ENTRY(iCount, targetSortClause) = ENTRY(NUM-ENTRIES(ENTRY(iCount, targetSortClause),'.'),ENTRY(iCount, targetSortClause),'.').
            
                ASSIGN sortReturn = sortReturn + "," + ENTRY(iCount, targetSortClause) + 
                    IF INDEX(cIdxOrderDesc, STRING(iCount)) > 0 THEN 
                        " DESC "
                    ELSE
                        ""
                .
        END.
        ASSIGN sortReturn = REPLACE(sortReturn, ',', ' BY ').
        return sortReturn.
    end method.
    //TODO: usar o m�todo startRow
    method public static JsonObject applyPagination (input tableHandle as handle, input targetHandle as handle, input pageNumber as int, input pageSize as int, input sortClause as char):
        DEFINE VARIABLE hQueryResult         AS HANDLE        NO-UNDO.
        DEFINE VARIABLE hBufferResult        AS HANDLE        NO-UNDO.
        DEFINE VARIABLE cQuery               AS CHARACTER     NO-UNDO.        
        DEFINE VARIABLE iTotal        AS INTEGER       NO-UNDO INITIAL 0.
        DEFINE VARIABLE iTotalResults AS INTEGER       NO-UNDO INITIAL 0.

        ASSIGN hBufferResult = tableHandle:DEFAULT-BUFFER-HANDLE.
        CREATE QUERY hQueryResult.
        hQueryResult:ADD-BUFFER(hBufferResult:HANDLE).
        ASSIGN cQuery = 'PRESELECT EACH ' + hBufferResult:NAME
                    + (IF sortClause <> '' THEN sortClause  ELSE '').
        hQueryResult:QUERY-PREPARE(cQuery).    
        IF hQueryResult:QUERY-OPEN() = FALSE THEN
            LEAVE.
        ASSIGN iTotalResults = hQueryResult:NUM-RESULTS.
        targetHandle:Empty-temp-table().

        IF pageSize > 0 THEN DO:
            IF pageNumber > 1 THEN
                hQueryResult:REPOSITION-TO-ROW(((pageNumber - 1) * pageSize) + 1).
            
            REPEAT:
                hQueryResult:GET-NEXT().
                IF hQueryResult:QUERY-OFF-END THEN
                    LEAVE.
                targetHandle:default-buffer-handle:buffer-create().
                targetHandle:default-buffer-handle:buffer-copy(hBufferResult).

                ASSIGN iTotal = iTotal + 1.
                IF iTotal = pageSize THEN 
                    LEAVE.
            END.
        END.
        def var retorno as JsonObject no-undo.
        retorno = new JsonObject().
        retorno:add('total', iTotalResults).
        retorno:add('hasNext', not hQueryResult:QUERY-OFF-END).        
        return retorno.

    end method.

    method public static JsonObject longcharToJsonObject (INPUT jsonOutput AS LONGCHAR):
        DEFINE VARIABLE jsonObjOutput AS JsonObject NO-UNDO.
        DEFINE VARIABLE objParse AS ObjectModelParser NO-UNDO.

        jsonObjOutput = NEW JsonObject().
        objParse = NEW ObjectModelParser().
        jsonObjOutput = CAST(objParse:Parse(jsonOutput), JsonObject).
        DELETE OBJECT objParse no-error.
        RETURN jsonObjOutput.
    END method.
    
    method public static JsonObject convertTempTableToJson (INPUT ttHandle AS handle):    
        def var longcharRetorno as longchar no-undo.
        def var jsonConverted as JsonObject no-undo.

        ttHandle:write-json('longchar', longcharRetorno).
        jsonConverted = longcharToJsonObject(longcharRetorno).
        return jsonConverted.
    end method.

    method public static JsonObject stripNameFromOneRecordHandle (INPUT targetHandle AS handle):
        def var jsonOutput as JsonObject no-undo.
        def var jsonConverted as JsonObject no-undo.        

        jsonConverted = convertTempTableToJson(targetHandle).
        jsonOutput = jsonConverted:getJsonArray(targetHandle:name):getJsonObject(1).
        return jsonOutput.
    end method.

    method public static JsonArray stripNameFromMultipleRecordHandle (INPUT targetHandle AS handle):
        def var jsonConverted as JsonObject no-undo.
        def var jsonOutput as JsonArray no-undo.

        jsonConverted = convertTempTableToJson(targetHandle).
        jsonOutput = jsonConverted:getJsonArray(targetHandle:name).
        return jsonOutput.
    end method.

    method public static handle createDynamicTempTableFromJsonArray (INPUT dataToImport as JsonArray):

        DEFINE VARIABLE hQuery AS HANDLE    NO-UNDO.
        DEFINE VARIABLE ttsource AS HANDLE    NO-UNDO.        

        CREATE TEMP-TABLE ttSource.
        ttSource:READ-JSON("longchar", dataToImport:getJsonText(), "empty").
        CREATE QUERY hQuery.
        hQuery:SET-BUFFERS(ttSource:DEFAULT-BUFFER-HANDLE).
        hQuery:QUERY-PREPARE("FOR EACH " + ttSource:NAME).
        return hQuery.
    end method.

end class.
