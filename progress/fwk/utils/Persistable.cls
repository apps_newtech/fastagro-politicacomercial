BLOCK-LEVEL ON ERROR UNDO, THROW.
USING Progress.Json.ObjectModel.*.
USING fwk.utils.*.
class fwk.utils.Persistable SERIALIZABLE abstract:

    METHOD PUBLIC abstract void fromJson(input json as JsonObject). 
    METHOD PROTECTED LOGICAL hasAttribute(INPUT cAttributes AS CHARACTER, INPUT cAttribute AS CHARACTER):
        RETURN cAttributes = ? OR TRIM(cAttributes) = "" OR LOOKUP(cAttribute,cAttributes) > 0.
    END METHOD.
    METHOD PUBLIC abstract JsonObject serialize().
END class.

