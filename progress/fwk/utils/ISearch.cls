
interface fwk.utils.ISearch:

    METHOD PUBLIC handle getFieldRelation().
    METHOD PUBLIC handle getFieldBreak().    
    METHOD PUBLIC handle getTableRelation().

END interface.

