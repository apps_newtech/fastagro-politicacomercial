 
CLASS fwk.utils.QueryParamData SERIALIZABLE:
    
    DEFINE SERIALIZABLE PUBLIC PROPERTY property AS char NO-UNDO 
        GET.
        SET.
    
    DEFINE SERIALIZABLE PUBLIC PROPERTY value AS longchar NO-UNDO
        GET.
        SET.

END CLASS.
